package com.yzhg.jetpack;

import java.util.Stack;

/**
 * 编辑 : yzhg
 * 时间 ：22:14-06-18 22:14
 * 包名 : com.yzhg.jetpack
 * 描述 : 一句话描述
 */
public class HexTools {

    /**
     * 十进制转十六进制
     */
    public static String baseString(int num,int base){
        if(base > 16){
            throw new RuntimeException("进制数超出范围，base<=16");
        }
        StringBuffer str = new StringBuffer("");
        String digths ="0123456789ABCDEF";
        Stack<Character> s = new Stack<Character>();
        while(num != 0){
            s.push(digths.charAt(num%base));
            num/=base;
        }
        while(!s.isEmpty()){
            str.append(s.pop());
        }
        return str.toString();
    }

    /**
     * 十六进制转byte数组
     */


    /**
     * byte数组 转十六进制
     */


    /**
     * 十六进制转十进制
     */


    /**
     * byte数组转十进制
     */


}
