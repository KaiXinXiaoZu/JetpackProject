package com.yzhg.jetpack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.yzhg.jetpack.databinding.activity.NumDataBindingActivity
import com.yzhg.jetpack.kotlin.room.activity.KotlinRoomActivity
import com.yzhg.jetpack.room.viewmodel_livedata.RoomActivity
import com.yzhg.jetpack.viewmodel.ViewModelActivity
import com.yzhg.jetpack.weight.SVGAPlayActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        findViewById<Button>(R.id.butViewModel).setOnClickListener {
            startActivity(Intent(this@MainActivity, ViewModelActivity::class.java))
        }
        findViewById<Button>(R.id.butRoomViewModel).setOnClickListener {
            startActivity(Intent(this@MainActivity, RoomActivity::class.java))
        }
        findViewById<Button>(R.id.butKotlinRoomViewModel).setOnClickListener {
            startActivity(Intent(this@MainActivity, KotlinRoomActivity::class.java))
        }
        findViewById<Button>(R.id.butDataBinding).setOnClickListener {
            startActivity(Intent(this@MainActivity, NumDataBindingActivity::class.java))

        }
        val findViewById = findViewById<Button>(R.id.butSVGA)
        findViewById.setOnClickListener {
            startActivity(Intent(this@MainActivity, SVGAPlayActivity::class.java))
        }
    }
}