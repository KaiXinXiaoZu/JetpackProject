package com.yzhg.jetpack.weight;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.http.HttpResponseCache;
import android.os.Handler;
import android.view.View;

import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.wsdz.tools.common.LogTools;
import com.yzhg.jetpack.music.MediaPlayerManager;
import com.yzhg.jetpack.music.SVGABean;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;

/**
 * 编辑 : yzhg
 * 时间 ：20:55-04-10 20:55
 * 包名 : com.yzhg.jetpack.weight
 * 描述 : 一句话描述
 */
public class SVGAManagerTools {

    private Context context;

    public static SVGAManagerTools instance;

    private MediaPlayerManager manager;

    private SVGAImageView svgaImageView;

    /**
     * 储存正在使用的MediaPlayer
     */
    private List<MediaPlayer> mediaPlayerList = new LinkedList<>();

    LinkedList<SVGABean> svgaList = new LinkedList<>();

    /*当前是否正在播放SVGA*/
    private boolean isPlayerSVGA = false;

    private SVGAManagerTools() {
    }


    public static SVGAManagerTools getInstance() {
        if (instance == null) {
            instance = new SVGAManagerTools();
        }
        return instance;
    }


    /**
     * 设置需要播放的SVGA进入集合
     */
    public void setSVGAPath(SVGABean svgaPath, Context context, SVGAImageView svgaImageView) {
        setSVGACache(context);
        if (manager == null) {
            manager = new MediaPlayerManager();
        }
        svgaList.add(svgaPath);
        this.context = context;
        this.svgaImageView = svgaImageView;
        startPlayerSVGA();
    }


    public void setSVGAPath(List<SVGABean> svgaList, Context context, SVGAImageView svgaImageView) {
        setSVGACache(context);
        if (manager == null) {
            manager = new MediaPlayerManager();
        }
        this.svgaList.addAll(svgaList);
        this.context = context;
        this.svgaImageView = svgaImageView;

        startPlayerSVGA();

    }

    /**
     * 循环播放
     */
    private void startPlayerSVGA() {
        if (isPlayerSVGA) {
            return;
        }
        if (svgaList.size() > 0) {
            SVGABean svgaBean = svgaList.get(0);
            startPlayerSVGA(svgaBean);
        } else {
            LogTools.INSTANCE.e("SVGA全部播放完毕");
        }
    }


    /**
     * 开始播放SVGA  "yzhg.svga"
     */
    private void startPlayerSVGA(SVGABean svgaPath) {
        SVGAParser parser = new SVGAParser(context);
        try {
            parser.decodeFromAssets(svgaPath.getSvgaPath(), new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NonNull SVGAVideoEntity videoItem) {
                    isPlayerSVGA = true;
                    SVGADrawable drawable = new SVGADrawable(videoItem);
                    svgaImageView.setLoops(1);
                    svgaImageView.setImageDrawable(drawable);
                    svgaImageView.startAnimation();

                    new Handler().postDelayed(() -> svgaImageView.setCallback(svgaCallback), 800);
                }

                @Override
                public void onError() {

                }
            }, list -> {
                mediaPlayerList.clear();

                for (File file : list) {
                    LogTools.INSTANCE.e("视频播放序列===播放完成111" + file.getAbsolutePath());
                    // 播放音频文件
                    MediaPlayer player = manager.getPlayer();
                    mediaPlayerList.add(player);
                    try {
                        player.setDataSource(file.getPath());
                        player.prepareAsync();
                        player.setOnPreparedListener(MediaPlayer::start);
                        // 音频文件播放完毕或需要回收
                        player.setOnCompletionListener(mediaPlayer -> {
                            manager.releasePlayer(mediaPlayer);
                            mediaPlayerList.remove(mediaPlayer);
                        });
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private SVGACallback svgaCallback = new SVGACallback() {

        @Override
        public void onPause() {
            LogTools.INSTANCE.e("=============播放完成1");
        }

        @Override
        public void onFinished() {
            isPlayerSVGA = false;
            LogTools.INSTANCE.e("=============播放完成2");
            //manager.releaseAllPlayers();
            svgaImageView.setVisibility(View.GONE);

            //移除首个
            svgaList.removeFirst();

            for (MediaPlayer mediaPlayer : mediaPlayerList) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    mediaPlayer.stop();
                    mediaPlayer.release(); // 释放资源
                }
            }
            mediaPlayerList.clear();
            startPlayerSVGA();

            svgaImageView.setCallback(null);
        }

        @Override
        public void onRepeat() {
            LogTools.INSTANCE.e("=============播放完成3");
        }

        @Override
        public void onStep(int i, double v) {
            //     LogTools.INSTANCE.e("=============播放完成4");
            svgaImageView.setVisibility(View.VISIBLE);
        }
    };

    private void setSVGACache(Context context) {
        File cacheDir = new File(context.getApplicationContext().getCacheDir(), "http");
        try {
            HttpResponseCache.getInstalled().install(cacheDir, 1024 * 1024 * 128);
        } catch (IOException e) {
            // throw new RuntimeException(e);
        }
    }
}
