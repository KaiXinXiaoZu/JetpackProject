package com.yzhg.jetpack.weight;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.net.http.HttpResponseCache;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;

import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.opensource.svgaplayer.utils.log.ILogger;
import com.opensource.svgaplayer.utils.log.SVGALogger;
import com.wsdz.tools.common.LogTools;
import com.yzhg.jetpack.databinding.ActivityViewScrollLayoutBinding;
import com.yzhg.jetpack.music.MediaPlayerManager;
import com.yzhg.jetpack.music.SVGABean;
import com.yzhg.study.base.ui.activity.BaseBindingActivity;
import com.yzhg.toollibrary.ThreadUtils;
import com.yzhg.toollibrary.common.ToastTools;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * 编辑 : yzhg
 * 时间 ：20:50-01-31 20:50
 * 包名 : com.yzhg.jetpack.weight
 * 描述 : 一句话描述
 */
public class SVGAPlayActivity extends BaseBindingActivity<ActivityViewScrollLayoutBinding> {


    String musicPath = "https://yzhg.oss-cn-beijing.aliyuncs.com/wodeloulan.mp3";
    String musicPath1 = "https://yzhg.oss-cn-beijing.aliyuncs.com/%E7%83%9F%28%E8%AE%B8%E4%BD%B3%E8%B1%AA%29%20-%20%E5%86%8D%E8%A7%81%E6%88%91%E7%9A%84%E5%A5%B3%E5%AD%A9.mp3";

    String svgaUrl = "http://yzhg.oss-cn-beijing.aliyuncs.com/yzhg.svga";


    private MediaPlayerManager manager;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void initView() {
        manager = new MediaPlayerManager();
        try {
            File cacheDir = new File(this.getApplicationContext().getCacheDir(), "http");
            HttpResponseCache.getInstalled().install(cacheDir, 1024 * 1024 * 128);
            SVGALogger.INSTANCE.setLogEnabled(true);
            SVGALogger.INSTANCE.injectSVGALoggerImp(new ILogger() {
                @Override
                public void verbose(@NonNull String s, @NonNull String s1) {
                    LogTools.INSTANCE.e("=============SVGA==verbose===========" + s + "=====" + s1);
                }

                @Override
                public void info(@NonNull String s, @NonNull String s1) {
                    LogTools.INSTANCE.e("=============SVGA==info===========" + s + "=====" + s1);
                }

                @Override
                public void debug(@NonNull String s, @NonNull String s1) {
                    LogTools.INSTANCE.e("=============SVGA==debug===========" + s + "=====" + s1);
                }

                @Override
                public void warn(@NonNull String s, @NonNull String s1) {
                    LogTools.INSTANCE.e("=============SVGA==warn===========" + s + "=====" + s1);
                }

                @Override
                public void error(@NonNull String s, @Nullable String s1, @Nullable Throwable throwable) {
                    LogTools.INSTANCE.e("=============SVGA==error===========" + s + "=====" + s1);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

/*
        binding.butPlaySvga.setOnClickListener(view -> {
            MediaPlayer mediaPlayer = new MediaPlayer();
            try {
                mediaPlayer.setDataSource("/data/user/0/com.yzhg.jetpack/cache/svga/audio_417.mp3");
                mediaPlayer.prepareAsync();
                mediaPlayer.setOnPreparedListener(mediaPlayer1 -> mediaPlayer.start());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        });*/
        binding.butPlaySvga.setOnClickListener(view -> {
            SVGABean svgaBean = new SVGABean();
            svgaBean.setSvgaPath("yzhg.svga");
            SVGAManagerTools.getInstance().setSVGAPath(svgaBean,this,binding.svgaPath);
           /* SVGAParser parser = new SVGAParser(this);
            // 第三个为可缺省参数，默认为 null，如果设置该方法，则内部不在处理音频的解析以及播放，会通过 PlayCallback 把音频 File 实例回传给开发者，有开发者自行控制音频的播放与停止。
            try {
                parser.decodeFromAssets("yzhg.svga", new SVGAParser.ParseCompletion() {
                    @Override
                    public void onComplete(@NonNull SVGAVideoEntity videoItem) {
                        SVGADrawable drawable = new SVGADrawable(videoItem);
                        binding.svgaPath.setLoops(1);
                        binding.svgaPath.setImageDrawable(drawable);
                  //      binding.svgaPath.setVideoItem(videoItem);
                        binding.svgaPath.startAnimation();

                        new Handler().postDelayed(() -> {
                            binding.svgaPath.setCallback(new SVGACallback() {
                                @Override
                                public void onPause() {
                                    LogTools.INSTANCE.e("=============播放完成1");
                                }

                                @Override
                                public void onFinished() {
                                    LogTools.INSTANCE.e("=============播放完成2");
                                    binding.svgaPath.setVisibility(View.GONE);
                                    manager.pauseMusicPlayer();

                                }

                                @Override
                                public void onRepeat() {
                                    LogTools.INSTANCE.e("=============播放完成3");
                                }

                                @Override
                                public void onStep(int i, double v) {
                                    LogTools.INSTANCE.e("=============播放完成4");
                                    // binding.svgaPath.setVisibility(View.VISIBLE);
                                }
                            });
                        },1000);

                    }

                    @Override
                    public void onError() {

                    }
                }, list -> {
                    if (isPlayMusic) {
                        return;
                    }
                    isPlayMusic = true;
                    for (File file : list) {

                        LogTools.INSTANCE.e("视频播放序列===播放完成111" + file.getAbsolutePath());
                        // 播放音频文件
                        MediaPlayer player = manager.getPlayer();
                        try {
                            player.setDataSource(file.getPath());
                            player.prepareAsync();
                            player.setOnPreparedListener(MediaPlayer::start);
                            // 音频文件播放完毕或需要回收
                            player.setOnCompletionListener(mediaPlayer -> {
                                manager.releasePlayer(mediaPlayer);
                            });
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
            } catch (Exception e) {
                throw new RuntimeException(e);
            }*/
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        manager.releaseAllPlayers();
    }

}
