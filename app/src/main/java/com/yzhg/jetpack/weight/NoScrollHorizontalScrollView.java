package com.yzhg.jetpack.weight;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

/**
 * 编辑 : yzhg
 * 时间 ：21:29-01-31 21:29
 * 包名 : com.yzhg.jetpack.weight
 * 描述 : 一句话描述
 */
public class NoScrollHorizontalScrollView extends HorizontalScrollView {
    public NoScrollHorizontalScrollView(Context context) {
        super(context);
    }

    public NoScrollHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoScrollHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private boolean isTouch;

    public void setOnTouch(boolean isTouch) {
        this.isTouch = isTouch;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return isTouch;
    }

}
