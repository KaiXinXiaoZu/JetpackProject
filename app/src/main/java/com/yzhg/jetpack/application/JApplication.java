package com.yzhg.jetpack.application;

import android.app.Application;

import com.kingja.loadsir.core.LoadSir;
import com.yzhg.toollibrary.BaseApplication;

/**
 * 包名：com.yzhg.jetpack.application
 * 创建人：yzhg
 * 时间：2021/06/18 17:55
 * 描述：
 */
public class JApplication extends BaseApplication {


    private static Application application;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;


      /*  LoadSir.beginBuilder()
                .addCallback(new ErrorCallback())//添加各种状态页
                .addCallback(new EmptyCallback())
                .addCallback(new LoadingCallback())
                .addCallback(new TimeoutCallback())
                .addCallback(new CustomCallback())
                .setDefaultCallback(LoadingCallback.class)//设置默认状态页
                .commit();*/
    }

    public static Application getApplication(){
        return application;
    }



}
