package com.yzhg.jetpack.room.dao;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.yzhg.jetpack.room.table.RoomTableBean;

import java.util.List;

/**
 * 包名：com.yzhg.jetpack.room.dao
 * 创建人：yzhg
 * 时间：2021/06/18 17:15
 * 描述：
 */
@Dao
public interface RoomDao {


    @Insert
    void insertRoom(RoomTableBean roomTableBean);

    @Delete
    void deleteRoom(RoomTableBean roomTableBean);

    @Query("SELECT * FROM ROOM_TABLE")
    LiveData<List<RoomTableBean>> getRoomList();

}
