package com.yzhg.jetpack.room.viewmodel_livedata;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.room.RoomDatabase;

import com.yzhg.jetpack.ThreadUtils;
import com.yzhg.jetpack.room.database.RoomDataBase;
import com.yzhg.jetpack.room.table.RoomTableBean;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * 包名：com.yzhg.jetpack.room.viewmodel_livedata
 * 创建人：yzhg
 * 时间：2021/06/18 17:31
 * 描述：
 */
public class RoomViewModel extends AndroidViewModel {


    //数据库
    private RoomDataBase roomDatabase;

    private LiveData<List<RoomTableBean>> dataList;


    public RoomViewModel(@NonNull @NotNull Application application) {
        super(application);
        roomDatabase = RoomDataBase.getInstance(application);
        dataList = roomDatabase.roomDao().getRoomList();
    }

    //获取数据
    public LiveData<List<RoomTableBean>> getDataList() {
        return dataList;
    }

    //插入数据
    public void insertData(RoomTableBean roomTableBean) {
        ThreadUtils.runOnSubThread(() -> roomDatabase.roomDao().insertRoom(roomTableBean));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }
}
