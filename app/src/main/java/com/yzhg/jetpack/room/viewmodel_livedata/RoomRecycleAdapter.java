package com.yzhg.jetpack.room.viewmodel_livedata;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.yzhg.jetpack.R;
import com.yzhg.jetpack.room.table.RoomTableBean;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * 包名：com.yzhg.jetpack.room.viewmodel_livedata
 * 创建人：yzhg
 * 时间：2021/06/18 17:26
 * 描述：
 */
public class RoomRecycleAdapter extends BaseQuickAdapter<RoomTableBean, BaseViewHolder> {


    public RoomRecycleAdapter(@Nullable List<RoomTableBean> data) {
        super(R.layout.item_room_activity, data);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, RoomTableBean roomTableBean) {
        baseViewHolder.setText(R.id.tvNumItem, roomTableBean.getName());
    }
}
