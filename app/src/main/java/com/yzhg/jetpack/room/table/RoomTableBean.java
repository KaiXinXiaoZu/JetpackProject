package com.yzhg.jetpack.room.table;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

/**
 * 包名：com.yzhg.jetpack.room.table
 * 创建人：yzhg
 * 时间：2021/06/18 17:06
 * 描述：
 */
@Entity(tableName = "ROOM_TABLE")
public class RoomTableBean {


    //设置自动增长
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID", typeAffinity = ColumnInfo.INTEGER)
    public int uId;

    @ColumnInfo(name = "NAME", typeAffinity = ColumnInfo.TEXT)
    public String name;

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoomTableBean(int uId, String name) {
        this.uId = uId;
        this.name = name;
    }

    @Ignore
    public RoomTableBean(String name) {
        this.name = name;
    }
}
































