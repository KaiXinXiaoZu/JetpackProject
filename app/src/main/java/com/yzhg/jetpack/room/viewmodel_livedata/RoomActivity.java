package com.yzhg.jetpack.room.viewmodel_livedata;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yzhg.jetpack.R;
import com.yzhg.jetpack.application.JApplication;
import com.yzhg.jetpack.room.table.RoomTableBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 包名：com.yzhg.jetpack.room.viewmodel_livedata
 * 创建人：yzhg
 * 时间：2021/06/18 17:22
 * 描述：
 */
public class RoomActivity extends AppCompatActivity {


    //数据类
    private List<RoomTableBean> roomData = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_room);


        //设置列表
        RecyclerView recycleRoom = findViewById(R.id.recycleRoom);
        recycleRoom.setLayoutManager(new LinearLayoutManager(this));
        RoomRecycleAdapter roomRecycleAdapter = new RoomRecycleAdapter(roomData);
        recycleRoom.setAdapter(roomRecycleAdapter);


        // RoomViewModel roomViewModel = new ViewModelProvider(this).get(RoomViewModel.class);
        //创建  AndroidViewModel 实例
        RoomViewModel roomViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(JApplication.getApplication()).create(RoomViewModel.class);
        roomViewModel.getDataList().observe(this, new Observer<List<RoomTableBean>>() {
            @Override
            public void onChanged(List<RoomTableBean> roomTableBeans) {
                //清空数据
                roomRecycleAdapter.getData().clear();

                roomRecycleAdapter.addData(roomTableBeans);
            }
        });


        Button butInsertData = findViewById(R.id.butInsertData);
        //添加数据按钮
        butInsertData.setOnClickListener(v -> {
            Random random = new Random();
            int nextInt = random.nextInt(10000);
            RoomTableBean roomTableBean = new RoomTableBean("我是" + nextInt + "号");
            roomViewModel.insertData(roomTableBean);

        });


    }


}
