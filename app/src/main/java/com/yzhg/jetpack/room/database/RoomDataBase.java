package com.yzhg.jetpack.room.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.yzhg.jetpack.kotlin.room.dao.KotlinRoomDao;
import com.yzhg.jetpack.room.dao.RoomDao;
import com.yzhg.jetpack.room.table.RoomTableBean;

import org.jetbrains.annotations.NotNull;

/**
 * 包名：com.yzhg.jetpack.room.database
 * 创建人：yzhg
 * 时间：2021/06/18 17:09
 * 描述：
 */
@Database(entities = {RoomTableBean.class}, version = 1, exportSchema = false)
public abstract class RoomDataBase extends RoomDatabase {


    private static RoomDataBase dataBaseInstance;


    public static synchronized RoomDataBase getInstance(Context context) {
        if (dataBaseInstance == null) {
            dataBaseInstance =
                    Room.databaseBuilder(
                            context.getApplicationContext(),
                            RoomDataBase.class,
                            "my_db")
                            .fallbackToDestructiveMigration()
                            .addMigrations(MIGRATION_1_2)
                            .build();
        }
        return dataBaseInstance;
    }

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull @NotNull SupportSQLiteDatabase database) {

        }
    };


    public abstract RoomDao roomDao();


    public abstract KotlinRoomDao kotlinRoomDao();

}


















