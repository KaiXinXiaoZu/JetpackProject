package com.yzhg.jetpack.music;

/**
 * 编辑 : yzhg
 * 时间 ：20:15-04-10 20:15
 * 包名 : com.yzhg.jetpack.music
 * 描述 : 一句话描述
 */
import android.media.MediaPlayer;

import com.wsdz.tools.common.LogTools;

import java.util.LinkedList;
import java.util.Queue;

public class MediaPlayerManager {
    private static final int MAX_PLAYERS = 5; // 最多同时播放的播放器数量
    private Queue<MediaPlayer> playerQueue = new LinkedList<>();

    public MediaPlayerManager() {
        // 预先创建一些播放器实例
        for (int i = 0; i < MAX_PLAYERS; i++) {
            MediaPlayer player = new MediaPlayer();
            playerQueue.add(player);
        }
    }

    public MediaPlayer getPlayer() throws IllegalStateException {
        MediaPlayer player = playerQueue.poll();
        if (player == null) {
            throw new IllegalStateException("No available players");
        }
        return player;
    }

    public void releasePlayer(MediaPlayer player) {
        if (player != null) {
            player.reset(); // 重置播放器状态
            playerQueue.add(player); // 将播放器返回到队列中
        }
    }

    /**
     * 暂停所有音乐播放
     */
    public void pauseMusicPlayer(){

        for (MediaPlayer player : playerQueue) {
            player.pause();
            player.stop();

            LogTools.INSTANCE.e("=============播放完成 == 结束播放"+playerQueue.size());
            //player.release(); // 释放资源
        }
    }

    public void releaseAllPlayers() {
        for (MediaPlayer player : playerQueue) {
            if (player.isPlaying()) {
                player.stop();
            }
            player.release(); // 释放资源
        }
        playerQueue.clear();
    }
}