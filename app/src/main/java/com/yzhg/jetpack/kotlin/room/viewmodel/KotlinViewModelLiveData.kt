package com.yzhg.jetpack.kotlin.room.viewmodel

import android.app.Application
import android.media.MediaRouter
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.yzhg.jetpack.room.database.RoomDataBase
import com.yzhg.jetpack.room.table.RoomTableBean
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * 包名：com.yzhg.jetpack.kotlin.room.viewmodel
 * 创建人：yzhg
 * 时间：2021/06/19 09:35
 * 描述：
 */
class KotlinViewModelLiveData(application: Application) : AndroidViewModel(application) {

    private var roomDataBase: RoomDataBase? = null

    /**
     * 获取数据集合
     */
    private lateinit var roomList: LiveData<MutableList<RoomTableBean>>

    init {
        roomDataBase = RoomDataBase.getInstance(application)
    }


    /**
     * 获取数据
     */
    fun getRoomKotlinList(): LiveData<MutableList<RoomTableBean>> {
        viewModelScope.launch {
            roomDataBase?.let {
                roomList = it.kotlinRoomDao().getKotlinRoomList()
            }
        }
        return roomList
    }


    /**
     * 插入数据
     */
    fun addRoomKotlinData(roomTableBean: RoomTableBean) {
        viewModelScope.launch {
            val one = async { insertRoom(roomTableBean) }
            one.await()
        }
    }

    suspend fun insertRoom(roomTableBean: RoomTableBean) {
        withContext(Dispatchers.IO){
            roomDataBase?.let {
                it.kotlinRoomDao()?.insertKotlinRoom(roomTableBean)
            }
        }

    }
}