package com.yzhg.jetpack.kotlin.room.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.yzhg.jetpack.room.table.RoomTableBean

/**
 * 包名：com.yzhg.jetpack.kotlin.room.dao
 * 创建人：yzhg
 * 时间：2021/06/19 09:24
 * 描述：
 */
@Dao
interface KotlinRoomDao {


    /**
     * 插入数据库
     */
    @Insert
    fun insertKotlinRoom(roomTableBean: RoomTableBean)


    /**
     * 查询数据库
     */
    @Query("SELECT * FROM ROOM_TABLE")
    fun getKotlinRoomList(): LiveData<MutableList<RoomTableBean>>
}