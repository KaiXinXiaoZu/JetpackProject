package com.yzhg.jetpack.kotlin.room.activity

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yzhg.jetpack.R
import com.yzhg.jetpack.application.JApplication
import com.yzhg.jetpack.kotlin.room.viewmodel.KotlinViewModelLiveData
import com.yzhg.jetpack.room.table.RoomTableBean
import com.yzhg.jetpack.room.viewmodel_livedata.RoomRecycleAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import java.util.*

/**
 * 包名：com.yzhg.jetpack.kotlin.room.activity
 * 创建人：yzhg
 * 时间：2021/06/19 09:52
 * 描述：
 */
class KotlinRoomActivity : AppCompatActivity(), CoroutineScope by MainScope() {
    //数据类
    private val roomData: MutableList<RoomTableBean> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room)

        //设置列表
        val recycleRoom = findViewById<RecyclerView>(R.id.recycleRoom)
        recycleRoom.layoutManager = LinearLayoutManager(this)
        val roomRecycleAdapter = RoomRecycleAdapter(roomData)
        recycleRoom.adapter = roomRecycleAdapter


        val kotlinViewModelLiveData: KotlinViewModelLiveData =
            ViewModelProvider.AndroidViewModelFactory.getInstance(JApplication.getApplication()).create(KotlinViewModelLiveData::class.java)

        kotlinViewModelLiveData.getRoomKotlinList().observe(this,
            { roomTableBeans -> //清空数据
                roomRecycleAdapter.data.clear()

                roomTableBeans?.let {
                    roomRecycleAdapter.addData(it)
                }
            })


        val butInsertData: Button = findViewById<Button>(R.id.butInsertData)
        butInsertData.setOnClickListener {
            val random = Random()
            val nextInt = random.nextInt(10000)
            val roomTableBean = RoomTableBean("我是" + nextInt + "号")
            kotlinViewModelLiveData.addRoomKotlinData(roomTableBean)
        }
    }


    override fun onDestroy() {
        //取消协程
        cancel()
        super.onDestroy()
    }

}