package com.yzhg.jetpack.lifecycle

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel

/**
 * 包名：com.yzhg.jetpack.lifecycle
 * 创建人：yzhg
 * 时间：2021/06/19 14:46
 * 描述：
 */
abstract class BaseViewModel : ViewModel(), ViewModelLifecycle {
    override fun onAny(owner: LifecycleOwner, event: Lifecycle.Event) {

    }

    override fun onCreate() {
    }

    override fun onStart() {

    }

    override fun onResume() {

    }

    override fun onPause() {

    }

    override fun onStop() {

    }

    override fun onDestroy() {

    }


}