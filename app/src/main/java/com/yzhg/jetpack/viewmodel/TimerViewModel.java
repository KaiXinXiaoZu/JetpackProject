package com.yzhg.jetpack.viewmodel;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.yzhg.jetpack.lifecycle.BaseViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 包名：com.yzhg.jetpack.viewmodel
 * 创建人：yzhg
 * 时间：2021/06/18 15:50
 * 描述：
 * <p>
 * ViewModel注释：
 * ViewModel是一个抽象类，其中只有一个onCleared方法，当ViewModule不再被需要
 * 即与之相关的Activity都被销毁时，该方法会被系统调用。我们可以在该方法中执行一些
 * 资源释放的相关操作。由于屏幕旋转而导致的Activity重建。并不会调用这个方法
 * <p>
 * <p>
 * 使用注意：不能将任何Context或者含有Context引用的对象传入ViewModel
 * 如果想使用Context  可以使用AndroidViewModel类
 * AndroidViewModel 初始化方法：ViewModelProvider.AndroidViewModelFactory.getInstance(JApplication.getApplication()).create(RoomViewModel.class);
 * <p>
 * <p>
 * ViewModel 与 OnSaveInstanceState()
 * OnSaveInstanceState能保存少量的、能序列化的数据   ViewModel没有限制
 * <p>
 * ViewModel不支持数据的持久化。页面被销毁时，数据就没了。OnSaveInstanceState可以保存
 * <p>
 * <p>
 * LiveData注释：
 * LiveData我们不能直接使用，故：我们可以使用MutableLiveData
 * <p>
 * setValue()  和  postValue() 区别:
 * setValue()  只能在主线程使用
 * postValue（） 可以在任意线程使用
 * <p>
 * suspend关键字  挂起函数
 */
public class TimerViewModel extends BaseViewModel {


    private static final String TAG = "测试生命周期";
    //将currentSecond字段用MutableLiveData包装起来
    private MutableLiveData<Integer> currentSecond;


    public LiveData<Integer> getCurrentSecond() {
        if (currentSecond == null) {
            currentSecond = new MutableLiveData<>();
        }
        return currentSecond;
    }

    private Timer timer;

    public void startTiming() {
        if (timer == null) {
            currentSecond.postValue(0);
            timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    //获取当前值
                    Integer value = currentSecond.getValue();
                    currentSecond.postValue(value + 1);
                }
            };
            timer.schedule(timerTask, 1000, 1000);
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.i(TAG, "onCleared: ======  TimerViewModel ");
        timer.cancel();
    }

    @Override
    public void onAny(@NotNull LifecycleOwner owner, @NotNull Lifecycle.Event event) {

    }

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate: TimerViewModel");
    }

    @Override
    public void onStart() {
        Log.i(TAG, "onStart: TimerViewModel");
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume: TimerViewModel");
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause: TimerViewModel");
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop: TimerViewModel");
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy: TimerViewModel");
    }
}



















