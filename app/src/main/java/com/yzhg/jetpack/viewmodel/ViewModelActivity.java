package com.yzhg.jetpack.viewmodel;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.yzhg.jetpack.R;

/**
 * 包名：com.yzhg.jetpack.viewmodel
 * 创建人：yzhg
 * 时间：2021/06/18 15:57
 * 描述：
 */
public class ViewModelActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_model);

        iniComponent();

    }

    @SuppressLint("SetTextI18n")
    private void iniComponent() {

        final TextView tvNum = findViewById(R.id.tvNum);


        /**
         *  ViewModelProvider 接收一个ViewModelStoreOwner对象作为参数
         *
         *
         *  Activity继承FragmentActivity 继承 ComponentActivity  实现 ViewModelStoreOwner
         */

        TimerViewModel timerViewModel = new ViewModelProvider(this).get(TimerViewModel.class);
        // timerViewModel.setOnTimeChangeListener(second -> runOnUiThread(() -> tvNum.setText("数字跳动" + second)));
        //   timerViewModel.startTiming();
        
        final MutableLiveData<Integer> liveData = (MutableLiveData<Integer>) timerViewModel.getCurrentSecond();
        liveData.observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                tvNum.setText("数字跳动" + integer);
            }
        });
        timerViewModel.startTiming();

        getLifecycle().addObserver(timerViewModel);
    }

    public static String TAG = "测试生命周期";

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart: activity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: activity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause: activity");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop: activity");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: activity");
    }
}























