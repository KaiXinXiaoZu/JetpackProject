package com.yzhg.jetpack.databinding.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.yzhg.jetpack.R
import com.yzhg.jetpack.databinding.ActivityNumDatabindingBinding
import com.yzhg.jetpack.databinding.bean.NumBean
import com.yzhg.jetpack.room.table.RoomTableBean
import java.util.*

/**
 * 包名：com.yzhg.jetpack.databinding.activity
 * 创建人：yzhg
 * 时间：2021/06/19 11:06
 * 描述：
 */
class NumDataBindingActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val activityNumDatabindingBinding: ActivityNumDatabindingBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_num_databinding)


        val random = Random()
        val nextInt = random.nextInt(10000)
        val roomTableBean = NumBean(3,"我是" + nextInt + "号")
        activityNumDatabindingBinding.numBean = roomTableBean


        activityNumDatabindingBinding.eventHandleListener =  EventHandleListener(this)

    }


}