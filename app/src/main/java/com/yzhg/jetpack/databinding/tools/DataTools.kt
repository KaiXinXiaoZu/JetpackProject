package com.yzhg.jetpack.databinding.tools

/**
 * 包名：com.yzhg.jetpack.databinding.tools
 * 创建人：yzhg
 * 时间：2021/06/19 11:21
 * 描述：
 */
object DataTools {

    public fun getRatingString(rating: Int): String {
        when (rating) {
            0 -> {
                return "零星"
            }
            1 -> {
                return "一星"
            }
            2 -> {
                return "二星"
            }
            3 -> {
                return "三星"
            }
            else -> {
                return "未知"
            }
        }
    }

}