package com.yzhg.jetpack.databinding.activity

import android.content.Context
import android.view.View
import android.widget.Toast

/**
 * 包名：com.yzhg.jetpack.databinding.activity
 * 创建人：yzhg
 * 时间：2021/06/19 11:29
 * 描述：
 */
open class EventHandleListener(private var context: Context) {

    fun onButtonClicked(view: View) {
        Toast.makeText(context, "点击事件", Toast.LENGTH_LONG).show()
    }

}