package com.yzhg.jetpack.databinding.bean

/**
 * 包名：com.yzhg.jetpack.databinding.bean
 * 创建人：yzhg
 * 时间：2021/06/19 11:05
 * 描述：
 */
data class NumBean(
    public var rating: Int = 0,
    public var numData: String
)