package com.yzhg.jetpack;

import java.util.Stack;

/**
 * 编辑 : yzhg
 * 时间 ：22:14-06-18 22:14
 * 包名 : com.yzhg.jetpack
 * 描述 : 一句话描述
 */
public class HexTools2 {

    /**
     * 十进制转十六进制
     */
    public static String baseString(int num, int base) {
        if (base > 16) {
            throw new RuntimeException("进制数超出范围，base<=16");
        }
        StringBuffer str = new StringBuffer("");
        String digths = "0123456789ABCDEF";
        Stack<Character> s = new Stack<Character>();
        while (num != 0) {
            s.push(digths.charAt(num % base));
            num /= base;
        }
        while (!s.isEmpty()) {
            str.append(s.pop());
        }
        return str.toString();
    }


    public static String decimalToHexString(int decimal) {
        // 转换为十六进制字符串
        String hexString = Integer.toHexString(decimal);
        // 补足位数
        while (hexString.length() < 4) {
            hexString = "0" + hexString;
        }
        return hexString;
    }

    /**
     * 十六进制转byte数组
     */
    /**
     * bytes字符串转换为Byte值
     *
     * @param src Byte字符串，每个Byte之间没有分隔符
     * @return byte[]
     */
    public static byte[] hexStr2Bytes(String src) {
        int m = 0, n = 0;
        int l = src.length() / 2;
        System.out.println(l);
        byte[] ret = new byte[l];
        for (int i = 0; i < l; i++) {
            m = i * 2 + 1;
            n = m + 1;
            ret[i] = Byte.decode("0x" + src.substring(i * 2, m) + src.substring(m, n));
        }
        return ret;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        // 如果长度为奇数，则补一位'0'
        if (len % 2 != 0) {
            s = "0" + s;
            len = len + 1;
        }
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            // 将十六进制的两个字符转换为一个字节
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * byte数组 转十六进制
     */
    /**
     * bytes转换成十六进制字符串
     *
     * @param byte[] b byte数组
     * @return String 每个Byte值之间空格分隔
     */
    public static String byte2HexStr(byte[] b) {
        String stmp = "";
        StringBuilder sb = new StringBuilder("");
        for (int n = 0; n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0xFF);
            sb.append((stmp.length() == 1) ? "0" + stmp : stmp);
//            sb.append(" ");
        }
        return sb.toString().toUpperCase().trim();
    }

    /**
     * 十六进制转十进制
     */

    public static int hexToDecimal(String hex) {
        return Integer.parseInt(hex, 16);
    }

    /**
     * byte数组转十进制
     */


}
