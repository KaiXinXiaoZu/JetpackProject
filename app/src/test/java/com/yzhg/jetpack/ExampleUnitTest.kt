package com.yzhg.jetpack

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        // assertEquals(4, 2 + 2)
        //val baseString = HexTools2.baseString(866, 16)
        val baseString = HexTools2.decimalToHexString(0)
        println("十进制转十六进制 === $baseString")
        println("-------------------------------")
        val hexStr2Bytes = HexTools2.hexStr2Bytes(baseString)
        println("十六进制转byte ===")
        hexStr2Bytes.forEach {
            print("$it  ")
        }
        println("")
        println("-------------------------------")

        val byte2HexStr = HexTools2.byte2HexStr(hexStr2Bytes)
        println("-------------------------------")
        println("byte转十六进制 ===$byte2HexStr")
        println("-------------------------------")


        val hexToDecimal = HexTools2.hexToDecimal(byte2HexStr)
        println("-------------------------------")
        println("十六进制转十进制 ===$hexToDecimal")
        println("-------------------------------")
    }
}