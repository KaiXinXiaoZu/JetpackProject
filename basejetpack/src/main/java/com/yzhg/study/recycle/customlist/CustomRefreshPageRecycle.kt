package com.yzhg.study.recycle.customlist

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import com.scwang.smart.refresh.layout.SmartRefreshLayout
import com.scwang.smart.refresh.layout.api.RefreshLayout
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener
import com.yzhg.study.R
import com.yzhg.study.databinding.RecyclePageRefreshCustomBinding
import com.yzhg.study.http.enums.DataState
import com.yzhg.study.recycle.adapter.BaseCommentAdapter
import com.yzhg.study.recycle.listener.RecycleRefreshLoadListener
import com.yzhg.toollibrary.weight.page.StatusFrameLayout

/**
 * author : yzhg
 * 包名: com.yzhg.toollibrary.recycle.customlist
 * 时间: 2023-02-18 10:04
 * 描述：自定义  刷新列表
 *
 *      整合：
 *          下拉刷新和上拉加载
 *          页面状态（加载中、加载成功、加载失败页面）
 *          recycleView
 */
class CustomRefreshPageRecycle<T, BD : ViewDataBinding> : FrameLayout, OnRefreshLoadMoreListener {


    /** 页面 */
    private lateinit var imageSelectBinding: RecyclePageRefreshCustomBinding

    /** 页码 */
    private var pageIndex: Int = 1

    /**
     * 适配器
     */
    private var baseCommentAdapter: BaseCommentAdapter<T, BD>? = null


    constructor(context: Context) : this(context, null) {
        // orientation = VERTICAL
    }

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0) {
        // orientation = VERTICAL
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        //  orientation = VERTICAL
        initView(context)
    }


    fun setAdapter(baseCommentAdapter: BaseCommentAdapter<T, BD>?, refreshLoadListener: RecycleRefreshLoadListener) {
        setAdapter(baseCommentAdapter, refreshLoadListener, true)
    }

    private var refreshLoadListener: RecycleRefreshLoadListener? = null


    /**
     * 设置适配器
     */
    fun setAdapter(baseCommentAdapter: BaseCommentAdapter<T, BD>?, refreshLoadListener: RecycleRefreshLoadListener, isRequestNet: Boolean) {
        if (baseCommentAdapter != null) {
            this.baseCommentAdapter = baseCommentAdapter
            imageSelectBinding.recycleCustomList.adapter = this.baseCommentAdapter
        }

        this.refreshLoadListener = refreshLoadListener
        if (isRequestNet) {
            refreshLoadListener.onStartRequest(getPageIndex())
        }
    }

    /**
     * 设置数据
     */
    fun setFinishData(listData: MutableList<T>) {
        if (getPageIndex() == 1) {
            baseCommentAdapter?.data?.clear()
        }
        baseCommentAdapter?.addData(listData)
    }

    private fun initView(context: Context) {
        imageSelectBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.recycle_page_refresh_custom, null, false)

        imageSelectBinding.srlCustomList.setOnRefreshLoadMoreListener(this)
        imageSelectBinding.recycleCustomList.layoutManager = LinearLayoutManager(context)

        addView(imageSelectBinding.root)
        //默认显示加载中布局
        showLoadingView()
    }


    /**
     * 下拉刷新
     */
    override fun onRefresh(refreshLayout: RefreshLayout) {
        //设置页面为1
        resetPageIndex(1)
        //请求数据
        if (refreshLoadListener != null) {
            refreshLoadListener?.onStartRequest(getPageIndex())
        }
    }

    /**
     * 上拉加载
     */
    override fun onLoadMore(refreshLayout: RefreshLayout) {
        if (refreshLoadListener != null) {
            refreshLoadListener?.onStartRequest(getPageIndex())
        }
    }

    /**
     * 设置页面状态
     */
    fun setPageStatus(dataState: DataState?) {
        when (dataState) {
            DataState.STATE_LOADING -> {
                //设置加载中布局
                showLoadingView()
            }
            DataState.STATE_SUCCESS -> {
                //数据请求成功
                showSuccessView()
                //设置刷新完成
                setLoadFinish(true)
                setRefreshFinish(true)
                //设置页码加1
                setPageIndexAdd()
            }
            DataState.STATE_EMPTY -> {
                setRefreshFinish(true)
                //数据请求为空
                if (getPageIndex() == 1) {
                    //空布局
                    showEmptyView()
                    setLoadFinish(true)
                } else {
                    //设置没有数据
                    setNoDataLoad()
                }

            }
            DataState.STATE_FAILED, DataState.STATE_ERROR -> {
                //数据请求失败
                setRefreshFinish(true)
                setLoadFinish(true)

                if (getPageIndex() == 1) {
                    showErrorView()
                } else {
                    setLoadFinish(false)
                }

            }
            else -> {
                setRefreshFinish(true)
                setLoadFinish(true)
                if (getPageIndex() == 1) {
                    showErrorView()
                } else {
                    setLoadFinish(false)
                }
            }
        }
    }

    fun getPageStatus(): StatusFrameLayout {
        return imageSelectBinding.clCustomList
    }

    /**
     * 获取上拉加载和下拉刷新的布局
     */
    fun getSmartRefreshLayout(): SmartRefreshLayout {
        return imageSelectBinding.srlCustomList
    }

    /**
     * 设置下拉刷新完成
     */
    fun setRefreshFinish(isSuccess: Boolean) {
        getSmartRefreshLayout().finishRefresh(isSuccess)
    }

    /**
     * 设置上拉加载完成
     */
    fun setLoadFinish(isSuccess: Boolean) {
        getSmartRefreshLayout().finishLoadMore(isSuccess)
    }

    /**
     * 设置没有数据
     */
    fun setNoDataLoad() {
        getSmartRefreshLayout().finishLoadMoreWithNoMoreData()
    }

    /**
     * 重置当前页码
     */
    fun resetPageIndex(pageIndex: Int) {
        this.pageIndex = pageIndex
    }

    /**
     * 获取当前页码
     */
    fun getPageIndex(): Int {
        return pageIndex
    }

    /**
     * 设置加载中布局
     */
    fun showLoadingView() {
        imageSelectBinding.clCustomList.showLoadingContent()
    }

    /**
     * 显示成功布局
     */
    fun showSuccessView() {
        imageSelectBinding.clCustomList.showContent()
    }

    /**
     * 显示空布局
     */
    fun showEmptyView() {
        imageSelectBinding.clCustomList.showEmptyContent()
    }

    /**
     * 设置错误布局
     */
    fun showErrorView() {
        imageSelectBinding.clCustomList.showErrorContent()
    }

    /**
     * 设置页码+1
     */
    fun setPageIndexAdd() {
        pageIndex += 1
    }


}