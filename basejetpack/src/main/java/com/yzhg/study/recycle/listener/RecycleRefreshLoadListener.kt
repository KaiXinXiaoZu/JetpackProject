package com.yzhg.study.recycle.listener

/**
 * author : yzhg
 * 包名: com.yzhg.study.recycle.listener
 * 时间: 2023-02-23 14:42
 * 描述：
 */
interface RecycleRefreshLoadListener {

    fun onStartRequest(pageIndex:Int)
}