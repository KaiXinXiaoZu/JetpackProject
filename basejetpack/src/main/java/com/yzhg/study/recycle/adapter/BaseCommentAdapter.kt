package com.yzhg.study.recycle.adapter

import android.view.View
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.chad.library.adapter.base.viewholder.BaseViewHolder

open class BaseCommentAdapter<T, BD : ViewDataBinding>(@LayoutRes private val layoutResId: Int) :  BaseQuickAdapter<T, BaseDataBindingHolder<BD>>(layoutResId) {



    override fun convert(holder: BaseDataBindingHolder<BD>, item: T) {

    }

    override fun setOnItemClick(v: View, position: Int) {
        super.setOnItemClick(v, position)
    }


}