package com.yzhg.study.recycle.customlist;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;
import com.yzhg.study.R;
import com.yzhg.study.databinding.RecyclePageRefreshCustomBinding;
import com.yzhg.study.http.enums.DataState;
import com.yzhg.study.recycle.adapter.BaseCommentAdapter;
import com.yzhg.study.recycle.listener.RecycleRefreshLoadListener;
import com.yzhg.toollibrary.weight.page.StatusFrameLayout;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * author : yzhg
 * 包名: com.yzhg.study.recycle.customlist
 * 时间: 2023-02-23 15:52
 * 描述：
 */
public class CustomRefreshPageRecycle2 extends FrameLayout implements OnRefreshLoadMoreListener {

    private RecyclePageRefreshCustomBinding imageSelectBinding;

    private int pageIndex = 1;

    private BaseCommentAdapter baseCommentAdapter;

    private RecycleRefreshLoadListener refreshLoadListener;

    public CustomRefreshPageRecycle2(Context context) {
        super(context, null);
        //setOrientation(LinearLayout.VERTICAL);
    }

    public CustomRefreshPageRecycle2(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, 0);
       // setOrientation(LinearLayout.VERTICAL);
        initView(context);
    }

    public CustomRefreshPageRecycle2(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
       // setOrientation(LinearLayout.VERTICAL);
        initView(context);
    }

    private void initView(Context context) {
        imageSelectBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.recycle_page_refresh_custom, null, false);

        imageSelectBinding.srlCustomList.setOnRefreshLoadMoreListener(this);
        imageSelectBinding.recycleCustomList.setLayoutManager(new LinearLayoutManager(context));

        addView(imageSelectBinding.getRoot());
        //默认显示加载中布局
        showLoadingView();
    }


    public void setAdapter(BaseCommentAdapter commentAdapter, RecycleRefreshLoadListener recycleRefreshLoadListener) {
        setAdapter(commentAdapter, recycleRefreshLoadListener, true);
    }

    public void setAdapter(BaseCommentAdapter baseCommentAdapter, RecycleRefreshLoadListener refreshLoadListener, Boolean isRequestNet) {
        if (baseCommentAdapter != null) {
            this.baseCommentAdapter = baseCommentAdapter;
            imageSelectBinding.recycleCustomList.setAdapter(this.baseCommentAdapter);
        }

        this.refreshLoadListener = refreshLoadListener;

        if (isRequestNet) {
            refreshLoadListener.onStartRequest(getPageIndex());
        }
    }

    /**
     * 设置数据
     */
    public <T> void setFinishData(List<T> list) {
        if (getPageIndex() == 1) {
            baseCommentAdapter.getData().clear();
        }
        baseCommentAdapter.addData(list);
    }


    @Override
    public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
        //设置页面为1
        resetPageIndex(1);
        //请求数据
        if (refreshLoadListener != null) {
            refreshLoadListener.onStartRequest(getPageIndex());
        }
    }

    @Override
    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
        if (refreshLoadListener != null) {
            refreshLoadListener.onStartRequest(getPageIndex());
        }
    }

    /**
     * 设置页面状态
     */
    public void setPageStatus(DataState dataState) {
        switch (dataState) {
            case STATE_LOADING:
                showLoadingView();
                break;
            case STATE_SUCCESS:
                //数据请求成功
                showSuccessView();
                //设置刷新完成
                setLoadFinish(true);
                setRefreshFinish(true);
                //设置页码加1
                setPageIndexAdd();
                break;
            case STATE_EMPTY:
                setRefreshFinish(true);
                //数据请求为空
                if (getPageIndex() == 1) {
                    //空布局
                    showEmptyView();
                    setLoadFinish(true);
                } else {
                    //设置没有数据
                    setNoDataLoad();
                }
                break;
            case STATE_FAILED:
            case STATE_ERROR:
                //数据请求失败
                setRefreshFinish(true);
                setLoadFinish(true);

                if (getPageIndex() == 1) {
                    showErrorView();
                } else {
                    setLoadFinish(false);
                }
                break;
            default: {
                setRefreshFinish(true);
                setLoadFinish(true);
                if (getPageIndex() == 1) {
                    showErrorView();
                } else {
                    setLoadFinish(false);
                }
            }
        }
    }

    public StatusFrameLayout getPageStatus() {
        return imageSelectBinding.clCustomList;
    }

    /**
     * 获取上拉加载和下拉刷新的布局
     */
    SmartRefreshLayout getSmartRefreshLayout() {
        return imageSelectBinding.srlCustomList;
    }

    /**
     * 设置下拉刷新完成
     */
    public void setRefreshFinish(Boolean isSuccess) {
        getSmartRefreshLayout().finishRefresh(isSuccess);
    }

    /**
     * 设置上拉加载完成
     */
    public void setLoadFinish(Boolean isSuccess) {
        getSmartRefreshLayout().finishLoadMore(isSuccess);
    }

    /**
     * 设置没有数据
     */
    public void setNoDataLoad() {
        getSmartRefreshLayout().finishLoadMoreWithNoMoreData();
    }

    /**
     * 重置当前页码
     */
    public void resetPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    /**
     * 获取当前页码
     */
    public int getPageIndex() {
        return pageIndex;
    }

    /**
     * 设置加载中布局
     */
    public void showLoadingView() {
        imageSelectBinding.clCustomList.showLoadingContent();
    }

    /**
     * 显示成功布局
     */
    public void showSuccessView() {
        imageSelectBinding.clCustomList.showContent();
    }

    /**
     * 显示空布局
     */
    public void showEmptyView() {
        imageSelectBinding.clCustomList.showEmptyContent();
    }

    /**
     * 设置错误布局
     */
    public void showErrorView() {
        imageSelectBinding.clCustomList.showErrorContent();
    }

    /**
     * 设置页码+1
     */
    public void setPageIndexAdd() {
        pageIndex += 1;
    }
}
