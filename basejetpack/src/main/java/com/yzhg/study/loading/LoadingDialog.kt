package com.yzhg.study.loading

import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.yzhg.study.R
import com.yzhg.toollibrary.dialog.BaseDialog
import com.yzhg.toollibrary.dialog.ViewHolder


/**
 * 包名：com.yzhg.study.loading
 * 创建人：yzhg
 * 时间：2021/07/03 15:11
 * 描述：
 */
class LoadingDialog : BaseDialog() {

    companion object {

        /**
         * @param message 弹框提示语，如果为空，则会隐藏提示语
         */

        fun newInstance(message: String): LoadingDialog {
            val customLoading: LoadingDialog = LoadingDialog()
            val bundle = Bundle()
            bundle.putString("ALERT_MESSAGE", message)
            customLoading.arguments = bundle
            return customLoading
        }
    }

    //获取弹框提示语
    private var message: String = ""

    override fun initParamsView() {
        //获取弹框提示语
        message = arguments?.getString("ALERT_MESSAGE", "") ?: ""
    }


    override fun setUpLayoutId(): Int = R.layout.dialog_request_load

    override fun convertView(viewHolder: ViewHolder?, baseDialog: BaseDialog?) {
        val messageAlert: TextView? = viewHolder?.getView<TextView>(R.id.tv_message_dialog)
        if (messageAlert == null || message.isEmpty()) {
            //提示语为空
            messageAlert?.visibility = View.GONE
        } else {
            messageAlert.visibility = View.VISIBLE
            messageAlert.text = message
        }
    }
}