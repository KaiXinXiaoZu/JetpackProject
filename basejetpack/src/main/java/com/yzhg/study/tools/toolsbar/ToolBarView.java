package com.yzhg.study.tools.toolsbar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.yzhg.study.R;
import com.yzhg.toollibrary.weight.DrawableCenterTextView;


/**
 * Created by $(yzhg) on 2018/4/4 0004.
 * 用一句话描述()
 */

public class ToolBarView extends LinearLayout {

    /*上下文*/
    private Context context;
    /*标题大小*/
    private int titleSize;
    /*标题颜色*/
    private int titleColor;
    /*右侧标题颜色*/
    private int rightTextColor;
    /*设置左边字体大小*/
    private int leftSize;
    /*设置右边字体大小*/
    private int rightSize;
    /*设置标题*/
    private String titleText;
    /*设置左侧标题*/
    private String leftText;
    /*设置右侧的标题*/
    private String rightText;
    /*设置背景图片*/
    private int bgImage;
    /*背景颜色*/
    private int bgColor;
    /*是否显示左侧箭头,默认为true*/
    private boolean isShowLeft;
    /*是否显示左侧的文本,默认false不显示*/
    private boolean isShowLeftText = false;
    /*是否显示右侧的文本 默认false不显示*/
    private boolean isShowRightText = false;
    /*是否显示右侧的放大镜*/
    private boolean isShowMagnifyingGlass;
    /*是否显示中间的文本*/
    private boolean isShowTitle;
    /*设置返回按钮图片*/
    private Drawable backImage;
    /*设置放大镜图片*/
    private int rightImage;
    private ImageButton tvbacktoolbar;
    private TextView imbacktoolbar;
    private TextView tvtitletoolbar;
    private TextView tvcleartoolbar;
    private ImageButton ivmagnifyingglass;
    private RelativeLayout rltoolbartitle;
    private DrawableCenterTextView dctSearchIcon;
    private final int DEFAULT_SIZE = 32;
    private boolean isShowRightAllSelect;
    private boolean isShowSearchIconText;

    public ToolBarView(Context context) {
        super(context);
        this.context = context;
    }

    public ToolBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setOrientation(LinearLayout.VERTICAL);
        initAttributes(context, attrs);
        initView(context);
    }

    public ToolBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setOrientation(LinearLayout.VERTICAL);
        initAttributes(context, attrs);
        initView(context);
    }

    private void initAttributes(Context context, AttributeSet attrs) {
        TypedArray toolbarArray = context.obtainStyledAttributes(attrs, R.styleable.ToolBarView);
        titleSize = toolbarArray.getDimensionPixelSize(R.styleable.ToolBarView_setTitleSize, DEFAULT_SIZE);
        rightTextColor = toolbarArray.getColor(R.styleable.ToolBarView_setRightTextColor, ContextCompat.getColor(context, R.color.color_000000));
        titleColor = toolbarArray.getColor(R.styleable.ToolBarView_setTitleColor, ContextCompat.getColor(context, R.color.color_000000));
        leftSize = toolbarArray.getDimensionPixelSize(R.styleable.ToolBarView_setLeftSize, DEFAULT_SIZE);
        rightSize = toolbarArray.getDimensionPixelSize(R.styleable.ToolBarView_setRightSize, DEFAULT_SIZE);
        titleText = toolbarArray.getString(R.styleable.ToolBarView_setTitleText);
        leftText = toolbarArray.getString(R.styleable.ToolBarView_setLeftText);
        rightText = toolbarArray.getString(R.styleable.ToolBarView_setRightText);
        bgImage = toolbarArray.getResourceId(R.styleable.ToolBarView_setBgImage, ContextCompat.getColor(context, R.color.color_FFFFFF));
        bgColor = toolbarArray.getColor(R.styleable.ToolBarView_setBgColor, ContextCompat.getColor(context, R.color.color_FFFFFF));

        isShowLeft = toolbarArray.getBoolean(R.styleable.ToolBarView_isShowLeft, true);
        isShowLeftText = toolbarArray.getBoolean(R.styleable.ToolBarView_isShowLeftText, false);
        isShowRightText = toolbarArray.getBoolean(R.styleable.ToolBarView_isShowRightText, false);
        isShowMagnifyingGlass = toolbarArray.getBoolean(R.styleable.ToolBarView_isShowMagnifyingGlass, false);
        isShowTitle = toolbarArray.getBoolean(R.styleable.ToolBarView_isShowTitle, true);
        backImage = toolbarArray.getDrawable(R.styleable.ToolBarView_setLeftBackImage);
        rightImage = toolbarArray.getResourceId(R.styleable.ToolBarView_setRightImage, com.yzhg.toollibrary.R.mipmap.search);
        isShowRightAllSelect = toolbarArray.getBoolean(R.styleable.ToolBarView_isShowRightAllSelect, false);
        isShowSearchIconText = toolbarArray.getBoolean(R.styleable.ToolBarView_isShowSearchIconText, false);
        toolbarArray.recycle();
    }


    private void initView(Context context) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.custom_toolbar_layout, null);
        this.rltoolbartitle = inflate.findViewById(R.id.rl_toolbar_title);
        this.ivmagnifyingglass = inflate.findViewById(R.id.iv_magnifying_glass);
        this.tvcleartoolbar = inflate.findViewById(R.id.tv_clear_toolbar);
        this.tvtitletoolbar = inflate.findViewById(R.id.tv_title_toolbar);
        this.imbacktoolbar = inflate.findViewById(R.id.im_back_toolbar);
        this.tvbacktoolbar = inflate.findViewById(R.id.tv_back_toolbar);
        this.dctSearchIcon = inflate.findViewById(R.id.dct_search_icon);


        rltoolbartitle.setBackgroundColor(bgColor);
        //    rltoolbartitle.setBackgroundResource(bgImage);

        tvbacktoolbar.setOnClickListener(v -> ((Activity) context).finish());  //点击返回
        tvbacktoolbar.setVisibility(getVisible(isShowLeft));  //设置是否显示左侧的箭头
        tvbacktoolbar.setBackground(backImage);  //设置返回箭头

        imbacktoolbar.setOnClickListener(v -> ((Activity) context).finish());  //点击返回
        imbacktoolbar.setVisibility(getVisible(isShowLeftText));  //设置是否显示左侧的文本,默认不显示
        imbacktoolbar.setText(leftText);  //设置左侧的文字
        imbacktoolbar.setTextSize(px2dip(leftSize));  //设置左侧文字的大小

        tvtitletoolbar.setVisibility(getVisible(isShowTitle)); //是否显示标题
        tvtitletoolbar.setText(titleText);  //设置标题的文本
        tvtitletoolbar.setTextColor(titleColor);  //设置标题的文本
        tvtitletoolbar.setTextSize(px2dip(titleSize));  //设置标题的大小


        tvcleartoolbar.setVisibility(getVisible(isShowRightText));
        tvcleartoolbar.setText(rightText);
        tvcleartoolbar.setTextSize(px2dip(rightSize));
        tvcleartoolbar.setTextColor(rightTextColor);
        tvcleartoolbar.setOnClickListener(v -> {
            if (rightClick != null) {
                rightClick.rightClick();
            }
        });

        ivmagnifyingglass.setVisibility(getVisible(isShowMagnifyingGlass));
        ivmagnifyingglass.setImageResource(rightImage);  //设置放大镜图片
        ivmagnifyingglass.setOnClickListener(v -> {
            if (searchClickListener != null) {
                searchClickListener.mgClick();
            }
        });

        dctSearchIcon.setVisibility(getVisible(isShowSearchIconText));  //设置是否显示左侧的箭头
        dctSearchIcon.setOnClickListener(view -> {
            if (searchClickListener != null) {
                searchClickListener.mgClick();
            }
        });

        addView(inflate);
    }

    /**
     * 设置隐藏 右侧搜索按钮（有放大镜的View）
     */
    public void setShowSearchIcon(Boolean isShowSearchIconText) {
        this.isShowSearchIconText = isShowSearchIconText;
        dctSearchIcon.setVisibility(getVisible(isShowSearchIconText));  //设置是否显示左侧的箭头
    }


    /**
     * 设置是否显示右侧文字
     */
    public void isShowRightText(boolean showRightText) {
        tvcleartoolbar.setVisibility(getVisible(showRightText));
    }

    /**
     * 设置Toolbar是否隐藏,此属性不在XML中设置
     *
     * @param visible
     */
    public void setToolbarHide(boolean visible) {
        rltoolbartitle.setVisibility(getVisible(visible));
    }


    //隐藏右侧图片
    public void setRightIconHide(boolean visible){
        ivmagnifyingglass.setVisibility(getVisible(visible));
    }

    /**
     * 设置TextVIew LeftImage 的图片资源
     *
     * @param imageID
     */
    public void setRightAllSelect(int imageID) {
        @SuppressLint("UseCompatLoadingForDrawables")
        Drawable drawableLeft = getResources().getDrawable(imageID);
    }

    /**
     * 设置标题的字体,在java文件中动态设置
     *
     * @param resourceId
     */
    public void setTitleText(int resourceId) {
        tvtitletoolbar.setText(getStirng(resourceId));
    }

    public void setTitleText(String resourceId) {
        tvtitletoolbar.setText(resourceId);
    }

    public void setRightTitle(String resourceId) {
        tvcleartoolbar.setText(resourceId);
    }

    public void setRightTitle(int resourceId) {
        tvcleartoolbar.setText(getStirng(resourceId));
    }

    public void setRightShow(boolean isShow) {
        tvcleartoolbar.setVisibility(getVisible(isShow));
    }

    public void setRightImageShow(boolean isShow) {
        ivmagnifyingglass.setVisibility(getVisible(isShow));
    }

    public void setVisibleRight(boolean isShow) {
        tvcleartoolbar.setVisibility(getVisible(isShow));
    }

    public void setVisibleTitle(boolean isShow) {
        tvtitletoolbar.setVisibility(isShow ? View.VISIBLE : View.INVISIBLE);
    }

    private int getVisible(boolean isShow) {
        return isShow ? View.VISIBLE : View.GONE;
    }

    public int dip2px(float dip) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (dip * density + 0.5f);
    }

    public float px2dip(float px) {
        float density = context.getResources().getDisplayMetrics().density;
        return px / density;
    }

    private OnToolBarClick rightClick;

    public void setOnRightClick(OnToolBarClick rightClick) {
        this.rightClick = rightClick;
    }

    public interface OnToolBarClick {
        void rightClick();
    }

    private SearchClickListener searchClickListener;

    public void setSearchClickListener(SearchClickListener searchClickListener) {
        this.searchClickListener = searchClickListener;
    }

    public interface SearchClickListener {
        void mgClick();
    }

    public ImageButton getRightImage() {
        return this.ivmagnifyingglass;
    }

    /**
     * 获取字符串
     */
    public String getStirng(int id) {
        return context.getResources().getString(id);
    }

}