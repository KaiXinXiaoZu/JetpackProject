package com.yzhg.study.tools;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yzhg.study.R;
import com.yzhg.toollibrary.Tools;
import com.yzhg.toollibrary.weight.ShapeTextView;


/*
 * 操作人 :  Administrator
 * 时  间 :  2019/9/18 0018
 * 描  述 : 自定义的页面  可以切换加载中，加载失败，加载成功的三种布局
 */
public class CustomLayout extends FrameLayout {


    /*定义三种布局*/
    /*显示空布局*/
    public static final int START_EMPTY = 0;
    /*显示错误布局*/
    public static final int START_FAILED = 1;
    /*显示成功布局*/
    public static final int START_SUCCESS = 2;
    /*显示加载中布局*/
    public static final int START_LOADING = 3;


    /*设置默认的状态  -- 加载中的状态*/
    private int mState = START_SUCCESS;

    /*获取到layoutInflater*/
    protected LayoutInflater mInflater;

    /*获取到空布局*/
    private View mEmptyView;
    /*获取到失败布局*/
    private View mFailedView;
    /*获取到成功布局*/
    private View mContentView;
    /*获取到加载中布局*/
    private View mLoadView;

    private ImageView ivEmptyIcon;
    private TextView tvEmptyText;
    private ShapeTextView sButAgainRequest;
    private ImageView ivFailedIcon;
    private TextView tvFailedText;
    private ShapeTextView sButFailedAgainRequest;


    public CustomLayout(@NonNull Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);
    }

    public CustomLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);
    }

    public CustomLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mInflater = LayoutInflater.from(context);
    }

    public void setLoadEmptyError(OnAgainRequestListener requestListener) {
        //设置加载中
        //   setLoadView();
        //设置空布局
        setEmptyView(Tools.getStirng(R.string.data_empty), R.mipmap.empty);
        //设置加载失败布局
        setFailedView(Tools.getStirng(R.string.data_error), R.mipmap.error);
        //设置为加载中的布局
        setState(START_LOADING);
        //设置监听布局
        setOnAgainRequestListener(requestListener);
    }

    public void setLoadEmptyErrorLoad(OnAgainRequestListener requestListener) {
        //设置加载中
        setLoadView();
        //设置空布局
        setEmptyView(Tools.getStirng(R.string.data_empty), R.mipmap.empty);
        //设置加载失败布局
        setFailedView(Tools.getStirng(R.string.data_error), R.mipmap.error);
        //设置为加载中的布局
        setState(START_LOADING);
        //设置监听布局
        setOnAgainRequestListener(requestListener);
    }


    public void setLoadEmptyErrorLoad(String emptyStr, String errorStr, OnAgainRequestListener requestListener) {
        //设置加载中
        setLoadView();
        //设置空布局
        setEmptyView(emptyStr, R.mipmap.empty);
        //设置加载失败布局
        setFailedView(errorStr, R.mipmap.error);
        //设置为加载中的布局
        setState(START_LOADING);
        //设置监听布局
        setOnAgainRequestListener(requestListener);
    }

    public void setLoadEmptyErrorLoad(String emptyStr, int emptyHintImage, OnAgainRequestListener requestListener) {
        //设置加载中
        setLoadView();
        //设置空布局
        setEmptyView(emptyStr, emptyHintImage);
        //设置加载失败布局
        setFailedView(Tools.getStirng(R.string.data_error), R.mipmap.error);
        //设置为加载中的布局
        setState(START_LOADING);
        //设置监听布局
        setOnAgainRequestListener(requestListener);
    }

    /**
     * 操作人 : yzhg
     * 描  述 : 在布局完成的时候调用此方法
     */
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (getChildCount() > 1) {
            throw new IllegalStateException("======================== CustomLayout 只允许有一个子布局 ========================");
        }
        mContentView = getChildAt(0);
        //加载成功的布局，默认不可见
        mContentView.setVisibility(VISIBLE);
    }

    /**
     * 操作人 : yzhg
     * 描  述 : 获取当前布局状态
     */
    public int getStateCurrent(View view) {
        return mState;
    }


    /**
     * 操作人 : yzhg
     * 描  述 : 设置空布局
     */
    public void setEmptyView(String hintText, int hintImage) {
        removeView(mEmptyView);
        mEmptyView = mInflater.inflate(R.layout.load_empty_callback, this, false);
        this.tvEmptyText = mEmptyView.findViewById(R.id.ivEmptyText);
        this.ivEmptyIcon = mEmptyView.findViewById(R.id.ivEmptyLayout);
        this.sButAgainRequest = mEmptyView.findViewById(R.id.sButAgainRequest);
        tvEmptyText.setText(hintText);
        ivEmptyIcon.setBackgroundResource(hintImage);
        //设置再次点击的事件
     /*   if (sButAgainRequest != null) {
            sButAgainRequest.setOnClickListener(v -> {
                if (requestEmptyListener != null) {
                    setState(CustomLayout.START_LOADING);
                    requestEmptyListener.onEmptyRequestClick();
                }
            });
        }*/
        addStateView(mEmptyView);
        updateViewState();
    }

    /**
     * 操作人 : yzhg
     * 描  述 : 设置空布局
     */
    public void setEmptyView(String hintText, String butMsg, int hintImage) {
        removeView(mEmptyView);
        mEmptyView = mInflater.inflate(R.layout.load_empty_callback, this, false);
        this.tvEmptyText = mEmptyView.findViewById(R.id.ivEmptyText);
        this.ivEmptyIcon = mEmptyView.findViewById(R.id.ivEmptyLayout);
        this.sButAgainRequest = mEmptyView.findViewById(R.id.sButAgainRequest);
        tvEmptyText.setText(hintText);
        ivEmptyIcon.setBackgroundResource(hintImage);
        sButAgainRequest.setText(butMsg);
        //显示点击事件
        sButAgainRequest.setVisibility(View.VISIBLE);

        addStateView(mEmptyView);
        updateViewState();
    }

    /**
     * 设置空布局
     */
    public void setCustomEmpty(View customEmpty) {
        removeView(mEmptyView);
        mEmptyView = customEmpty;
        addStateView(customEmpty);
        updateViewState();
    }

    /**
     * 操作人 : yzhg
     * 描  述 : 设置空布局时的数据
     */
    private void setEmptyData(String hintText, int hintImage) {
        if (mEmptyView != null) {
            if (hintText != null) {
                tvEmptyText.setText(hintText);
            }
            if (hintImage != 0) {
                ivEmptyIcon.setBackgroundResource(hintImage);
            }
            mState = START_EMPTY;  //设置当前为空布局
            updateViewState();  //更改当前显示状态
        }
    }

    public void setEmptyData(String hintText, String butMsg,OnAgainRequestEmptyListener againRequestEmptyListener) {
        if (mEmptyView != null) {
            if (hintText != null) {
                tvEmptyText.setText(hintText);
            }
            sButAgainRequest.setText(butMsg);
            //显示点击事件
            sButAgainRequest.setVisibility(View.VISIBLE);

            //设置再次点击的事件
            if (sButAgainRequest != null) {
                sButAgainRequest.setOnClickListener(v -> {
                    if (againRequestEmptyListener != null) {
                        againRequestEmptyListener.onEmptyRequestClick();
                    }
                });
            }
            mState = START_EMPTY;  //设置当前为空布局
            updateViewState();  //更改当前显示状态
        }
    }

    /**
     * 操作人 : yzhg
     * 描  述 : 设置错误布局
     */
    public void setFailedView(String hintText, int hintImage) {
        removeView(mFailedView);
        mFailedView = mInflater.inflate(R.layout.load_error_layout, this, false);
        this.sButFailedAgainRequest = mFailedView.findViewById(R.id.sButFailedAgainRequest);
        this.tvFailedText = mFailedView.findViewById(R.id.tvFailedText);
        this.ivFailedIcon = mFailedView.findViewById(R.id.ivEmptyLayout);
        tvFailedText.setText(hintText);
        ivFailedIcon.setBackgroundResource(hintImage);
        //设置再次点击的事件
        if (sButFailedAgainRequest != null) {
            sButFailedAgainRequest.setOnClickListener(v -> {
                if (requestListener != null) {
                    setState(CustomLayout.START_LOADING);
                    requestListener.onAgainRequestClick();
                }
            });
        }
        addStateView(mFailedView);
        updateViewState();
    }

    /**
     * 操作人 : yzhg
     * 描  述 : 修改错误布局数据
     */
    public void setFailedData(String hintText, String againText, int hintImage) {
        if (mFailedView != null) {
            if (hintText != null) {
                tvFailedText.setText(hintText);
            }
            if (hintImage != 0) {
                ivFailedIcon.setBackgroundResource(hintImage);
            }
            if (againText != null) {
                sButFailedAgainRequest.setText(againText);
            }
            mState = START_FAILED;  //设置当前为空布局
            updateViewState();  //更改当前显示状态
        }
    }


    /**
     * 操作人 : yzhg
     * 描  述 : 设置加载中布局
     */
    public void setLoadView() {
        removeView(mLoadView);
        mLoadView = mInflater.inflate(R.layout.load_loading_callback, this, false);
        addStateView(mLoadView);
        updateViewState();
    }


    /**
     * 操作人 : yzhg
     * 描  述 : 添加布局的方法
     */
    private void addStateView(View view) {
        if (view != null) {
            ViewGroup.LayoutParams defaultParams = view.getLayoutParams();
            //如果默认没有LayoutParams或者不是FrameLayout.LayoutParams,重新生成一个
            if (defaultParams == null || !(defaultParams instanceof LayoutParams)) {
                defaultParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup
                        .LayoutParams.WRAP_CONTENT);
            }
            LayoutParams params = (LayoutParams) defaultParams;
            params.gravity = Gravity.CENTER;//局中显示
            view.setVisibility(GONE);
            addView(view, params);
        }
    }


    /**
     * 操作人 : yzhg
     * 描  述 : 设置当前显示的状态
     */
    public void setState(int state) {
        switch (state) {
            case START_EMPTY:
            case START_FAILED:
            case START_SUCCESS:
            case START_LOADING:
                mState = state;
                updateViewState();
                break;
            default:
                throw new IllegalStateException("================ 只允许指定以上四种模块 ================");
        }
    }


    /**
     * 操作人 : yzhg
     * 描  述 :  更改布局状态
     */
    private void updateViewState() {
        if (mEmptyView != null) {
            mEmptyView.setVisibility(mState == START_EMPTY ? VISIBLE : GONE);
        }
        if (mFailedView != null) {
            mFailedView.setVisibility(mState == START_FAILED ? VISIBLE : GONE);
        }
        if (mContentView != null) {
            mContentView.setVisibility(mState == START_SUCCESS ? VISIBLE : GONE);
        }
        if (mLoadView != null) {
            mLoadView.setVisibility(mState == START_LOADING ? VISIBLE : GONE);
        }
    }


    //================================接口回调的方式去处理再次请求==================================//

    private OnAgainRequestListener requestListener;

    public void setOnAgainRequestListener(OnAgainRequestListener requestListener) {
        this.requestListener = requestListener;
    }

    public interface OnAgainRequestListener {
        void onAgainRequestClick();
    }



    private OnAgainRequestEmptyListener requestEmptyListener;

    public void setOnAgainRequestListener(OnAgainRequestEmptyListener requestListener) {
        this.requestEmptyListener = requestEmptyListener;
    }

    public interface OnAgainRequestEmptyListener {
        void onEmptyRequestClick();
    }
}
