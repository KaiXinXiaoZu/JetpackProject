package com.yzhg.study.tools.language

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.LocaleList
import android.text.TextUtils
import androidx.annotation.RequiresApi
import com.wsdz.tools.common.SpTools
import java.util.*

/**
 * 包名：com.yzhg.examples.ui.activity.language
 * 创建人：yzhg
 * 时间：2021/07/07 14:17
 * 描述：语言工具类  存放常用语言
 */
object LanguageTools {

    /**
     * 简体中文
     */
    const val SIMPLIFIED_CHINESE = "zh_cn"

    /**
     * 英语
     */
    const val ENGLISH = "en";

    /**
     * 日语
     */
    const val JAPANESE = "ja"

    /**
     * 韩语
     */
    const val KOREAN = "ko"

    /**
     * 俄语
     */
    const val RUSSIAN = "ru"

    /**
     * 法语
     */
    const val FRENCH = "fr"

    /**
     * 西班牙语
     */
    const val SPANISH = "es"

    /**
     * 阿拉伯语
     */
    const val ARABIC = "ar"

    /**
     * Locale语言项集合
     */
    var mAllLanguages: HashMap<String, Locale> = object : HashMap<String, Locale>(8) {
        init {
            put(SIMPLIFIED_CHINESE, Locale.SIMPLIFIED_CHINESE)
            put(ENGLISH, Locale.ENGLISH)
            put(JAPANESE, Locale.JAPANESE)
            put(KOREAN, Locale.KOREAN)
            put(RUSSIAN, Locale(SPANISH, "RU"))
            put(FRENCH, Locale.FRENCH)
            put(SPANISH, Locale(SPANISH, "ES"))
            put(ARABIC, Locale(ARABIC, "AR"))
        }
    }


     fun getAppLanguage(base: Context?): String {
        //获取保存到本地的语言
        var currentLanguage: String = SpTools.getString(base!!, "select_language")
        //判断本地是否选择了语言，如果没有选择就获取机系统默认语言
        if (TextUtils.isEmpty(currentLanguage)) {
            //获取默认语言
            val locale: Locale = Locale.getDefault()
            //获取设置的语言列表项
            val mAllLanguages: HashMap<String, Locale> = LanguageTools.mAllLanguages
            for (key in mAllLanguages.keys) {
                //如果在列表中找到
                if (mAllLanguages[key] != null && TextUtils.equals(mAllLanguages[key]?.language?.lowercase(Locale.getDefault()),
                        locale.language.lowercase(Locale.getDefault()))
                ) {
                    currentLanguage = locale.language
                }
            }
            if (!TextUtils.isEmpty(currentLanguage)) {
                SpTools.putString(base, "select_language", currentLanguage)
            }
        }
        return if (currentLanguage.isEmpty()) {
            LanguageTools.SIMPLIFIED_CHINESE
        } else {
            currentLanguage
        }
    }

    fun attachBaseContext(context: Context, language: String): Context {
        return if (!TextUtils.isEmpty(language) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            updateResources(context, language)
        } else {
            changeAppLanguage(context, language)
            context
        }
    }


    /**
     * Android N以上使用
     */
    @RequiresApi(Build.VERSION_CODES.N)
    private fun updateResources(context: Context, language: String): Context {
        val resources: Resources = context.resources

        //获取语言项
        val locale: Locale = getLocaleByLanguage(language)
        val configuration: Configuration = resources.configuration
        configuration.setLocale(locale)
        configuration.setLocales(LocaleList(locale))
        return context.createConfigurationContext(configuration)
    }


     fun changeAppLanguage(context: Context, language: String) {
        val resources: Resources = context.resources
        val configuration: Configuration = resources.configuration

        //获取语言项
        val locale: Locale = getLocaleByLanguage(language)
        configuration.setLocale(locale)

        // updateConfiguration
        val dm = resources.displayMetrics
        resources.updateConfiguration(configuration, dm)
    }

    private fun getLocaleByLanguage(language: String): Locale {
        if (isSupportLanguage(language)) {
            return mAllLanguages[language]!!
        } else {
            val locale: Locale = Locale.getDefault()
            for (key in mAllLanguages.keys) {
                if (mAllLanguages[key] != null && TextUtils.equals(mAllLanguages[key]?.language?.lowercase(Locale.getDefault()),
                        locale.language.lowercase(Locale.getDefault()))
                ) {
                    return locale
                }
            }
        }
        return Locale.SIMPLIFIED_CHINESE
    }

    /**
     * 获取当前语言
     */
    open fun getSupportLanguage(language: String): String? {
        return if (isSupportLanguage(language)) {
            language
        } else SIMPLIFIED_CHINESE
    }


    /**
     * 判断设置的集合是否包含这个语言
     */
   private fun isSupportLanguage(language: String): Boolean {
        return mAllLanguages.keys.contains(language.lowercase())
    }


}