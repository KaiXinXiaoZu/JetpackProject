package com.yzhg.study.tools.back;

/*
 * 作者：yzhg
 * 时间：2022-01-04 15:04
 * 包名：com.yzhg.study.tools.back
 * 描述：
 */
public interface HandleBackInterface {
    boolean onBackPressed();
}
