package com.yzhg.study.loadsir.callback

import com.kingja.loadsir.callback.Callback
import com.yzhg.study.R

/**
 * 包名：com.yzhg.study.loadsir.callback
 * 创建人：yzhg
 * 时间：2021/06/21 15:41
 * 描述：超时状态
 */
class TimeoutCallback : Callback() {
    override fun onCreateView(): Int = R.layout.load_timeout_callback
}