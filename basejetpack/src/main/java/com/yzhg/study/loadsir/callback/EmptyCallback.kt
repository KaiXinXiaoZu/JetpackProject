package com.yzhg.study.loadsir.callback

import com.kingja.loadsir.callback.Callback
import com.yzhg.study.R

/**
 * 包名：com.yzhg.study.loadsir.callback
 * 创建人：yzhg
 * 时间：2021/06/21 15:40
 * 描述： 数据为空
 */
class EmptyCallback : Callback() {

    override fun onCreateView(): Int = R.layout.load_empty_callback

}