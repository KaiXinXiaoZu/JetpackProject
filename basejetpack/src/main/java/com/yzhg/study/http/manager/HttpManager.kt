package com.yzhg.study.http.manager

import com.yzhg.study.http.config.HttpConfig
import com.yzhg.study.http.interceptor.AddCookiesInterceptor
import com.yzhg.study.http.interceptor.BaseUrlInterceptor
import com.yzhg.study.http.interceptor.HeadersInterceptor
import com.yzhg.study.http.interceptor.SaveCookiesInterceptor
import com.yzhg.study.http.tools.SSLContextUtil
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit

/**
 * 包名：com.yzhg.study.http.manager
 * 创建人：yzhg
 * 时间：2021/06/19 15:49
 * 描述：网络请求管理类
 */
class HttpManager {

    /**
     * 创建OkHttp管理类
     */
    private val okHttpBuilder: OkHttpClient.Builder = OkHttpClient.Builder()

    /**
     * retrofit
     */
    private val retrofit: Retrofit

    /**
     * OkHttp日志
     */
    private val logger: HttpLoggingInterceptor
        get() {
            val loggingInterceptor = HttpLoggingInterceptor()
            if (HttpConfig.isDeBug()) {
                //启用日志
                loggingInterceptor.apply { level = HttpLoggingInterceptor.Level.BODY }
            } else {
                loggingInterceptor.apply { level = HttpLoggingInterceptor.Level.NONE }
            }
            return loggingInterceptor
        }

    init {


        // 设置日志
        okHttpBuilder.addInterceptor(logger)
        //添加Cookie
       // okHttpBuilder.cookieJar(LocalCookieJar());
        okHttpBuilder.addInterceptor(AddCookiesInterceptor())
        //保存
        okHttpBuilder.addInterceptor(SaveCookiesInterceptor())
        //添加请求拦截器
        okHttpBuilder.addInterceptor(HeadersInterceptor())
        //添加拦截器 可设置多个域名
        okHttpBuilder.addInterceptor(BaseUrlInterceptor())

        //获取自定义拦截器
        val interceptors = HttpConfig.getInterceptor()
        if (interceptors.isNotEmpty()) {
            interceptors.forEach { okHttpBuilder.addInterceptor(it) }
        }
        //设置连接时间  单位秒
        okHttpBuilder.connectTimeout(HttpConfig.getDefaultTimeOut().toLong(), TimeUnit.SECONDS)
        //设置读取时间 单位秒
        okHttpBuilder.readTimeout(HttpConfig.getDefaultTimeOut().toLong(), TimeUnit.SECONDS)
        //设置写入时间
        okHttpBuilder.writeTimeout(HttpConfig.getDefaultTimeOut().toLong(), TimeUnit.SECONDS)
        //设置连接失败后重试
        okHttpBuilder.retryOnConnectionFailure(true)

        //判断是否启用Https
        if (HttpConfig.isEnableHttps()) {
            //给client的builder添加了增加可以忽略SSL
            val sslParams = SSLContextUtil.getSslSocketFactory()
            okHttpBuilder.sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager)
            okHttpBuilder.hostnameVerifier(SSLContextUtil.UnSafeHostnameVerifier)
        }

        val client = okHttpBuilder.build()

        val builder = Retrofit.Builder()

        builder
            //设置总域名
            .baseUrl(HttpConfig.getBaseUrl())
            //设置 Gson
            // .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        val converter = HttpConfig.getConverter()
        if (converter == null) {
            builder.addConverterFactory(GsonConverterFactory.create())
        } else {
            builder.addConverterFactory(converter)
        }

        retrofit = builder.build()


    }


    /**
     * 设置APi
     */
    fun <S> create(serviceClass: Class<S>): S = retrofit.create(serviceClass)

    /**
     * 创建API
     */
    inline fun <reified T> create(): T = create(T::class.java)

}