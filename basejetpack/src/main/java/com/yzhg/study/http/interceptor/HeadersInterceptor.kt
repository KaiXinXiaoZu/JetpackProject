package com.yzhg.study.http.interceptor

import com.yzhg.study.http.config.HttpConfig
import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 * 包名：com.yzhg.study.http.interceptor
 * 创建人：yzhg
 * 时间：2021/06/25 16:06
 * 描述：请求头信息
 */
class HeadersInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val builder: Headers.Builder = request.headers.newBuilder()

        //获取请求头信息
        val headers: MutableMap<String, String> = HttpConfig.getHeaders()
        if (!headers.isNullOrEmpty()) {
            for (key in headers.keys) {
                builder.add(key, headers[key] ?: "")
            }
        }
        val headerBuild: Headers = builder.build()

        val requestBuilder: Request.Builder = request.newBuilder()
        val build: Request = requestBuilder.headers(headerBuild).build()
        return chain.proceed(build)
    }


}


















