package com.yzhg.study.http.config

import okhttp3.Interceptor
import retrofit2.Converter

/**
 * 包名：com.wsdz.httplibrary.http.config
 * 创建人：yzhg
 * 时间：2021/06/18 11:35
 * 描述：网络请求  配置类。
 *
 * 配置baseUrl  :  注意必须以  /  结尾
 * 配置超时时间 和  连接时间
 * 配置请求头信息
 * 配置常用 interceptors
 */
class HttpConfig private constructor(builder: Builder) {


    init {
        baseUrl = builder.baseUrl
        defaultTimeOut = builder.defaultTimeOut
        interceptors = builder.interceptors
        enableHttps = builder.enableHttps
        isDeBug = builder.isDeBug
        converter = builder.converter
        headers = builder.headers
        httpBaseUrl = builder.httpBaseUrl
    }

    companion object {

        /**
         * OkHttp  BaseUrl   必须以 / 结尾
         */
        private var baseUrl: String = ""

        /**
         * 获取用户baseUrl
         */
        fun getBaseUrl(): String = baseUrl

        /**
         * 连接超时
         */
        private var defaultTimeOut: Int = 0

        fun getDefaultTimeOut(): Int = defaultTimeOut

        /**
         * 配置请求头信息
         */
        private var headers: MutableMap<String, String> = mutableMapOf()

        fun getHeaders(): MutableMap<String, String> = headers

        /**
         * 设置拦截器
         */
        private var interceptors: MutableList<Interceptor> = mutableListOf()

        fun getInterceptor(): List<Interceptor> = interceptors

        /**
         * 是否启用Https
         */
        private var enableHttps: Boolean = false

        fun isEnableHttps(): Boolean = enableHttps

        /**
         * 是否启用日志
         */
        private var isDeBug: Boolean = false

        fun isDeBug(): Boolean = isDeBug

        /**
         * 设置网络拦截器
         */
        private var converter: Converter.Factory? = null

        fun getConverter(): Converter.Factory? = converter

        /**
         * 多域名配置
         */
        private var httpBaseUrl: MutableMap<String, String> = mutableMapOf()

        fun getHttpBaseUrl(): MutableMap<String, String> {
            return httpBaseUrl
        }
    }


    open class Builder {
        var baseUrl: String = ""
        var defaultTimeOut: Int = 0
        var interceptors: MutableList<Interceptor> = mutableListOf()
        var enableHttps: Boolean = false
        var isDeBug: Boolean = false
        var converter: Converter.Factory? = null
        var headers: MutableMap<String, String> = mutableMapOf()
        var httpBaseUrl: MutableMap<String, String> = mutableMapOf()

        /**
         * 设置总域名  常用域名
         */
        open fun setBaseUrl(baseUrl: String): Builder = apply {
            this.baseUrl = baseUrl
        }

        /**
         * 设置超时时间
         * @param defaultTimeout 时间单位 秒
         */
        open fun setDefaultTimeout(defaultTimeout: Int): Builder = apply {
            this.defaultTimeOut = defaultTimeout
        }

        /**
         * 设置Header 将Token设置给Headers
         * @param tokenKey 请求头键
         * @param tokenValue 请求头值
         */
        open fun setHeaders(tokenKey: String, tokenValue: String): Builder = apply {
            this.headers[tokenKey] = tokenValue
        }

        /**
         * 批量设置请求头
         */
        open fun setHeaders(header: MutableMap<String, String>): Builder = apply {
            this.headers.putAll(headers)
        }

        /**
         * 设置拦截器
         */
        open fun addInterceptor(interceptor: Interceptor): Builder = apply {
            this.interceptors.add(interceptor)
        }

        /**
         * 是否启用Https  默认不启用
         * @param enableHttps true启用Https 会进行跳过验证
         */
        open fun enableHttps(enableHttps: Boolean): Builder = apply {
            this.enableHttps = enableHttps
        }

        /**
         * 是否启用bug日志 默认启用
         * @param isDeBug false 关闭日志
         */
        open fun isDeBug(isDeBug: Boolean): Builder = apply {
            this.isDeBug = isDeBug
        }

        /**
         * 配置gson解析器。默认GsonConverterFactory
         * @param converter
         */
        open fun setConverter(converter: Converter.Factory): Builder = apply {
            this.converter = converter
        }

        /**
         * 批量配置多个域名
         * @param httpBaseUrl 键值对形式
         */
        open fun setHttpBaseUrl(httpBaseUrl: MutableMap<String, String>): Builder = apply {
            this.httpBaseUrl.putAll(httpBaseUrl)
        }
        /**
         * 配置多域名
         * @param urlHeader 名称 拼接在url_name:  后面
         * @param baseUrl 域名地址
         */
        open fun setHttpBaseUrl(urlHeader: String, baseUrl: String): Builder = apply {
            this.httpBaseUrl[urlHeader] = baseUrl
        }

        open fun build(): HttpConfig = HttpConfig(this)
    }
}