package com.yzhg.study.http.enums

/**
 * 包名：com.yzhg.study.http.enum
 * 创建人：yzhg
 * 时间：2021/06/21 14:06
 * 描述：网络请求状态
 */
enum class DataState {
    //创建
    STATE_CREATE,

    //加载中
    STATE_LOADING,

    //加载成功
    STATE_SUCCESS,

    //加载完成
    STATE_COMPLETED,

    //数据为null
    STATE_EMPTY,

    //接口请求成功但是服务器返回error
    STATE_FAILED,

    //请求失败
    STATE_ERROR,

    //未知
    STATE_UNKNOWN
}