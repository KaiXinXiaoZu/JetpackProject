package com.yzhg.study.http.observer

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import com.google.gson.JsonParseException
import com.kingja.loadsir.callback.Callback
import com.kingja.loadsir.callback.SuccessCallback
import com.kingja.loadsir.core.Convertor
import com.kingja.loadsir.core.LoadService
import com.kingja.loadsir.core.LoadSir
import com.scwang.smart.refresh.layout.SmartRefreshLayout
import com.yzhg.study.R
import com.yzhg.study.application.BasicApplication
import com.yzhg.study.http.bean.ResponseBean
import com.yzhg.study.http.enums.DataState
import com.yzhg.study.http.exception.ResultException
import com.yzhg.study.loadsir.callback.EmptyCallback
import com.yzhg.study.loadsir.callback.ErrorCallback
import com.yzhg.study.loadsir.callback.LoadingCallback
import com.yzhg.study.loadsir.callback.TimeoutCallback
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONException
import retrofit2.HttpException
import java.io.InterruptedIOException
import java.net.ConnectException
import java.net.UnknownHostException
import java.text.ParseException

/**
 * 包名：com.yzhg.study.http.observer
 * 创建人：yzhg
 * 时间：2021/06/21 16:31
 * 描述：
 */
abstract class BasicLoadStateObserver<T>(view: View?, private var smartRefreshLayout: SmartRefreshLayout) : Observer<ResponseBean<T>>,
    Callback.OnReloadListener {


    public var mLoadService: LoadService<Any>? = null

    init {
        if (view != null) {
            mLoadService = LoadSir.getDefault().register(view, this,
                Convertor<ResponseBean<T>> { t ->
                    //设置初始的页面状态
                    var resultCode: Class<out Callback> = SuccessCallback::class.java
                    //通过indexPage判断是否是上拉加载更多。如果是上拉加载更多则 不去判断页面状态。直接展示成功页面
                    when (t?.dataState) {
                        DataState.STATE_CREATE, DataState.STATE_LOADING ->
                            resultCode = if (t.indexPage ?: 1 <= 1) {
                                Log.i("executeRequest", "LoadingCallback异常")
                                LoadingCallback::class.java
                            } else {
                                SuccessCallback::class.java
                            }
                        //请求成功
                        DataState.STATE_SUCCESS -> resultCode = SuccessCallback::class.java
                        //数据为空
                        DataState.STATE_EMPTY -> resultCode = if (t.indexPage ?: 1 <= 1) {
                            EmptyCallback::class.java
                        } else {
                            SuccessCallback::class.java
                        }

                        DataState.STATE_FAILED, DataState.STATE_ERROR -> {
                            val error: Throwable? = t.errorException
                            //可以根据不同的错误类型，设置错误界面时的UI
                            when (error) {
                                is HttpException, is ConnectException, is UnknownHostException -> {
                                    if (t.indexPage ?: 1 <= 1) {
                                        Log.i("executeRequest", "超时异常")
                                        resultCode = TimeoutCallback::class.java
                                    } else {
                                        resultCode = SuccessCallback::class.java
                                    }
                                }
                                else -> {
                                    if (t.indexPage ?: 1 <= 1) {
                                        resultCode = ErrorCallback::class.java
                                    } else {
                                        resultCode = SuccessCallback::class.java
                                    }
                                }
                            }
                        }
                        DataState.STATE_COMPLETED, DataState.STATE_UNKNOWN -> {

                        }
                        else -> resultCode = SuccessCallback::class.java
                    }
                    resultCode
                })
        }
    }


    override fun onChanged(responseBean: ResponseBean<T>) {
        if (smartRefreshLayout.isRefreshing) {
            smartRefreshLayout.finishRefresh(true)
        }
        when (responseBean?.dataState) {
            DataState.STATE_SUCCESS -> {
                smartRefreshLayout.finishLoadMore()
                //数据请求成功
                onDataSuccess(responseBean.data!!)
            }
            DataState.STATE_EMPTY -> {
                if (responseBean.indexPage > 1) {
                    //上拉加载数据为空了
                    smartRefreshLayout.finishLoadMoreWithNoMoreData()
                } else {
                    //数据请求为空
                    onDataEmpty()
                }
            }
            DataState.STATE_FAILED, DataState.STATE_ERROR -> {
                if (responseBean.indexPage > 1) {
                    //上拉加载数据为空了
                    smartRefreshLayout.finishLoadMore(false)

                } else {
                    //数据请求失败
                    val errorException = responseBean.errorException
                    setErrorException(errorException)
                }
            }
            else -> {
                if (smartRefreshLayout.isLoading) {
                    smartRefreshLayout.finishLoadMore(false)
                }
                setErrorException(null)
            }
        }

        GlobalScope.launch {
            delay(500)
            mLoadService?.showWithConvertor(responseBean)
        }
    }
    private fun setErrorException(errorException: Exception?) {
        when (errorException) {
            is HttpException -> {
                //网络连接失败，请检查网络状况
                onDataError(getContext().getString(R.string.net_connect_error))
            }
            is ConnectException, is UnknownHostException -> {
                onDataError(getContext().getString(R.string.net_connect_timeOut))
            }
            is InterruptedIOException -> {
                onDataError(getContext().getString(R.string.net_connect_timeOut))
            }
            is JsonParseException, is JSONException, is ParseException -> {
                onDataError(getContext().getString(R.string.net_parse_error))
            }
            is ResultException -> {
                onDataError(errorException.message ?: getContext().getString(R.string.net_unKnown_error))
            }
            else -> {
                onDataError(getContext().getString(R.string.net_unKnown_error))
            }
        }
    }


    /**
     * 数据请求失败
     */
    open fun onDataError(string: String) {

    }

    /**
     * 数据请求为空
     */
    open fun onDataEmpty() {

    }

    abstract fun onDataSuccess(data: T)


    override fun onReload(v: View?) {

    }


    private fun getContext(): Context {
        return BasicApplication.getContext()
    }

}














