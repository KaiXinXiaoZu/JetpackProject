package com.yzhg.study.http

import com.yzhg.study.http.bean.ResponseBean
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.QueryMap
import retrofit2.http.Url

interface HttpApi {

    /**
     * get方式请求 无参数
     */
    @GET()
    suspend fun get(@Url url: String): Response<ResponseBody>

    /**
     * get方式请求
     */
    @GET()
    suspend fun get(@Url url: String, @QueryMap queryMap: Map<String, Any>): Response<ResponseBody>


    /**
     * post方式请求 无参数
     */
    @FormUrlEncoded
    @GET()
    suspend fun post(@Url url: String): Response<ResponseBody>

    /**
     * post请求 有参数
     */
    @FormUrlEncoded
    @GET()
    suspend fun post(@Url url: String, @FieldMap queryMap: Map<String, Any>): Response<ResponseBody>

    /**
     * Raw 、 json方式请求
     */
    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST
    suspend fun postBody(@Url url: String, @Body requestBody: RequestBody): Response<ResponseBody>

    /**
     * 上传单张图片
     */
    @Multipart
    @POST
    suspend fun upload(@Url url: String, @Part file: MultipartBody.Part): Response<ResponseBody>

    /**
     * 上传多张图片
     */
    @Multipart
    @POST
    suspend fun upload(@Url url: String, @Part file: List<MultipartBody.Part>): Response<ResponseBody>
}