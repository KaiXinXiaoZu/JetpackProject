package com.yzhg.study.http.converter

import com.google.gson.Gson
import com.yzhg.study.http.bean.ResponseBean
import com.yzhg.study.http.constant.HConstant
import com.yzhg.study.http.exception.ResultException
import okhttp3.ResponseBody
import retrofit2.Converter
import java.io.IOException
import java.lang.reflect.Type

/**
 * 包名：com.yzhg.study.http.converter
 * 创建人：yzhg
 * 时间：2021/06/19 17:00
 * 描述：解析器
 */

class GsonResponseBodyConverter<T>(gson: Gson, type: Type) : Converter<ResponseBody, T> {


    private val gson: Gson = gson

    private val type: Type = type




    @Throws(Exception::class)
    override fun convert(value: ResponseBody): T {
        //获取到参数
        var response: String = value.string()
        //将后台返回的字符串类型的数据转化为null。方便转化
        response = response.replace("\"data\":\"\"}", "\"data\":null}")
        if (response.indexOf("errorCode") > 0) {
            //说明有返回码,解析对象
            //先将返回的json数据解析到Response中，如果code==200，则解析到我们的实体基类中，否则抛异常
            val result: ResponseBean<Any> = gson.fromJson(response, ResponseBean::class.java) as ResponseBean<Any>
            if (HConstant.HTTP_REQUEST_SUCCESS_CODE == result.code) {
                //数据请求成功，直接解析，
                return gson.fromJson(response, type)
            } else {
                //数据请求失败，抛出异常
                val message = result.message
                throw ResultException(
                    result.code.toString(), if (message.isNullOrEmpty()) {
                        HConstant.HTTP_REQUEST_DATA_ERROR
                    } else {
                        message
                    }
                )
            }
        } else {
            //没有返回码 直接解析后返回原对象
            return gson.fromJson(response, type)
        }
    }
}