package com.yzhg.study.http.bean

import com.google.gson.annotations.SerializedName
import com.yzhg.study.http.enums.DataState
import okhttp3.Response

/**
 * 包名：com.yzhg.study.http
 * 创建人：yzhg
 * 时间：2021/06/19 16:55
 * 描述：网络请求基类
 */
class ResponseBean<T> {
    @SerializedName("errorCode")
    var code: Int = 0

    @SerializedName("errorMsg")
    var message: String? = ""

    @SerializedName("data")
    var data: T? = null

    /**
     * 网络请求状态
     */
    var dataState: DataState? = null

    /**
     * 当前请求的页面，判断是否是上拉加载更多
     */
    var indexPage: Int = 1

    /**
     * 异常信息
     */
    var errorException: Exception? = null

}
