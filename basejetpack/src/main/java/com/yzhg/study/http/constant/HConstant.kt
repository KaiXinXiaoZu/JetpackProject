package com.yzhg.study.http.constant

/**
 * 包名：com.yzhg.study.http.constant
 * 创建人：yzhg
 * 时间：2021/06/19 17:17
 * 描述：
 */
class HConstant {

    companion object {

        //请求成功
        const val HTTP_REQUEST_SUCCESS_CODE = 0

        //请求失败字符串
        const val HTTP_REQUEST_DATA_ERROR = "数据错误"

        //配置URL_NAME
        const val HTTP_BASE_URL = "url_name"
    }
}