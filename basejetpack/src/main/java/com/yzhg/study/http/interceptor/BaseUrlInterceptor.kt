package com.yzhg.study.http.interceptor

import com.yzhg.study.http.config.HttpConfig
import com.yzhg.study.http.constant.HConstant
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 * 包名：com.yzhg.study.http.interceptor
 * 创建人：yzhg
 * 时间：2021/06/25 17:47
 * 描述：
 */
class BaseUrlInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        //获取到request
        val request: Request = chain.request()
        //从request中获取原有的HttpUrl
        val oldHttpUrl = request.url
        //获取request的创建者builder
        val builder = request.newBuilder()
        //从request中获取headers,通过给定的键url_name
        val headerValues: List<String> = request.headers(HConstant.HTTP_BASE_URL)
        //获取设置的域名
        val httpBaseUrl: MutableMap<String, String> = HttpConfig.getHttpBaseUrl()
        //判断是否双向配置
        if (headerValues.isNotEmpty() && httpBaseUrl.isNullOrEmpty()) {
            // 如果有这个header，先将配置的header删除，因此header仅用作app和okhttp之间使用
            builder.removeHeader(HConstant.HTTP_BASE_URL)
            // 匹配获得新的BaseUrl
            val headerValue = headerValues[0]
            //查找域名
            val newBaseUrlStr: String = httpBaseUrl[headerValue] ?: ""
            //创建HttpUrl
            val newBaseUrl: HttpUrl
            if (newBaseUrlStr.isEmpty()) {
                newBaseUrl = oldHttpUrl
            } else {
                newBaseUrl = newBaseUrlStr.toHttpUrlOrNull() ?: oldHttpUrl
            }
            // 重建新的HttpUrl，修改需要修改的url部分
            val newFullUrl: HttpUrl = oldHttpUrl.newBuilder()
                .scheme(newBaseUrl.scheme)
                .host(newBaseUrl.host)
                .port(newBaseUrl.port)
                .build()
            return chain.proceed(builder.url(newFullUrl).build())
        }
        return chain.proceed(request)
    }
}












