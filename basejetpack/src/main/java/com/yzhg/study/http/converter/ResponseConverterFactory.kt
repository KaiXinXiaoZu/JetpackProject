package com.yzhg.study.http.converter

import com.google.gson.Gson
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

/**
 * 包名：com.yzhg.study.http.converter
 * 创建人：yzhg
 * 时间：2021/06/19 17:20
 * 描述：自定义gson解析器
 */
class ResponseConverterFactory(private val gson: Gson) : Converter.Factory() {


    companion object {

        fun create(): ResponseConverterFactory {
            return create(Gson())
        }

        fun create(gson: Gson): ResponseConverterFactory {
            return ResponseConverterFactory(gson)
        }
    }

    //Converter<ResponseBody, T>
    override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit): Converter<ResponseBody, *> {
        return GsonResponseBodyConverter<Any>(gson, type)
    }

    override fun requestBodyConverter(
        type: Type, parameterAnnotations: Array<out Annotation>, methodAnnotations: Array<out Annotation>, retrofit: Retrofit
    ): Converter<*, RequestBody> {
        return GsonResponseBodyConverter(gson, type)
    }


}