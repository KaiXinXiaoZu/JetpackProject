package com.yzhg.study.http.param

import com.wsdz.tools.common.LogTools
import com.yzhg.study.http.config.HttpConfig
import com.yzhg.toollibrary.common.JsonUtil
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody


/*
 * 作者：yzhg
 * 时间：2022-03-02 10:15
 * 包名：shuini.manager.yiande.demo
 * 描述：
 */
class ParamMapTools {

    open class Builder {
        var parameterMap: MutableMap<String, Any> = mutableMapOf()

        /**
         * 批量配置多个域名
         * @param httpBaseUrl 键值对形式
         */
        open fun setParameter(parameterMap: MutableMap<String, Any>): Builder = apply {
            this.parameterMap.putAll(parameterMap)
        }

        /**
         * 配置多域名
         * @param urlHeader 名称 拼接在url_name:  后面
         * @param baseUrl 域名地址
         */
        open fun setParameter(key: String, value: Any): Builder = apply {
            this.parameterMap[key] = value
        }

        open fun requestBody(): RequestBody {
            val parseMapToJson = JsonUtil.parseMapToJson(parameterMap)
            LogTools.e("请求参数$parseMapToJson")
            return RequestBody.create("application/json; charset=utf-8".toMediaTypeOrNull(), parseMapToJson)
        }

        open fun buildMap(): MutableMap<String, Any> {
            return parameterMap
        }
    }
}