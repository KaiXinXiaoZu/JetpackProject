package com.yzhg.study.http.exception

/**
 * 包名：com.yzhg.study.http.exception
 * 创建人：yzhg
 * 时间：2021/06/19 16:57
 * 描述：自定义异常类
 */
class ResultException(errorCode: String, message: String) : RuntimeException(message) {


    private var errorCode: String = ""

    init {
        this.errorCode = errorCode
    }

    fun getErrorCode(): String {
        return errorCode
    }
}