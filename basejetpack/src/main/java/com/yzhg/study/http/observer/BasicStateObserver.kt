package com.yzhg.study.http.observer

import android.content.Context
import android.view.View
import androidx.lifecycle.Observer
import com.google.gson.JsonParseException
import com.kingja.loadsir.callback.Callback
import com.kingja.loadsir.core.LoadService
import com.youth.banner.util.LogUtils
import com.yzhg.study.R
import com.yzhg.study.application.BasicApplication
import com.yzhg.study.http.bean.ResponseBean
import com.yzhg.study.http.enums.DataState
import com.yzhg.study.http.exception.ResultException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONException
import retrofit2.HttpException
import java.io.InterruptedIOException
import java.net.ConnectException
import java.net.UnknownHostException
import java.text.ParseException

/**
 * 包名：com.yzhg.study.http.observer
 * 创建人：yzhg
 * 时间：2021/06/21 16:31
 * 描述：
 */
abstract class BasicStateObserver<T>() : Observer<ResponseBean<T>>,
    Callback.OnReloadListener {


    private var mLoadService: LoadService<Any>? = null



    override fun onChanged(responseBean: ResponseBean<T>) {
        when (responseBean?.dataState) {
            DataState.STATE_LOADING -> {
              //  ToastUtils.show("加载中....")
                LogUtils.d("加载中....")
            }
            DataState.STATE_SUCCESS -> {
                //数据请求成功
                onDataSuccess(responseBean.data!!)
            }
            DataState.STATE_EMPTY -> {
                //数据请求为空
                onDataEmpty()
            }
            DataState.STATE_FAILED, DataState.STATE_ERROR -> {
                //数据请求失败
                val errorException = responseBean.errorException
                setErrorException(errorException)
            }
            else -> {
                setErrorException(null)
            }
        }
        GlobalScope.launch {
            delay(500)
            mLoadService?.showWithConvertor(responseBean)
        }
        //   mLoadService?.showWithConvertor(responseBean)
    }

    private fun setErrorException(errorException: Exception?) {
        when (errorException) {
            is HttpException -> {
                //网络连接失败，请检查网络状况
                onDataError(getContext().getString(R.string.net_connect_error))
            }
            is ConnectException, is UnknownHostException -> {
                onDataError(getContext().getString(R.string.net_connect_timeOut))
            }
            is InterruptedIOException -> {
                onDataError(getContext().getString(R.string.net_connect_timeOut))
            }
            is JsonParseException, is JSONException, is ParseException -> {
                onDataError(getContext().getString(R.string.net_parse_error))
            }
            is ResultException -> {
                onDataError(
                    errorException.message ?: getContext().getString(R.string.net_unKnown_error)
                )
            }
            else -> {
                onDataError(getContext().getString(R.string.net_unKnown_error))
            }
        }
    }


    /**
     * 数据请求失败
     */
    open fun onDataError(string: String) {

    }

    /**
     * 数据请求为空
     */
    open fun onDataEmpty() {

    }

    abstract fun onDataSuccess(data: T)


    override fun onReload(v: View?) {

    }


    private fun getContext(): Context {
        return BasicApplication.getContext()
    }

}














