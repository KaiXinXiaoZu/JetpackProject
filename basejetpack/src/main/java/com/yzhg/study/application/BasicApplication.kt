package com.yzhg.study.application

import android.content.Context
import android.util.Log
import com.kingja.loadsir.core.LoadSir
import com.yzhg.study.http.config.HttpConfig
import com.yzhg.study.http.converter.ResponseConverterFactory
import com.yzhg.study.loadsir.callback.EmptyCallback
import com.yzhg.study.loadsir.callback.ErrorCallback
import com.yzhg.study.loadsir.callback.LoadingCallback
import com.yzhg.study.loadsir.callback.TimeoutCallback
import com.yzhg.toollibrary.BaseApplication

/**
 * 包名：com.yzhg.study.application
 * 创建人：yzhg
 * 时间：2021/06/21 09:24
 * 描述：
 */
open class BasicApplication : BaseApplication() {


    override fun onCreate() {
        super.onCreate()

        application = this.applicationContext


        //初始化网络框架
        initHttpConfig()

        //初始化LoadSir
        initLoadSir()
    }


    companion object {
        private lateinit var application: Context


        fun getContext(): Context {
            return application
        }
    }

    /**
     * 初始化状态组件
     */
    private fun initLoadSir() {
        LoadSir
            .beginBuilder()
            .addCallback(EmptyCallback())
            .addCallback(ErrorCallback())
            .addCallback(LoadingCallback())
            .addCallback(TimeoutCallback())
            .setDefaultCallback(LoadingCallback::class.java)
            .commit()
    }

    /**
     * 初始化网络请求  https://www.wanandroid.com/article/list/0/json
     */
    private fun initHttpConfig() {
        HttpConfig.Builder()
            .setBaseUrl("https://www.wanandroid.com/")
            .setDefaultTimeout(30)
            .setConverter(ResponseConverterFactory.create())
            //.addInterceptor(ResponseLogInterceptor())
            .isDeBug(true)
            .enableHttps(true)
            .build()
    }
}