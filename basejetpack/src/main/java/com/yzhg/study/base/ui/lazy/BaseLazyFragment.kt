package com.yzhg.study.base.ui.lazy

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.gyf.immersionbar.ImmersionBar
import com.gyf.immersionbar.components.ImmersionFragment
import com.wsdz.tools.common.LogTools
import com.yzhg.study.loading.LoadingDialog
import com.yzhg.study.tools.back.HandleBackInterface
import com.yzhg.study.tools.back.HandleBackUtil
import com.yzhg.toollibrary.dialog.BaseDialog

/**
 * 包名：com.yzhg.study.base.ui.fragment
 * 创建人：yzhg
 * 时间：2021/06/24 14:55
 * 描述：
 */
abstract class BaseLazyFragment : ImmersionFragment(), ILazyLoad, HandleBackInterface {

    /**
     * 缓存视图，如果视图已经创建，则不再初始化视图
     */
    private var rootView: View? = null

    /**
     * 是否开启懒加载，默认开启
     */
    private var lazyLoadEnable = true

    /**
     * 当前的状态
     */
    private var currentState = ILazyLoad.ON_ATTACH


    /**
     * 是否已经执行懒加载
     */
    private var hasLazyLoad = false


    /**
     * 当前Fragment是否对用户可见
     */
    private var isVisibleToUser = false

    /**
     * 是否调用了setUserVisibleHint方法。
     * 处理show+add+hide模式下，默认可见Fragment不调用onHiddenChanged方法，进而不执行懒加载方法的问题。
     */
    private var isCallUserVisibleHint = false


    /**
     * loading dialog弹框
     */
    private var loadingDialog: BaseDialog? = null

    /**
     * 没有提示语的弹框
     */
    protected open fun showDialogLoading() {
        showDialogLoading("")
    }


    /**
     * 显示弹框
     */
    protected open fun showDialogLoading(alertMessage: String) {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialog.newInstance(alertMessage)
                .setDimAmout(0f)
                .setOutCancel(true)

        }
        if (loadingDialog?.dialog?.isShowing == true) {
            //弹框已经显示了，不能重复显示
            LogTools.d("弹框已经显示了不能重复显示!!!")
        } else {
            //显示弹框
            loadingDialog?.showDialog(parentFragmentManager)
        }
    }

    /**
     * 关闭弹框
     */
    protected open fun dismissDialogLoading() {
        if (loadingDialog != null) {
            loadingDialog?.dismissDialog()
        }
    }


    /**
     * 是否开启懒加载，调用此方法建议在getLazyInitState()所返回的状态之前
     */
    protected fun enableLazyLoad(enable: Boolean) {
        this.lazyLoadEnable = enable
    }


    /**
     * 懒加载的调用时机
     */
    protected fun getLazyLoadtState() = ILazyLoad.ON_RESUME


    protected fun setCurrentState(state: Int) {
        this.currentState = state
    }


    /**
     * 是否是在setUserVisibleHint中调用
     */
    protected fun doLazyLoad(callInUserVisibleHint: Boolean) {
        if (!callInUserVisibleHint) {
            if (!isCallUserVisibleHint) isVisibleToUser = !isHidden
        }
        if (lazyLoadEnable && !hasLazyLoad && isVisibleToUser && currentState >= getLazyLoadtState()) {
            hasLazyLoad = true
            onLazyLoad()
        }
    }

    protected fun getRootView(): View? {
        return rootView
    }

    protected fun setRootView(view: View) {
        this.rootView = view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        setCurrentState(ILazyLoad.ON_ATTACH)
        doLazyLoad(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCurrentState(ILazyLoad.ON_CREATE)
        doLazyLoad(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        setCurrentState(ILazyLoad.ON_CREATE_VIEW)
        if (rootView != null) {
            return rootView
        }
      //  rootView = inflater.inflate(getLayoutId(), container, false)
        initialize(savedInstanceState)
        doLazyLoad(false)
        return getRootView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setCurrentState(ILazyLoad.ON_ACTIVITY_CREATED)
        doLazyLoad(false)
    }

    override fun onStart() {
        super.onStart()
        setCurrentState(ILazyLoad.ON_START)
        doLazyLoad(false)
    }

    override fun onResume() {
        super.onResume()
        setCurrentState(ILazyLoad.ON_RESUME)
        doLazyLoad(false)
    }

    /**
     * 主要针对add+show+hide模式下，Fragment的隐藏与显示调用的方法
     */
    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        isVisibleToUser = !hidden
        doLazyLoad(false)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        this.isVisibleToUser = isVisibleToUser
        isCallUserVisibleHint = true
        doLazyLoad(true)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        hasLazyLoad = false
        isVisibleToUser = false
        isCallUserVisibleHint = false
    }

    override fun initImmersionBar() {
        ImmersionBar.with(this)
            .statusBarDarkFont(false)
            .titleBarMarginTop(getRootView())
            .init()
    }

    override fun immersionBarEnabled(): Boolean {
        return false
    }

    /**
     * 懒加载，在view初始化之前调用
     */
    override fun onLazyAfterView() {
        super.onLazyAfterView()
    }

    /**
     * 懒加载，在view初始化之后调用
     */
    override fun onLazyBeforeView() {
        super.onLazyBeforeView()
    }

    /**
     * 当前Fragment对用户可见的时候调用
     */
    override fun onVisible() {
        super.onVisible()
    }

    /**
     * 当前Fragment不可见的时候调用
     */
    override fun onInvisible() {
        super.onInvisible()
    }

  //  @LayoutRes
  //  protected abstract fun getLayoutId(): Int


    /**
     *  初始化操作
     */
    protected abstract fun initialize(savedInstanceState: Bundle?)


    override fun onLazyLoad() {

    }


    /**
     * Fragment中处理  返回事件
     *
     * 用法： 子类重写 onBackPressed 返回true则表示被fragment处理
     */
    override fun onBackPressed(): Boolean {
        return HandleBackUtil.handleBackPress(this)
    }
}