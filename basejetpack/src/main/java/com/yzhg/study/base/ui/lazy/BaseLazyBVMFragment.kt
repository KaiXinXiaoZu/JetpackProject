package com.yzhg.study.base.ui.lazy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.yzhg.study.base.ui.fragment.BaseBindingFragment
import com.yzhg.study.base.viewmodel.BaseViewModel
import com.yzhg.study.base.viewmodel.StateLiveData

/**
 * 包名：com.yzhg.study.base.ui.lazy
 * 创建人：yzhg
 * 时间：2021/06/24 15:01
 * 描述：懒加载使用Fragment  正常Fragment也可以使用  enableLazyLoad中设置是否开启懒加载
 */
abstract class BaseLazyBVMFragment<B : ViewDataBinding, VM : BaseViewModel> : BaseLazyBindingFragment<B>() {

    protected lateinit var viewModel: VM
        private set


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setCurrentState(ILazyLoad.ON_CREATE_VIEW)
        if (getRootView() != null) {
            return getRootView()
        }
        injectDataBinding(inflater, container)
        injectViewModel()
        initialize(savedInstanceState)
        doLazyLoad(false)
        return getRootView()
    }


    protected fun injectViewModel() {
        val vm = createViewModel()
        viewModel = ViewModelProvider(this, BaseViewModel.createViewModelFactory(vm)).get(vm::class.java)
        viewModel.application = requireActivity().application
        viewModel.fragmentManager = setFragmentManager()
        lifecycle.addObserver(viewModel)
    }
    /**
     * 设置Fragment
     * 默认 parentFragmentManager 如果是Fragment嵌套Fragment 则需要制定childFragmentManager
     */
    protected open fun setFragmentManager(): FragmentManager {
        return parentFragmentManager
    }


    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(viewModel)
    }

    protected abstract fun createViewModel(): VM;
}