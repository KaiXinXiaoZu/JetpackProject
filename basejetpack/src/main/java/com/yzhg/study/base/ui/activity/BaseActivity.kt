package com.yzhg.study.base.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import com.gyf.immersionbar.ImmersionBar
import com.wsdz.tools.common.LogTools
import com.yzhg.study.R
import com.yzhg.study.loading.LoadingDialog
import com.yzhg.study.tools.back.HandleBackUtil
import com.yzhg.study.tools.language.LanguageTools
import com.yzhg.toollibrary.common.ActivitysManager
import com.yzhg.toollibrary.dialog.BaseDialog
import java.lang.RuntimeException

/**
 * 包名：com.yzhg.study.base.ui.activity
 * 创建人：yzhg
 * 时间：2021/06/30 15:34
 * 描述：
 */
open class BaseActivity : AppCompatActivity() {
    open val TAG = javaClass.simpleName + "=====>"


    /**activity管理类*/
    private lateinit var activitysManager: ActivitysManager
    private var isRecreate = false
    protected var mCurrentIsNightMod = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isRecreate = false
        //将Activity压栈操作
        activitysManager = ActivitysManager.getInstance()
        //将当前Activity进行压栈
        activitysManager.addActivity(this)
        mCurrentIsNightMod = isNightMode(this)

    }

    override fun attachBaseContext(newBase: Context) {
        val language = LanguageTools.getAppLanguage(newBase)
        super.attachBaseContext(LanguageTools.attachBaseContext(newBase, language))
    }

    /**
     * 设置状态栏  透明充满屏幕。默认充满屏幕
     */
    protected open fun isBarStatusFull(): Boolean = true

    open fun initImmersionBar(contentView: View) {
        if (setStatusBarColor() == 0) {
            ImmersionBar.with(this)
                .statusBarDarkFont(true)
                .init()
        } else {
            ImmersionBar.with(this)
                .statusBarColor(setStatusBarColor())
                .statusBarDarkFont(true) //状态栏字体是深色，不写默认为亮色
                .titleBarMarginTop(contentView) //解决状态栏和布局重叠问题，任选其一
                .init() //必须调用方可应用以上所配置的参数
        }
    }

    /**
     * 设置状态栏颜色
     */
    protected open fun setStatusBarColor(): Int {
        return R.color.color_FFFFFF
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (mCurrentIsNightMod != isNightMode(applicationContext)) {
            Log.e(TAG, "onConfigurationChanged: ----------->" + "uI模式变化了")
            isRecreate = true
            recreate()
        }
    }

    open fun isNightMode(context: Context): Boolean {
        try {
            return context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
        } catch (ex: java.lang.Exception) {
        }
        return false
    }

    /**
     * loading dialog弹框
     */
    private var loadingDialog: BaseDialog? = null

    /**
     * 没有提示语的弹框
     */
    protected open fun showDialogLoading() {
        showDialogLoading("")
    }


    /**
     * 显示弹框
     */
    protected open fun showDialogLoading(alertMessage: String) {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialog.newInstance(alertMessage)
                .setDimAmout(0f)
                .setOutCancel(true)

        }
        if (loadingDialog?.dialog?.isShowing == true) {
            //弹框已经显示了，不能重复显示
            LogTools.d("弹框已经显示了不能重复显示!!!")
        } else {
            //显示弹框
            loadingDialog?.showDialog(supportFragmentManager)
        }
    }

    /**
     * 关闭弹框
     */
    protected open fun dismissDialogLoading() {
        if (loadingDialog != null) {
            loadingDialog?.dismissDialog()
        }
    }


    /**
     * 开启页面跳转
     */
    protected fun startActivity(clazz: Class<*>) {
        val intent = Intent(this, clazz)
        startActivity(intent)
    }

    /**开启页面跳转*/
    protected fun startActivity(clazz: Class<*>, isFinish: Boolean) {
        val intent = Intent(this, clazz)
        startActivity(intent)
        if (isFinish) {
            finish()
        }
    }

    /**Activity页面跳转 带参数*/
    protected fun startBundleActivity(clazz: Class<*>, bundle: Bundle, isFinish: Boolean) {
        val intent = Intent(this, clazz)
        //传递参数
        intent.putExtras(bundle)
        startActivity(intent)
        if (isFinish) {
            finish()
        }
    }


    private var myActivityLauncher: ActivityResultLauncher<Intent>? = null

    /**
     * 注册带有回调的Activity
     */
    fun startForActivityResult(@NonNull callback: ActivityResultCallback<ActivityResult>) {
        myActivityLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult(), callback)
    }


    /**
     * 启动带参数跳转
     */
    protected fun startBundleActivityForResult(
        clazz: Class<*>,
        bundle: Bundle,
        isFinish: Boolean,
    ) {
        if (myActivityLauncher != null) {
            val intent = Intent(this, clazz).apply {
                putExtras(bundle)
            }
            myActivityLauncher?.launch(intent)
            if (isFinish) {
                finish()
            }
        } else {
            throw RuntimeException("请先执行startForActivityResult方法进行注册")
        }
    }

    /**
     * ActivityForResult 回调
     */
    fun closeActivityForResultIntent(bundle: Bundle) {
        val intent = Intent()
        intent.putExtras(bundle)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }


    override fun onDestroy() {
        super.onDestroy()
        /**将当前Activity弹出栈*/
        if (isRecreate) {
            activitysManager.removeActivity(this)
        }
    }


    override fun onBackPressed() {
        if (!HandleBackUtil.handleBackPress(this)) {
            super.onBackPressed()
        }
    }
}













