package com.yzhg.study.base.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.util.Log
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wsdz.tools.common.LogTools
import com.yzhg.study.base.lifecycle.ViewModelLifecycle
import com.yzhg.study.http.HttpApi
import com.yzhg.study.http.bean.ResponseBean
import com.yzhg.study.http.constant.HConstant
import com.yzhg.study.http.enums.DataState
import com.yzhg.study.http.exception.ResultException
import com.yzhg.study.http.manager.HttpManager
import com.yzhg.study.loading.LoadingDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response

/**
 * 包名：com.yzhg.study.base.viewmodel
 * 创建人：yzhg
 * 时间：2021/06/19 18:20
 * 描述：基类viewModel  继承ViewModelLifecycle 绑定生命周期
 *
 *
 * https://blog.csdn.net/liyi1009365545/article/details/107139177?spm=1001.2014.3001.5501
 */
abstract class BaseViewModel : ViewModel(), ViewModelLifecycle {


    companion object {
        @JvmStatic
        fun <T : BaseViewModel> createViewModelFactory(viewModel: T): ViewModelProvider.Factory {
            return ViewModelFactory(viewModel)
        }
    }

    @SuppressLint("StaticFieldLeak")
    lateinit var application: Application

    /**
     * fragmentManager
     */
    lateinit var fragmentManager: FragmentManager

    //创建api
    var httpApi2 = createHttpApi<HttpApi>()

    inline fun <reified T> createHttpApi(): T {
        return HttpManager().create<T>()
    }


    var loadingDialog: LoadingDialog? = null


    /**
     * 处理网络请求状态
     */
    suspend fun <T : Any> executeRequest(
        block: suspend () -> ResponseBean<T>,
        stateLiveData: StateLiveData<T>,
        vararg isShowDialog: Boolean,
    ) {
        LogTools.d("开始加载网络请求，开始弹框。。。。。。。。。。。。")
        if (isShowDialog.isNotEmpty() && isShowDialog[0]) {
            viewModelScope.launch(Dispatchers.Main) {
                LogTools.d("显示弹框。。。。。。。。。。。。")
                if (loadingDialog == null) {
                    loadingDialog = LoadingDialog.newInstance("")
                    loadingDialog?.showDialog(fragmentManager)
                }
            }
        }
        var responseBean = ResponseBean<T>()
        try {
            //设置当前状态为加载中
            responseBean.dataState = DataState.STATE_LOADING
            //开始请求网络
            val invoke = block.invoke()
            //将结果复制
            responseBean = invoke
            if (responseBean.code == HConstant.HTTP_REQUEST_SUCCESS_CODE) {
                //有拦截器  走到这里肯定为成功的请求方案。在这里再次验证。其实可以忽略
                if (responseBean.data == null || "" == responseBean.data || responseBean.data is List<*> && (responseBean.data as List<*>).isEmpty()) {
                    //data数据结构  只允许为对象或者集合  和空字符（如果为空字符拦截器中已经强制转为null.这里也可以不用验证）
                    responseBean.dataState = DataState.STATE_EMPTY
                } else {
                    //数据请求成功
                    responseBean.dataState = DataState.STATE_SUCCESS
                }
            } else {
                //服务器返回 code失败
                responseBean.dataState = DataState.STATE_FAILED
            }
        } catch (e: Exception) {
            //   Log.i("executeRequest", "executeRequest: 捕捉到异常")
            //设置异常信息
            responseBean.errorException = e
            when (e) {
                is ResultException -> {
                    //自定义异常，返回服务器失败
                    responseBean.dataState = DataState.STATE_FAILED
                }

                else -> {
                    //捕捉到异常
                    responseBean.dataState = DataState.STATE_ERROR
                }
            }
        } finally {
            LogTools.d("加载网络请求，完成隐藏。。。。。。。。。。。。")
            //将数据设置给LiveData
            stateLiveData.postValue(responseBean)
            if (isShowDialog.isNotEmpty() && isShowDialog[0]) {
                viewModelScope.launch(Dispatchers.Main) {
                    LogTools.d("隐藏。。。。。。。。。。。。")
                    loadingDialog?.dismissDialog()
                    loadingDialog = null
                }
            }
        }
    }


    /**
     * 处理网络请求状态
     */
    suspend fun <T : Any> executeRequest(block: suspend () -> ResponseBean<T>, stateLiveData: StateLiveData<T>, indexPage: Int) {
        var responseBean = ResponseBean<T>()
        try {
            //设置当前状态为加载中
            responseBean.dataState = DataState.STATE_LOADING
            //开始请求网络
            val invoke = block.invoke()
            //赋值页码
            responseBean.indexPage = indexPage
            //将结果复制
            responseBean = invoke
            if (responseBean.code == HConstant.HTTP_REQUEST_SUCCESS_CODE) {
                //有拦截器  走到这里肯定为成功的请求方案。在这里再次验证。其实可以忽略
                if (responseBean.data == null || "" == responseBean.data || responseBean.data is List<*> && (responseBean.data as List<*>).isEmpty()) {
                    //data数据结构  只允许为对象或者集合  和空字符（如果为空字符拦截器中已经强制转为null.这里也可以不用验证）
                    responseBean.dataState = DataState.STATE_EMPTY
                } else {
                    //数据请求成功
                    responseBean.dataState = DataState.STATE_SUCCESS
                }
            } else {
                //服务器返回 code失败
                responseBean.dataState = DataState.STATE_FAILED
            }
        } catch (e: Exception) {
            Log.i("executeRequest", "executeRequest: 捕捉到异常" + e.message.toString())
            //设置异常信息
            responseBean.errorException = e
            when (e) {
                is ResultException -> {
                    Log.i("executeRequest", "eResultException")
                    //自定义异常，返回服务器失败
                    responseBean.dataState = DataState.STATE_FAILED
                }

                else -> {
                    Log.i("executeRequest", "不知道异常")
                    responseBean.dataState = DataState.STATE_ERROR
                }
            }
        } finally {
            //将数据设置给LiveData
            stateLiveData.postValue(responseBean)
        }
    }


    /**
     * 处理网络请求状态
     */
    suspend fun <T> executeRequest2(
        block: suspend () -> Response<ResponseBody>,
        stateLiveData: StateLiveData<T>,
        indexPage: Int,
    ) {
        val responseBean: ResponseBean<T> = ResponseBean<T>()
        try {
            //设置当前状态为加载中
            responseBean.dataState = DataState.STATE_LOADING
            //开始请求网络
            val invoke = block.invoke()
            //赋值页码
            responseBean.indexPage = indexPage
            val requestCode = invoke.code()

            //判断上层请求是否成功，这里其实不用写
            if (requestCode != 200) {
                //数据请求成功
                responseBean.dataState = DataState.STATE_FAILED
                stateLiveData.postValue(responseBean)
                return
            }
            //请求到的json数据
            val requestBody: String = invoke.body()!!.string()

            /****************************************自动解析失效通过jsonObject配合解析进行重新组装数据******************************************/

            //通过JSONObject 进行解析外层
            val jsonObject = JSONObject(requestBody)
            //解析到code
            val errorCode = jsonObject.getInt("errorCode")
            responseBean.code = errorCode

            //解析message
            val errorMsg = jsonObject.getString("errorMsg")
            responseBean.message = errorMsg;

            //解析内层的data
            val dataResult = jsonObject.getString("data")
            val typeT = object : TypeToken<T>() {}.type
            val parseJsonToBean: T? = Gson().fromJson<T>(dataResult, typeT)
          //
          //  val gson = Gson()
          // var t = gson.fromJson<T>(dataResult, cls)
            responseBean.data = parseJsonToBean
            /****************************************自动解析失效通过jsonObject配合解析进行重新组装数据*****************************************/
            //将结果复制
            // responseBean = invoke
            if (responseBean.code == HConstant.HTTP_REQUEST_SUCCESS_CODE) {
                //有拦截器  走到这里肯定为成功的请求方案。在这里再次验证。其实可以忽略
                if (responseBean.data == null || "" == responseBean.data || responseBean.data is List<*> && (responseBean.data as List<*>).isEmpty()) {
                    //data数据结构  只允许为对象或者集合  和空字符（如果为空字符拦截器中已经强制转为null.这里也可以不用验证）
                    responseBean.dataState = DataState.STATE_EMPTY
                } else {
                    //数据请求成功
                    responseBean.dataState = DataState.STATE_SUCCESS
                }
            } else {
                //服务器返回 code失败
                responseBean.dataState = DataState.STATE_FAILED
            }
        } catch (e: Exception) {
            Log.i("executeRequest", "executeRequest: 捕捉到异常" + e.message.toString())
            //设置异常信息
            responseBean.errorException = e
            when (e) {
                is ResultException -> {
                    Log.i("executeRequest", "eResultException")
                    //自定义异常，返回服务器失败
                    responseBean.dataState = DataState.STATE_FAILED
                }

                else -> {
                    Log.i("executeRequest", "不知道异常")
                    responseBean.dataState = DataState.STATE_ERROR
                }
            }
        } finally {
            //将数据设置给LiveData
            stateLiveData.postValue(responseBean)
        }
    }


    private lateinit var lifcycleOwner: LifecycleOwner

    override fun onAny(owner: LifecycleOwner, event: Lifecycle.Event) {
        this.lifcycleOwner = owner
    }

    override fun onCreate() {
    }

    override fun onStart() {
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onStop() {
    }

    override fun onDestroy() {

    }
}