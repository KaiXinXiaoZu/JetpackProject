package com.yzhg.study.base.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * 包名：com.yzhg.study.base.viewmodel
 * 创建人：yzhg
 * 时间：2021/06/24 15:04
 * 描述：
 */
class ViewModelFactory (val viewModel: BaseViewModel) : ViewModelProvider.Factory {

   /* override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return viewModel as T
    }*/

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return viewModel as T
    }
}