package com.yzhg.study.base.viewmodel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.yzhg.study.http.bean.ResponseBean
import java.util.concurrent.atomic.AtomicBoolean

/**
 * 包名：com.yzhg.study.base.viewmodel
 * 创建人：yzhg
 * 时间：2021/06/21 11:47
 * 描述：用于将请求状态分发给UI,
 */
class StateLiveData<T> : MutableLiveData<ResponseBean<T>>() {


}