package com.yzhg.study.base.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType

/**
 * 包名：com.yzhg.study.base.ui.fragment
 * 创建人：yzhg
 * 时间：2021/06/20 18:54
 * 描述：Fragment 基类
 */
abstract class BaseBindingFragment<B : ViewDataBinding> : BaseFragment() {

    /**
     * 缓存视图
     */
    protected var rootView: View? = null


    protected lateinit var binding: B
        private set


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView != null) {
            return rootView
        }
      //  rootView = inflater.inflate(getLayoutId(), container, false)
        injectBinding(inflater, container)
        initialize(savedInstanceState)
        return binding.root
    }

    override fun setContentView(): View? = rootView

    protected open fun injectBinding(inflater: LayoutInflater, container: ViewGroup?) {
      //  binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)

        val viewBindingClazz: Class<B> = getViewBindingClazz() ?: return
        val method: Method = viewBindingClazz.getMethod("inflate", LayoutInflater::class.java)
        binding = method.invoke(null, layoutInflater) as B
        binding.lifecycleOwner = this
        rootView = binding.root
    }

    private fun getViewBindingClazz(): Class<B>? {
        val parameterizedType = this.javaClass.genericSuperclass as ParameterizedType ?: return null
        return parameterizedType.actualTypeArguments[0] as Class<B>?;
    }


   // @LayoutRes
  //  protected abstract fun getLayoutId(): Int

    protected abstract fun initialize(savedInstanceState: Bundle?)

    override fun onDestroyView() {
        binding.unbind()
        super.onDestroyView()
    }

}














