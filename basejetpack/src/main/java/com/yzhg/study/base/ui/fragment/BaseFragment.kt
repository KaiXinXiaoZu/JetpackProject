package com.yzhg.study.base.ui.fragment

import android.view.View
import com.gyf.immersionbar.ImmersionBar
import com.gyf.immersionbar.components.SimpleImmersionFragment
import com.wsdz.tools.common.LogTools
import com.yzhg.study.R
import com.yzhg.study.loading.LoadingDialog
import com.yzhg.study.tools.back.HandleBackInterface
import com.yzhg.study.tools.back.HandleBackUtil
import com.yzhg.toollibrary.dialog.BaseDialog


/**
 * 包名：com.yzhg.study.base.ui.fragment
 * 创建人：yzhg
 * 时间：2021/07/01 10:27
 * 描述：15737165795
 */
abstract class BaseFragment : SimpleImmersionFragment(), HandleBackInterface {

    /**
     * loading dialog弹框
     */
    private var loadingDialog: BaseDialog? = null

    override fun initImmersionBar() {
        ImmersionBar.with(this)
            .statusBarDarkFont(true)
            //.titleBarMarginTop(setContentView()) //解决状态栏和布局重叠问题，任选其一

            .statusBarColor(setStatusBarColor())
            .init()

        /*if (setStatusBarColor() != 0) {
            ImmersionBar.with(this)
                .statusBarDarkFont(true)
                .titleBarMarginTop(setContentView()) //解决状态栏和布局重叠问题，任选其一
                .statusBarColor(setStatusBarColor())
                .init()
        } else {
            ImmersionBar.with(this)
                .statusBarDarkFont(true)
                .init()
        }*/
    }


    /**
     * 没有提示语的弹框
     */
    protected open fun showDialogLoading() {
        showDialogLoading("")
    }


    /**
     * 显示弹框
     */
    protected open fun showDialogLoading(alertMessage: String) {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialog.newInstance(alertMessage)
                .setDimAmout(0f)
                .setOutCancel(true)

        }
        if (loadingDialog?.dialog?.isShowing == true) {
            //弹框已经显示了，不能重复显示
            LogTools.d("弹框已经显示了不能重复显示!!!")
        } else {
            //显示弹框
            loadingDialog?.showDialog(parentFragmentManager)
        }
    }

    /**
     * 关闭弹框
     */
    protected open fun dismissDialogLoading() {
        if (loadingDialog != null) {
            loadingDialog?.dismissDialog()
        }
    }



    abstract fun setContentView(): View?

    private fun setStatusBarColor(): Int {
        return R.color.color_FFFFFF
    }


    override fun immersionBarEnabled(): Boolean {
        return false
    }


    /**
     * Fragment中处理  返回事件
     */
    override fun onBackPressed(): Boolean {
        return HandleBackUtil.handleBackPress(this)
    }
}