package com.yzhg.study.base.ui.activity

import android.os.Bundle
import android.view.LayoutInflater
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType


/**
 * 包名：com.yzhg.study.base.ui.activity
 * 创建人：yzhg
 * 时间：2021/06/20 15:22
 * 描述：
 */
abstract class BaseBindingActivity<B : ViewDataBinding> : BaseActivity() {

    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectBinding()


        initImmersionBar(binding.root)

        //初始化方法
        initView()
    }

    /**
     * 绑定布局
     */
    protected open fun injectBinding() {
        // binding = DataBindingUtil.setContentView(this, getLayoutId())
       // binding = DataBindingUtil.setContentView(this, getLayoutId())
        //setContentView(binding.root)
        val viewBindingClazz: Class<B> = getViewBindingClazz() ?: return
        val method: Method = viewBindingClazz.getMethod("inflate", LayoutInflater::class.java)
        binding = method.invoke(null, layoutInflater) as B
        setContentView(binding.root)
        binding.lifecycleOwner = this
    }

    private fun getViewBindingClazz(): Class<B>? {
        val parameterizedType = this.javaClass.genericSuperclass as ParameterizedType ?: return null
        return parameterizedType.actualTypeArguments[0] as Class<B>?;
    }

    /**
     * 指定资源类型
     */
  //  @LayoutRes
   // protected abstract fun getLayoutId(): Int

    /**
     * 初始化方法
     */
    abstract fun initView()


    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
    }


}