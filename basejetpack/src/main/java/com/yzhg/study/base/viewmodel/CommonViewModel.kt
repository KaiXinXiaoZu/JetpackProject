package com.yzhg.study.base.viewmodel

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * 包名：com.yzhg.examples.ui.activity.system_detail.fragment
 * 创建人：yzhg
 * 时间：2021/06/24 17:36
 * 描述：
 */
open class CommonViewModel : BaseViewModel() {


    public fun <T> getLiveData(): StateLiveData<T> {
        return StateLiveData();
    }

    //获取网络数据
    open fun <T> get(pageId: Int, url: String) {
        viewModelScope.launch(Dispatchers.IO) {
            executeRequest2<T>({ httpApi2.get(url) }, getLiveData<T>(), pageId)
        }
    }


    open fun <T> post(pageId: Int, url: String, liveData: StateLiveData<T>) {
        viewModelScope.launch(Dispatchers.IO) {
            executeRequest2<T>({ httpApi2.get(url) }, liveData, pageId)
        }
    }


}