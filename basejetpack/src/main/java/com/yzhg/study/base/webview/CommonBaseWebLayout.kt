package com.yzhg.study.base.webview

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.just.agentweb.IWebLayout
import com.yzhg.study.R
import com.yzhg.study.databinding.WebviewBaseLayoutBinding

/**
 * 包名：com.yzhg.study.base.webview
 * 创建人：yzhg
 * 时间：2021/07/02 10:47
 * 描述：
 */
class CommonBaseWebLayout(activity: Activity) : IWebLayout<WebView, LinearLayout> {


    private var webviewBaseLayoutBinding: WebviewBaseLayoutBinding? = null

    init {
        webviewBaseLayoutBinding =
            DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.webview_base_layout, null, false)
    }

    override fun getLayout(): LinearLayout {
        return webviewBaseLayoutBinding?.llBaseLayout!!
    }

    override fun getWebView(): WebView? {
        return webviewBaseLayoutBinding?.webBaseLayout
    }


}