package com.yzhg.study.base.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.yzhg.study.base.viewmodel.BaseViewModel
import com.yzhg.study.base.viewmodel.StateLiveData

/**
 * 包名：com.yzhg.study.base.ui.fragment
 * 创建人：yzhg
 * 时间：2021/06/20 19:00
 * 描述：
 */
abstract class BaseBVMFragment<B : ViewDataBinding, VM : BaseViewModel
        > : BaseBindingFragment<B>() {


    protected lateinit var viewModel: VM
        private set


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //   rootView = super.onCreateView(inflater, container, savedInstanceState)
        if (rootView != null) {
            return rootView
        }
        injectBinding(inflater, container)
        injectViewModel()
        initialize(savedInstanceState)
        return rootView
    }

    private fun injectViewModel() {
        val vm = createViewModel()
        viewModel = ViewModelProvider(this, BaseViewModel.createViewModelFactory(vm))[vm::class.java]
        viewModel.application = requireActivity().application
        viewModel.fragmentManager = setFragmentManager()
        lifecycle.addObserver(viewModel)
    }


    /**
     * 设置Fragment
     * 默认 parentFragmentManager 如果是Fragment嵌套Fragment 则需要制定childFragmentManager
     */
    protected open fun setFragmentManager(): FragmentManager {
        return parentFragmentManager
    }


    protected abstract fun createViewModel(): VM


    override fun onDestroyView() {
        lifecycle.removeObserver(viewModel)
        super.onDestroyView()
    }

}