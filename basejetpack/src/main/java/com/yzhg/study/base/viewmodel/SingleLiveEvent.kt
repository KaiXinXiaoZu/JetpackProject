package com.yzhg.study.base.viewmodel

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.util.concurrent.atomic.AtomicBoolean

/**
 * 包名：com.yzhg.study.base.viewmodel
 * 创建人：yzhg
 * 时间：2021/06/23 21:46
 * 描述：
 */
open class SingleLiveEvent<T> : MutableLiveData<T>() {


    private var mPeding: AtomicBoolean = AtomicBoolean(false)


    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        super.observe(owner, object : Observer<T>{
            override fun onChanged(t: T) {
               if(mPeding.compareAndSet(true,false)){
                   observer.onChanged(t)
               }
            }
        })
    }

    override fun setValue(value: T?) {
        mPeding.set(true)
        super.setValue(value)
    }

    @MainThread
    public fun call(){
        value = null
    }
}