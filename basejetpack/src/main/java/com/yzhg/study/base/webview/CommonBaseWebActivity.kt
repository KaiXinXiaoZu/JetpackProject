package com.yzhg.study.base.webview

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.KeyEvent
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.widget.LinearLayout
import com.just.agentweb.AgentWeb
import com.just.agentweb.DefaultWebClient
import com.just.agentweb.WebChromeClient
import com.yzhg.study.R
import com.yzhg.study.base.ui.activity.BaseBindingActivity
import com.yzhg.study.databinding.WebviewCommonActivityBinding

/**
 * 包名：com.yzhg.study.base.webview
 * 创建人：yzhg
 * 时间：2021/07/02 10:20
 * 描述：普通的WebView
 */
class CommonBaseWebActivity : BaseBindingActivity<WebviewCommonActivityBinding>() {

   // override fun getLayoutId(): Int = R.layout.webview_common_activity

    protected var mAgentWeb: AgentWeb? = null

    companion object {

        //Html   地址
        const val BUNDLE_HTML_PATH = "html_path"

        //标题
        const val BUNDLE_URL_TITLE = "url_title"

        fun newInstance(context: Context, htmlUrl: String, urlTitle: String) {
            val intent = Intent(context, CommonBaseWebActivity::class.java)
            val bundle = Bundle()
            bundle.putString(BUNDLE_HTML_PATH, htmlUrl)
            bundle.putString(BUNDLE_URL_TITLE, urlTitle)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
    }

    override fun isBarStatusFull(): Boolean = false

    //请求地址
    private var htmlPathUrl = ""

    //网页标题
    private var htmlTitle = ""


    override fun initView() {
        binding.ibBack.setOnClickListener { finish() }
        //获取传值
        val bundle: Bundle? = intent.extras
        //获取请求地址
        htmlPathUrl = bundle?.getString(BUNDLE_HTML_PATH) ?: ""
        //获取标题
        htmlTitle = bundle?.getString(BUNDLE_URL_TITLE) ?: "浏览网页"

        mAgentWeb = AgentWeb.with(this)
            .setAgentWebParent(binding.llWebBaseLayout, LinearLayout.LayoutParams(-1, -1))
            .useDefaultIndicator()
            .setWebChromeClient(mWebChromeClient)
            .setWebViewClient(mWebViewClient)
            .setMainFrameErrorView(R.layout.load_error_layout, -1)
            .setSecurityType(AgentWeb.SecurityType.STRICT_CHECK)
            .setWebLayout(CommonBaseWebLayout(this))
            .setOpenOtherPageWays(DefaultWebClient.OpenOtherPageWays.ASK)//打开其他应用时，弹窗咨询用户是否前往其他应用
            .interceptUnkownUrl() //拦截找不到相关页面的Scheme
            .createAgentWeb()
            .ready()
            .go(htmlPathUrl)
    }

    val mWebChromeClient = object : WebChromeClient() {
        @SuppressLint("SetTextI18n")
        override fun onReceivedTitle(view: WebView?, title: String?) {
            if (title?.isEmpty() == true) {
                binding.tvWebTitle.text = htmlTitle
            } else {
                if (title!!.length > 8) {
                    binding.tvWebTitle.text = title.substring(0, 8) + "..."
                } else {
                    binding.tvWebTitle.text = title
                }
            }
        }
    }

    val mWebViewClient = object : com.just.agentweb.WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            return super.shouldOverrideUrlLoading(view, request)
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {

        }
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        mAgentWeb.let {
            if (mAgentWeb?.handleKeyEvent(keyCode, event) == true) {
                return true
            }
        }
        return super.onKeyDown(keyCode, event)
    }


    override fun onPause() {
        mAgentWeb?.webLifeCycle?.onPause()
        super.onPause()
    }

    override fun onResume() {
        mAgentWeb.let {
            it?.webLifeCycle?.onResume()
        }
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        mAgentWeb.let {
            it?.webLifeCycle?.onDestroy()
        }
    }

}


















