package com.yzhg.study.base.ui.lazy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType

/**
 * 包名：com.yzhg.study.base.ui.lazy
 * 创建人：yzhg
 * 时间：2021/06/24 14:59
 * 描述：
 */
abstract class BaseLazyBindingFragment<B : ViewDataBinding> : BaseLazyFragment() {

    protected lateinit var binding: B
        private set


    override fun onCreateView(  inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setCurrentState(ILazyLoad.ON_CREATE_VIEW)
        if (getRootView() != null) {
            return getRootView()
        }
        injectDataBinding(inflater, container)
        initialize(savedInstanceState)
        doLazyLoad(false)
        return binding.root
    }

    protected open fun injectDataBinding(inflater: LayoutInflater, container: ViewGroup?) {
       // binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        val viewBindingClazz: Class<B> = getViewBindingClazz() ?: return
        val method: Method = viewBindingClazz.getMethod("inflate", LayoutInflater::class.java)
        binding = method.invoke(null, layoutInflater) as B
        binding.lifecycleOwner = this
        setRootView(binding.root)
    }


    private fun getViewBindingClazz(): Class<B>? {
        val parameterizedType = this.javaClass.genericSuperclass as ParameterizedType ?: return null
        return parameterizedType.actualTypeArguments[0] as Class<B>?;
    }



    override fun onDestroyView() {
        binding.unbind()
        super.onDestroyView()
    }
}