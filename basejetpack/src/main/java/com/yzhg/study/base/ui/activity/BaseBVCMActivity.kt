package com.yzhg.study.base.ui.activity

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.yzhg.study.base.viewmodel.BaseViewModel
import com.yzhg.study.base.viewmodel.CommonViewModel

/**
 * 包名：com.yzhg.study.base.ui.activity
 * 创建人：yzhg
 * 时间：2021/06/20 18:08
 * 描述：基类  自动绑定了 CommonViewModel  不需要重新实现viewModel
 */
abstract class BaseBVCMActivity<B : ViewDataBinding>() : BaseBindingActivity<B>() {


    protected lateinit var viewModel: CommonViewModel

    /**
     * 注入viewModel
     */
    override fun injectBinding() {
        super.injectBinding()
        //创建viewModel
        viewModel = ViewModelProvider(this, BaseViewModel.createViewModelFactory(CommonViewModel()))[CommonViewModel::class.java]
        //设置全局生命周期
        viewModel.application = application
        viewModel.fragmentManager = supportFragmentManager
        //绑定生命周期
        lifecycle.addObserver(viewModel)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
        //移除监听
        lifecycle.removeObserver(viewModel)
    }
}






















