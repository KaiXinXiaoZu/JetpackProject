package com.yzhg.nfc;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcAntennaInfo;
import android.nfc.NfcEvent;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.nfc.tech.TagTechnology;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class StartNFCActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback {

    private NfcAdapter nfcAdapter = null;
    private PendingIntent pendingIntent = null;
    private IntentFilter[] intentFilter = null;
    private String[][] techList = null;
    private TextView textView = null;

    @SuppressLint("MissingInflatedId")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc_start_layout);
        //获取控件
        textView = findViewById(R.id.butStartNFC);
        //获取NFC适配器
        nfcAdapter = NfcAdapter.getDefaultAdapter(StartNFCActivity.this);
        //判断设备是否有NFC功能
        if (nfcAdapter == null) {
            textView.setText("设备无NFC功能");
        }
        //判断设备是否开启NFC功能
        else if (!nfcAdapter.isEnabled()) {
            textView.setText("设备未开启NFC功能");
            //跳转至NFC设置界面
            Intent intent = new Intent(Settings.ACTION_NFC_SETTINGS);
            startActivity(intent);
        } else {
            textView.setText("设备已开启NFC功能");
        }

        //准备NFC感应启动参数
        //用于触发的待定意图
        Intent intent = new Intent(StartNFCActivity.this, StartNFCActivity.class);
        //intent=intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        pendingIntent = PendingIntent.getActivity(StartNFCActivity.this, 12345, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        //过滤器(会触发待定意图的NFC事件类型)
        intentFilter = new IntentFilter[]{new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED)};
        //指定NFC技术类型
        techList = new String[][]{new String[]
                {NfcA.class.getName()},
                {NfcB.class.getName()},
                {NfcF.class.getName()},
                {NfcV.class.getName()},
                {Ndef.class.getName()},
                {NdefFormatable.class.getName()},
                {MifareClassic.class.getName()},
                {MifareUltralight.class.getName()},
                {IsoDep.class.getName()}
        };
    }



    protected void onResume() {
        super.onResume();
        if (nfcAdapter != null && nfcAdapter.isEnabled()) {
            //启用NFC感应
            nfcAdapter.enableForegroundDispatch(StartNFCActivity.this, pendingIntent, intentFilter, techList);
        }
    }

    protected void onPause() {
        super.onPause();
        if (nfcAdapter != null && nfcAdapter.isEnabled()) {
            //禁用NFC感应
            nfcAdapter.disableForegroundDispatch(StartNFCActivity.this);
        }
    }


    /**
     * Parses the NDEF Message from the intent and prints to the TextView
     */
    void processIntent(Intent intent) {

        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
                NfcAdapter.EXTRA_NDEF_MESSAGES);
        // only one message sent during the beam
        NdefMessage msg = (NdefMessage) rawMsgs[0];
        // record 0 contains the MIME type, record 1 is the AAR, if present
        textView.setText(new String(msg.getRecords()[0].getPayload()));
    }

    //该界面触发NFC自动调用该方法
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        processIntent(intent);
        /*
        String outString = "NFC卡无数据";
        //读取NFC信息
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        if (tag != null) {
            //获取序列号
            byte[] id_bytes = tag.getId();
            String id = "";
            for (int i = 0; i < id_bytes.length; i++) {
                id = id + id_bytes[i];
            }
            outString = "序列号为：" + id + "\n";
            //创建MifareClassic对象
            MifareClassic classic = MifareClassic.get(tag);
            try {
                //连接卡片
                classic.connect();
                //获取类型
                int typeI = classic.getType();
                String type = null;
                if (typeI == MifareClassic.TYPE_CLASSIC) {
                    type = "传统类型";
                } else if (typeI == MifareClassic.TYPE_PLUS) {
                    type = "增强类型";
                } else if (typeI == MifareClassic.TYPE_PRO) {
                    type = "专业类型";
                } else {
                    type = "未知类型";
                }
                //获取其他数据
                int i1 = classic.getSectorCount();//扇形区域
                int i2 = classic.getBlockCount();//分块个数
                int i3 = classic.getSize();//内存大小
                outString = outString + type + "\n" + "扇形区域：" + i1 + "\n分块个数：" + i2 + "\n内存大小：" + i3;
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    //无论是否发生异常都要关闭连接
                    classic.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        textView.setText(outString);
        if (outString.equals("NFC卡无数据")) {
            textView.setText(outString);
        }*/
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        String text = ("Beam me up, Android!\n\n" +
                "Beam Time: " + System.currentTimeMillis());
        NdefMessage msg = new NdefMessage(
                new NdefRecord[] { NdefRecord.createMime("application/vnd.com.example.android.beam", text.getBytes())
                        /**
                         * The Android Application Record (AAR) is commented out. When a device
                         * receives a push with an AAR in it, the application specified in the AAR
                         * is guaranteed to run. The AAR overrides the tag dispatch system.
                         * You can add it back in to guarantee that this
                         * activity starts when receiving a beamed message. For now, this code
                         * uses the tag dispatch system.
                        */
                        //,NdefRecord.createApplicationRecord("com.example.android.beam")
                });
        return msg;
    }
}
