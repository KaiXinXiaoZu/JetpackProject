package com.yzhg.examples.content;

/**
 * 包名：com.yzhg.examples.content
 * 创建人：yzhg
 * 时间：2021/06/24 11:43
 * 描述：
 */
public class Content {


    public static final int EVENT_SYSTEM_TAG = 1000;

    public static final int EVENT_LOGIN_SUCCESS = 1001;


    /*SP储存当前登录信息*/
    public static final String SP_LOGIN_INFO = "login_info";
    /*SP 储存用户等级信息*/
    public static final String SP_PERSON_RANK_BEAN = "person_rank_bean";

    /*SP保存选择的语言*/
    public static final String SP_SELECT_LANGUAGE = "select_language";
}
