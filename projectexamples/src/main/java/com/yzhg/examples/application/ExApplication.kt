package com.yzhg.examples.application

import android.content.Context
import android.content.res.Configuration
import com.yzhg.study.application.BasicApplication
import com.yzhg.study.tools.language.LanguageTools

/**
 * 包名：com.yzhg.examples.application
 * 创建人：yzhg
 * 时间：2021/06/23 10:52
 * 描述：
 */
class ExApplication : BasicApplication() {

    override fun onCreate() {
        super.onCreate()

    }


    override fun attachBaseContext(base: Context) {
        //获取保存的语言
        val language = LanguageTools.getAppLanguage(base)
        if (language.isNotEmpty()) {
            super.attachBaseContext(LanguageTools.attachBaseContext(base, language))
        } else {
            super.attachBaseContext(base)
        }
    }


    /**
     * 设置语言项
     */
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        onLanguageChange()
    }


    private fun onLanguageChange() {
        LanguageTools.getSupportLanguage(LanguageTools.getAppLanguage(this))?.let { LanguageTools.attachBaseContext(this, it) }
    }
}



































