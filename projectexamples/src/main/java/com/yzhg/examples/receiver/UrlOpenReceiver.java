package com.yzhg.examples.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

/**
 * 编辑 : yzhg
 * 时间 ：20:26-01-14 20:26
 * 包名 : com.yzhg.examples.receiver
 * 描述 : 一句话描述
 */
public class UrlOpenReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri data = intent.getData();
            if (data != null) {
                String url = data.toString();
                Toast.makeText(context, "Opened URL: " + url, Toast.LENGTH_SHORT).show();
               /* if (url.contains("your-target-url")) {
                    // 处理特定URL
                    Toast.makeText(context, "Opened URL: " + url, Toast.LENGTH_SHORT).show();
                }*/
            }
        }
    }
}