package com.yzhg.examples.demo

import androidx.lifecycle.viewModelScope
import com.yzhg.study.base.viewmodel.CommonViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PageStatusViewModel : CommonViewModel() {


     fun <T> get2(pageId: Int, url: String) {
        viewModelScope.launch(Dispatchers.IO) {
            executeRequest2<T>({ httpApi2.get(url) }, getLiveData<T>(), pageId)
        }
    }
}