package com.yzhg.examples.demo.adapter

import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.yzhg.examples.R
import com.yzhg.examples.bean.home.BringTopBean
import com.yzhg.examples.bean.home.TagsBean
import com.yzhg.examples.databinding.ItemDemoLayoutBinding
import com.yzhg.examples.databinding.ItemHomeLayoutBinding
import com.yzhg.examples.databinding.ItemHomeTagBinding
import com.yzhg.study.recycle.adapter.BaseCommentAdapter
import com.yzhg.toollibrary.ShapeTools
import com.yzhg.toollibrary.Tools

/**
 * author : yzhg
 * 包名: com.yzhg.examples.demo.adapter
 * 时间: 2023-02-23 15:08
 * 描述：
 */
class PageStatusAdapter : BaseCommentAdapter<BringTopBean, ItemHomeLayoutBinding>(R.layout.item_home_layout) {


    override fun convert(holder: BaseDataBindingHolder<ItemHomeLayoutBinding>, item: BringTopBean) {
        val dataBinding: ItemHomeLayoutBinding? = holder.dataBinding
        if (dataBinding != null) {
            dataBinding.bringTopBean = item
            val llHomeTag = dataBinding.llHomeTag
            val tags: MutableList<TagsBean>? = item.tags
            if (!tags.isNullOrEmpty()) {
                for (tag in tags) {
                    val itemHomeTag: ItemHomeTagBinding =
                        DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_home_tag, null, false)
                    itemHomeTag.tvTagLayout.text = tag.name
                    val backgroundDrawable = ShapeTools.getBackgroundDrawable(
                        Tools.getColor(R.color.color_FFFFFF), Tools.getColor(R.color.color_007AFF),
                        2, 6.0F
                    )
                    itemHomeTag.tvTagLayout.background = backgroundDrawable
                    llHomeTag.addView(itemHomeTag.root)
                }
            }
        }
    }

}