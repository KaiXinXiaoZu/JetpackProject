package com.yzhg.examples.demo

import com.yzhg.examples.bean.BaseDataBean
import com.yzhg.examples.bean.home.BringTopBean
import com.yzhg.examples.databinding.ActivityStatusPageBinding
import com.yzhg.examples.demo.adapter.PageStatusAdapter
import com.yzhg.study.base.ui.activity.BaseBVCMActivity
import com.yzhg.study.base.viewmodel.CommonViewModel
import com.yzhg.study.base.ui.activity.BaseBVMActivity
import com.yzhg.study.http.observer.BasicState2Observer
import com.yzhg.study.recycle.listener.RecycleRefreshLoadListener

class PageStatusActivity : BaseBVCMActivity<ActivityStatusPageBinding>() {


    private val urlPath: String = "article/list/${0}/json";


    override fun initView() {

        //创建适配器
        val pageStatusAdapter = PageStatusAdapter()
        //设置适配器和网络请求
        binding.recycleDemo.setAdapter(pageStatusAdapter, object : RecycleRefreshLoadListener {
            override fun onStartRequest(pageIndex: Int) {
                viewModel.get<BaseDataBean<MutableList<BringTopBean>>>(pageIndex, urlPath)
            }
        })

        //接收网络请求
        viewModel.getLiveData<BaseDataBean<MutableList<BringTopBean>>>().observe(this,
            object : BasicState2Observer<BaseDataBean<MutableList<BringTopBean>>>(binding.recycleDemo) {
                override fun onDataSuccess(data: BaseDataBean<MutableList<BringTopBean>>) {
                    val datas: MutableList<BringTopBean>? = data.datas
                    //设置数据
                    binding.recycleDemo.setFinishData(datas!!)
                }
            })
    }
}