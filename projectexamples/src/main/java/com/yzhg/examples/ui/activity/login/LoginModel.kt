package com.yzhg.study.demo.ui.activity.login

import androidx.lifecycle.viewModelScope
import com.yzhg.examples.bean.login.LoginBean
import com.yzhg.examples.http.WanAPI
import com.yzhg.study.base.viewmodel.BaseViewModel
import com.yzhg.study.base.viewmodel.CommonViewModel
import com.yzhg.study.base.viewmodel.StateLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * 包名：com.yzhg.study.demo.ui.activity.login
 * 创建人：yzhg
 * 时间：2021/06/22 17:20
 * 描述：登录Model类
 */
class LoginModel : CommonViewModel() {

    /**
     * 创建liveData
     */
    var loginLiveData: StateLiveData<LoginBean> = StateLiveData()


    /**
     * 创建API
     */
    var httpApi3 = createHttpApi<WanAPI>()

    /**
     * 去登录
     */
    fun userGoLogin(userName: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            executeRequest({ httpApi3.userLogin(userName, password) }, loginLiveData, true)
        }
    }
}