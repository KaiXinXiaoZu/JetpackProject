package com.yzhg.examples.ui.fragment.mine

import androidx.lifecycle.viewModelScope
import com.yzhg.examples.bean.login.PersonRankBean
import com.yzhg.examples.http.WanAPI
import com.yzhg.study.base.viewmodel.BaseViewModel
import com.yzhg.study.base.viewmodel.StateLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * 包名：com.yzhg.examples.ui.fragment.mine
 * 创建人：yzhg
 * 时间：2021/06/22 18:36
 * 描述：
 */
class MineModel : BaseViewModel() {


    /**
     * 创建LiveData
     */
    var stateLiveData: StateLiveData<PersonRankBean> = StateLiveData()


    /**
     * 创建API
     */
    var httpApi = createHttpApi<WanAPI>()


    fun requestLgRankInfo() {
        viewModelScope.launch(Dispatchers.IO) {
            executeRequest({ httpApi.getUserRankInfo() }, stateLiveData)
        }
    }

}