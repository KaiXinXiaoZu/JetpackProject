package com.yzhg.examples.ui.activity.language

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.yzhg.examples.R
import com.yzhg.examples.bean.language.LanguageBean
import com.yzhg.examples.databinding.ItemLanguageLayoutBinding
import com.yzhg.toollibrary.Tools

/**
 * 包名：com.yzhg.examples.ui.activity.language
 * 创建人：yzhg
 * 时间：2021/07/07 10:54
 * 描述：
 */
class LanguageAdapter(languageList: MutableList<LanguageBean>) :
    BaseQuickAdapter<LanguageBean, BaseDataBindingHolder<ItemLanguageLayoutBinding>>(R.layout.item_language_layout, languageList) {


    override fun convert(holder: BaseDataBindingHolder<ItemLanguageLayoutBinding>, item: LanguageBean) {
        //设置语言项
        holder.dataBinding?.tvLanguageText?.id?.let { holder.setText(it, item.languageName) }
        //设置是否选中
        holder.dataBinding?.ivLanguageState?.let {
            if (item.isSelect) {
                it.setBackgroundResource(R.mipmap.icon_select)
            } else {
                it.setBackgroundResource(R.mipmap.icon_normal)
            }
        }
    }
}