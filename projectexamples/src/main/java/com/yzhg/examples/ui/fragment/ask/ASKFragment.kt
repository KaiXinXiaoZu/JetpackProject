package com.yzhg.study.demo.ui.fragment.ask

import android.os.Bundle
import com.yzhg.examples.R
import com.yzhg.examples.databinding.FragmentAskBinding
import com.yzhg.examples.ui.fragment.ask.AskModel
import com.yzhg.study.base.ui.fragment.BaseBVMFragment

/**
 * 包名：com.yzhg.study.demo.ui.fragment.ask
 * 创建人：yzhg
 * 时间：2021/06/22 17:31
 * 描述：问答页面
 */
class ASKFragment : BaseBVMFragment<FragmentAskBinding, AskModel>() {


    override fun createViewModel(): AskModel = AskModel()

    //override fun getLayoutId(): Int = R.layout.fragment_ask

    override fun initialize(savedInstanceState: Bundle?) {

        binding.tv.text = "我要问题"
    }
}