package com.yzhg.examples.ui.fragment.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.youth.banner.indicator.CircleIndicator
import com.yzhg.examples.R
import com.yzhg.examples.bean.home.BannerBean
import com.yzhg.examples.bean.home.BringTopBean
import com.yzhg.examples.databinding.ItemBringItemLayoutBinding
import com.yzhg.examples.databinding.ItemHomeBannerBinding
import com.yzhg.examples.databinding.ItemHomeLayoutBinding
import com.yzhg.examples.databinding.ItemHomeTagBinding
import com.yzhg.examples.ui.fragment.home.holder.BannerHomeHolder
import com.yzhg.examples.ui.fragment.home.holder.BringTopHomeHolder
import com.yzhg.examples.ui.fragment.home.holder.ItemHomeHolder
import com.yzhg.toollibrary.ShapeTools
import com.yzhg.toollibrary.Tools

/**
 * 包名：com.yzhg.examples.ui.fragment.home.adapter
 * 创建人：yzhg
 * 时间：2021/06/23 13:27
 * 描述：首页多列表Adapter  三个View  轮播图   顶置列表   常规列表
 */
class HomeAdapter(context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /**上下文*/
    private var context: Context = context


    /**首页轮播图*/
    private val VIEW_HOME_BANNER = 0

    /**顶置View*/
    private val VIEW_BRING_HOME = 1

    /** 常规View  */
    private val VIEW_COMMENT_HOME = 2


    /**首页轮播图数据*/
    private var bannerList: MutableList<BannerBean> = mutableListOf()

    /**顶置页面数据*/
    private var bringTopList: MutableList<BringTopBean> = mutableListOf()

    /**常规数据*/
    private var commentList: MutableList<BringTopBean> = mutableListOf()


    /**
     * 设置首页轮播图数据
     */
    fun setBannerListData(bannerList: MutableList<BannerBean>?) {
        //清除原有数据
        this.bannerList.clear()
        if (!bannerList.isNullOrEmpty()) {
            this.bannerList.addAll(bannerList)
            //刷新数据
            notifyItemChanged(VIEW_HOME_BANNER)
        }
    }

    /**
     * 设置置顶数据
     */
    fun setTopBring(bringTopList: MutableList<BringTopBean>) {
        //清除数据
        this.bringTopList.clear()
        if (!bringTopList.isNullOrEmpty()) {
            this.bringTopList.addAll(bringTopList)
            //刷新 数据
            notifyItemChanged(VIEW_BRING_HOME)
        }
    }

    /**
     * 设置常规数据
     */
    fun setCommentList(commentList: MutableList<BringTopBean>, isMore: Boolean) {
        //清除数据
        if (!isMore) {
            this.commentList.clear()
        }
        if (!commentList.isNullOrEmpty()) {
            this.commentList.addAll(commentList)
            //刷新数据
            notifyItemChanged(VIEW_COMMENT_HOME)
        }
    }

    /**
     * 获取常规数据
     */
    fun getCommentList(): MutableList<BringTopBean> {
        return commentList
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> {
                VIEW_HOME_BANNER
            }
            1 -> {
                VIEW_BRING_HOME
            }
            2 -> {
                VIEW_COMMENT_HOME
            }
            else -> {
                VIEW_COMMENT_HOME
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            VIEW_HOME_BANNER -> {
                val itemHomeBannerBinding: ItemHomeBannerBinding =
                    DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_home_banner, parent, false)
                return BannerHomeHolder(itemHomeBannerBinding)
            }
            VIEW_BRING_HOME -> {
                val itemBringItemLayoutBinding: ItemBringItemLayoutBinding =
                    DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_bring_item_layout, parent, false)
                return BringTopHomeHolder(itemBringItemLayoutBinding)
            }
            else -> {
                val itemTopBringBinding: ItemHomeLayoutBinding =
                    DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_home_layout, parent, false)

                return ItemHomeHolder(itemTopBringBinding)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is BannerHomeHolder -> {
                val bannerHomeHolder: BannerHomeHolder = holder
                setBannerImage(bannerHomeHolder)
            }
            is BringTopHomeHolder -> {
                val bringTopHomeHolder: BringTopHomeHolder = holder
                setBringTopView(bringTopHomeHolder)
            }
            else -> {
                val itemHomeHolder: ItemHomeHolder = holder as ItemHomeHolder
                val bringTopBean = commentList[position - 1 - 1]
                val tags = bringTopBean.tags
                val llHomeTag = itemHomeHolder.itemHomeBinding.llHomeTag
                if (!tags.isNullOrEmpty()) {
                    llHomeTag.removeAllViews()
                    for (tag in tags) {
                        val itemHomeTag: ItemHomeTagBinding =
                            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_home_tag, null, false)
                        itemHomeTag.tvTagLayout.text = tag.name
                        val backgroundDrawable = ShapeTools.getBackgroundDrawable(
                            Tools.getColor(R.color.color_FFFFFF), Tools.getColor(R.color.color_007AFF),
                            1, 6.0F
                        )
                        itemHomeTag.tvTagLayout.background = backgroundDrawable
                        llHomeTag.addView(itemHomeTag.root)
                    }
                } else {
                    llHomeTag.removeAllViews()
                }
                itemHomeHolder.itemHomeBinding.bringTopBean = bringTopBean
            }
        }
    }

    private fun setBringTopView(bringTopHomeHolder: BringTopHomeHolder) {
        //RecycleVIew列表
        val recycleBringItem = bringTopHomeHolder.itemTopBringBinding.recycleBringItem
        //设置LinearLayoutManager
        recycleBringItem.layoutManager = LinearLayoutManager(context)
        //设置Adapter
        val bringTopAdapter = BringTopAdapter(bringTopList)
        recycleBringItem.adapter = bringTopAdapter
    }

    /**
     * 设置轮播图
     */
    private fun setBannerImage(bannerHomeHolder: BannerHomeHolder) {
        val homeBannerAdapter: HomeBannerAdapter = HomeBannerAdapter(bannerList)
        val bannerHomeLayout = bannerHomeHolder.itemHomeBannerBinding.bannerHomeLayout
        bannerHomeLayout.setAdapter(homeBannerAdapter)
            .setCurrentItem(300)
            .setIndicator(CircleIndicator(context))
            .start()
    }

    /**
     * 总条目数
     */
    override fun getItemCount(): Int = commentList.size + 1 + 1
}












