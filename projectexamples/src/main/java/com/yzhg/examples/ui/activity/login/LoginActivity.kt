package com.yzhg.examples.ui.activity.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.gson.reflect.TypeToken
import com.hjq.toast.ToastUtils
import com.jeremyliao.liveeventbus.LiveEventBus
import com.wsdz.tools.common.SpTools
import com.youth.banner.util.LogUtils
import com.yzhg.examples.R
import com.yzhg.examples.bean.login.LoginBean
import com.yzhg.examples.bean.system.SystemBean
import com.yzhg.examples.content.Content
import com.yzhg.examples.databinding.ActivityLoginBinding
import com.yzhg.examples.event.EventBean
import com.yzhg.study.base.ui.activity.BaseBVMActivity
import com.yzhg.study.demo.ui.activity.login.LoginModel
import com.yzhg.study.http.bean.ResponseBean
import com.yzhg.study.http.observer.BasicStateObserver
import java.util.*

/**
 * 包名：com.yzhg.study.demo.ui.activity.login
 * 创建人：yzhg
 * 时间：2021/06/22 17:19
 * 描述：登录类
 */
class LoginActivity : BaseBVMActivity<ActivityLoginBinding, LoginModel>() {


    companion object {
        fun newIntent(context: Context) {
            val intent: Intent = Intent()
            intent.setClass(context, LoginActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun createViewModel(): LoginModel = LoginModel()

   // override fun getLayoutId(): Int = R.layout.activity_login


    override fun initView() {


        binding.butUserLogin.setOnClickListener {
            //点击登录
            val userName = binding.etUserName.text.toString().trim()
            //密码
            val password = binding.etUserPsw.text.toString().trim()
            if (userName.isEmpty() || password.isEmpty()) {
                ToastUtils.show("用户名和密码都不能为空")
            } else {
                viewModel.userGoLogin(userName, password)
            }
        }

        viewModel.loginLiveData.observe(this, object : BasicStateObserver<LoginBean>() {
            override fun onDataSuccess(data: LoginBean) {
                //请求成功
                LogUtils.d("请求成功$data")
                finish()
                SpTools.putObject(this@LoginActivity,Content.SP_LOGIN_INFO, data)
                LiveEventBus.get(EventBean::class.java).post(EventBean<SystemBean>(Content.EVENT_LOGIN_SUCCESS))
            }
        })
    }
}