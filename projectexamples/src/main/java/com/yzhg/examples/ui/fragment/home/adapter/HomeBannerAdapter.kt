package com.yzhg.examples.ui.fragment.home.adapter

import android.view.ViewGroup
import android.widget.ImageView
import com.youth.banner.adapter.BannerAdapter
import com.yzhg.examples.R
import com.yzhg.examples.bean.home.BannerBean
import com.yzhg.examples.ui.fragment.home.holder.HomeBannerViewMolder
import com.yzhg.toollibrary.Tools
import com.yzhg.toollibrary.image.GlideTools

/**
 * 包名：com.yzhg.examples.ui.fragment.home
 * 创建人：yzhg
 * 时间：2021/06/23 10:45
 * 描述：
 */
class HomeBannerAdapter(mData: MutableList<BannerBean>) : BannerAdapter<BannerBean, HomeBannerViewMolder>(mData) {

    /**
     * 创建Holder
     */
    override fun onCreateHolder(parent: ViewGroup, viewType: Int): HomeBannerViewMolder {
        val imageView: ImageView = ImageView(parent.context)
        imageView.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        imageView.scaleType = ImageView.ScaleType.CENTER_CROP;
        return HomeBannerViewMolder(imageView)
    }

    /**
     * 绑定View
     */
    override fun onBindView(holder: HomeBannerViewMolder?, data: BannerBean?, position: Int, size: Int) {
        GlideTools.Builder(Tools.getContext())
            .setImageView(holder?.imageView)
            .setImagePath(data?.imagePath)
            .setPlaceholderImage(R.mipmap.banner_default)
            .onCreate()

    }


}