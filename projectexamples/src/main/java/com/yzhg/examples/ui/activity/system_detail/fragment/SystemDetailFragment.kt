package com.yzhg.examples.ui.activity.system_detail.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.scwang.smart.refresh.layout.api.RefreshLayout
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener
import com.yzhg.examples.bean.BaseDataBean
import com.yzhg.examples.bean.home.BringTopBean
import com.yzhg.examples.databinding.FragmentDetailSystemBinding
import com.yzhg.examples.ui.fragment.home.adapter.BringTopAdapter
import com.yzhg.study.base.ui.lazy.BaseLazyBVMFragment
import com.yzhg.study.base.viewmodel.CommonViewModel
import com.yzhg.study.http.observer.BasicLoadStateObserver

/**
 * 包名：com.yzhg.examples.ui.activity.system_detail.fragment
 * 创建人：yzhg
 * 时间：2021/06/24 17:36
 * 描述：
 */
class SystemDetailFragment : BaseLazyBVMFragment<FragmentDetailSystemBinding, CommonViewModel>(), OnRefreshLoadMoreListener {

    override fun createViewModel(): CommonViewModel = CommonViewModel()

  //  override fun getLayoutId(): Int = R.layout.fragment_detail_system


    companion object {

        //传递数据
        var BUNDLE_CID = "bundle_cid"

        fun newInstance(cID: Int): SystemDetailFragment {
            val bundle: Bundle = Bundle()
            bundle.putInt(BUNDLE_CID, cID)
            val homeGoodsListFragment: SystemDetailFragment = SystemDetailFragment()
            homeGoodsListFragment.arguments = bundle
            return homeGoodsListFragment
        }
    }

    /**
     * 传递过来的CID
     */
    private var cId: Int = 0

    /**
     * 请求下来的数据
     */
    private var bringTopList: MutableList<BringTopBean> = mutableListOf()

    /**
     * 上拉加载页码
     */
    private var indexPage: Int = 1

    /**
     * 列表适配器
     */
    private var bringTopAdapter: BringTopAdapter? = null

    /**
     * 条目总数
     */
    private var totalNum: Int = 0


    override fun initialize(savedInstanceState: Bundle?) {
        //获取CID

        cId = arguments?.getInt(BUNDLE_CID) ?: 0


        //初始化页面
        binding.slDetailSystemLayout.setOnRefreshLoadMoreListener(this)


        val recycleDetailSystemLayout = binding.recycleDetailSystemLayout
        recycleDetailSystemLayout.layoutManager = LinearLayoutManager(context)
        //设置Adapter
        bringTopAdapter = BringTopAdapter(bringTopList)
        recycleDetailSystemLayout.adapter = bringTopAdapter
    }

    /**
     * 下拉刷新
     */
    override fun onRefresh(refreshLayout: RefreshLayout) {
        requestTreeDetail(false)
    }

    /**
     * 上拉加载
     */
    override fun onLoadMore(refreshLayout: RefreshLayout) {
        if (bringTopAdapter?.data?.size ?: 0 >= totalNum) {
            binding.slDetailSystemLayout.finishLoadMoreWithNoMoreData()
        } else {
            requestTreeDetail(true)
        }
    }

    override fun onLazyLoad() {
        //根据Cid获取请求
        requestTreeDetail(false)

        //创建数据监听
      /*  viewModel.stateLiveData.observe(this,
            object : BasicLoadStateObserver<BaseDataBean<MutableList<BringTopBean>>>(binding.slDetailSystemLayout, binding.slDetailSystemLayout) {
                override fun onDataSuccess(data: BaseDataBean<MutableList<BringTopBean>>) {
                    val datas = data.datas
                    totalNum = data.total ?: 0
                    if (!datas.isNullOrEmpty()) {
                        if (indexPage == 1) {
                            bringTopAdapter?.data?.clear()
                        }
                        bringTopAdapter?.addData(datas)
                    }
                    indexPage += 1
                }
            })*/
    }

    fun requestTreeDetail(isMore: Boolean) {
        if (!isMore) {
            indexPage = 1
        }
        //根据Cid获取请求
       // viewModel.getTreeDetailJson(indexPage, cId)
    }

}