package com.yzhg.examples.ui.fragment.mine

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.jeremyliao.liveeventbus.LiveEventBus
import com.luck.picture.lib.config.SelectMimeType
import com.luck.picture.lib.entity.LocalMedia
import com.luck.picture.lib.interfaces.OnResultCallbackListener
import com.wsdz.tools.common.LogTools
import com.wsdz.tools.common.SpTools
import com.yzhg.examples.R
import com.yzhg.examples.bean.login.LoginBean
import com.yzhg.examples.bean.login.PersonRankBean
import com.yzhg.examples.content.Content
import com.yzhg.examples.databinding.FragmentMineBinding
import com.yzhg.examples.databinding.ItemHeadLayoutBinding
import com.yzhg.examples.event.EventBean
import com.yzhg.examples.ui.activity.integral.IntegralActivity
import com.yzhg.examples.ui.activity.language.LanguageActivity
import com.yzhg.examples.ui.activity.login.LoginActivity
import com.yzhg.examples.ui.dialog.DBBaseDialogDemo
import com.yzhg.study.base.ui.lazy.BaseLazyBVMFragment
import com.yzhg.study.http.observer.BasicStateObserver
import com.yzhg.toollibrary.Tools
import com.yzhg.toollibrary.image.PictureSelectorTools
import com.yzhg.toollibrary.recycle.selectimage.listener.SelectImageListener
import java.util.ArrayList

/**
 * 包名：com.yzhg.study.demo.ui.fragment.mine
 * 创建人：yzhg
 * 时间：2021/06/22 17:31
 * 描述：我的页面
 */
class MineFragment : BaseLazyBVMFragment<FragmentMineBinding, MineModel>(), View.OnClickListener {


    override fun createViewModel(): MineModel = MineModel()

   // override fun getLayoutId(): Int = R.layout.fragment_mine

    override fun initialize(savedInstanceState: Bundle?) {


    }

    private var loginBean: LoginBean? = null


    override fun onLazyLoad() {
        loginBean = getUserLoginData()

        if (loginBean != null) {
            //获取排名
            viewModel.requestLgRankInfo()
        }

        //设置点击事件
        binding.viewMineTopBg.setOnClickListener {
            if (loginBean == null) {
                //去登录
                LoginActivity.newIntent(requireContext())
            } else {

            }
        }


        //订阅消息
        LiveEventBus
            .get(EventBean::class.java)
            .observe(this) { t ->
                if (t.eventTag == Content.EVENT_LOGIN_SUCCESS) {
                    loginBean = getUserLoginData()
                    viewModel.requestLgRankInfo()
                }
            }

        //获取本地存放的积分排行数据
        viewModel.stateLiveData.observe(this, object : BasicStateObserver<PersonRankBean>() {
            override fun onDataSuccess(data: PersonRankBean) {
                binding.personRankBean = data
            }
        })

        binding.clIntegralLayout.setOnClickListener(this)
        binding.clLanguageLayout.setOnClickListener(this)
        binding.tvDialogDb.setOnClickListener(this)


        // var imageList = binding.recycleSelectImage.getImageList()

        //设置选择图片

        binding.recycleSelectImage.initPictureSelect(this)

        //获取图片地址

        val list: MutableList<String> = mutableListOf()
        list.add("1")  //  160    160
        list.add("2")   //  120
        // list.add("3")   //  80
        // list.add("4")   //  40
        // list.add("5")   //  0


        for (index in list.indices) {
            val itemHeadLayoutBinding: ItemHeadLayoutBinding =
                DataBindingUtil.inflate(LayoutInflater.from(requireContext()), R.layout.item_head_layout, binding.clHeadLayout, false)

            val ivLeftLayoutParams: ConstraintLayout.LayoutParams = ConstraintLayout.LayoutParams(Tools.dip2px(60f), 0)
            ivLeftLayoutParams.rightToRight = binding.phView.id
            ivLeftLayoutParams.topToTop = binding.phView.id
            ivLeftLayoutParams.bottomToBottom = binding.phView.id
            //动态计算  marginEnd
            val totalMargin = (list.size - (index + 1)) * 40
            ivLeftLayoutParams.marginEnd = Tools.dip2px(totalMargin.toFloat())
            itemHeadLayoutBinding.root.layoutParams = ivLeftLayoutParams
            binding.clHeadLayout.addView(itemHeadLayoutBinding.root)
        }

    }

    private fun getUserLoginData(): LoginBean? {
        //获取本地存放的登录数据
        val loginBean: LoginBean? = SpTools.getObject<LoginBean>(requireContext(), Content.SP_LOGIN_INFO)
        binding.loginBane = loginBean
        return loginBean
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            binding.clIntegralLayout.id -> {
                IntegralActivity.newIntent(requireContext())
                /*val imageList = binding.recycleSelectImage.getImageList()
                for (s in imageList) {
                    LogTools.d("选择的图片地址"+s)
                }*/
            }
            binding.clLanguageLayout.id -> {
                LanguageActivity.newIntent(requireContext())
            }
            binding.tvDialogDb.id -> {
                showDbDialog()
            }
        }
    }

    private fun showDbDialog() {
        val dbBaseDialog = DBBaseDialogDemo()
        dbBaseDialog.setDimAmout(0.6f)
        dbBaseDialog.showDialog(parentFragmentManager)
    }

}



















