package com.yzhg.examples.ui.fragment.home.holder

import androidx.recyclerview.widget.RecyclerView
import com.yzhg.examples.databinding.ItemHomeLayoutBinding

/**
 * 包名：com.yzhg.examples.ui.fragment.home.holder
 * 创建人：yzhg
 * 时间：2021/06/23 15:25
 * 描述：
 */
class ItemHomeHolder(itemHomeLayoutBinding: ItemHomeLayoutBinding) : RecyclerView.ViewHolder(itemHomeLayoutBinding.root) {

    var itemHomeBinding: ItemHomeLayoutBinding = itemHomeLayoutBinding

}