package com.yzhg.examples.ui.main

import android.content.Intent
import android.content.IntentFilter
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.yzhg.examples.R
import com.yzhg.examples.databinding.ActivityMainBinding
import com.yzhg.examples.receiver.UrlOpenReceiver
import com.yzhg.examples.ui.fragment.home.HomeFragment
import com.yzhg.examples.ui.fragment.mine.MineFragment
import com.yzhg.examples.ui.fragment.system.SystemFragment
import com.yzhg.study.base.ui.activity.BaseBindingActivity
import com.yzhg.study.demo.ui.fragment.ask.ASKFragment
import com.yzhg.toollibrary.weight.TabMainAdapter


//实现 切面编程 ： https://www.jianshu.com/p/49d2be4c508d
class MainActivity : BaseBindingActivity<ActivityMainBinding>() {

    /**
     * 创建ViewModel
     */
    fun createViewModel(): MainModel = MainModel()

   // override fun getLayoutId(): Int = R.layout.activity_main

    /**
     * 底部导航名称
     */
    private val TAB_TITLES = intArrayOf(R.string.home, R.string.ask, R.string.system, R.string.mine)


    //  override fun setStatusBarColor(): Int = R.color.color_28B28B

    override fun setStatusBarColor(): Int {
        return 0
    }

    /**
     * 作 者: yzhg
     * 描 述: 底部导航图片
     */
    private val TAB_IMGS = intArrayOf(
        R.drawable.tab_home_selector,
        R.drawable.tab_ask_selector,
        R.drawable.tab_system_selector,
        R.drawable.tab_mine_selector
    )


    /**
     * 作 者: yzhg
     * 描 述: 底部融器   百度智能小程序  13140016414
     */
    private val TAB_FRAGMENTS:Array<Fragment> = arrayOf(

        HomeFragment(),
        ASKFragment(),
        SystemFragment(),
        MineFragment()
    )

    override fun initView() {

        setTabs(binding.tabMainLayout, this.layoutInflater, TAB_TITLES, TAB_IMGS)
        binding.vpMainLayout.offscreenPageLimit = 4
        val tabMainAdapter = TabMainAdapter(supportFragmentManager, TAB_FRAGMENTS)
        binding.vpMainLayout.adapter = tabMainAdapter
        binding.vpMainLayout.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(binding.tabMainLayout))
        binding.tabMainLayout.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(binding.vpMainLayout))

        binding.vpMainLayout.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                /*when (position) {
                    0 -> {
                        ImmersionBar.with(this@MainActivity)
                            .statusBarColor(R.color.color_000000)
                            .statusBarDarkFont(true) //状态栏字体是深色，不写默认为亮色
                            .titleBarMarginTop(binding.root) //解决状态栏和布局重叠问题，任选其一
                            .init() //必须调用方可应用以上所配置的参数
                    }
                    1 -> {
                        ImmersionBar.with(this@MainActivity)
                            .statusBarColor(R.color.color_FFFFFF)
                            .statusBarDarkFont(true) //状态栏字体是深色，不写默认为亮色
                            .titleBarMarginTop(binding.root) //解决状态栏和布局重叠问题，任选其一
                            .init() //必须调用方可应用以上所配置的参数
                    }
                    2 -> {
                        ImmersionBar.with(this@MainActivity)
                            .statusBarColor(R.color.color_28B28B)
                            .statusBarDarkFont(true) //状态栏字体是深色，不写默认为亮色
                            .titleBarMarginTop(binding.root) //解决状态栏和布局重叠问题，任选其一
                            .init() //必须调用方可应用以上所配置的参数
                    }
                    3 -> {
                        ImmersionBar.with(this@MainActivity)
                            .statusBarColor(R.color.color_FC5F0C)
                            .statusBarDarkFont(true) //状态栏字体是深色，不写默认为亮色
                            .titleBarMarginTop(binding.root) //解决状态栏和布局重叠问题，任选其一
                            .init() //必须调用方可应用以上所配置的参数
                    }
                }*/
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })

        binding.butDialogPop.setOnClickListener {
            //showDialogLoading()
          //  startActivity(StartNFCActivity::class.java)
        }

        val myReceiver = UrlOpenReceiver()
        // 注册BroadcastReceiver
        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED) // 监听飞行模式变化
        registerReceiver(myReceiver, filter)
    }


    /**
     * 作 者: yzhg
     * 历 史: (版本) 1.0
     * 描 述: 动态填充Fragment
     */
    private fun setTabs(tabLayoutMain: TabLayout, layoutInflater: LayoutInflater, tabTitles: IntArray, tabImgs: IntArray) {
        for (i in tabImgs.indices) {
            val tab = tabLayoutMain.newTab()
            val view: View = layoutInflater.inflate(R.layout.tab_main_layout, null)
            tab.customView = view
            val tvTitle = view.findViewById<TextView>(R.id.tv_tab_main)
            tvTitle.setText(tabTitles[i])
            val imgTab = view.findViewById<ImageView>(R.id.iv_tab_main)
            imgTab.setImageResource(tabImgs[i])
            tabLayoutMain.addTab(tab)
        }
    }
}