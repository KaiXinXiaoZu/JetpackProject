package com.yzhg.examples.ui.fragment.home.holder

import androidx.recyclerview.widget.RecyclerView
import com.yzhg.examples.databinding.ItemHomeBannerBinding

/**
 * 包名：com.yzhg.examples.ui.fragment.home.holder
 * 创建人：yzhg
 * 时间：2021/06/23 15:20
 * 描述：
 */
class BannerHomeHolder(itemHomeBannerBinding: ItemHomeBannerBinding) : RecyclerView.ViewHolder(itemHomeBannerBinding.root) {

     var itemHomeBannerBinding: ItemHomeBannerBinding = itemHomeBannerBinding
}