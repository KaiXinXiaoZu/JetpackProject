package com.yzhg.examples.ui.fragment.system

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.jeremyliao.liveeventbus.LiveEventBus
import com.jeremyliao.liveeventbus.core.Observable
import com.yzhg.examples.R
import com.yzhg.examples.bean.system.SystemBean
import com.yzhg.examples.content.Content
import com.yzhg.examples.databinding.FragmentSystemBinding
import com.yzhg.examples.event.EventBean
import com.yzhg.examples.ui.activity.system_detail.SystemDetailActivity
import com.yzhg.examples.ui.fragment.system.SystemModel
import com.yzhg.examples.ui.fragment.system.adapter.SystemAdapter
import com.yzhg.study.base.ui.fragment.BaseBVMFragment
import com.yzhg.study.base.ui.lazy.BaseLazyBVMFragment
import com.yzhg.study.http.observer.BasicStateObserver

/**
 * 包名：com.yzhg.study.demo.ui.fragment.system
 * 创建人：yzhg
 * 时间：2021/06/22 17:32
 * 描述：体系页面
 */
class SystemFragment : BaseLazyBVMFragment<FragmentSystemBinding, SystemModel>() {


    override fun createViewModel(): SystemModel = SystemModel()

   // override fun getLayoutId(): Int = R.layout.fragment_system

    private var systemList: MutableList<SystemBean> = mutableListOf()

    /**
     * 列表适配器
     */
    private var systemAdapter: SystemAdapter? = null


    override fun onLazyLoad() {
        //请求体系数据
      //  viewModel.requestSystemData()

        viewModel.requestSystemData()

    }

    override fun initialize(savedInstanceState: Bundle?) {
        //初始化RecycleView
        val recycleSystemLayout = binding.recycleSystemLayout
        recycleSystemLayout.layoutManager = LinearLayoutManager(context)
        systemAdapter = SystemAdapter(systemList)
        recycleSystemLayout.adapter = systemAdapter


        //接收体系数据
        viewModel.stateLiveData.observe(this, object : BasicStateObserver<MutableList<SystemBean>>() {
            override fun onDataSuccess(data: MutableList<SystemBean>) {
                Log.i("测试LiveEventBus", "onDataSuccess:请求成功 ")
                systemAdapter?.addData(data)
            }
        })

        //订阅消息
        LiveEventBus
            .get(EventBean::class.java)
            .observe(this, { t ->
                if (t.eventTag == Content.EVENT_SYSTEM_TAG) {
                    val systemBean: SystemBean = t?.data as SystemBean
                    Log.i("测试LiveEventBus", "initialize:收到订阅的消息 " + systemBean.name)
                    //跳转到订阅详情页面
                    SystemDetailActivity.newInstance(requireContext(), systemBean, t.eventInt ?: 0)
                }
            })
    }
}
