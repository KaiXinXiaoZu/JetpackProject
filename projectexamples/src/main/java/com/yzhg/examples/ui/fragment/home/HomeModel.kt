package com.yzhg.examples.ui.fragment.home

import androidx.lifecycle.viewModelScope
import com.yzhg.examples.bean.BaseDataBean
import com.yzhg.examples.bean.home.BannerBean
import com.yzhg.examples.bean.home.BringTopBean
import com.yzhg.examples.http.Wan2API
import com.yzhg.study.base.viewmodel.BaseViewModel
import com.yzhg.study.base.viewmodel.StateLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * 包名：com.yzhg.examples.ui.fragment.home
 * 创建人：yzhg
 * 时间：2021/06/22 18:30
 * 描述：
 */
class HomeModel : BaseViewModel() {


    /**
     * 创建liveData
     */
    public var bannerLiveData: StateLiveData<MutableList<BannerBean>> = StateLiveData()

    /**
     * 创建置顶LiveData
     */
    public var topBringLiveData: StateLiveData<MutableList<BringTopBean>> = StateLiveData()

    /**
     * 创建常规的LiveData
     */
    public var commentLiveData: StateLiveData<BaseDataBean<MutableList<BringTopBean>>> = StateLiveData()

    /**
     * 创建API
     */
    var httpApi = createHttpApi<Wan2API>()

    /**
     * 请求首页轮播图数据
     */
    fun requestHomeBanner() {
        viewModelScope.launch(Dispatchers.IO) {
            executeRequest({ httpApi.getHomeBanner() }, bannerLiveData)
        }
    }

    /**
     * 获取置顶数据
     */
    fun requestTopBringData() {
        viewModelScope.launch(Dispatchers.IO) {
            executeRequest({ httpApi.getTopBringData() }, topBringLiveData)
        }
    }

    /**
     * 获取文章列表
     */
    fun requestArticleListMore(pageIndex: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            executeRequest({ httpApi.getArticleListMore((pageIndex - 1)) }, commentLiveData, pageIndex)
        }
    }


}