package com.yzhg.examples.ui.activity.integral

import android.annotation.SuppressLint
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.yzhg.examples.R
import com.yzhg.examples.bean.integral.IntegralBean
import com.yzhg.examples.databinding.ItemIntegralLayoutBinding

/**
 * 包名：com.yzhg.examples.ui.activity.integral
 * 创建人：yzhg
 * 时间：2021/07/24 10:27
 * 描述：
 */
class IntegralAdapter(languageList: MutableList<IntegralBean>) :
    BaseQuickAdapter<IntegralBean, BaseDataBindingHolder<ItemIntegralLayoutBinding>>(R.layout.item_integral_layout, languageList) {

    override fun convert(holder: BaseDataBindingHolder<ItemIntegralLayoutBinding>, item: IntegralBean) {
        holder.dataBinding?.integralBean = item
    }
}