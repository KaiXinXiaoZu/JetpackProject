package com.yzhg.examples.ui.fragment.home.holder

import androidx.recyclerview.widget.RecyclerView
import com.yzhg.examples.databinding.ItemBringItemLayoutBinding

/**
 * 包名：com.yzhg.examples.ui.fragment.home.holder
 * 创建人：yzhg
 * 时间：2021/06/23 15:24
 * 描述：
 */
class BringTopHomeHolder(itemTopBringBinding: ItemBringItemLayoutBinding) : RecyclerView.ViewHolder(itemTopBringBinding.root) {

    var itemTopBringBinding: ItemBringItemLayoutBinding = itemTopBringBinding

}