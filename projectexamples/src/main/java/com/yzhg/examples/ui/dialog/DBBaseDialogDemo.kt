package com.yzhg.examples.ui.dialog

import com.yzhg.examples.R
import com.yzhg.examples.databinding.DialogDbLayoutBinding
import com.yzhg.toollibrary.dialog.ViewHolder
import com.yzhg.toollibrary.dialog.databinding.DBBaseDialog


/*
 * 作者：yzhg
 * 时间：2022-02-28 09:41
 * 包名：com.yzhg.examples.ui.dialog
 * 描述：
 */
class DBBaseDialogDemo : DBBaseDialog<DialogDbLayoutBinding>() {

    override fun setUpLayoutId(): Int = R.layout.dialog_db_layout

    override fun initParamsView() {
        viewBinding.tvContent.text = "DataBindingDemo"
    }
}