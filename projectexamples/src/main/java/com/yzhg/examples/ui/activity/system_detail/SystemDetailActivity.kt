package com.yzhg.examples.ui.activity.system_detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.yzhg.examples.R
import com.yzhg.examples.bean.system.SystemBean
import com.yzhg.examples.databinding.ActivityDetailSystemBinding
import com.yzhg.examples.ui.activity.system_detail.fragment.SystemDetailFragment
import com.yzhg.examples.ui.main.TabFragmentAdapter
import com.yzhg.study.base.ui.activity.BaseBVMActivity

/**
 * 包名：com.yzhg.examples.ui.activity
 * 创建人：yzhg
 * 时间：2021/06/24 17:28
 * 描述：体系详情页面
 */
class SystemDetailActivity : BaseBVMActivity<ActivityDetailSystemBinding, SystemDetailModel>() {


    override fun createViewModel(): SystemDetailModel = SystemDetailModel()

   // override fun getLayoutId(): Int = R.layout.activity_detail_system


    companion object {

        var BUNDLE_SYSTEM_BEAN = "bundel_system_bean"

        //传递角标
        var BUNDLE_POSITION = "bundle_position"

        fun newInstance(context: Context, systemBean: SystemBean, position: Int) {
            val intent = Intent(context, SystemDetailActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable(BUNDLE_SYSTEM_BEAN, systemBean)
            bundle.putInt(BUNDLE_POSITION, position)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
    }

    /**
     * 传递过来的数据
     */
    private var systemBean: SystemBean? = null


    override fun initView() {

        binding.tvBackLayout.setOnClickListener { finish() }

        val bundle = intent.extras
        //获取传递过来的数据
        systemBean = bundle?.getSerializable(BUNDLE_SYSTEM_BEAN) as SystemBean?

        //获取角标
        val position = bundle?.getInt(BUNDLE_POSITION) ?: 0

        binding.tabTitle = systemBean?.name ?: "体系详情"
        //获取tab title
        val titleList: MutableList<String> = getTabTitle()
        //创建Fragment数组
        val fragmentArray: MutableList<SystemDetailFragment> = getFragmentArray()

        val tabFragmentAdapter = TabFragmentAdapter(supportFragmentManager, fragmentArray.toTypedArray(), titleList)
        binding.tabDetailSystemLayout.setupWithViewPager(binding.vpDetailSystemLayout)
        binding.vpDetailSystemLayout.adapter = tabFragmentAdapter
        //设置加载数量
        binding.vpDetailSystemLayout.offscreenPageLimit = fragmentArray.size
        //设置默认显示第几个页面
        val tabAt = binding.tabDetailSystemLayout.getTabAt(position)
        tabAt.let { it?.select() }
    }

    private fun getFragmentArray(): MutableList<SystemDetailFragment> {
        val systemDetailFragment: MutableList<SystemDetailFragment> = mutableListOf()
        val children = systemBean?.children
        if (!children.isNullOrEmpty()) {
            for (child in children) {
                val newInstance: SystemDetailFragment = SystemDetailFragment.newInstance(child.id ?: 0)
                systemDetailFragment.add(newInstance)
            }
        }
        return systemDetailFragment
    }

    private fun getTabTitle(): MutableList<String> {
        val titles: MutableList<String> = mutableListOf()
        val children = systemBean?.children
        if (!children.isNullOrEmpty()) {
            children.forEach { it ->
                it.name.let { titles.add(it ?: "未知") }
            }
        }
        return titles
    }
}