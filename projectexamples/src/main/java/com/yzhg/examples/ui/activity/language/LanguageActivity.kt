package com.yzhg.examples.ui.activity.language

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.wsdz.tools.common.SpTools
import com.yzhg.toollibrary.common.ToastTools
import com.yzhg.examples.R
import com.yzhg.examples.bean.language.LanguageBean
import com.yzhg.examples.content.Content
import com.yzhg.examples.databinding.ActivityLanguageLayoutBinding
import com.yzhg.examples.ui.main.MainActivity
import com.yzhg.study.base.ui.activity.BaseBVMActivity
import com.yzhg.study.tools.language.LanguageTools
import com.yzhg.toollibrary.Tools
import com.yzhg.toollibrary.common.ActivitysManager

/**
 * 包名：com.yzhg.examples.ui.activity.language
 * 创建人：yzhg
 * 时间：2021/07/07 10:19
 * 描述：
 */
class LanguageActivity : BaseBVMActivity<ActivityLanguageLayoutBinding, LanguageModel>(), OnItemClickListener {

    companion object {
        fun newIntent(context: Context) {
            val intent: Intent = Intent()
            intent.setClass(context, LanguageActivity::class.java)
            context.startActivity(intent)
        }
    }

    /**
     * 获取语言列表  汉语   日语   韩语   俄语  法语  西班牙语  阿拉伯语
     */
    var languageList: MutableList<LanguageBean> = mutableListOf()

    private var languageAdapter: LanguageAdapter? = null

    /**
     * 当前选中的语言
     */
    private var selectLanguage: LanguageBean? = null

    override fun createViewModel(): LanguageModel = LanguageModel()

   // override fun getLayoutId(): Int = R.layout.activity_language_layout

    override fun initView() {
        //设置语音列表
        setLanguageList()

        //设置列表
        binding.recycleLanguageLayout.layoutManager = LinearLayoutManager(this)
        //列表适配器
        languageAdapter = LanguageAdapter(languageList)
        //给列表设置适配器
        binding.recycleLanguageLayout.adapter = languageAdapter
        //设置列表点击事件
        languageAdapter.let {
            it?.setOnItemClickListener(this)
        }

        //设置默认语言
        // selectLanguage = languageList[0]
        //设置提交按钮
        binding.butSubmitLanguage.setOnClickListener {
            if (selectLanguage != null) {
                setSystemLanguage()
            } else {
                ToastTools.showToast(R.string.place_select_language)
            }
        }
    }

    /**
     * 设置系统语言
     */
    private fun setSystemLanguage() {
        if (this.selectLanguage == null) {
            ToastTools.showToast(R.string.place_select_language)
            return
        }
        //保存当前选择的语言
        SpTools.putString(this, Content.SP_SELECT_LANGUAGE, selectLanguage?.languageCode ?: "")

        ActivitysManager.getInstance().clearAll()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }


    /**
     * 条目点击事件
     */
    override fun onItemClick(adapter: BaseQuickAdapter<*, *>, view: View, position: Int) {

        val selectLanguage = languageList[position]
        if (this.selectLanguage != selectLanguage) {
            //将上次选中的设置为false
            this.selectLanguage?.isSelect = false
            //设置当前列表选中
            selectLanguage.isSelect = true
            //重新赋值
            this.selectLanguage = selectLanguage
            //刷新列表
            languageAdapter.let {
                it?.notifyDataSetChanged()
            }
        }
    }


    private fun setLanguageList() {
        languageList.add(LanguageBean(LanguageTools.SIMPLIFIED_CHINESE, Tools.getStirng(R.string.chinese), false))
        languageList.add(LanguageBean(LanguageTools.ENGLISH, Tools.getStirng(R.string.english), false))
        languageList.add(LanguageBean(LanguageTools.JAPANESE, Tools.getStirng(R.string.japanese), false))
        languageList.add(LanguageBean(LanguageTools.KOREAN, Tools.getStirng(R.string.korean), false))
        languageList.add(LanguageBean(LanguageTools.RUSSIAN, Tools.getStirng(R.string.russian), false))
        languageList.add(LanguageBean(LanguageTools.FRENCH, Tools.getStirng(R.string.french), false))
        languageList.add(LanguageBean(LanguageTools.SPANISH, Tools.getStirng(R.string.spanish), false))
        languageList.add(LanguageBean(LanguageTools.ARABIC, Tools.getStirng(R.string.arabic), false))
    }
}