package com.yzhg.examples.ui.fragment.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.yzhg.examples.R
import com.yzhg.examples.bean.home.BringTopBean
import com.yzhg.examples.bean.home.TagsBean
import com.yzhg.examples.databinding.ItemHomeLayoutBinding
import com.yzhg.examples.databinding.ItemHomeTagBinding
import com.yzhg.toollibrary.ShapeTools
import com.yzhg.toollibrary.Tools

/**
 * 包名：com.yzhg.examples.ui.fragment.home.adapter
 * 创建人：yzhg
 * 时间：2021/06/23 16:05
 * 描述：使用BaseHelpRecycleViewAdapter + dataBinding 实现
 */
class BringTopAdapter(data: MutableList<BringTopBean>) :
    BaseQuickAdapter<BringTopBean, BaseDataBindingHolder<ItemHomeLayoutBinding>>(R.layout.item_home_layout, data) {


    override fun convert(holder: BaseDataBindingHolder<ItemHomeLayoutBinding>, item: BringTopBean) {
        val dataBinding: ItemHomeLayoutBinding? = holder.dataBinding
        if (dataBinding != null) {
            dataBinding.bringTopBean = item
            val llHomeTag = dataBinding.llHomeTag
            val tags: MutableList<TagsBean>? = item.tags
            if (!tags.isNullOrEmpty()) {
                for (tag in tags) {
                    val itemHomeTag: ItemHomeTagBinding =
                        DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_home_tag, null, false)
                    itemHomeTag.tvTagLayout.text = tag.name
                    val backgroundDrawable = ShapeTools.getBackgroundDrawable(
                        Tools.getColor(R.color.color_FFFFFF), Tools.getColor(R.color.color_007AFF),
                        2, 6.0F
                    )
                    itemHomeTag.tvTagLayout.background = backgroundDrawable
                    llHomeTag.addView(itemHomeTag.root)
                }
            }
        }
    }


}