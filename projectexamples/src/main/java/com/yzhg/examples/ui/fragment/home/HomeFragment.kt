package com.yzhg.examples.ui.fragment.home

import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.gyf.immersionbar.ImmersionBar
import com.scwang.smart.refresh.layout.api.RefreshLayout
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener
import com.yzhg.examples.R
import com.yzhg.examples.bean.BaseDataBean
import com.yzhg.examples.bean.home.BannerBean
import com.yzhg.examples.bean.home.BringTopBean
import com.yzhg.examples.databinding.FragmentHomeBinding
import com.yzhg.examples.ui.fragment.home.adapter.HomeAdapter
import com.yzhg.examples.ui.fragment.home.adapter.HomeBannerAdapter
import com.yzhg.study.base.ui.fragment.BaseBVMFragment
import com.yzhg.study.base.ui.lazy.BaseLazyBVMFragment
import com.yzhg.study.base.ui.lazy.ILazyLoad
import com.yzhg.study.http.bean.ResponseBean
import com.yzhg.study.http.observer.BasicLoadStateObserver
import com.yzhg.study.http.observer.BasicStateObserver
import com.yzhg.study.loadsir.callback.EmptyCallback

/**
 * 包名：com.yzhg.study.demo.ui.fragment.home
 * 创建人：yzhg
 * 时间：2021/06/22 17:31
 * 描述：首页
 */
class HomeFragment : BaseBVMFragment<FragmentHomeBinding, HomeModel>(), OnRefreshLoadMoreListener {

    override fun createViewModel(): HomeModel = HomeModel()

   // override fun getLayoutId(): Int = R.layout.fragment_home


    /**
     * 列表适配器
     */
    private var homeAdapter: HomeAdapter? = null

    /**
     * 请求页码
     */
    private var indexPage: Int = 0

    /**
     * 总数
     */
    private var total: Int = 0


    override fun initialize(savedInstanceState: Bundle?) {

        ImmersionBar.with(this)
            .statusBarDarkFont(true)
            //.titleBarMarginTop(setContentView()) //解决状态栏和布局重叠问题，任选其一
            .titleBar(binding.clHomeBar)
            .statusBarColor(R.color.color_28B28B)
            .init()


        //初始化上拉加载和下拉刷新
        binding.slHomeLayout.setOnRefreshLoadMoreListener(this)
        binding.slHomeLayout.setEnableOverScrollBounce(true)
        binding.slHomeLayout.setEnableScrollContentWhenLoaded(true)
        binding.slHomeLayout.setEnableScrollContentWhenRefreshed(true)
        //设置RecycleView
        val recycleHomeLayout = binding.recycleHomeLayout
        //设置LayoutManager
        recycleHomeLayout.layoutManager = LinearLayoutManager(context)
        //设置Adapter
        homeAdapter = HomeAdapter(requireContext())
        //设置Adapter
        recycleHomeLayout.adapter = homeAdapter

        binding.icHomeSearch.setOnClickListener {

        }

        Log.i("=================", "onLazyLoad: 执行懒加载")
        //获取数据
        initData()
    }

    override fun setFragmentManager(): FragmentManager {
        return super.setFragmentManager()
    }

    /**
     * 下拉刷新控件
     */
    override fun onRefresh(refreshLayout: RefreshLayout) {
        requestArticleListMore(false)
    }

    /**
     * 上拉加载控件
     */
    override fun onLoadMore(refreshLayout: RefreshLayout) {
        val dataSize = homeAdapter?.getCommentList()?.size ?: 0
        if (dataSize >= total) {
            binding.slHomeLayout.finishLoadMoreWithNoMoreData()
        } else {
            // binding.slHomeLayout.setEnableLoadMore(true)
            requestArticleListMore(true)
        }
    }


    /**
     * 获取数据
     */
    private fun initData() {
        viewModel.requestTopBringData()
        //请求首页轮播图数据
        viewModel.requestHomeBanner()
        //获取列表数据
        requestArticleListMore(false)


        viewModel.bannerLiveData.observe(this, object : BasicStateObserver<MutableList<BannerBean>>() {
            override fun onDataSuccess(data: MutableList<BannerBean>) {
                homeAdapter?.setBannerListData(data)
            }
        })

        //获取置顶数据
        viewModel.topBringLiveData.observe(this, object : BasicStateObserver<MutableList<BringTopBean>>() {
            override fun onDataSuccess(data: MutableList<BringTopBean>) {
                data.forEach { it.stick = true }
                homeAdapter?.setTopBring(data)
            }
        })

        viewModel.commentLiveData.observe(this, object : BasicLoadStateObserver<BaseDataBean<MutableList<BringTopBean>>>(
            binding.slHomeLayout, binding.slHomeLayout
        ) {
            override fun onDataSuccess(data: BaseDataBean<MutableList<BringTopBean>>) {
                Log.i("=================", "onDataSuccess: 接收到返回值")
                indexPage += 1
                if (!data.datas.isNullOrEmpty()) {
                    total = data.total ?: 0
                    homeAdapter?.setCommentList(data.datas, true)
                } else {
                    this.mLoadService?.showCallback(EmptyCallback::class.java)
                }
            }
        })
    }

    private fun requestArticleListMore(isMore: Boolean) {
        if (!isMore) {
            indexPage = 1
        }
        viewModel.requestArticleListMore(indexPage)
    }


}