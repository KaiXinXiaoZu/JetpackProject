package com.yzhg.examples.ui.activity.integral

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.scwang.smart.refresh.layout.api.RefreshLayout
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener
import com.yzhg.examples.R
import com.yzhg.examples.bean.BaseDataBean
import com.yzhg.examples.bean.integral.IntegralBean
import com.yzhg.examples.databinding.ActivityIntegralBinding
import com.yzhg.examples.ui.activity.login.LoginActivity
import com.yzhg.study.base.ui.activity.BaseBVMActivity
import com.yzhg.study.http.observer.BasicLoadStateObserver
import com.yzhg.study.http.observer.BasicStateObserver

/**
 * 包名：com.yzhg.examples.ui.activity.integral
 * 创建人：yzhg
 * 时间：2021/07/06 14:10
 * 描述：积分列表
 */
class IntegralActivity : BaseBVMActivity<ActivityIntegralBinding, IntegralModel>(), OnRefreshLoadMoreListener {


    companion object {
        fun newIntent(context: Context) {
            val intent: Intent = Intent()
            intent.setClass(context, IntegralActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun createViewModel(): IntegralModel = IntegralModel()

   // override fun getLayoutId(): Int = R.layout.activity_integral

    /**
     * 积分列表
     */
    var integralList: MutableList<IntegralBean> = mutableListOf()

    /**
     * 适配器
     */
    var integralAdapter: IntegralAdapter? = null

    /**
     * 请求页码
     */
    var indexPosition = 1

    override fun initView() {

        //设置上拉加载和下拉刷新
        binding.slIntegralLayout.setOnRefreshLoadMoreListener(this)
        //设置列表
        binding.recycleIntegralLayout.layoutManager = LinearLayoutManager(this)
        //适配器
        integralAdapter = IntegralAdapter(integralList)
        //给layout设置adapter
        binding.recycleIntegralLayout.adapter = integralAdapter


        //数据回调
        viewModel.stateLiveData.observe(this,
            object :
                BasicLoadStateObserver<BaseDataBean<MutableList<IntegralBean>>>(binding.slIntegralLayout, binding.slIntegralLayout) {
                override fun onDataSuccess(data: BaseDataBean<MutableList<IntegralBean>>) {
                    indexPosition += 1
                    //拿到数据
                    //integralList.addAll(data.datas!!)
                    integralAdapter?.addData(data.datas!!)
                    //刷新页面
                   // integralAdapter?.notifyDataSetChanged()
                }
            })


        //请求数据
        initData()
    }


    private fun initData() {
        binding.slIntegralLayout.autoRefresh()
    }

    /**
     * 请求网络数据
     */
    fun getIntegralData(isMore: Boolean) {
        if (!isMore) {
            indexPosition = 1
        }
        viewModel.getIntegralList(indexPosition)
    }


    /**
     * 下拉刷新
     */
    override fun onRefresh(refreshLayout: RefreshLayout) {
        getIntegralData(false)
    }

    /**
     * 上拉加载
     */
    override fun onLoadMore(refreshLayout: RefreshLayout) {
        if (integralList.size < 20) {
            binding.slIntegralLayout.finishLoadMoreWithNoMoreData()
        } else {
            getIntegralData(true)
        }
    }
}