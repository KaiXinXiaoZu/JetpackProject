package com.yzhg.examples.ui.activity

import com.yzhg.examples.R
import com.yzhg.examples.databinding.ActivityDemoLayoutBinding
import com.yzhg.examples.demo.PageStatusActivity
import com.yzhg.study.base.ui.activity.BaseBindingActivity
import com.yzhg.toollibrary.common.ToastTools

class DemoActivity : BaseBindingActivity<ActivityDemoLayoutBinding>() {

    //  override fun getLayoutId(): Int  = R.layout.activity_demo_layout
//  5
    override fun initView() {
        binding.butPageState.setOnClickListener {
            startActivity(PageStatusActivity::class.java)
        }

        binding.edDemo.setOnEditorCallBackListener {
            ToastTools.showToast("搜索内容：$it")
        }
        binding.edDemo.setTextChangedListener {
            ToastTools.showToast("搜索内容：$it")
        }
        binding.edDemo.setBackFinishClickListener {
            ToastTools.showToast("点击了返回键")
        }

    }
}