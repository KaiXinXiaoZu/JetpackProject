package com.yzhg.examples.ui.fragment.system

import androidx.lifecycle.viewModelScope
import com.yzhg.examples.bean.system.SystemBean
import com.yzhg.examples.http.WanAPI
import com.yzhg.study.base.viewmodel.BaseViewModel
import com.yzhg.study.base.viewmodel.StateLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * 包名：com.yzhg.examples.ui.fragment.system
 * 创建人：yzhg
 * 时间：2021/06/22 18:33
 * 描述：
 */
class SystemModel : BaseViewModel() {

    /**
     * 创建LiveData
     */
    var stateLiveData: StateLiveData<MutableList<SystemBean>> = StateLiveData()

    /**
     * 创建Api
     */
    private var httpApi = createHttpApi<WanAPI>()

    /**
     * 请求体系数据
     */
    fun requestSystemData() {
        viewModelScope.launch(Dispatchers.IO) {
            executeRequest({ httpApi.getTreeJson() }, stateLiveData)
        }
    }
}