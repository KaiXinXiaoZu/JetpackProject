package com.yzhg.examples.ui.activity.integral

import androidx.lifecycle.viewModelScope
import com.yzhg.examples.bean.BaseDataBean
import com.yzhg.examples.bean.integral.IntegralBean
import com.yzhg.examples.http.WanAPI
import com.yzhg.study.base.viewmodel.BaseViewModel
import com.yzhg.study.base.viewmodel.CommonViewModel
import com.yzhg.study.base.viewmodel.StateLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * 包名：com.yzhg.examples.ui.activity.integral
 * 创建人：yzhg
 * 时间：2021/07/06 14:11
 * 描述：
 */
class IntegralModel : CommonViewModel() {


    var stateLiveData: StateLiveData<BaseDataBean<MutableList<IntegralBean>>> = StateLiveData()

    /**
     * 创建API
     */
    var httpApi3 = createHttpApi<WanAPI>()


    /**
     * 获取积分列表
     */
    fun getIntegralList(indexPage: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            executeRequest({ httpApi3.getIntegralList(indexPage) }, stateLiveData,indexPage)
        }
    }


}