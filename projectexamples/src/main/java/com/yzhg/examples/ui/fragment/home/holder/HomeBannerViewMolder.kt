package com.yzhg.examples.ui.fragment.home.holder

import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

/**
 * 包名：com.yzhg.examples.ui.fragment.home.holder
 * 创建人：yzhg
 * 时间：2021/06/23 10:47
 * 描述：
 */
class HomeBannerViewMolder(view: ImageView) : RecyclerView.ViewHolder(view) {


    public var imageView: ImageView = view

}