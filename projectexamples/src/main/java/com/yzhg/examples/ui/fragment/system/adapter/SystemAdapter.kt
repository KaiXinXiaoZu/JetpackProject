package com.yzhg.examples.ui.fragment.system.adapter

import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.jeremyliao.liveeventbus.LiveEventBus
import com.yzhg.examples.R
import com.yzhg.examples.bean.system.SystemBean
import com.yzhg.examples.bean.system.SystemChildrenBean
import com.yzhg.examples.content.Content
import com.yzhg.examples.databinding.ItemHomeBannerBinding
import com.yzhg.examples.databinding.ItemSystemFragmentBinding
import com.yzhg.examples.databinding.ItemSystemRecycleLayoutBinding
import com.yzhg.examples.event.EventBean
import com.yzhg.examples.tools.flowlayout.BaseTagAdapter
import com.yzhg.examples.tools.flowlayout.FlowLayout
import com.yzhg.examples.tools.flowlayout.TagFlowLayout
import com.yzhg.toollibrary.ShapeTools
import com.yzhg.toollibrary.Tools

/**
 * 包名：com.yzhg.examples.ui.fragment.system.adapter
 * 创建人：yzhg
 * 时间：2021/06/24 10:41
 * 描述：
 */
class SystemAdapter(systemList: MutableList<SystemBean>) :
    BaseQuickAdapter<SystemBean, BaseDataBindingHolder<ItemSystemFragmentBinding>>(R.layout.item_system_fragment, systemList) {

    override fun convert(holder: BaseDataBindingHolder<ItemSystemFragmentBinding>, item: SystemBean) {
        val dataBinding = holder.dataBinding
        if (dataBinding != null) {
            dataBinding.tvItemTitle.text = item.name ?: ""
            val systemChildren = item.children
            dataBinding.flItemLayout.adapter = object : BaseTagAdapter<SystemChildrenBean>(systemChildren) {
                override fun getView(parent: FlowLayout, position: Int, systemChildrenBean: SystemChildrenBean?): View {
                    val itemSystemRecycleLayoutBinding: ItemSystemRecycleLayoutBinding =
                        DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_system_recycle_layout, parent, false)
                    itemSystemRecycleLayoutBinding.systemChildrenBean = systemChildrenBean
                    val backgroundDrawable = ShapeTools.getBackgroundDrawable(
                        Tools.getColor(R.color.color_F5F5F5), Tools.getColor(R.color.color_F5F5F5),
                        1, 50.0F
                    )
                    itemSystemRecycleLayoutBinding.tvTagRecycle.background = backgroundDrawable
                    return itemSystemRecycleLayoutBinding.root
                }
            }
            dataBinding.flItemLayout.setOnTagClickListener(object : TagFlowLayout.OnTagClickListener {
                override fun onTagClick(view: View?, position: Int, parent: FlowLayout?): Boolean {
                    val cId: Int = systemChildren?.get(position)?.id ?: 0
                    if (cId == 0) {
                        //ID无效
                    } else {
                        //点击某一个  tag,将tag和 本对象传到Fragment
                        LiveEventBus.get(EventBean::class.java).post(EventBean<SystemBean>(Content.EVENT_SYSTEM_TAG, item, position))
                    }
                    return true
                }
            })
        }
    }
}