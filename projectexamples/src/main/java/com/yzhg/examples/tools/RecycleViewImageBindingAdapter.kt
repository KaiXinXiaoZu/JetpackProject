package com.yzhg.examples.tools

import android.graphics.Color
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.yzhg.toollibrary.Tools
import com.yzhg.toollibrary.image.GlideTools

/**
 * 包名：com.yzhg.examples.ui.fragment.mine.demo
 * 创建人：yzhg
 * 时间：2021/06/23 11:30
 * 描述：
 */
class RecycleViewImageBindingAdapter {

    companion object {

        @BindingAdapter("itemImage")
        @JvmStatic
        fun setImage(imageView: ImageView, imageUrl: String) {
            if (imageUrl.isNotEmpty()) {
                GlideTools.Builder(Tools.getContext())
                    .setImageView(imageView)
                    .setImagePath(imageUrl)
                    .onCreate()
            } else {
                imageView.setBackgroundColor(Color.BLACK)
            }
        }
    }
}