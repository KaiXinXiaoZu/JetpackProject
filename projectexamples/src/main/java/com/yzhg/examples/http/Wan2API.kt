package com.yzhg.examples.http

import com.yzhg.examples.bean.BaseDataBean
import com.yzhg.examples.bean.home.BannerBean
import com.yzhg.examples.bean.home.BringTopBean
import com.yzhg.examples.bean.integral.IntegralBean
import com.yzhg.examples.bean.login.LoginBean
import com.yzhg.examples.bean.login.PersonRankBean
import com.yzhg.examples.bean.system.SystemBean
import com.yzhg.study.http.bean.ResponseBean
import retrofit2.http.*

/**
 * 包名：com.yzhg.examples.http
 * 创建人：yzhg
 * 时间：2021/06/23 10:10
 * 描述：
 */
interface Wan2API {

    /**
     * 首页轮播图
     */
    @GET("banner/json")
    suspend fun getHomeBanner(): ResponseBean<MutableList<BannerBean>>

    /**
     * 获取首页顶置数据
     */
    @GET("article/top/json")
    suspend fun getTopBringData(): ResponseBean<MutableList<BringTopBean>>

    /**
     * 获取首页文章列表
     */
    @GET("article/list/{pageId}/json")
    suspend fun getArticleListMore(@Path("pageId") pageId: Int): ResponseBean<BaseDataBean<MutableList<BringTopBean>>>


    /**
     * 获取体系
     */
    @GET("tree/json")
    suspend fun getTreeJson(): ResponseBean<MutableList<SystemBean>>

    /**
     * 获取体系下的文章
     */
    @GET("article/list/{pageId}/json")
    suspend fun getTreeDetailJson(
        @Path("pageId") pageId: Int,
        @Query("cid") cid: Int,
    ): ResponseBean<BaseDataBean<MutableList<BringTopBean>>>

    /**
     * 登录数据
     */
    @FormUrlEncoded
    @POST("user/login")
    suspend fun userLogin(
        @Field("username") username: String,
        @Field("password") password: String,
    ): ResponseBean<LoginBean>

    /**
     * 获取积分信息 https://www.wanandroid.com/lg/coin/userinfo/json
     */
    @GET("lg/coin/userinfo/json")
    suspend fun getUserRankInfo(): ResponseBean<PersonRankBean>


    /**
     * 获取积分列表
     *
     * 地址： https://www.wanandroid.com//lg/coin/list/1/json
     */
    @GET("lg/coin/list/{pageId}/json")
    suspend fun getIntegralList(@Path("pageId") pageId: Int,): ResponseBean<BaseDataBean<MutableList<IntegralBean>>>

}















