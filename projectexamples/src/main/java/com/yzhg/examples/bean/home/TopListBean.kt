package com.yzhg.examples.bean.home

import com.google.gson.annotations.SerializedName

/**
 * 包名：com.yzhg.examples.bean.home
 * 创建人：yzhg
 * 时间：2021/08/05 10:30
 * 描述：
 */
class TopListBean(
    @SerializedName("href")
    val href: String?,
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("name")
    val name: String?,
)