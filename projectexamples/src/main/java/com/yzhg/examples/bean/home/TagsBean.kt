package com.yzhg.examples.bean.home
import com.google.gson.annotations.SerializedName


/**
 * 包名：com.yzhg.examples.bean.home
 * 创建人：yzhg
 * 时间：2021/06/23 16:55
 * 描述：
 */
data class TagsBean(
    @SerializedName("name")
    val name: String?,
    @SerializedName("url")
    val url: String?
)