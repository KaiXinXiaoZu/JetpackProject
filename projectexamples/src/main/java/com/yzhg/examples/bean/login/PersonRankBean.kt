package com.yzhg.examples.bean.login

import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * 包名：com.yzhg.examples.bean.login
 * 创建人：yzhg
 * 时间：2021/06/29 10:16
 * 描述：
 */
data class PersonRankBean(
    @SerializedName("coinCount")
    val coinCount: Int?,
    @SerializedName("rank")
    val rank: Int?,
    @SerializedName("userId")
    val userId: Int?,
    @SerializedName("username")
    val username: String?
) : Serializable