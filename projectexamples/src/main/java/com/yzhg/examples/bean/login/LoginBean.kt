package com.yzhg.examples.bean.login

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * 包名：com.yzhg.examples.bean.login
 * 创建人：yzhg
 * 时间：2021/06/28 17:12
 * 描述：
 */
data class LoginBean(

 //   @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id", typeAffinity = ColumnInfo.INTEGER)
    val uId: Int?,
    @SerializedName("admin")
    val admin: Boolean?,
    @SerializedName("chapterTops")
    val chapterTops: List<Any>?,
    @SerializedName("coinCount")
    val coinCount: Int?,
    @SerializedName("collectIds")
    val collectIds: List<Int>?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("nickname")
    val nickname: String?,
    @SerializedName("password")
    val password: String?,
    @SerializedName("publicName")
    val publicName: String?,
    @SerializedName("token")
    val token: String?,
    @SerializedName("type")
    val type: Int?,
    @SerializedName("username")
    val username: String?


) : Serializable {
    override fun toString(): String {
        return "LoginBean(uId=$uId, admin=$admin, chapterTops=$chapterTops, coinCount=$coinCount, collectIds=$collectIds, email=$email, icon=$icon, id=$id, nickname=$nickname, password=$password, publicName=$publicName, token=$token, type=$type, username=$username)"
    }
}