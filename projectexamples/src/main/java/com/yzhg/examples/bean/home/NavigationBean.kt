package com.yzhg.examples.bean.home
import com.google.gson.annotations.SerializedName


/**
 * 包名：com.yzhg.examples.bean.home
 * 创建人：yzhg
 * 时间：2021/08/05 10:28
 * 描述：首页功能导航
 */
data class NavigationBean(
    @SerializedName("topList")
    val topList: List<TopListBean>?
)
