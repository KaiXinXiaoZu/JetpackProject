package com.yzhg.examples.bean.home
import com.google.gson.annotations.SerializedName


/**
 * 包名：com.yzhg.examples.bean.home
 * 创建人：yzhg
 * 时间：2021/06/23 10:11
 * 描述：
 */
data class BannerBean(
    @SerializedName("desc")
    val desc: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("imagePath")
    val imagePath: String?,
    @SerializedName("isVisible")
    val isVisible: Int?,
    @SerializedName("order")
    val order: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("type")
    val type: Int?,
    @SerializedName("url")
    val url: String?
)