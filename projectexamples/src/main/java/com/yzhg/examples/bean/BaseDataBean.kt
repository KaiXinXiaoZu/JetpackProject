package com.yzhg.examples.bean

import com.google.gson.annotations.SerializedName


/**
 * 包名：com.yzhg.examples.bean
 * 创建人：yzhg
 * 时间：2021/06/23 14:02
 * 描述：
 */
data class BaseDataBean<T>(
    @SerializedName("curPage")
    val curPage: Int?,
    @SerializedName("datas")
    val datas: T?,
    @SerializedName("offset")
    val offset: Int?,
    @SerializedName("over")
    val over: Boolean?,
    @SerializedName("pageCount")
    val pageCount: Int?,
    @SerializedName("size")
    val size: Int?,
    @SerializedName("total")
    val total: Int?
)