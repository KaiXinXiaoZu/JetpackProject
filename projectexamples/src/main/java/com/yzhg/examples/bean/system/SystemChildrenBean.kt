package com.yzhg.examples.bean.system

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * 包名：com.yzhg.examples.bean.system
 * 创建人：yzhg
 * 时间：2021/06/24 10:43
 * 描述：
 */
data class SystemChildrenBean(
    @SerializedName("children")
    val children: List<Any>?,
    @SerializedName("courseId")
    val courseId: Int?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("order")
    val order: Int?,
    @SerializedName("parentChapterId")
    val parentChapterId: Int?,
    @SerializedName("userControlSetTop")
    val userControlSetTop: Boolean?,
    @SerializedName("visible")
    val visible: Int?
):Serializable