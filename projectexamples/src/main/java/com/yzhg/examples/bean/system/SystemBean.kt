package com.yzhg.examples.bean.system
import com.google.gson.annotations.SerializedName
import com.jeremyliao.liveeventbus.core.LiveEvent


/**
 * 包名：com.yzhg.examples.bean.system
 * 创建人：yzhg
 * 时间：2021/06/24 10:42
 * 描述：
 */
data class SystemBean(
    @SerializedName("children")
    val children: MutableList<SystemChildrenBean>?,
    @SerializedName("courseId")
    val courseId: Int?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("order")
    val order: Int?,
    @SerializedName("parentChapterId")
    val parentChapterId: Int?,
    @SerializedName("userControlSetTop")
    val userControlSetTop: Boolean?,
    @SerializedName("visible")
    val visible: Int?
):LiveEvent