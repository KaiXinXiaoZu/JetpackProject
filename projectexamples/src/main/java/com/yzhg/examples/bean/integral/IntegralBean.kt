package com.yzhg.examples.bean.integral
import com.google.gson.annotations.SerializedName


/**
 * 包名：com.yzhg.examples.bean.integral
 * 创建人：yzhg
 * 时间：2021/07/06 14:04
 * 描述：
 */
data class IntegralBean(
    @SerializedName("coinCount")
    val coinCount: Int?,
    @SerializedName("date")
    val date: Long?,
    @SerializedName("desc")
    val desc: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("reason")
    val reason: String?,
    @SerializedName("type")
    val type: Int?,
    @SerializedName("userId")
    val userId: Int?,
    @SerializedName("userName")
    val userName: String?
)