package com.yzhg.examples.bean.language

import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * 包名：com.yzhg.examples.bean.language
 * 创建人：yzhg
 * 时间：2021/07/07 10:44
 * 描述：
 */
data class LanguageBean(
    @SerializedName("languageCode")
    val languageCode: String?,
    @SerializedName("languageName")
    val languageName: String?,


    var isSelect: Boolean,
):Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LanguageBean

        if (languageCode != other.languageCode) return false
        if (languageName != other.languageName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = languageCode?.hashCode() ?: 0
        result = 31 * result + (languageName?.hashCode() ?: 0)
        return result
    }
}