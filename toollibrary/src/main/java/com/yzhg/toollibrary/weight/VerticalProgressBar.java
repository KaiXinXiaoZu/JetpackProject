package com.yzhg.toollibrary.weight;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.yzhg.toollibrary.R;


/*
 * 作者：yzhg
 * 时间：2021-12-07 07:59
 * 包名：shuini.yiande.projectdemo.example
 * 描述：
 */
public class VerticalProgressBar extends View {

    private Paint paint;// 画笔
    private int progress;// 进度值
    private int width;// 宽度值
    private int height;// 高度值

    private int radius = 4;

    public VerticalProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public VerticalProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VerticalProgressBar(Context context) {
        super(context);
        init();
    }

    private void init() {
        paint = new Paint();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //画布的高度和宽度
        width = getMeasuredWidth() - 1;// 得到布局设置的宽度值
        height = getMeasuredHeight() - 1;// 得到布局设置的高度值
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //画背景
        paint.setColor(getResources().getColor(R.color.color_DDDDDD));// 设置背景画笔颜色
        // canvas.drawRect(0, 0, width, height, paint);// 画背景矩形
        RectF rectF = new RectF(0, 0, width, height);
        canvas.drawRoundRect(rectF, radius, radius, paint);

        //画进度  #FF0000 10%    #F8EC22  40%    #DDDDDD
        if (this.progress <= 10) {
            paint.setColor(getResources().getColor(R.color.color_FF0000));// 设置进度画笔颜色
        } else if (this.progress <= 40) {
            paint.setColor(getResources().getColor(R.color.color_F8EC22));// 设置进度画笔颜色
        } else {
            paint.setColor(getResources().getColor(R.color.color_00FF00));// 设置进度画笔颜色
        }
        //canvas.drawRect(0, height - progress / 100f * height, width, height, paint);// 画矩形


        RectF rectF2 = new RectF(0, height - progress / 100f * height, width, height);
        canvas.drawRoundRect(rectF2, radius, radius, paint);


        super.onDraw(canvas);
    }

    /**
     * 设置progressbar进度
     */
    public void setProgress(int progress) {
        this.progress = progress;
        postInvalidate();
    }

    public void setRadius(int radius){
        this.radius = radius;
    }

    /**
     * 获取进度
     */
    public int getProgress() {
        return this.progress;
    }
}
