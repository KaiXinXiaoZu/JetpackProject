package com.yzhg.toollibrary.weight.edittext;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yzhg.toollibrary.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

/**
 * 编辑 : yzhg
 * 时间 ：21:21-12-25 21:21
 * 包名 : com.yzhg.toollibrary.weight
 * 描述 : 搜索封装  V1.0版本 : 2024-01-08
 */
public class SearchEditText extends LinearLayout {

    private Context context;

    public SearchEditText(Context context) {
        super(context, null);
        this.context = context;
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);
    }

    public SearchEditText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, 0);
        this.context = context;
        setOrientation(LinearLayout.VERTICAL);
        initAttributes(context, attrs);
        initView(context);
        setGravity(Gravity.CENTER_VERTICAL);
    }


    public SearchEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setOrientation(LinearLayout.VERTICAL);
        initAttributes(context, attrs);
        initView(context);
        setGravity(Gravity.CENTER_VERTICAL);
    }

    /**
     * 默认icon的宽高
     */
    private int DEFAULT_DP_18 = 18;
    private int DEFAULT_DP_12 = 12;
    private int DEFAULT_DP_Z_1 = -1;

    /**
     * 搜索图标的宽度
     */
    private int iconWidth;

    /**
     * 搜索图标的高度
     */
    private int iconHeight;

    private int iconHeightWidth;


    /**
     * 左侧icon
     */
    private ImageView ivLeftIcon;
    /**
     * 右侧的icon
     */
    private ImageView ivRightIcon;

    /**
     * 图标的方向
     */
    private int iconGravity = 0;

    /**
     * 文字的方向
     */
    private int textGravity = 0;

    /**
     * 左右侧图标点击区域
     */
    private LinearLayout llLeftIconView;
    private LinearLayout llRightIconView;

    /**
     * 输入框
     */
    private EditText etSearchConstant;

    /**
     * 删除图标
     */
    private LinearLayout llClearIcon;
    private ImageView ivClearIcon;
    /**
     * 左右搜索
     */
    private TextView leftSearchText;
    private TextView rightSearchText;
    private LinearLayout llLeftSearchText;
    private LinearLayout llRightSearchText;
    private ImageView ivBackIcon;
    private LinearLayout llBackIcon;

    /**
     * 左右icon边距
     */
    private int iconLeftMargins;
    private int iconRightMargins;
    private int iconLeftRightMargins;

    /**
     * 图标
     */
    private Drawable searchIcon;

    /**
     * 删除图标
     */
    private Drawable editClearIcon;
    private int editClearIconSize;

    /**
     * 是否显示删除图标 默认不显示
     */
    private boolean isShowClearIcon;

    /**
     * 输入框背景
     */
    private int editSolidColor;
    private int editStrokeColor;
    /**
     * 边框的宽度
     */
    private int editStrokeWidth;
    /**
     * 四个角的弧度
     */
    private float editRadius;
    private float topLeftRadius;
    private float topRightRadius;
    private float bottomLeftRadius;
    private float bottomRightRadius;
    /**
     * 输入框的高度
     */
    private int editHeight;
    /**
     * 输入框的字体大小  文字颜色  hint的颜色  文本的长度
     */
    private int seEditTextSize;
    private int editTextColor;
    private int editTextHintColor;
    private int editTextLength;
    /**
     * 提示文本
     */
    private String editTextHintString;

    /**
     * 是否设置软键盘未搜索按键
     */
    private int editImeOptions;
    /**
     * 是否动态监听输入框 默认为true
     */
    private boolean editInputMonitor;
    /**
     * 设置左右padding
     */
    private int editLeftPadding;
    private int editRightPadding;

    private String searchTextString;

    /**
     * 搜索文字大小
     */
    private int searchTextSize;

    /**
     * 搜索字体颜色
     */
    private int searchTextColor;

    /**
     * 搜索文字背景颜色
     */
    private int searchSolidColor;
    private int searchStarColor;
    private int searchEndColor;
    /**
     * 搜索字体的圆角设置
     */
    private float searchRadius;
    private float searchTopLeftRadius;
    private float searchTopRightRadius;
    private float searchBottomLeftRadius;
    private float searchBottomRightRadius;

    /**
     * 搜索框的颜色和高度
     */
    private int searchStrokeColor;
    private float searchStrokeWidth;

    /**
     * 左右margin
     */
    private int searchLeftRightMargin;
    private int searchLeftMargin;
    private int searchRightMargin;

    private int searchLeftRightPadding;
    private int searchLeftPadding;
    private int searchRightPadding;

    /**
     * 搜索上下内边距
     */
    private int searchTopBotPadding;

    /**
     * 搜索文字的位置
     */
    private int searchGravity = 2;

    private Drawable backIcon;
    //设置左右间距
    private int backIconLeftRightPadding;

    private boolean backIconIsShow;

    /**
     * @param context
     * @param attrs
     */
    private void initAttributes(Context context, AttributeSet attrs) {
        TypedArray toolbarArray = context.obtainStyledAttributes(attrs, R.styleable.SearchEditText);
        //设置icon的宽高
        iconHeightWidth = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seIconHeightWidth, dip2px(DEFAULT_DP_18));
        //设置icon的宽高
        iconHeight = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seIconHeight, iconHeightWidth);
        //设置icon的宽度
        iconWidth = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seIconWidth, iconHeightWidth);

        //icon的类型
        iconGravity = toolbarArray.getInt(R.styleable.SearchEditText_seIconGravity, 2);
        //文字的方向
        textGravity = toolbarArray.getInt(R.styleable.SearchEditText_seTextGravity, 0);
        //icon左右边距
        iconLeftRightMargins = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seIconLeftRightMargins, dip2px(DEFAULT_DP_12));
        iconLeftMargins = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seIconLeftMargins, iconLeftRightMargins);
        iconRightMargins = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seIconRightMargins, iconLeftRightMargins);

        //设置icon的图标 getDrawable
        searchIcon = toolbarArray.getDrawable(R.styleable.SearchEditText_seSearchIcon);

        //输入框的背景
        editSolidColor = toolbarArray.getColor(R.styleable.SearchEditText_seEditSolidColor, Color.parseColor("#EEEEEE"));
        //输入框边框颜色
        editStrokeColor = toolbarArray.getColor(R.styleable.SearchEditText_seEditStrokeColor, 0);
        //边框的宽度
        editStrokeWidth = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seEditStrokeWidth, 0);
        editRadius = toolbarArray.getDimension(R.styleable.SearchEditText_seEditRadius, dip2px(30));
        topLeftRadius = toolbarArray.getDimension(R.styleable.SearchEditText_seTopLeftRadius, editRadius);
        topRightRadius = toolbarArray.getDimension(R.styleable.SearchEditText_seTopRightRadius, editRadius);
        bottomLeftRadius = toolbarArray.getDimension(R.styleable.SearchEditText_seBottomLeftRadius, editRadius);
        bottomRightRadius = toolbarArray.getDimension(R.styleable.SearchEditText_seBottomRightRadius, editRadius);
        //输入框的高度
        editHeight = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seEditHeight, dip2px(40));
        //设置字体的颜色
        seEditTextSize = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seEditTextSize, dip2px(14));
        editTextColor = toolbarArray.getColor(R.styleable.SearchEditText_seEditTextColor, Color.parseColor("#333333"));
        editTextHintColor = toolbarArray.getColor(R.styleable.SearchEditText_seEditTextHintColor, Color.parseColor("#999999"));
        editTextLength = toolbarArray.getInt(R.styleable.SearchEditText_seEditTextLength, 12);
        editTextHintString = toolbarArray.getString(R.styleable.SearchEditText_seEditTextHintString);
        editImeOptions = toolbarArray.getInt(R.styleable.SearchEditText_seEditImeOptions, -1);
        editInputMonitor = toolbarArray.getBoolean(R.styleable.SearchEditText_seEditInputMonitor, true);
        //EditText 左右padding
        editLeftPadding = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seEditLeftPadding, 0);
        editRightPadding = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seEditRightPadding, 0);
        //删除图标
        editClearIcon = toolbarArray.getDrawable(R.styleable.SearchEditText_seEditClearIcon);
        editClearIconSize = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seEditClearIconSize, dip2px(16));
        isShowClearIcon = toolbarArray.getBoolean(R.styleable.SearchEditText_seIsShowClearIcon, false);


        //搜索文字大小
        searchTextString = toolbarArray.getString(R.styleable.SearchEditText_seSearchTextString);
        searchTextSize = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seSearchTextSize, dip2px(14));
        searchTextColor = toolbarArray.getColor(R.styleable.SearchEditText_seSearchTextColor, Color.parseColor("#333333"));
        //背景颜色
        searchSolidColor = toolbarArray.getColor(R.styleable.SearchEditText_seSearchSolidColor, 0);
        searchStarColor = toolbarArray.getColor(R.styleable.SearchEditText_seSearchStarColor, searchSolidColor);
        searchEndColor = toolbarArray.getColor(R.styleable.SearchEditText_seSearchEndColor, searchSolidColor);
        //圆角
        searchRadius = toolbarArray.getDimension(R.styleable.SearchEditText_seSearchRadius, 0);
        searchTopLeftRadius = toolbarArray.getDimension(R.styleable.SearchEditText_seSearchTopLeftRadius, searchRadius);
        searchTopRightRadius = toolbarArray.getDimension(R.styleable.SearchEditText_seSearchTopRightRadius, searchRadius);
        searchBottomLeftRadius = toolbarArray.getDimension(R.styleable.SearchEditText_seSearchBottomLeftRadius, searchRadius);
        searchBottomRightRadius = toolbarArray.getDimension(R.styleable.SearchEditText_seSearchBottomRightRadius, searchRadius);
        searchStrokeColor = toolbarArray.getColor(R.styleable.SearchEditText_seSearchStrokeColor, Color.parseColor("#FFFFFF"));
        searchStrokeWidth = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seSearchStrokeWidth, 0);
        //搜索文本左右的margin
        searchLeftRightMargin = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seSearchLeftRightMargin, dip2px(DEFAULT_DP_12));
        searchLeftMargin = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seSearchLeftMargin, searchLeftRightMargin);
        searchRightMargin = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seSearchRightMargin, searchLeftRightMargin);

        searchLeftRightPadding = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seSearchLeftRightPadding, 0);
        searchLeftPadding = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seSearchLeftPadding, searchLeftRightPadding);
        searchRightPadding = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seSearchRightPadding, searchLeftRightPadding);
        searchTopBotPadding = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seSearchTopBotPadding, 4);

        searchGravity = toolbarArray.getInt(R.styleable.SearchEditText_seSearchGravity, 1);


        backIcon = toolbarArray.getDrawable(R.styleable.SearchEditText_seBackIcon);
        //左右间距
        backIconLeftRightPadding = toolbarArray.getDimensionPixelSize(R.styleable.SearchEditText_seBackIconLeftRightPadding, dip2px(DEFAULT_DP_12));
        //是否显示左侧的返回
        backIconIsShow = toolbarArray.getBoolean(R.styleable.SearchEditText_seBackIconIsShow, true);
        toolbarArray.recycle();
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    private void initView(Context context) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_edittext_customer_layout, null);
        //如果图标在中间 则文字只能居中
        if (iconGravity == 4) {
            textGravity = 2;
        }
        //设置icon的宽高
        ivLeftIcon = inflate.findViewById(R.id.ivLeftIcon);
        ivRightIcon = inflate.findViewById(R.id.ivRightIcon);
        //左右侧图标点击区域
        llLeftIconView = inflate.findViewById(R.id.llLeftIconView);
        llRightIconView = inflate.findViewById(R.id.llRightIconView);
        //edittext
        etSearchConstant = inflate.findViewById(R.id.etSearchConstant);
        llClearIcon = inflate.findViewById(R.id.llClearIcon);
        ivClearIcon = inflate.findViewById(R.id.ivClearIcon);
        leftSearchText = inflate.findViewById(R.id.tvLeftSearchText);
        rightSearchText = inflate.findViewById(R.id.tvRightSearchText);
        llLeftSearchText = inflate.findViewById(R.id.llLeftSearchText);
        llRightSearchText = inflate.findViewById(R.id.llRightSearchText);
        ivBackIcon = inflate.findViewById(R.id.ivBackIcon);
        llBackIcon = inflate.findViewById(R.id.llBackIcon);


        if (TextUtils.isEmpty(editTextHintString)) {
            editTextHintString = "请输入搜索内容";
        }
        if (TextUtils.isEmpty(searchTextString)) {
            searchTextString = "搜索";
        }
        //设置默认的搜索图标
        if (searchIcon == null) {
            searchIcon = context.getResources().getDrawable(R.mipmap.img_search_icon);
        }
        if (backIcon == null) {
            backIcon = context.getResources().getDrawable(R.mipmap.back_finish);
        }


        setLeftBackIconView();
        //设置清除view
        setClearView();
        //设置左右icon
        setLeftRightIconView();
        //设置editText内部左右的icon
        setEditTextLeftRightIconView(context);
        //设置图标的方向
        setIconGravity();
        //设置文字的方向
        setTextGravity();
        //设置输入框的操作
        setEditTextView();
        //设置左右搜索文字
        setSearchTextView();

        addView(inflate);
    }

    private void setLeftBackIconView() {
        ivBackIcon.setImageDrawable(backIcon);
        //设置左右padding
        llBackIcon.setPadding(backIconLeftRightPadding, llBackIcon.getPaddingTop(), backIconLeftRightPadding, llBackIcon.getPaddingBottom());
        //设置左侧的返回
        if (backIconIsShow) {
            llBackIcon.setVisibility(View.VISIBLE);
        } else {
            llBackIcon.setVisibility(View.GONE);
        }
        //设置左侧返回按键的点击事件
        llBackIcon.setOnClickListener(view -> {
            if (backFinishClickListener != null) {
                backFinishClickListener.bankFinish(ivBackIcon);
            } else {
                ((Activity) context).finish();
            }
        });
    }

    private void setSearchTextView() {
        leftSearchText.setText(searchTextString);
        rightSearchText.setText(searchTextString);
        //设置文字大小
        leftSearchText.setTextSize(TypedValue.COMPLEX_UNIT_PX, searchTextSize);
        rightSearchText.setTextSize(TypedValue.COMPLEX_UNIT_PX, searchTextSize);
        //文字颜色
        leftSearchText.setTextColor(searchTextColor);
        rightSearchText.setTextColor(searchTextColor);

        //设置背景渐变
        GradientDrawable backgroundDrawable2 = getBackgroundDrawable2(
                searchStarColor,
                searchEndColor,
                GradientDrawable.Orientation.LEFT_RIGHT,
                searchStrokeColor,
                (int) searchStrokeWidth,
                searchTopLeftRadius,
                searchTopRightRadius,
                searchBottomRightRadius,
                searchBottomLeftRadius
        );
        leftSearchText.setBackground(backgroundDrawable2);
        rightSearchText.setBackground(backgroundDrawable2);

        LinearLayout.LayoutParams leftSearchLayoutParams = (LayoutParams) leftSearchText.getLayoutParams();
        LinearLayout.LayoutParams rightSearchLayoutParams = (LayoutParams) rightSearchText.getLayoutParams();

        leftSearchLayoutParams.setMargins(searchLeftMargin, 0, searchRightMargin, 0);
        rightSearchLayoutParams.setMargins(searchLeftMargin, 0, searchRightMargin, 0);

        //设置内边距
        leftSearchText.setPadding(searchLeftPadding, searchTopBotPadding, searchRightPadding, searchTopBotPadding);
        rightSearchText.setPadding(searchLeftPadding, searchTopBotPadding, searchRightPadding, searchTopBotPadding);

        if (searchGravity == 0) {
            leftSearchText.setVisibility(View.VISIBLE);
            rightSearchText.setVisibility(View.GONE);
        } else if (searchGravity == 1) {
            leftSearchText.setVisibility(View.GONE);
            rightSearchText.setVisibility(View.VISIBLE);
        } else if (searchGravity == 2) {
            leftSearchText.setVisibility(View.GONE);
            rightSearchText.setVisibility(View.GONE);
        }

        llLeftSearchText.setOnClickListener(view -> {
            if (onEditorCallBackListener != null) {
                String searchConstant = etSearchConstant.getText().toString().trim();
                onEditorCallBackListener.editorResult(searchConstant);
            }
        });
        llRightSearchText.setOnClickListener(view -> {
            if (onEditorCallBackListener != null) {
                String searchConstant = etSearchConstant.getText().toString().trim();
                onEditorCallBackListener.editorResult(searchConstant);
            }
        });
    }

    private void setClearView() {
        //设置删除图标
        ivClearIcon.setImageDrawable(editClearIcon);
        LinearLayout.LayoutParams layoutParams = (LayoutParams) ivClearIcon.getLayoutParams();
        layoutParams.height = editClearIconSize;
        layoutParams.width = editClearIconSize;
        ivClearIcon.setLayoutParams(layoutParams);
        llClearIcon.setOnClickListener(view -> {
            etSearchConstant.setText("");
        });
        //如果在内右侧默认不显示
        if (iconGravity != 3) {
            llClearIcon.setVisibility(isShowClearIcon ? View.VISIBLE : View.GONE);
        }
    }

    private void setEditTextView() {
        etSearchConstant.setBackground(getBackgroundDrawable(
                editSolidColor,
                editStrokeColor,
                editStrokeWidth,
                topLeftRadius,
                topRightRadius,
                bottomLeftRadius,
                bottomRightRadius));

        //设置输入框的高度
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) etSearchConstant.getLayoutParams();
        layoutParams.height = editHeight;
        etSearchConstant.setLayoutParams(layoutParams);

        //输入框的字体大小，颜色 和 长度
        if (iconGravity != 4) {
            etSearchConstant.setHint(editTextHintString);
        }
        etSearchConstant.setTextSize(TypedValue.COMPLEX_UNIT_PX, seEditTextSize);
        etSearchConstant.setTextColor(editTextColor);
        etSearchConstant.setHintTextColor(editTextHintColor);
        etSearchConstant.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editTextLength)});

        //设置软键盘未搜索
        if (editImeOptions >= 0) {
            if (editImeOptions == 0) {
                etSearchConstant.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
                etSearchConstant.setMaxLines(1);
                etSearchConstant.setSingleLine(true);
            }

            //设置软键盘点击事件
            etSearchConstant.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (onEditorCallBackListener != null) {
                        String searchConstant = etSearchConstant.getText().toString().trim();
                        onEditorCallBackListener.editorResult(searchConstant);
                    }
                    return true;
                }
                return false;
            });
        }

        if (editInputMonitor) {
            etSearchConstant.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String searchConstant = s.toString();
                    if (textChangedListener != null) {
                        textChangedListener.afterTextChanged(searchConstant);
                    }
                }
            });
        }
    }

    /**
     * 设置editText左右的icon
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    private void setEditTextLeftRightIconView(Context context) {
        //llClearIcon.setVisibility(View.VISIBLE);
        if (iconGravity == 2) {
            searchIcon.setBounds(0, 0, iconWidth, iconHeight);
            etSearchConstant.setCompoundDrawablePadding(iconRightMargins);
            //  etSearchConstant.setPadding(iconLeftMargins + editLeftPadding, etSearchConstant.getPaddingTop(), etSearchConstant.getPaddingRight(), etSearchConstant.getPaddingBottom());
            etSearchConstant.setPadding(iconLeftMargins + editLeftPadding, 0, 0, 0);
            etSearchConstant.setCompoundDrawables(searchIcon, null, null, null);
        }
        if (iconGravity == 3) {
            llClearIcon.setVisibility(View.GONE);
            searchIcon.setBounds(0, 0, iconWidth, iconHeight);
            etSearchConstant.setCompoundDrawablePadding(iconLeftMargins);
            // etSearchConstant.setPadding(etSearchConstant.getPaddingLeft(), etSearchConstant.getPaddingTop(), iconRightMargins + editRightPadding, etSearchConstant.getPaddingBottom());
            etSearchConstant.setPadding(0, 0, iconRightMargins + editRightPadding, 0);
            etSearchConstant.setCompoundDrawables(null, null, searchIcon, null);
        } else if (iconGravity == 1 || iconGravity == 0 || iconGravity == 5) {
            etSearchConstant.setPadding(editLeftPadding, 0, editRightPadding, 0);
        }
    }

    private void setLeftRightIconView() {
        //searchIcon
        if (searchIcon != null) {
            ivLeftIcon.setImageDrawable(searchIcon);
            ivRightIcon.setImageDrawable(searchIcon);
        }

        //设置图标的宽高
        LayoutParams leftIconLayoutParams = (LayoutParams) ivLeftIcon.getLayoutParams();
        LayoutParams rightIconLayoutParams = (LayoutParams) ivRightIcon.getLayoutParams();

        //设置icon的高度
        leftIconLayoutParams.height = iconHeight;
        leftIconLayoutParams.width = iconWidth;


        //设置右侧icon的高度
        rightIconLayoutParams.height = iconHeight;
        rightIconLayoutParams.width = iconWidth;

        //设置点击区域的边距
        leftIconLayoutParams.setMargins(iconLeftMargins, 0, iconRightMargins, 0);
        rightIconLayoutParams.setMargins(iconLeftMargins, 0, iconRightMargins, 0);


        ivLeftIcon.setLayoutParams(leftIconLayoutParams);
        ivRightIcon.setLayoutParams(rightIconLayoutParams);
    }

    /**
     * 设置图标的方向
     */
    private void setIconGravity() {
        setDefaultIconGravity();
        if (iconGravity == 0) {
            //图标在外左侧
            llLeftIconView.setVisibility(View.VISIBLE);
        } else if (iconGravity == 1) {
            //图标在外右侧
            llRightIconView.setVisibility(View.VISIBLE);
        } else if (iconGravity == 2) {
            //图标在左内侧
        } else if (iconGravity == 3) {
            //图标在右内侧
        } else if (iconGravity == 4) {
            //图标据中
            if (editTextHintString.length() > 0) {
                editTextHintString = "占位\u0020" + editTextHintString;
            }
            SpannableString sp = new SpannableString(editTextHintString);
            //拿到所加图片的drawable对象
            searchIcon.setBounds(0, 0, iconWidth, iconHeight);
            //给SpannableString设置ImageSpan
            CenterAlignImageSpan imageSpan = new CenterAlignImageSpan(searchIcon);
            sp.setSpan(imageSpan, 0, 2, ImageSpan.ALIGN_BASELINE);
            etSearchConstant.setHint(sp);
        } else if (iconGravity == 5) {
            //不显示icon
        }
    }


    private void setDefaultIconGravity() {
        llLeftIconView.setVisibility(View.GONE);
        llRightIconView.setVisibility(View.GONE);
    }

    /**
     * 设置文字的方向
     */
    private void setTextGravity() {
        if (textGravity == 0) {
            //文字在左侧 ，如果图标在中间则只能居中
            etSearchConstant.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        } else if (textGravity == 1) {
            //文字在右侧
            etSearchConstant.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        } else if (textGravity == 2) {
            //文字居中
            etSearchConstant.setGravity(Gravity.CENTER);
        }
    }


    /**
     * 1、2两个参数表示左上角，3、4表示右上角，5、6表示右下角，7、8表示左下角
     *
     * @param solidColor        背景色
     * @param strokeColor       边框颜色
     * @param strokeWidth       边框宽度
     * @param topLeftRadius     左上方
     * @param topRightRadius    右上方
     * @param bottomRightRadius 右下方
     * @param bottomLeftRadius  左下方
     * @return 资源
     */
    public static GradientDrawable getBackgroundDrawable(int solidColor, int strokeColor, int strokeWidth, float topLeftRadius, float topRightRadius
            , float bottomRightRadius, float bottomLeftRadius) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(solidColor);
        drawable.setStroke(strokeWidth, strokeColor);
        drawable.setCornerRadii(new float[]{
                topLeftRadius,
                topLeftRadius,
                topRightRadius,
                topRightRadius,
                bottomRightRadius,
                bottomRightRadius,
                bottomLeftRadius,
                bottomLeftRadius});
        return drawable;
    }

    public static GradientDrawable getBackgroundDrawable2(
            int startColor,
            int endColor,
            GradientDrawable.Orientation orientation,
            int strokeColor,
            int strokeWidth,
            float topLeftRadius,
            float topRightRadius,
            float bottomRightRadius,
            float bottomLeftRadius) {
        int[] colors = {startColor, endColor};
        GradientDrawable drawable = new GradientDrawable(orientation, colors);
        drawable.setStroke(strokeWidth, strokeColor);
        drawable.setCornerRadii(new float[]{
                topLeftRadius,
                topLeftRadius,
                topRightRadius,
                topRightRadius,
                bottomRightRadius,
                bottomRightRadius,
                bottomLeftRadius,
                bottomLeftRadius});
        return drawable;
    }

    public float px2dip(float px) {
        float density = getResources().getDisplayMetrics().density;
        return px / density;

    }

    /**
     * dp 与 px之间的转换
     */
    public int dip2px(float dip) {
        //获取屏幕的密度
        float density = getResources().getDisplayMetrics().density;
        return (int) (dip * density + 0.5f);
    }


    /**
     * 获取布局的View
     */
    /**
     * 获取输入框
     */
    public EditText getEditText() {
        return etSearchConstant;
    }

    /**
     * 获取右侧的搜索文本
     */
    public TextView getRightSearchText() {
        return rightSearchText;
    }

    public TextView getLeftSearchText() {
        return leftSearchText;
    }

    /**
     * 获取左右侧图标
     */
    public ImageView getRightSearchIcon() {
        return ivRightIcon;
    }

    public ImageView getLeftSearchIcon() {
        return ivLeftIcon;
    }

    /**
     * 返回图标
     */
    public ImageView getBackIcon() {
        return ivBackIcon;
    }

    /**
     * 设置EditText的 hint和text
     */
    public void setEditTextHintString(String hintString) {
        etSearchConstant.setHint(hintString);
    }

    /**
     * 设置EditText的 Text
     */
    public void setEditTextString(String searchText) {
        etSearchConstant.setText(searchText);
    }



    private OnEditorCallBackListener onEditorCallBackListener;

    /**
     * 软键盘搜索按钮点击事件
     */
    public interface OnEditorCallBackListener {
        void editorResult(String message);
    }

    public void setOnEditorCallBackListener(OnEditorCallBackListener onEditorCallBackListener) {
        this.onEditorCallBackListener = onEditorCallBackListener;
    }

    private TextChangedListener textChangedListener;

    public interface TextChangedListener {
        void afterTextChanged(String message);
    }

    public void setTextChangedListener(TextChangedListener textChangedListener) {
        this.textChangedListener = textChangedListener;
    }

    private BackFinishClickListener backFinishClickListener;

    public void setBackFinishClickListener(BackFinishClickListener backFinishClickListener) {
        this.backFinishClickListener = backFinishClickListener;
    }

    //设置返回按键的点击事件
    public interface BackFinishClickListener {
        void bankFinish(View view);
    }


    class CenterAlignImageSpan extends ImageSpan {
        public CenterAlignImageSpan(Drawable drawable) {
            super(drawable);

        }

        @Override
        public void draw(@NonNull Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom,
                         @NonNull Paint paint) {

            Drawable b = getDrawable();
            Paint.FontMetricsInt fm = paint.getFontMetricsInt();
            int transY = (y + fm.descent + y + fm.ascent) / 2 - b.getBounds().bottom / 2;//计算y方向的位移
            canvas.save();
            canvas.translate(x, transY);//绘制图片位移一段距离
            b.draw(canvas);
            canvas.restore();
        }
    }
}


/*

    <declare-styleable name="SearchEditText">
        <!--设置icon的高度 search-->
        <attr name="seIconHeight" format="dimension" />
        <!--设置icon的宽度-->
        <attr name="seIconWidth" format="dimension" />
        <!--设置icon的宽高-->
        <attr name="seIconHeightWidth" format="dimension" />
        <!--设置icon距离左侧的距离-->
        <attr name="seIconLeftMargins" format="dimension" />
        <!--设置icon右侧的距离-->
        <attr name="seIconRightMargins" format="dimension" />
        <!--设置icon的左右边距-->
        <attr name="seIconLeftRightMargins" format="dimension" />
        <!--设置搜索图标-->
        <attr name="seSearchIcon" format="reference" />
        <!--设置输入框的背景颜色-->
        <attr name="seEditSolidColor" format="color" />
        <!--这是输入框的边框颜色-->
        <attr name="seEditStrokeColor" format="color" />
        <!--设置输入框的高度-->
        <attr name="seEditStrokeWidth" format="dimension" />
        <!--输入框的圆角-->
        <attr name="seEditRadius" format="dimension" />
        <!--四个角的圆角弧度-->
        <attr name="seTopLeftRadius" format="dimension" />
        <attr name="seTopRightRadius" format="dimension" />
        <attr name="seBottomLeftRadius" format="dimension" />
        <attr name="seBottomRightRadius" format="dimension" />
        <!--输入框的高度-->
        <attr name="seEditHeight" format="dimension" />
        <!--editText字体颜色和大小-->
        <attr name="seEditTextHintString" format="string" />
        <attr name="seEditTextSize" format="dimension" />
        <attr name="seEditTextColor" format="color" />
        <attr name="seEditTextHintColor" format="color" />
        <!--字数限制-->
        <attr name="seEditTextLength" format="integer" />
        <!--软键盘右下角变成搜索框-->
        <attr name="seEditImeOptions" format="enum">
            <enum name="search" value="0" />
        </attr>
        <!--输入及时出结果-->
        <!--输入监听,默认为true一般不用设置-->
        <attr name="seEditInputMonitor" format="boolean" />
        <!--设置EditText左右的内边距-->
        <attr name="seEditLeftPadding" format="dimension" />
        <attr name="seEditRightPadding" format="dimension" />

        <!--搜索框内带叉号，用于清除数据,如果搜索图标在右侧则不显示-->
        <attr name="seEditClearIcon" format="reference" />
        <attr name="seEditClearIconSize" format="dimension" />
        <!--是否显示删除图标-->
        <attr name="seIsShowClearIcon" format="boolean" />

        <!--搜索框外部 带文字，文字需要带背景颜色-->
        <!--搜索文字的位置，左侧和右侧-->
        <attr name="seSearchTextString" format="string" />
        <!--搜索文字的大小-->
        <attr name="seSearchTextSize" format="dimension" />
        <!--搜索文字的颜色-->
        <attr name="seSearchTextColor" format="color" />
        <!--搜索文字的背景，支持渐变-->
        <attr name="seSearchSolidColor" format="color" />
        <attr name="seSearchStarColor" format="color" />
        <attr name="seSearchEndColor" format="color" />
        <!--搜索文字的背景圆角-->
        <!--输入框的圆角-->
        <attr name="seSearchRadius" format="dimension" />
        <!--四个角的圆角弧度-->
        <attr name="seSearchTopLeftRadius" format="dimension" />
        <attr name="seSearchTopRightRadius" format="dimension" />
        <attr name="seSearchBottomLeftRadius" format="dimension" />
        <attr name="seSearchBottomRightRadius" format="dimension" />
        <!--这是搜索文字的边框颜色-->
        <attr name="seSearchStrokeColor" format="color" />
        <!--设置搜索文字边框的高度-->
        <attr name="seSearchStrokeWidth" format="dimension" />

        <!--文本左右padding-->
        <attr name="seSearchLeftPadding" format="dimension" />
        <attr name="seSearchRightPadding" format="dimension" />
        <attr name="seSearchLeftRightPadding" format="dimension" />
        <!--文本左右margin-->
        <attr name="seSearchLeftMargin" format="dimension" />
        <attr name="seSearchRightMargin" format="dimension" />
        <attr name="seSearchLeftRightMargin" format="dimension" />

        <!--文本上下边距-->
        <attr name="seSearchTopBotPadding" format="dimension" />


        <!--左右返回按键的图标-->
        <attr name="seBackIcon" format="reference" />
        <!--左侧返回按键的左右间距-->
        <attr name="seBackIconLeftRightPadding" format="dimension" />
        <!--是否显示左侧返回按钮-->
        <attr name="seBackIconIsShow" format="boolean"/>

        <!--显示那边的搜索-->
        <attr name="seSearchGravity" format="enum">
            <enum name="left_gravity" value="0" />
            <enum name="right_gravity" value="1" />
            <enum name="no_search" value="2" />
        </attr>


        <!--图标的类型-->
        <attr name="seIconGravity" format="enum">
            <!--图标在外左侧-->
            <enum name="left_margin" value="0" />
            <!--图标在外右侧-->
            <enum name="right_margin" value="1" />
            <!--图标在左内侧-->
            <enum name="left_padding" value="2" />
            <!--图标在右内侧-->
            <enum name="right_padding" value="3" />
            <!--图标据中-->
            <enum name="icon_center" value="4" />
            <!--不显示icon-->
            <enum name="no_icon" value="5" />
        </attr>
        <!--文字的方向-->
        <attr name="seTextGravity" format="enum">
            <!--文字在左侧-->
            <enum name="left_text" value="0" />
            <!--文字在右侧-->
            <enum name="right_text" value="1" />
            <!--文字居中-->
            <enum name="center_text" value="2" />
        </attr>


    </declare-styleable>
 */


/*
布局：
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:gravity="center_vertical">


    <LinearLayout
        android:id="@+id/llBackIcon"
        android:layout_width="wrap_content"
        android:layout_height="match_parent"
        android:layout_gravity="center"
        android:gravity="center">

        <ImageView
            android:id="@+id/ivBackIcon"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:src="@mipmap/back_finish" />

    </LinearLayout>


    <LinearLayout
        android:id="@+id/llLeftSearchText"
        android:layout_width="wrap_content"
        android:layout_height="match_parent"
        android:gravity="center">

        <TextView
            android:id="@+id/tvLeftSearchText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="搜索" />
    </LinearLayout>


    <LinearLayout
        android:id="@+id/llLeftIconView"
        android:layout_width="wrap_content"
        android:layout_height="match_parent"
        android:gravity="center">

        <ImageView
            android:id="@+id/ivLeftIcon"
            android:layout_width="@dimen/dp_18"
            android:layout_height="@dimen/dp_18"
            android:background="@mipmap/img_search_icon" />

    </LinearLayout>


    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_weight="1">

        <EditText
            android:id="@+id/etSearchConstant"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:gravity="center_vertical"
            app:layout_constraintLeft_toLeftOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            tools:hint="请输入搜索内容" />

        <LinearLayout
            android:id="@+id/llClearIcon"
            android:layout_width="@dimen/dp_28"
            android:layout_height="@dimen/dp_28"
            android:layout_marginEnd="@dimen/dp_12"
            android:gravity="center"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintRight_toRightOf="parent"
            app:layout_constraintTop_toTopOf="parent">

            <ImageView
                android:id="@+id/ivClearIcon"
                android:layout_width="@dimen/dp_16"
                android:layout_height="@dimen/dp_16"
                android:background="@mipmap/img_clear_edittext_icon" />

        </LinearLayout>


    </androidx.constraintlayout.widget.ConstraintLayout>


    <LinearLayout
        android:id="@+id/llRightIconView"
        android:layout_width="wrap_content"
        android:layout_height="match_parent"
        android:gravity="center">

        <ImageView
            android:id="@+id/ivRightIcon"
            android:layout_width="@dimen/dp_18"
            android:layout_height="@dimen/dp_18"
            android:background="@mipmap/img_search_icon" />

    </LinearLayout>


    <LinearLayout
        android:id="@+id/llRightSearchText"
        android:layout_width="wrap_content"
        android:layout_height="match_parent"
        android:gravity="center">

        <TextView
            android:id="@+id/tvRightSearchText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="搜索" />
    </LinearLayout>


</LinearLayout>

 */
