package com.yzhg.toollibrary.weight;

import android.content.Context;
import android.os.Environment;

import com.wsdz.tools.common.LogTools;
import com.yzhg.toollibrary.Tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

/*
 * 作者：yzhg
 * 时间：2021-12-11 14:15
 * 包名：com.yzhg.toollibrary.weight
 * 描述：


public static File ExistSDCardMkdirs(Context context, String path) {
        return context.(Environment.DIRECTORY_PICTURES);
        }

 */
public class CacheUtils {


    /**
     * 存储Json文件
     *
     * @param json     json字符串
     * @param fileName 存储的文件名
     */
    public static boolean writeJson(String json, String address, String fileName) {
        FileOutputStream out = null;
        try {
            File dir = new File(address);
            //创建文件
            dir.mkdirs();
            out = new FileOutputStream(dir + "/" + fileName);
            out.write(json.getBytes());
            return true;
        } catch (IOException e) {
            LogTools.INSTANCE.e("保存失败" + e.getMessage());
            return false;
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * 从本地读取json
     */
    public static String readJson(String address, String fileName) {

        BufferedReader br = null;
        FileInputStream fis = null;
        try {
            File file = new File(address);
            fis = new FileInputStream(file+ "/" + fileName);
            br = new BufferedReader(new InputStreamReader(fis));

            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (Exception e) {
            LogTools.INSTANCE.e("获取失败");
            return null;
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    LogTools.INSTANCE.e("关闭失败");
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    LogTools.INSTANCE.e("关闭失败");
                }
            }
        }
    }

}
