package com.yzhg.toollibrary.weight;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.yzhg.toollibrary.R;


/**
 * 包名：com.yzhg.dydemo
 * 创建人：yzhg
 * 时间：2021/04/30 11:18
 * 描述：
 */
public class UpdateAppProgressBar extends View {
    private Paint mPaint = null;
    private int max = 100;
    private int progress = 0;
    private int type = 1;
    private int NORMAL_TYPE = 1;
    private int ALERT_TYPE = 2;

    private Context context;

    public UpdateAppProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        //与属性名称一致
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.UpdateAppProgressBar);
        //第一个是传递参数，第二个是默认值
        max = array.getInteger(R.styleable.UpdateAppProgressBar_max_bar, 100);
        progress = array.getInteger(R.styleable.UpdateAppProgressBar_progress_bar, 0);
        type = array.getInt(R.styleable.UpdateAppProgressBar_type_bar, 1);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Point left_top = new Point(0, 0);
        Point right_bottom = new Point(getWidth(), getHeight());
        double rate = (double) progress / (double) max;
        drawProgressBar(canvas, left_top, right_bottom, rate);
    }

    public void setProgress(int progress) {
        this.progress = progress;
        //使得onDraw重绘
        invalidate();
    }

    private void drawProgressBar(Canvas canvas, Point left_top, Point right_bottom, double rate) {
        int width = 1;
        int rad = 50;
        //画边框
        mPaint.setColor(getColor(context, R.color.color_9A9B9D));
        //设置空心
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(width);
        RectF rectF = new RectF(left_top.x, left_top.y, right_bottom.x, right_bottom.y);
        canvas.drawRoundRect(rectF, rad, rad, mPaint);

        //画progress bar
        mPaint.setColor(getColor(context, R.color.color_2F55E1));

        if (type == ALERT_TYPE) {
            if (rate > 0.9)
                mPaint.setColor(Color.RED);
        }
        mPaint.setStyle(Paint.Style.FILL);
        int x_end = (int) (right_bottom.x * rate);
        RectF rectF2 = new RectF(left_top.x + width, left_top.y + width, x_end - width, right_bottom.y - width);
        canvas.drawRoundRect(rectF2, rad, rad, mPaint);


       // mPaint.setColor(Color.WHITE);
      //  mPaint.setTextSize(16f);
      //  canvas.drawText("下载中：50%",x_end/2,14,mPaint);
    }

    /**
     * 获取颜色
     */
    public static int getColor(Context context, int id) {
        return ContextCompat.getColor(context, id);
    }
}