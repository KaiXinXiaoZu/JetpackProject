package com.yzhg.toollibrary.weight;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.yzhg.toollibrary.R;
import com.yzhg.toollibrary.Tools;


/**
 * 类 名: TextInputView
 * 作 者: yzhg
 * 创 建: 2018/11/20 0020
 * 版 本: 1.0
 * 历 史: (版本) 作者 时间 注释
 * 描 述: 自定义LinearLayout 属于 左侧TextView 右侧EditText  + line 下划线样式
 */
public class TextInputView2 extends LinearLayout {

    private final int DEFAULT_SIZE = 32;
    /*左侧文字*/
    private String leftText;
    /*EditText文字*/
    private String hintText;
    /*左侧文字大小*/
    private float leftSize;
    /*右侧文字大小*/
    private float hintSize;
    /*设置左侧宽度*/
    private float leftWidth;
    /*左侧文字的外边距*/
    private float leftMarginLeft;
    /*底部线条左右间距*/
    private float bottomMargins;
    /*是否显示下方下划线*/
    private boolean bottomLine;
    /*是否显示上方下划线*/
    private boolean topLine;
    /*输入限制条件*/
    private int inputType;
    /*左侧字体颜色*/
    private int leftTextColor;
    /*设置右侧字体颜色*/
    private int rightTextColor;
    /*设置右侧EditText hint颜色*/
    private int rightHintTextColor;
    /**
     * 下划线的颜色
     **/
    private int bottomLineColor;
    /*设置是否显示  提示框*/
    private boolean exclamationPoint;
    /*设置EditText 点击事件*/
    private boolean editOnclick;
    /*左侧文字显示*/
    private TextView tvLeftText;
    /*左侧文字*/
    private ConstraintLayout llLeftText;
    /*右侧 EditText  hint*/
    private EditText evRightHint;
    //小红星
    private ImageView ivEmphasis;
    /*设置可以输入的字符数*/
    private int editLength;
    //设置是否显示小红星
    private boolean redStarShow;
    //右侧展示图标
    private Drawable inputRightImage;
    //右侧图片居右
    private float inputPointMarginRight;
    private Context context;
    private ImageButton inputPoint;

    //边框宽度
    private int strokeWidth = 0;
    //边框颜色
    private int strokeColor;
    //是否使用shao
    private boolean isUserShap = false;
    //shap背景颜色
    private int solidColor;
    //四个角的弧度
    private float radius;
    private float topLeftRadius;
    private float topRightRadius;
    private float bottomLeftRadius;
    private float bottomRightRadius;

    public TextInputView2(Context context) {
        super(context);
        this.context = context;
        setOrientation(LinearLayout.VERTICAL);
    }

    public TextInputView2(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setOrientation(LinearLayout.VERTICAL);
        initAttributes(context, attrs);
        initView(context);
    }


    public TextInputView2(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setOrientation(LinearLayout.VERTICAL);
        initAttributes(context, attrs);
        initView(context);
    }


    private void initAttributes(Context context, AttributeSet attrs) {
        TypedArray toolbarArray = context.obtainStyledAttributes(attrs, R.styleable.TextInputView2);
        leftText = toolbarArray.getString(R.styleable.TextInputView2_sInputLeftText);  //左侧的文字
        hintText = toolbarArray.getString(R.styleable.TextInputView2_sInputEdTextHint); //设置EdIitText hint文字
        leftSize = toolbarArray.getDimension(R.styleable.TextInputView2_sInputLeftSize, DEFAULT_SIZE);
        hintSize = toolbarArray.getDimension(R.styleable.TextInputView2_sInputRightSize, DEFAULT_SIZE);
        leftWidth = toolbarArray.getDimension(R.styleable.TextInputView2_sLeftWidth, 120);
        bottomMargins = toolbarArray.getDimension(R.styleable.TextInputView2_sBottomLinePadding, px2dip(12));
        bottomLine = toolbarArray.getBoolean(R.styleable.TextInputView2_sInputBottomLine, true); //是否显示下划线
        topLine = toolbarArray.getBoolean(R.styleable.TextInputView2_sInputTopLine, false);  //是否显示上划线
        inputType = toolbarArray.getInt(R.styleable.TextInputView2_sInputTextType, -1);  //设置EditText 输入样式
        leftTextColor = toolbarArray.getColor(R.styleable.TextInputView2_sInputLeftColor, getColor(R.color.color_333333));  //设置左侧文字字体颜色
        rightTextColor = toolbarArray.getColor(R.styleable.TextInputView2_sInputRightColor, getColor(R.color.color_333333));   //设置右侧字体颜色
        rightHintTextColor = toolbarArray.getColor(R.styleable.TextInputView2_sInputRightHintColor, getColor(R.color.color_999999));  //设置右侧EditText hint颜色
        bottomLineColor = toolbarArray.getColor(R.styleable.TextInputView2_sInputBottomLineColor, ContextCompat.getColor(context, R.color.color_F2F4F7));
        exclamationPoint = toolbarArray.getBoolean(R.styleable.TextInputView2_sExclamationPoint, false);  //设置是否显示  提示框
        editOnclick = toolbarArray.getBoolean(R.styleable.TextInputView2_sEditOnclick, false);    //设置EditText 点击事件
        editLength = toolbarArray.getInt(R.styleable.TextInputView2_sEditLength, 120);  //设置可以输入的字符数
        redStarShow = toolbarArray.getBoolean(R.styleable.TextInputView2_sRedStarShow, false);  //设置是否显示小红星
        inputRightImage = toolbarArray.getDrawable(R.styleable.TextInputView2_sInputPoint);
        leftMarginLeft = toolbarArray.getDimension(R.styleable.TextInputView2_sLeftMarginLeft, 0);
        inputPointMarginRight = toolbarArray.getDimension(R.styleable.TextInputView2_sInputsPointMarginRight, 0f);

        //是否使用shap
        isUserShap = toolbarArray.getBoolean(R.styleable.TextInputView2_sInputUserShap, false);
        //shap背景颜色
        solidColor = toolbarArray.getColor(R.styleable.TextInputView2_sInputSolidColor, getColor(R.color.color_FFFFFF));

        strokeWidth = (int) toolbarArray.getDimension(R.styleable.TextInputView2_sInputStrokeWidth, 0);
        //边框颜色
        strokeColor = toolbarArray.getColor(R.styleable.TextInputView2_sInputStrokeColor, getColor(R.color.color_E8E8E8));
        //四个角单独设置会覆盖radius设置
        radius = toolbarArray.getDimension(R.styleable.TextInputView2_sInputRadius, 0);
        topLeftRadius = toolbarArray.getDimension(R.styleable.TextInputView2_sInputTopLeftRadius, radius);
        topRightRadius = toolbarArray.getDimension(R.styleable.TextInputView2_sInputTopRightRadius, radius);
        bottomLeftRadius = toolbarArray.getDimension(R.styleable.TextInputView2_sInputBottomLeftRadius, radius);
        bottomRightRadius = toolbarArray.getDimension(R.styleable.TextInputView2_sInputBottomRightRadius, radius);
        toolbarArray.recycle();
    }


    private void initView(Context context) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_text_input2, null);
        ConstraintLayout clViewBg = inflate.findViewById(R.id.cl_view_bg);
        tvLeftText = inflate.findViewById(R.id.tv_left_text);
        llLeftText = inflate.findViewById(R.id.ll_left_text);
        evRightHint = inflate.findViewById(R.id.tv_right_hint);
        View view_bottom_line = inflate.findViewById(R.id.view_bottom_line);
        inputPoint = inflate.findViewById(R.id.iv_input_Point);
        ivEmphasis = inflate.findViewById(R.id.IvEmphasis);
        View view_top_line = inflate.findViewById(R.id.view_top_line);
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) llLeftText.getLayoutParams();

        //是否使用Shap  只有设置了sInputUserShap才可以使用shap
        if (isUserShap) {
            GradientDrawable gradientDrawable
                    = getBackgroundDrawable1(
                    solidColor,
                    strokeColor,
                    strokeWidth,
                    topLeftRadius,
                    topRightRadius,
                    bottomLeftRadius,
                    bottomRightRadius);
            //设置背景
            clViewBg.setBackground(gradientDrawable);
        }

        params.width = (int) leftWidth;
        params.setMargins((int) leftMarginLeft, 0, 0, 0);
        llLeftText.setLayoutParams(params);

        //设置下划线颜色
        view_bottom_line.setBackgroundColor(bottomLineColor);


        tvLeftText.setText(leftText);
        evRightHint.setHint(hintText);
        tvLeftText.setTextSize(px2dip(leftSize));
        evRightHint.setTextSize(px2dip(hintSize));
        tvLeftText.setTextColor(leftTextColor);
        evRightHint.setTextColor(rightTextColor);
        evRightHint.setHintTextColor(rightHintTextColor);
        evRightHint.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editLength)});
        ivEmphasis.setVisibility(getVisible(redStarShow));

        inputPoint.setImageDrawable(inputRightImage);
        inputPoint.setVisibility(getVisible(exclamationPoint));
        ConstraintLayout.LayoutParams inputPointParams = (ConstraintLayout.LayoutParams) inputPoint.getLayoutParams();
        //设置右侧图片的右边距
        inputPointParams.setMargins(0, 0, (int) inputPointMarginRight, 0);
        inputPoint.setLayoutParams(inputPointParams);

        if (inputType != -1) {
            int inputTypeInt = InputType.TYPE_CLASS_TEXT;
            if (inputType == 0) {
                inputTypeInt = InputType.TYPE_CLASS_PHONE;
            } else if (inputType == 1) {
                inputTypeInt = InputType.TYPE_CLASS_TEXT;
            } else if (inputType == 2) {
                inputTypeInt = InputType.TYPE_TEXT_VARIATION_PASSWORD;
            } else if (inputType == 3) {
                inputTypeInt = InputType.TYPE_CLASS_NUMBER;
            }
            evRightHint.setInputType(inputTypeInt);
        }

        view_bottom_line.setVisibility(getVisible(bottomLine));
        view_top_line.setVisibility(getVisible(topLine));

        if (editOnclick) {
            evRightHint.setCursorVisible(false);
            evRightHint.setFocusable(false);
            evRightHint.setFocusableInTouchMode(false);
            evRightHint.setOnClickListener(v -> {
                // ToastTools.INSTANCE.showDeBugToast("点击事件");
                if (editClick != null) {
                    editClick.editClick(v);
                }
            });
        }
        inputPoint.setOnClickListener(v -> {
            if (editClick != null) {
                editClick.editClick(v);
            }
        });
        evRightHint.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String phone = s.toString();
                if (!TextUtils.isEmpty(phone)) {
                    if (changedListener != null) {
                        changedListener.changedString(phone);
                    }
                } else {
                    if (changedListener != null) {
                        changedListener.changedString("");
                    }
                }
            }
        });
        addView(inflate);
    }


    public OnTextChangedListener changedListener;

    public void setOnTextChangedListener(OnTextChangedListener changedListener) {
        this.changedListener = changedListener;
    }

    public interface OnTextChangedListener {
        void changedString(String changed);
    }


    private int getVisible(boolean isShow) {
        return isShow ? View.VISIBLE : View.GONE;
    }

    /**
     * 作 者: yzhg
     * 历 史: (版本) 1.0
     * 描 述: 设置左侧文字内容
     *
     * @param msg 填入内容
     */
    public void setLeftText(String msg) {
        tvLeftText.setText(msg);
    }


    public void setEditClick(boolean click) {
        if (click) {
            evRightHint.setCursorVisible(false);
            evRightHint.setFocusable(false);
            evRightHint.setFocusableInTouchMode(false);
            evRightHint.setOnClickListener(v -> {
                if (editClick != null) {
                    editClick.editClick(v);
                }
            });
        } else {
            evRightHint.setCursorVisible(true);
            evRightHint.setFocusable(true);
            evRightHint.setFocusableInTouchMode(true);
            editClick = null;
        }

    }

    /**
     * 作 者: yzhg
     * 历 史: (版本) 1.0
     * 描 述: 设置左侧文字内容
     *
     * @param msg 填入内容
     */
    public void setLeftText(int msg) {
        tvLeftText.setText(getString(msg));
    }

    /**
     * 作 者: yzhg
     * 历 史: (版本) 1.0
     * 描 述: 设置右侧EditTextHint文字内容
     *
     * @param msg 填入内容
     */

    public void setRightEditHintText(String msg) {
        evRightHint.setHint(msg);
    }

    /**
     * 作 者: yzhg
     * 历 史: (版本) 1.0
     * 描 述: 设置右侧EditTextHint文字内容
     *
     * @param msg 填入内容
     */
    public void setRightEditHintText(int msg) {
        evRightHint.setHint(msg);
    }

    /**
     * 作 者: yzhg
     * 历 史: (版本) 1.0
     * 描 述: 设置右侧EditText 文字内容
     *
     * @param msg 填入内容
     */
    public void setRightEditText(String msg) {
        if (msg != null) {
            evRightHint.setText(msg);
            // evRightHint.setSelection(msg.length());
        }
    }

    /**
     * 作 者: yzhg
     * 历 史: (版本) 1.0
     * 描 述: 设置右侧EditText 文字内容
     *
     * @param msg 填入内容
     */
    public void setRightEditText(int msg) {
        evRightHint.setText(getString(msg));
    }

    /**
     * 操作人 : yzhg
     * 描  述 : 设置 Selection
     */
    public void setEdSelection(String msg) {
        evRightHint.setSelection(msg.length());
    }

    /**
     * 操作人 : yzhg
     * 描  述 : 是否显示下一步
     */
    public void setInputPoint(int visible) {
        inputPoint.setVisibility(visible);
    }

    /**
     * 作 者: yzhg
     * 历 史: (版本) 1.0
     * 描 述: 获取EditTExt输入内容
     */
    public String getEditText() {
        return evRightHint.getText().toString().trim();
    }

    public void setShowShade(boolean isShow) {
        if (isShow) {
        } else {
            tvLeftText.setTextColor(Tools.getColor(R.color.color_999999));
            evRightHint.setTextColor(Tools.getColor(R.color.color_999999));
            evRightHint.setCursorVisible(isShow);
            evRightHint.setFocusable(isShow);
            evRightHint.setFocusableInTouchMode(isShow);
            editClick = null;
        }
    }


    /*
     * 作 者: yzhg
     * 历 史: (版本) 1.0
     * 描 述:  设置右侧文字颜色
     */
    public void setRightEditTextColor(int rightTextColor) {
        evRightHint.setTextColor(Tools.getColor(rightTextColor));
    }

    public void setEmphasis(boolean redStarShow) {
        ivEmphasis.setVisibility(getVisible(redStarShow));
    }

    /**
     * 作 者: yzhg
     * 历 史: (版本) 1.0
     * 描 述: 实现点击事件,点击选择
     */

    public SelectEditClick editClick;

    public void setSelectEditClick(SelectEditClick editClick) {
        this.editClick = editClick;
    }

    public interface SelectEditClick {
        void editClick(View v);
    }

    /**
     * 作 者: yzhg
     * 历 史: (版本) 1.0
     * 描 述: 接口实现 弹框事件回调
     */
    public ExclamationPointClick pointClick;

    public void setSelectPointClick(ExclamationPointClick pointClick) {
        this.pointClick = pointClick;
    }

    public interface ExclamationPointClick {
        void pointClick(View v);
    }

    public float px2dip(float px) {
        float density = context.getResources().getDisplayMetrics().density;
        return px / density;

    }

    /**
     * dp 与 px之间的转换
     */
    public int dip2px(float dip) {
        //获取屏幕的密度
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (dip * density + 0.5f);
    }

    //获取颜色
    public int getColor(int id) {
        return ContextCompat.getColor(context, id);
    }

    //获取字符串
    public String getString(int id) {
        return context.getResources().getString(id);
    }

    /**
     * 1、2两个参数表示左上角，3、4表示右上角，5、6表示右下角，7、8表示左下角
     *
     * @param solidColor        背景色
     * @param strokeColor       边框颜色
     * @param strokeWidth       边框宽度
     * @param topLeftRadius     左上方
     * @param topRightRadius    右上方
     * @param bottomRightRadius 右下方
     * @param bottomLeftRadius  左下方
     * @return 资源
     */
    public static GradientDrawable getBackgroundDrawable1(int solidColor, int strokeColor, int strokeWidth, float topLeftRadius, float topRightRadius
            , float bottomRightRadius, float bottomLeftRadius) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(solidColor);
        drawable.setStroke(strokeWidth, strokeColor);
        drawable.setCornerRadii(new float[]{
                topLeftRadius,
                topLeftRadius,
                topRightRadius,
                topRightRadius,
                bottomRightRadius,
                bottomRightRadius,
                bottomLeftRadius,
                bottomLeftRadius});
        return drawable;
    }
}
