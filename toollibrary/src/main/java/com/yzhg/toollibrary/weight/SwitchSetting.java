package com.yzhg.toollibrary.weight;

/**
 * 作者  时间 2017/4/6.
 */

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.yzhg.toollibrary.R;


public class SwitchSetting extends AppCompatImageView {

    public SwitchSetting(Context context) {
        super(context, null);
    }

    public SwitchSetting(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public SwitchSetting(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private boolean switchState = true;

    /**
     * 获取当前开关状态
     *
     * @return
     */
    public boolean  getSwitchState() {
        return switchState;
    }

    /**
     * 设置当前开关状态
     *
     * @param switchState
     */
    public void setSwitchState(boolean switchState) {
        this.switchState = switchState;
        if (switchState) {
            setImageResource(R.mipmap.switch_open_140);
        } else {
            setImageResource(R.mipmap.switch_close_140);
        }
    }

    /**
     * 改变开关状态
     */
    public void changeSwitchState() {
        setSwitchState(!switchState);
    }
}
