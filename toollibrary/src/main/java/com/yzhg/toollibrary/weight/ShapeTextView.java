package com.yzhg.toollibrary.weight;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.yzhg.toollibrary.R;

/**
 * 作者：yzhg
 * 时间：2021-11-23 09:37
 * 包名：com.yzhg.toollibrary.weight
 *
 * @description 实现自定义圆角背景
 * 支持
 * 1.四边圆角
 * 2.指定边圆角
 * 3.支持填充色以及边框色,边框虚线
 * 4.支持按下效果
 */
public class ShapeTextView extends AppCompatTextView {
    private boolean openSelector;
    //自定背景边框Drawable
    private GradientDrawable gradientDrawable;
    //按下时的Drawable
    private GradientDrawable selectorDrawable;
    //填充色
    private int solidColor = 0;
    //渐变色  开始
    private int gradientStartColor = 0;
    //渐变色 中间
    private int gradientCenterColor = -1;
    //渐变色  结尾
    private int gradientEndColor = 0;
    //渐变色的方向
    private int gradientAngle = 0;
    //边框色
    private int strokeColor = 0;
    private int shapOrientation = 5;
    //按下填充色
    private int solidTouchColor = 0;
    //按下边框色
    private int strokeTouchColor = 0;
    //是否使用渐变
    private boolean isGradientColor = false;
    //按下字体色
    //private int textTouchColor = 0;
    //边框宽度
    private int strokeWidth = 0;
    //四个角的弧度
    private float radius;
    private float topLeftRadius;
    private float topRightRadius;
    private float bottomLeftRadius;
    private float bottomRightRadius;
    //边框虚线的宽度
    float dashWidth = 0;
    //边框虚线的间隙
    float dashGap = 0;
    //字体色
    private int textColor = 0;


    public ShapeTextView(Context context) {
        this(context, null);
    }

    public ShapeTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShapeTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);

        if (!isGradientColor) {
            //默认背景
            gradientDrawable = getNeedDrawable(new float[]{topLeftRadius, topLeftRadius, topRightRadius, topRightRadius,
                            bottomRightRadius, bottomRightRadius, bottomLeftRadius, bottomLeftRadius},
                    solidColor, strokeWidth, strokeColor, dashWidth, dashGap);
        } else {
            //创建方向
            GradientDrawable.Orientation orientation = GradientDrawable.Orientation.LEFT_RIGHT;
            if (shapOrientation == 0) {
                orientation = GradientDrawable.Orientation.TOP_BOTTOM;
            } else if (shapOrientation == 1) {
                orientation = GradientDrawable.Orientation.TR_BL;
            } else if (shapOrientation == 2) {
                orientation = GradientDrawable.Orientation.RIGHT_LEFT;
            } else if (shapOrientation == 3) {
                orientation = GradientDrawable.Orientation.BR_TL;
            } else if (shapOrientation == 4) {
                orientation = GradientDrawable.Orientation.BOTTOM_TOP;
            } else if (shapOrientation == 5) {
                orientation = GradientDrawable.Orientation.BL_TR;
            } else if (shapOrientation == 6) {
                orientation = GradientDrawable.Orientation.LEFT_RIGHT;
            } else if (shapOrientation == 7) {
                orientation = GradientDrawable.Orientation.TL_BR;
            } else {
                orientation = GradientDrawable.Orientation.BL_TR;
            }

            //设置渐变集合;
            int[] gradientArrColor = null;
            if (gradientCenterColor == -1) {
                gradientArrColor = new int[]{gradientStartColor, gradientEndColor};
            } else {
                gradientArrColor = new int[]{gradientStartColor, gradientCenterColor, gradientEndColor};
            }
            //默认背景
            gradientDrawable = getGradientBackgroundDrawable(
                    gradientArrColor,
                    strokeWidth,
                    strokeColor,
                    dashWidth,
                    dashGap,
                    orientation,
                    gradientAngle,
                    topLeftRadius,
                    topRightRadius,
                    bottomLeftRadius,
                    bottomRightRadius
            );
        }

        //如果设置了选中时的背景
        if (openSelector) {
            selectorDrawable = getNeedDrawable(new float[]{topLeftRadius, topLeftRadius, topRightRadius, topRightRadius,
                            bottomRightRadius, bottomRightRadius, bottomLeftRadius, bottomLeftRadius},
                    solidTouchColor, strokeWidth, strokeTouchColor, dashWidth, dashGap);

            //动态生成Selector
            StateListDrawable stateListDrawable = new StateListDrawable();
            //是否按下
            int pressed = android.R.attr.state_pressed;

            stateListDrawable.addState(new int[]{pressed}, selectorDrawable);
            stateListDrawable.addState(new int[]{}, gradientDrawable);

            setBackground(stateListDrawable);
        } else {
            setBackground(gradientDrawable);
        }
    }

    /**
     * 初始化参数
     *
     * @param context
     * @param attrs
     */
    private void init(Context context, AttributeSet attrs) {
        TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ShapeTextView, 0, 0);

        openSelector = ta.getBoolean(R.styleable.ShapeTextView_openSelector, false);

        solidColor = ta.getInteger(R.styleable.ShapeTextView_solidColor, 0x00000000);
        gradientStartColor = ta.getInteger(R.styleable.ShapeTextView_gradientStartColor, 0);
        gradientCenterColor = ta.getInteger(R.styleable.ShapeTextView_gradientCenterColor, -1);
        gradientEndColor = ta.getInteger(R.styleable.ShapeTextView_gradientEndColor, 0);
        gradientAngle = ta.getInteger(R.styleable.ShapeTextView_gradientAngle, 0);
        strokeColor = ta.getInteger(R.styleable.ShapeTextView_strokeColor, 0x00000000);
        shapOrientation = ta.getInteger(R.styleable.ShapeTextView_shapOrientation, 0);
        solidTouchColor = ta.getInteger(R.styleable.ShapeTextView_solidTouchColor, 0x00000000);
        strokeTouchColor = ta.getInteger(R.styleable.ShapeTextView_strokeTouchColor, 0x00000000);
        // textTouchColor = ta.getInteger(R.styleable.ShapeTextView_textTouchColor, 0x00000000);
        textColor = getCurrentTextColor();



        isGradientColor = ta.getBoolean(R.styleable.ShapeTextView_isGradientColor, false);

        strokeWidth = (int) ta.getDimension(R.styleable.ShapeTextView_strokeWidth, 0);
        //四个角单独设置会覆盖radius设置
        radius = ta.getDimension(R.styleable.ShapeTextView_radius, 0);
        topLeftRadius = ta.getDimension(R.styleable.ShapeTextView_topLeftRadius, radius);
        topRightRadius = ta.getDimension(R.styleable.ShapeTextView_topRightRadius, radius);
        bottomLeftRadius = ta.getDimension(R.styleable.ShapeTextView_bottomLeftRadius, radius);
        bottomRightRadius = ta.getDimension(R.styleable.ShapeTextView_bottomRightRadius, radius);

        dashGap = ta.getDimension(R.styleable.ShapeTextView_dashGap, 0);
        dashWidth = ta.getDimension(R.styleable.ShapeTextView_dashWidth, 0);

        ta.recycle();
    }

    /**
     * @param radius      四个角的半径
     * @param colors      渐变的颜色
     * @param strokeWidth 边框宽度
     * @param strokeColor 边框颜色
     * @return
     */
    public static GradientDrawable getNeedDrawable(float[] radius, int[] colors, int strokeWidth, int strokeColor) {
        // 判断版本是否大于16  项目中默认的都是Linear散射 都是从左到右 都是只有开始颜色和结束颜色
        GradientDrawable drawable;
        drawable = new GradientDrawable();
        drawable.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);
        drawable.setColors(colors);

        drawable.setCornerRadii(radius);
        drawable.setStroke(strokeWidth, strokeColor);
        drawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);
        return drawable;
    }

    /**
     * @param radius      四个角的半径
     * @param bgColor     背景颜色
     * @param strokeWidth 边框宽度
     * @param strokeColor 边框颜色
     * @return
     */
    public static GradientDrawable getNeedDrawable(float[] radius, int bgColor, int strokeWidth, int strokeColor) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE);
        drawable.setCornerRadii(radius);
        drawable.setStroke(strokeWidth, strokeColor);
        drawable.setColor(bgColor);
        return drawable;
    }

    /**
     * @param radius      四个角的半径
     * @param bgColor     背景颜色
     * @param strokeWidth 边框宽度
     * @param strokeColor 边框颜色
     * @param dashWidth   虚线边框宽度
     * @param dashGap     虚线边框间隙
     * @return
     */
    public static GradientDrawable getNeedDrawable(float[] radius, int bgColor, int strokeWidth, int strokeColor, float dashWidth, float dashGap) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE);
        drawable.setCornerRadii(radius);
        drawable.setStroke(strokeWidth, strokeColor, dashWidth, dashGap);
        drawable.setColor(bgColor);
        return drawable;
    }

    /**
     * 产生shape类型的渐变drawable
     *
     * @param solidColor    填充的渐变颜色
     * @param orientation   渐变色方向
     * @param gradient      渐变色度数
     * @param topLeftRadius 圆角的度数
     * @return 左上  右上  左下  右下
     */
    @SuppressLint("WrongConstant")
    public static GradientDrawable getGradientBackgroundDrawable(
            int[] solidColor, int strokeWidth, int strokeColor, float dashWidth, float dashGap,
            GradientDrawable.Orientation orientation, int gradient, float topLeftRadius, float topRightRadius, float bottomLeftRadius, float bottomRightRadius
    ) {
        GradientDrawable drawable = new GradientDrawable(orientation, solidColor);
        drawable.setShape(gradient);
        drawable.setGradientType(GradientDrawable.RECTANGLE);
        drawable.setCornerRadii(new float[]{topLeftRadius, topLeftRadius, topRightRadius, topRightRadius, bottomRightRadius, bottomRightRadius, bottomLeftRadius, bottomLeftRadius});
        drawable.setStroke(strokeWidth, strokeColor, dashWidth, dashGap);
        return drawable;
    }
}