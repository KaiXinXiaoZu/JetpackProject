package com.yzhg.toollibrary.weight

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * Created by $(yzhg) on 2018/4/4 0004.
 * 用一句话描述()
 */
class TabMainAdapter(fm: FragmentManager, private val tab_fragments: Array<Fragment>) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return tab_fragments[position]
    }

    override fun getCount(): Int {
        return tab_fragments.size
    }
}