package com.yzhg.toollibrary.weight;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.yzhg.toollibrary.R;
import com.yzhg.toollibrary.Tools;

/**
 * 作者 xxx 时间 2015/12/12.
 */

public class QuickIndexBar extends View {

    private String[] indexArr = {"首", "热", "A", "B", "C", "D", "E", "F", "G", "H",
            "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
            "V", "W", "X", "Y", "Z"};

    Handler handler = new Handler();

    /**
     * 定义两个颜色,白色和深灰色
     */
    private int COLOR_DEFAULT = Tools.getColor(R.color.color_333333);
    private int COLOR_PRESSED = Tools.getColor(R.color.color_333333);
    private Paint paint;
    //每个格子的高度
    private float gridHeight;

    public QuickIndexBar(Context context) {
        this(context, null);
    }

    public QuickIndexBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public QuickIndexBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    /**
     * 设置索引
     */
    public void setIndex(String[] indexArr) {
        this.indexArr = indexArr;
        invalidate();
    }

    /**
     * 此方法用于初始化ViewDragHelper
     */
    private void initView() {
        //设置抗锯齿
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Tools.getColor(R.color.color_333333));
        //获取dimens中设置的字体大小
        paint.setTextSize(getResources().getDimensionPixelSize(R.dimen.sp_10));
        Typeface font = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
        paint.setTypeface(font);
        ////  paint.setStyle(Paint.Style.FILL);
        //设置文字被绘制的起点为底边的中心
        paint.setTextAlign(Paint.Align.CENTER);
    }

    /**
     * 此方法在onMeasure方法之后执行,获取格子的宽高
     *
     * @param w
     * @param h
     * @param oldw
     * @param oldh
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        //获得每个格子的高度
        gridHeight = (getMeasuredHeight() * 1f) / indexArr.length;

    }

    /**
     * 绘制
     *
     * @param canvas 画画板
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (int i = 0; i < indexArr.length; i++) {
            //x的宽度是,格子的一半
            float x = getMeasuredWidth() / 2;
            //y的高度 本格子的高度的一半(gridHeight / 2) + 本字体高度的一半 + 上几个个格子的高度(i * gridHeight)

            float y = (gridHeight / 2) + getTextHeight(indexArr[i]) / 2 + (i * gridHeight);
            //第一次手指按下时,绘制View,执行到invalidate();  又会绘制一次
            paint.setColor(i == currentIndex ? COLOR_PRESSED : COLOR_DEFAULT);
            canvas.drawText(indexArr[i], x, y, paint);
        }

    }

    private int currentIndex = -1;

    /**
     * 处理滑动事件
     *
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                //计算出按下之处对应的格子
                int indexGrid = (int) (event.getY() / gridHeight);
                //如果当前按下的字母是同一个
                if (indexGrid != currentIndex) {
                    if (indexGrid >= 0 && indexGrid < indexArr.length) {
                        String word = indexArr[indexGrid];
                        if (listener != null) {
                            listener.OnWordListener(word);
                        }
                    }
                }
                currentIndex = indexGrid;
                break;
            case MotionEvent.ACTION_UP:
                currentIndex = -1;
                if (listener != null) {
                    listener.OnWordListener("");
                }
                break;
        }
        //刷新onDraw
        invalidate();
        return true;
    }

    /**
     * 获取字体的高度
     */
    private float getTextHeight(String s) {
        //当前bounds还没有值
        Rect bounds = new Rect();
        //当此方法走完之后,bounds就有值
        paint.getTextBounds(s, 0, s.length(), bounds);
        return bounds.height();
    }

    private OnWordChangeListener listener;

    public void setOnWordChangeListener(OnWordChangeListener listener) {
        this.listener = listener;
    }

    /**
     * 定义接口的回调
     */
    public interface OnWordChangeListener {
        void OnWordListener(String word);
    }
}
