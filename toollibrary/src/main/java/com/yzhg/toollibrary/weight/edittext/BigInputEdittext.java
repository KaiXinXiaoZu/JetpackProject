package com.yzhg.toollibrary.weight.edittext;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yzhg.toollibrary.R;
import com.yzhg.toollibrary.common.ToastTools;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

/**
 * 编辑 : yzhg
 * 时间 ：20:58-01-09 20:58
 * 包名 : com.yzhg.toollibrary.weight.edittext
 * 描述 : 一句话描述
 */
public class BigInputEdittext extends LinearLayout {

    private Context context;

    public BigInputEdittext(Context context) {
        super(context, null);
        this.context = context;
        initView(context);
    }

    public BigInputEdittext(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, 0);
        this.context = context;
        initAttributes(context, attrs);
        initView(context);
    }


    public BigInputEdittext(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initAttributes(context, attrs);
        initView(context);
    }

    private LinearLayout llEditText;
    private EditText etBigInput;
    private TextView tvTextNumBotPadding, tvTextNumBotMargin, tvRightTextNum;

    private int bieEditTextSize;
    private int bieEditTextColor;
    private int bieEditHintColor;
    private int bieEditMaxLines;
    private int bieEditMinLines;
    private int bieEditLTRPadding;
    private int bieEditTopPadding;
    private int bieEditLeftPadding;
    private int bieEditRightPadding;
    private int bieEditBottomPadding;
    private int bieEditSolidColor;
    private int bieEditStrokeColor;
    private int bieEditStrokeWidth;
    private int bieEditRadius;
    private int bieNumTextSize;
    private int bieNumTextColor;
    private int bieNumMaxText;
    private int bieNumMargin;
    private int bieNumGravity;
    private int bieNumTopMargin, bieNumLeftMargin, bieNumRightMargin, bieNumBottomMargin;

    private String birEditTextString, birEditHintString;

    /**
     * 是否设置软键盘未搜索按键
     */
    private int bieEditImeOptions;

    private void initAttributes(Context context, AttributeSet attrs) {
        TypedArray toolbarArray = context.obtainStyledAttributes(attrs, R.styleable.BigInputEdittext);
        bieEditTextSize = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieEditTextSize, dip2px(14));
        bieEditTextColor = toolbarArray.getColor(R.styleable.BigInputEdittext_bieEditTextColor, Color.parseColor("#333333"));
        bieEditHintColor = toolbarArray.getColor(R.styleable.BigInputEdittext_bieEditHintColor, Color.parseColor("#333333"));
        bieEditMaxLines = toolbarArray.getInt(R.styleable.BigInputEdittext_bieEditMaxLines, 6);
        bieEditMinLines = toolbarArray.getInt(R.styleable.BigInputEdittext_bieEditMinLines, 4);
        bieEditLTRPadding = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieEditLTRBPadding, dip2px(12));
        bieEditTopPadding = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieEditTopPadding, bieEditLTRPadding);
        bieEditLeftPadding = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieEditLeftPadding, bieEditLTRPadding);
        bieEditRightPadding = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieEditRightPadding, bieEditLTRPadding);
        bieEditBottomPadding = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieEditBottomPadding, bieEditLTRPadding);

        bieEditSolidColor = toolbarArray.getColor(R.styleable.BigInputEdittext_bieEditSolidColor, Color.parseColor("#FFFFFF"));
        bieEditStrokeColor = toolbarArray.getColor(R.styleable.BigInputEdittext_bieEditStrokeColor, Color.parseColor("#EEEEEE"));
        bieEditStrokeWidth = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieEditStrokeWidth, dip2px(1));
        bieEditRadius = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieEditRadius, dip2px(4));

        bieNumTextSize = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieNumTextSize, dip2px(12));
        bieNumTextColor = toolbarArray.getColor(R.styleable.BigInputEdittext_bieNumTextColor, Color.parseColor("#333333"));
        bieNumMaxText = toolbarArray.getInt(R.styleable.BigInputEdittext_bieNumMax, 200);

        bieNumMargin = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieNumMargin, dip2px(12));
        bieNumTopMargin = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieNumTopMargin, bieNumMargin);
        bieNumLeftMargin = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieNumLeftMargin, bieNumMargin);
        bieNumRightMargin = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieNumRightMargin, bieNumMargin);
        bieNumBottomMargin = toolbarArray.getDimensionPixelSize(R.styleable.BigInputEdittext_bieNumBottomMargin, bieNumMargin);
        bieNumGravity = toolbarArray.getInt(R.styleable.BigInputEdittext_bieNumGravity, 2);
        birEditTextString = toolbarArray.getString(R.styleable.BigInputEdittext_birEditTextString);
        birEditHintString = toolbarArray.getString(R.styleable.BigInputEdittext_birEditHintString);

        bieEditImeOptions = toolbarArray.getInt(R.styleable.BigInputEdittext_bieEditImeOptions, -1);

        if (bieNumGravity == 2) {
            bieEditBottomPadding = 0;
        }

        toolbarArray.recycle();
    }


    private void initView(Context context) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.view_big_input_edittext_layout, this, false);
        llEditText = inflate.findViewById(R.id.llEditText);
        etBigInput = inflate.findViewById(R.id.etBigInput);
        tvTextNumBotPadding = inflate.findViewById(R.id.tvTextNumBotPadding);
        tvTextNumBotMargin = inflate.findViewById(R.id.tvTextNumBotMargin);
        tvRightTextNum = inflate.findViewById(R.id.tvRightTextNum);
        setEditTextView();
        setTextNumView(tvTextNumBotPadding);
        setTextNumView(tvTextNumBotMargin);
        setTextNumView(tvRightTextNum);
        tvTextNumBotPadding.setVisibility(View.GONE);
        tvTextNumBotMargin.setVisibility(View.GONE);
        tvRightTextNum.setVisibility(View.GONE);
        switch (bieNumGravity) {
            case 0:
                //显示右边的num
                tvRightTextNum.setVisibility(View.VISIBLE);
                break;
            case 1:
                //显示下边外
                tvTextNumBotMargin.setVisibility(View.VISIBLE);
                break;
            case 2:
                //显示下边内
                tvTextNumBotPadding.setVisibility(View.VISIBLE);
                break;
        }

        addView(inflate);
    }


    private TextWatcher TextChangeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            String changeNumText = editable.toString();
            if (textChangedListener != null) {
                textChangedListener.afterTextChanged(changeNumText);
            }
            if (TextUtils.isEmpty(changeNumText)) {
                setEditTextChangeNum(tvTextNumBotPadding, "0");
                setEditTextChangeNum(tvTextNumBotMargin, "0");
                setEditTextChangeNum(tvRightTextNum, "0");
            } else {
                setEditTextChangeNum(tvTextNumBotPadding, String.valueOf(changeNumText.length()));
                setEditTextChangeNum(tvTextNumBotMargin, String.valueOf(changeNumText.length()));
                setEditTextChangeNum(tvRightTextNum, String.valueOf(changeNumText.length()));
            }
        }
    };

    /**
     * 设置文字的属性
     */
    private void setTextNumView(TextView tvTextView) {
        tvTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, bieNumTextSize);
        tvTextView.setTextColor(bieNumTextColor);
        setEditTextChangeNum(tvTextView, "0");
        tvTextView.setPadding(bieNumLeftMargin, bieNumTopMargin, bieNumRightMargin, bieNumBottomMargin);
        etBigInput.addTextChangedListener(TextChangeListener);


    }

    private void setEditTextView() {
        etBigInput.setTextSize(TypedValue.COMPLEX_UNIT_PX, bieEditTextSize);
        etBigInput.setTextColor(bieEditTextColor);
        etBigInput.setHintTextColor(bieEditHintColor);
        etBigInput.setMinLines(bieEditMinLines);
        etBigInput.setMaxLines(bieEditMaxLines);
        etBigInput.setPadding(bieEditLeftPadding, bieEditTopPadding, bieEditRightPadding, bieEditBottomPadding);
        etBigInput.setFilters(new InputFilter[]{new InputFilter.LengthFilter(bieNumMaxText)});
        llEditText.setBackground(getBackgroundDrawable(bieEditSolidColor, bieEditStrokeColor, bieEditStrokeWidth, bieEditRadius, bieEditRadius, bieEditRadius, bieEditRadius));

        etBigInput.setHint(birEditHintString);
        etBigInput.setText(birEditTextString);

        //设置软键盘未搜索
        if (bieEditImeOptions >= 0) {
            if (bieEditImeOptions == 0) {
                etBigInput.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
                etBigInput.setMaxLines(1);
                etBigInput.setSingleLine(true);
            }

            //设置软键盘点击事件
            etBigInput.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (onEditorCallBackListener != null) {
                        String searchConstant = etBigInput.getText().toString().trim();
                        onEditorCallBackListener.editorResult(searchConstant);
                    }
                    return true;
                }
                return false;
            });
        }
    }

    /**
     * 设置输入框的文字
     */
    private void setEditTextChangeNum(TextView tvTextView, String changeNum) {
        tvTextView.setText(changeNum + "/" + bieNumMaxText);
    }


    public EditText getEditText() {
        return etBigInput;
    }

    public TextView getNumTextView() {
        if (0 == bieNumGravity) {
            return tvRightTextNum;
        } else if (1 == bieNumGravity) {
            return tvTextNumBotMargin;
        } else if (2 == bieNumGravity) {
            return tvTextNumBotPadding;
        }
        return tvTextNumBotPadding;
    }

    public String getEditTextString() {
        return etBigInput.getText().toString().trim();
    }

    /**
     * 1、2两个参数表示左上角，3、4表示右上角，5、6表示右下角，7、8表示左下角
     *
     * @param solidColor        背景色
     * @param strokeColor       边框颜色
     * @param strokeWidth       边框宽度
     * @param topLeftRadius     左上方
     * @param topRightRadius    右上方
     * @param bottomRightRadius 右下方
     * @param bottomLeftRadius  左下方
     * @return 资源
     */
    public static GradientDrawable getBackgroundDrawable(int solidColor, int strokeColor, int strokeWidth, float topLeftRadius, float topRightRadius
            , float bottomRightRadius, float bottomLeftRadius) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(solidColor);
        drawable.setStroke(strokeWidth, strokeColor);
        drawable.setCornerRadii(new float[]{
                topLeftRadius,
                topLeftRadius,
                topRightRadius,
                topRightRadius,
                bottomRightRadius,
                bottomRightRadius,
                bottomLeftRadius,
                bottomLeftRadius});
        return drawable;
    }

    /**
     * dp 与 px之间的转换
     */
    public int dip2px(float dip) {
        //获取屏幕的密度
        float density = getResources().getDisplayMetrics().density;
        return (int) (dip * density + 0.5f);
    }


    /**
     * 设置监听
     */

    private OnEditorCallBackListener onEditorCallBackListener;

    /**
     * 软键盘搜索按钮点击事件
     */
    public interface OnEditorCallBackListener {
        void editorResult(String message);
    }

    public void setOnEditorCallBackListener(OnEditorCallBackListener onEditorCallBackListener) {
        this.onEditorCallBackListener = onEditorCallBackListener;
    }

    private TextChangedListener textChangedListener;

    public interface TextChangedListener {
        void afterTextChanged(String message);
    }

    public void setTextChangedListener(TextChangedListener textChangedListener) {
        this.textChangedListener = textChangedListener;
    }
}


/**
 * <?xml version="1.0" encoding="utf-8"?>
 * <androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
 * xmlns:app="http://schemas.android.com/apk/res-auto"
 * xmlns:tools="http://schemas.android.com/tools"
 * android:layout_width="match_parent"
 * android:layout_height="wrap_content">
 * <p>
 * <LinearLayout
 * android:id="@+id/llEditText"
 * android:layout_width="0dp"
 * android:layout_height="wrap_content"
 * android:orientation="vertical"
 * app:layout_constraintLeft_toLeftOf="parent"
 * app:layout_constraintRight_toLeftOf="@+id/tvRightTextNum"
 * app:layout_constraintTop_toTopOf="parent">
 * <p>
 * <EditText
 * android:id="@+id/etBigInput"
 * android:layout_width="match_parent"
 * android:layout_height="wrap_content"
 * android:background="@null"
 * android:gravity="left|top"
 * android:maxLines="8"
 * android:minLines="4"
 * android:paddingLeft="@dimen/dp_12"
 * android:paddingTop="@dimen/dp_12"
 * android:paddingEnd="@dimen/dp_12"
 * android:paddingBottom="@dimen/dp_12"
 * android:textColor="@color/color_333333"
 * android:textSize="@dimen/sp_12"
 * tools:text="输入框" />
 * <p>
 * <TextView
 * android:id="@+id/tvTextNumBotPadding"
 * android:layout_width="match_parent"
 * android:layout_height="wrap_content"
 * android:gravity="right"
 * android:text="1/500"
 * app:layout_constraintBottom_toBottomOf="@+id/llEditText"
 * app:layout_constraintRight_toRightOf="@+id/llEditText" />
 * </LinearLayout>
 * <p>
 * <p>
 * <TextView
 * android:id="@+id/tvTextNumBotMargin"
 * android:layout_width="wrap_content"
 * android:layout_height="wrap_content"
 * android:layout_marginBottom="@dimen/dp_12"
 * android:text="1/500"
 * app:layout_constraintRight_toRightOf="@+id/llEditText"
 * app:layout_constraintTop_toBottomOf="@+id/llEditText" />
 * <p>
 * <p>
 * <TextView
 * android:id="@+id/tvRightTextNum"
 * android:layout_width="wrap_content"
 * android:layout_height="wrap_content"
 * android:text="1/500"
 * app:layout_constraintBottom_toBottomOf="@+id/llEditText"
 * app:layout_constraintRight_toRightOf="parent"
 * app:layout_constraintTop_toTopOf="@+id/llEditText" />
 * <p>
 * <p>
 * </androidx.constraintlayout.widget.ConstraintLayout>
 */

/*

    <declare-styleable name="BigInputEdittext">

        <attr name="birEditTextString" format="string" />
        <attr name="birEditHintString" format="string" />
        <!--输入框的文字大小-->
        <attr name="bieEditTextSize" format="dimension" />
        <!--输入框的文字的颜色，和hint颜色-->
        <attr name="bieEditTextColor" format="color" />
        <attr name="bieEditHintColor" format="color" />
        <!--输入框的最大和最小高度-->
        <attr name="bieEditMaxLines" format="integer" />
        <attr name="bieEditMinLines" format="integer" />
        <!--设置边距-->
        <attr name="bieEditTopPadding" format="dimension" />
        <attr name="bieEditLeftPadding" format="dimension" />
        <attr name="bieEditRightPadding" format="dimension" />
        <attr name="bieEditBottomPadding" format="dimension" />
        <!--设置左上右边的边距-->
        <attr name="bieEditLTRBPadding" format="dimension" />
        <!--设置输入框的背景颜色-->
        <attr name="bieEditSolidColor" format="color" />
        <!--这是输入框的边框颜色-->
        <attr name="bieEditStrokeColor" format="color" />
        <!--设置输入框的高度-->
        <attr name="bieEditStrokeWidth" format="dimension" />
        <!--输入框的圆角-->
        <attr name="bieEditRadius" format="dimension" />


        <!--文字操作-->
        <!--文字的颜色 大小  数字-->
        <attr name="bieNumTextSize" format="dimension" />
        <attr name="bieNumTextColor" format="color" />
        <!--设置最大的数字-->
        <attr name="bieNumMax" format="integer" />
        <!--设置文本的外边距-->
        <!--设置边距-->
        <attr name="bieNumTopMargin" format="dimension" />
        <attr name="bieNumLeftMargin" format="dimension" />
        <attr name="bieNumRightMargin" format="dimension" />
        <attr name="bieNumBottomMargin" format="dimension" />
        <attr name="bieNumMargin" format="dimension" />

        <!--设置文字的顺序-->
        <attr name="bieNumGravity" format="enum">
            <enum name="right_margin" value="0" />
            <enum name="bottom_margin" value="1" />
            <enum name="bottom_padding" value="2" />
            <enum name="no_num" value="3" />
        </attr>
        <!--软键盘右下角变成搜索框-->
        <attr name="bieEditImeOptions" format="enum">
            <enum name="search" value="0" />
        </attr>
        <!--设置文字显示的方式-->

    </declare-styleable>
 */