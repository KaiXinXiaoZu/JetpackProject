package com.yzhg.toollibrary.dialog;

/*
 * 操作人 :  Administrator
 * 时  间 :  2019/9/21 0021
 * 描  述 :
 */
public interface ViewConvertListener {

     void convertView(ViewHolder holder, BaseDialog dialog);
}
