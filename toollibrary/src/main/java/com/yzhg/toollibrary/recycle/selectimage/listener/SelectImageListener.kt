package com.yzhg.toollibrary.recycle.selectimage.listener

import android.view.View
import android.widget.ImageView
import com.yzhg.toollibrary.recycle.selectimage.SelectPictureBean

/**
 * author : yzhg
 * 包名: com.yzhg.toollibrary.recycle.selectimage.listener
 * 时间: 2023-02-16 11:35
 * 描述：选择图片
 */
interface SelectImageListener {

    /**
     * 选择图片
     */
    fun selectImageClick()

    /**
     * 查看图片
     */
    fun checkImageClick(imageList: MutableList<SelectPictureBean>, view: ImageView, position: Int){

    }
}