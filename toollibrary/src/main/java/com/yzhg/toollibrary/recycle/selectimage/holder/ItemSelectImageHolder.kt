package com.yzhg.toollibrary.recycle.selectimage.holder

import androidx.recyclerview.widget.RecyclerView
import com.yzhg.toollibrary.databinding.ItemSelectImageLayoutBinding

/**
 * author : yzhg
 * 包名: com.yzhg.toollibrary.recycle.selectimage.holder
 * 时间: 2023-02-15 11:41
 * 描述：
 */
class ItemSelectImageHolder(var selectImageBinding: ItemSelectImageLayoutBinding) : RecyclerView.ViewHolder(selectImageBinding.root) {

   // var selectImageBinding: ItemSelectImageLayoutBinding = selectImageBinding
}