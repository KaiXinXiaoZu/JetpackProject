package com.yzhg.toollibrary.recycle.selectimage

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.yzhg.toollibrary.R
import com.yzhg.toollibrary.Tools
import com.yzhg.toollibrary.databinding.RecycleImageSelectLayoutBinding
import com.yzhg.toollibrary.recycle.decoration.GridSpacingItemDecoration
import com.yzhg.toollibrary.recycle.selectimage.adapter.ItemSelectImageAdapter
import com.yzhg.toollibrary.recycle.selectimage.engine.glide.DisplayPictureEngine
import com.yzhg.toollibrary.recycle.selectimage.engine.permission.PermissionEngine
import com.yzhg.toollibrary.recycle.selectimage.engine.permission.PermissionEngineListener
import com.yzhg.toollibrary.recycle.selectimage.engine.select.SelectPictureEngine
import com.yzhg.toollibrary.recycle.selectimage.engine.select.SelectPictureEngineInterface
import com.yzhg.toollibrary.recycle.selectimage.engine.select.SelectPictureListInterface
import com.yzhg.toollibrary.recycle.selectimage.listener.SelectImageListener
import java.util.concurrent.TimeoutException

/**
 * author : yzhg
 * 包名: com.yzhg.toollibrary.recycle
 * 时间: 2023-02-14 16:25
 * 描述：
 *      选择图片用到的RecycleView
 *
 *      需求：
 *          1.能选择的最高图片数量
 *          2.在最后追加 选择按钮
 *          3.设置一行占几张图片
 */
class SelectImageRecycleView : LinearLayout {

    companion object {

        val D_RIGHT_TOP = 1

        val D_RIGHT_BOT = 2

        val D_CENTER = 3

        //选择图片
        val SELECT_PICTURE = 1

        //选择视频
        val SELECT_VIDEO = 2

        //选择视频和图片
        val SELECT_PICTURE_VIDEO = 3
    }

    /**
     * 能选择图片数量 默认可以选择9张
     * 可以通过xml中 sir_select_num 配置数量
     * @see setSelectImageNum 方法设置
     */
    private var selectImageNum: Int = 9

    /**
     * 设置一行占几张图片  默认4张
     * 可以通过xml 中 horizontal_num 设置
     * @see setHorizontalImageNum 也可以设置
     */
    private var horizontalImageNum: Int = 3

    /**
     * 是否显示删除按钮
     * 通过xml sir_deleteIcon 设置
     */
    private var isShowDeleteIcon: Boolean = true

    /**
     * 删除按钮图标
     * 通过 sir_show_delete 设置
     */
    @SuppressLint("SupportAnnotationUsage")
    @DrawableRes
    private var deleteIconRes: Int? = null

    /**
     * 设置删除图标的宽
     *
     * 删除图标的宽高,点击区域的宽高是 图标的二倍
     */
    private var deleteWidth = 0f

    /**
     * 设置删除图标的高
     *  删除图标的宽高,点击区域的宽高是 图标的二倍
     */
    private var deleteHeight = 0f

    /**
     * 统一设置删除图片的宽高，这个后起作用
     */
    private var deleteWidthHeight = 30.0f

    /**
     * 设置图片的圆角
     */
    private var imageRadius = 8.0f

    /**
     * 设置图片的间距 -- 分割线
     */
    private var gridSpacingSize = 2.0f

    /**
     * 设置是否可以查看图片
     */
    private var isCanLookImage = true

    /**
     * 选择图片  、  选择视频   、  选择图片和视频
     */
    private var selectType = SELECT_PICTURE

    /**
     * 视频选择的数量
     */
    private var selectVideoNum: Int = 1

    /**
     * 配置权限 引擎
     */
    private var permissionEngine: PermissionEngine? = null

    /**
     * 设置权限配置引擎
     */
    fun setPermissionEngine(permissionEngine: PermissionEngine) {
        this.permissionEngine = permissionEngine
    }

    /**
     * 设置删除图标显示的位置
     *      可以显示在 右上角 、 右下角  、 显示中间  、 右上角 超出边界
     */
    private var deleteLocation: Int = D_RIGHT_TOP

    /**
     * 显示中间需要设置 透明背景度
     */
    private var deleteBgAlpha = 0.6f

    /**
     * 选择图片的适配器
     */
    private lateinit var selectImageAdapter: ItemSelectImageAdapter


    constructor(context: Context) : this(context, null) {
        orientation = VERTICAL
    }

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0) {
        orientation = VERTICAL
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        orientation = VERTICAL
        context.obtainStyledAttributes(attrs, R.styleable.SelectImageRecycleView, defStyleAttr, 0)
            .apply {
                //可以设置图片的数量
                selectImageNum = getInt(R.styleable.SelectImageRecycleView_sir_select_num, 9)
                horizontalImageNum = getInt(R.styleable.SelectImageRecycleView_sir_horizontal_num, 3)
                deleteIconRes = getResourceId(R.styleable.SelectImageRecycleView_sir_deleteIcon, R.mipmap.img_delete_icon)
                isShowDeleteIcon = getBoolean(R.styleable.SelectImageRecycleView_sir_show_delete, true)
                deleteLocation = getInt(R.styleable.SelectImageRecycleView_sir_delete_location, D_RIGHT_TOP)
                deleteBgAlpha = getFloat(R.styleable.SelectImageRecycleView_sir_d_alpha, 0.6f)
                gridSpacingSize = getFloat(R.styleable.SelectImageRecycleView_sir_grid_spacing_size, 6.0f)
                imageRadius = getDimension(R.styleable.SelectImageRecycleView_sir_image_radius, dip2px(context, 8.0f))
                isCanLookImage = getBoolean(R.styleable.SelectImageRecycleView_sir_can_look_image, true)
                deleteWidthHeight = getDimension(R.styleable.SelectImageRecycleView_sir_delete_width_height, dip2px(context, 16.0f))
                deleteWidth = getDimension(R.styleable.SelectImageRecycleView_sir_delete_width, 0f)
                deleteHeight = getDimension(R.styleable.SelectImageRecycleView_sir_delete_height, 0f)
                selectType = getInt(R.styleable.SelectImageRecycleView_sir_select_type, SELECT_PICTURE)
                selectVideoNum = getInt(R.styleable.SelectImageRecycleView_sir_select_video_num, 1)
                //绘制
                recycle()
            }
        //如果没有设置宽高 则设置为统一的宽高
        if (deleteWidth == 0f) {
            deleteWidth = deleteWidthHeight
        }
        if (deleteHeight == 0f) {
            deleteHeight = deleteWidthHeight
        }
        initView(context)
    }

    /**
     * 初始化View
     */
    @SuppressLint("RestrictedApi")
    private fun initView(context: Context) {
        val imageSelectBinding: RecycleImageSelectLayoutBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.recycle_image_select_layout, null, false)
        imageSelectBinding.recycleSelectImage.layoutManager = GridLayoutManager(context, horizontalImageNum)
        selectImageAdapter = ItemSelectImageAdapter(context)
        selectImageAdapter.setDeleteLocation(deleteLocation)
        selectImageAdapter.isShowDeleteIcon(isShowDeleteIcon)
        selectImageAdapter.setImageRadius(imageRadius)
        selectImageAdapter.setDeleteIconRes(deleteIconRes)
        selectImageAdapter.setDeleteWidth(deleteWidth)
        selectImageAdapter.setDeleteHeight(deleteHeight)
        if (imageSelectBinding.recycleSelectImage.itemDecorationCount <= 0) {
            val gridSpacingItemDecoration = GridSpacingItemDecoration(horizontalImageNum, dip2px(context, gridSpacingSize).toInt(), false)
            imageSelectBinding.recycleSelectImage.addItemDecoration(gridSpacingItemDecoration)
        }

        imageSelectBinding.recycleSelectImage.adapter = selectImageAdapter
        //添加 选择控件
        addView(imageSelectBinding.root)
    }

    /**
     * 设置图片
     */
    fun setPictureList(imageList: MutableList<SelectPictureBean>) {
        selectImageAdapter.setPictureList(imageList)
    }

    /**
     * 选中图片
     * @param activity 在Fragment中使用
     */
    fun initPictureSelect(activity: FragmentActivity) {
        setPictureListener(activity, null)
    }

    /**
     * 选中图片
     * @param fragment 在Fragment中使用
     */
    fun initPictureSelect(fragment: Fragment) {
        setPictureListener(fragment, null)
    }


    /**
     * 选中图片
     * @param activity 在Fragment中使用
     * @param selectEngine 选择图片引擎 不传 使用默认的
     */
    fun setPictureListener(activity: FragmentActivity, selectEngine: SelectPictureEngineInterface?) {
        if (selectEngine == null) {
            setPictureListener(activity, SelectPictureEngine(), null)
        } else {
            setPictureListener(activity, selectEngine, null)
        }
    }

    /**
     * 选中图片
     * @param fragment 在Fragment中使用
     * @param selectEngine 选择图片引擎 不传 使用默认的
     */
    fun setPictureListener(fragment: Fragment, selectEngine: SelectPictureEngineInterface?) {
        if (selectEngine == null) {
            setPictureListener(fragment, SelectPictureEngine(), null)
        } else {
            setPictureListener(fragment, selectEngine, null)
        }
    }


    /**
     * 设置选择了 图片监听 、 选择图片和 查看图片
     * @param selectEngine 选择图片的引擎
     * @param imageEngine 不传图片引擎 使用默认的glide
     */
    fun setPictureListener(activity: FragmentActivity, selectEngine: SelectPictureEngineInterface?, imageEngine: DisplayPictureEngine?) {
        if (selectEngine == null) {
            throw TimeoutException("没有选择图片引擎")
        }
        selectImageAdapter.setSelectImageListener(object : SelectImageListener {
            override fun selectImageClick() {
                requestPermission(activity, selectEngine)
            }

            override fun checkImageClick(imageList: MutableList<SelectPictureBean>, view: ImageView, position: Int) {
                if (isCanLookImage) {
                    selectEngine.checkPicture(activity, imageList, view, position)
                }
            }
        }, imageEngine)
    }


    /**
     * Fragment里面 选择图片
     * @param selectEngine 选择图片的引擎
     * @param imageEngine 不传图片引擎 使用默认的glide
     */
    fun setPictureListener(fragment: Fragment, selectEngine: SelectPictureEngineInterface?, imageEngine: DisplayPictureEngine?) {
        if (selectEngine == null) {
            throw TimeoutException("没有选择图片引擎")
        }
        selectImageAdapter.setSelectImageListener(object : SelectImageListener {
            override fun selectImageClick() {
                requestPermission(fragment, selectEngine)
            }

            override fun checkImageClick(imageList: MutableList<SelectPictureBean>, view: ImageView, position: Int) {
                if (isCanLookImage) {
                    selectEngine.checkPicture(fragment.requireActivity(), imageList, view, position)
                }
            }
        }, imageEngine)
    }


    /**
     * activity中 获取权限
     * @param selectEngine 选择图片引擎
     */
    private fun requestPermission(activity: FragmentActivity, selectEngine: SelectPictureEngineInterface) {
        createPermissionEngine()
        permissionEngine?.requestStoragePermission(activity, object : PermissionEngineListener {
            override fun permissionPass() {
                when (selectType) {
                    SELECT_PICTURE -> {
                        activitySelectPicture(activity, selectEngine)
                    }
                    SELECT_VIDEO -> {
                        activitySelectVideo(activity, selectEngine)
                    }
                    SELECT_PICTURE_VIDEO -> {
                        activitySelectPictureVideo(activity, selectEngine)
                    }
                }
            }

            override fun permissionNoPass() {

            }
        })
    }


    /**
     * fragment中获取权限
     * @param selectEngine 选择图片引擎
     */
    private fun requestPermission(fragment: Fragment, selectEngine: SelectPictureEngineInterface) {
        createPermissionEngine()
        permissionEngine?.requestStoragePermission(fragment, object : PermissionEngineListener {
            override fun permissionPass() {
                when (selectType) {
                    SELECT_PICTURE -> {
                        fragmentSelectPicture(fragment, selectEngine)
                    }
                    SELECT_VIDEO -> {
                        fragmentSelectVideo(fragment, selectEngine)
                    }
                    SELECT_PICTURE_VIDEO -> {
                        fragmentSelectPictureVideo(fragment, selectEngine)
                    }
                }
            }

            override fun permissionNoPass() {

            }
        })
    }


    private fun activitySelectPicture(activity: FragmentActivity, selectEngine: SelectPictureEngineInterface) {
        selectEngine.selectPicture(activity, selectImageNum - getImageList().size, getImageList(), selectPictureListListener)
    }

    /**
     * 选择视频
     */
    private fun activitySelectVideo(activity: FragmentActivity, selectEngine: SelectPictureEngineInterface) {
        selectEngine.selectVideo(activity, selectVideoNum - getImageList().size, getImageList(), selectPictureListListener)
    }

    private fun fragmentSelectVideo(fragment: Fragment, selectEngine: SelectPictureEngineInterface) {
        selectEngine.selectVideo(fragment, selectVideoNum - getImageList().size, getImageList(), selectPictureListListener)
    }


    private fun activitySelectPictureVideo(activity: FragmentActivity, selectEngine: SelectPictureEngineInterface) {
        selectEngine.selectPictureVideo(
            activity,
            selectImageNum - getImageList().size,
            selectVideoNum - getImageList().size,

            getImageList(),
            selectPictureListListener
        )
    }

    private fun fragmentSelectPictureVideo(fragment: Fragment, selectEngine: SelectPictureEngineInterface) {
        selectEngine.selectPictureVideo(
            fragment, selectImageNum - getImageList().size,selectVideoNum - getImageList().size,  getImageList(), selectPictureListListener
        )
    }


    /**
     * 在Fragment中选择图片
     */
    private fun fragmentSelectPicture(fragment: Fragment, selectEngine: SelectPictureEngineInterface) {
        selectEngine.selectPicture(fragment, selectImageNum - getImageList().size, getImageList(), selectPictureListListener)
    }


    private var selectPictureListListener = object : SelectPictureListInterface {
        override fun selectPictureList(pictureList: MutableList<SelectPictureBean>) {
            //设置选中的图片
            setPictureList(pictureList)
        }

        override fun selectPictureFail() {

        }
    }

    private fun createPermissionEngine() {
        if (permissionEngine == null) {
            permissionEngine = PermissionEngine()
        }
    }


    /**
     * 设置图片引擎
     */
    fun setImageEngine(imageEngine: DisplayPictureEngine) {
        selectImageAdapter.setImageEngine(imageEngine)
    }

    /**
     * 获取图片集合
     */
    private fun getImageList(): MutableList<SelectPictureBean> {
        return selectImageAdapter.getPictureList()
    }

    /**
     * 获取筛选地址
     *
     * 适用于 已经上传的图片 和  选择本地图片混用。
     */
    fun getImageList(screenStr: String): MutableList<SelectPictureBean> {
        val imageList = selectImageAdapter.getPictureList()
        val selectPictureList = mutableListOf<SelectPictureBean>()
        for (path in imageList) {
            if (!path.imagePath.contains(screenStr)) {
                selectPictureList.add(path)
            }
        }
        return selectPictureList
    }


    /**
     * 可以选择的图片数量
     */
    fun setSelectImageNum(selectImageNum: Int) {
        this.selectImageNum = selectImageNum
    }

    /**
     * 设置一行显示几张图片
     */
    fun setHorizontalImageNum(horizontalImageNum: Int) {
        this.horizontalImageNum = horizontalImageNum
    }

    /**
     * 是否显示删除按钮
     */
    fun setShowDeleteIcon(isShowDelete: Boolean) {
        //如果图片资源未null 则强制不显示删除按钮
        if (deleteIconRes == null) {
            isShowDeleteIcon = false
            return
        }
        this.isShowDeleteIcon = isShowDelete
    }

    /**
     * 设置删除按钮图片资源
     */
    @SuppressLint("SupportAnnotationUsage")
    @DrawableRes
    fun setDeleteIcon(deleteIconRes: Int) {
        this.deleteIconRes = deleteIconRes
    }

    fun dip2px(context: Context, dip: Float): Float {
        //获取屏幕的密度
        val density = context.resources.displayMetrics.density
        return (dip * density + 0.5f)
    }
}














