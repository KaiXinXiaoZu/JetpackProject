package com.yzhg.toollibrary.recycle.selectimage.engine.glide

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.luck.picture.lib.utils.ActivityCompatHelper

/**
 * author : yzhg
 * 包名: com.yzhg.toollibrary.recycle.selectimage.engine
 * 时间: 2023-02-16 13:06
 * 描述：选择图片布局 显示引擎
 */
class GlideDisplayPictureEngine : DisplayPictureEngine {

    override fun loadImage(context: Context, url: String, errorImage: Int, imageView: ImageView) {
        if (!ActivityCompatHelper.assertValidRequest(context)) {
            return
        }
        val options = RequestOptions().placeholder(errorImage).placeholder(errorImage)
        Glide.with(context)
            .load(url)
            .apply(options)
            .into(imageView)
    }

    override fun loadImage(context: Context, imageRes: Int, errorImage: Int, imageView: ImageView) {
        if (!ActivityCompatHelper.assertValidRequest(context)) {
            return
        }
        val options = RequestOptions().placeholder(errorImage).placeholder(errorImage)
        Glide.with(context)
            .load(imageRes)
            .apply(options)
            .into(imageView)
    }

    override fun loadImage(context: Context, imageRes: Int, imageView: ImageView) {
        if (!ActivityCompatHelper.assertValidRequest(context)) {
            return
        }
        Glide.with(context)
            .load(imageRes)
            .into(imageView)
    }
}