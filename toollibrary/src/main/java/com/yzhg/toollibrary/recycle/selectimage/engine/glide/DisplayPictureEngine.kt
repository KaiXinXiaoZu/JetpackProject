package com.yzhg.toollibrary.recycle.selectimage.engine.glide

import android.content.Context
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.annotation.NonNull

/**
 * author : yzhg
 * 包名: com.yzhg.toollibrary.recycle.selectimage.engine
 * 时间: 2023-02-16 12:51
 * 描述：
 */
interface DisplayPictureEngine {

    /**
     * 显示图片
     * @param context 上下问
     * @param url 图片地址
     * @param imageView 图片控件
     */
    fun loadImage(@NonNull context: Context, @NonNull url: String, @DrawableRes errorImage: Int, @NonNull imageView: ImageView)


    /**
     * 加载本地资源图片
     */
    fun loadImage(@NonNull context: Context, @DrawableRes imageRes: Int, @DrawableRes errorImage: Int, @NonNull imageView: ImageView)


    /**
     * 加载本地资源图片
     */
    fun loadImage(@NonNull context: Context, @DrawableRes imageRes: Int, @NonNull imageView: ImageView)
}