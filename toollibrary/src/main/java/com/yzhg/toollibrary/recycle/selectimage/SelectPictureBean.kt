package com.yzhg.toollibrary.recycle.selectimage

class SelectPictureBean {

    //图片ID
    var id: Long = 0

    //视频缩略图
    var videoThumbnailPath: String? = ""

    //图片、视频地址
    var imagePath: String = ""

    var path: String? = ""

    var realPath: String? = ""

}