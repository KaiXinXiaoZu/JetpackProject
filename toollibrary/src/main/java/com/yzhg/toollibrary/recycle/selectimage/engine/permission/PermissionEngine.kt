package com.yzhg.toollibrary.recycle.selectimage.engine.permission

import android.Manifest
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.yzhg.toollibrary.permission.PermissionTool
import com.yzhg.toollibrary.permission.PermissionTool.OnPermissionListener

/**
 * author : yzhg
 * 包名: com.yzhg.toollibrary.recycle.selectimage.engine.permission
 * 时间: 2023-02-17 11:51
 * 描述：
 */
class PermissionEngine : PermissionEngineInterface {

    override fun requestStoragePermission(context: FragmentActivity,passListener:PermissionEngineListener) {
        PermissionTool.getInstance().requestImageAndVideoPermission(context,object :OnPermissionListener{
            override fun agreePermission() {
                passListener.permissionPass()
            }

            override fun refusePermission() {
                passListener.permissionNoPass()
            }
        })
    }

    override fun requestStoragePermission(context: Fragment, passListener: PermissionEngineListener) {
        PermissionTool.getInstance().requestImageAndVideoPermission(context.requireContext(),object :OnPermissionListener{
            override fun agreePermission() {
                passListener.permissionPass()
            }

            override fun refusePermission() {
                passListener.permissionNoPass()
            }
        })
    }

    override fun requestTakePhotosPermission(context: FragmentActivity,passListener:PermissionEngineListener) {
        PermissionTool.getInstance().requestCameraPermission(context,object :OnPermissionListener{
            override fun agreePermission() {
                passListener.permissionPass()
            }

            override fun refusePermission() {
                passListener.permissionNoPass()
            }
        })
    }

    override fun requestTakePhotosPermission(context: Fragment, passListener: PermissionEngineListener) {
        PermissionTool.getInstance().requestCameraPermission(context.requireContext(),object :OnPermissionListener{
            override fun agreePermission() {
                passListener.permissionPass()
            }

            override fun refusePermission() {
                passListener.permissionNoPass()
            }
        })
    }

    override fun requestStorageTakePhotosPermission(context: FragmentActivity,passListener:PermissionEngineListener) {
        PermissionTool.getInstance().requestImageVideoCameraPermission(context,object :OnPermissionListener{
            override fun agreePermission() {
                passListener.permissionPass()
            }

            override fun refusePermission() {
                passListener.permissionNoPass()
            }
        })
    }

    override fun requestStorageTakePhotosPermission(context: Fragment, passListener: PermissionEngineListener) {
        PermissionTool.getInstance().requestImageVideoCameraPermission(context.requireContext(),object :OnPermissionListener{
            override fun agreePermission() {
                passListener.permissionPass()
            }

            override fun refusePermission() {
                passListener.permissionNoPass()
            }
        })
    }
}