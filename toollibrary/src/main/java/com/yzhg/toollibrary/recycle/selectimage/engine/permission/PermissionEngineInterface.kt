package com.yzhg.toollibrary.recycle.selectimage.engine.permission

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

/**
 * author : yzhg
 * 包名: com.yzhg.toollibrary.recycle.selectimage.engine.permission
 * 时间: 2023-02-17 11:45
 * 描述：权限引擎
 */
interface PermissionEngineInterface {

    /**
     * 请求  储存权限
     */
    fun requestStoragePermission(context: FragmentActivity,passListener:PermissionEngineListener)

    /**
     * 请求拍照权限
     */
    fun requestTakePhotosPermission(context: FragmentActivity,passListener:PermissionEngineListener)

    /**
     * 请求全部权限
     */
    fun requestStorageTakePhotosPermission(context: FragmentActivity,passListener:PermissionEngineListener)

    /**
     * 请求  储存权限
     */
    fun requestStoragePermission(context: Fragment,passListener:PermissionEngineListener)

    /**
     * 请求拍照权限
     */
    fun requestTakePhotosPermission(context: Fragment,passListener:PermissionEngineListener)

    /**
     * 请求全部权限
     */
    fun requestStorageTakePhotosPermission(context: Fragment,passListener:PermissionEngineListener)
}