package com.yzhg.toollibrary.recycle.selectimage.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.yzhg.toollibrary.R
import com.yzhg.toollibrary.databinding.ItemSelectImageLayoutBinding
import com.yzhg.toollibrary.recycle.selectimage.SelectImageRecycleView
import com.yzhg.toollibrary.recycle.selectimage.SelectPictureBean
import com.yzhg.toollibrary.recycle.selectimage.engine.glide.GlideDisplayPictureEngine
import com.yzhg.toollibrary.recycle.selectimage.engine.glide.DisplayPictureEngine
import com.yzhg.toollibrary.recycle.selectimage.holder.ItemSelectImageHolder
import com.yzhg.toollibrary.recycle.selectimage.listener.SelectImageListener

/**
 * @param selectImageNum 设置最大选择数量 默认 9张
 */
class ItemSelectImageAdapter(var context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /**
     * 设置多条目  正常的图片
     */
    private val ITEM_TYPE_PICTURE = 0

    /**
     * 选择图片image
     */
    private val ITEM_TYPE_CAMERA = 1

    /**
     * 图片资源
     */
    private var pictureList: MutableList<SelectPictureBean> = mutableListOf()

    /**
     * 图片显示引擎
     */
    private var imageEngine: DisplayPictureEngine? = null

    /**
     * 删除按钮的位置 默认右上角
     */
    private var deleteLocation = SelectImageRecycleView.D_RIGHT_TOP

    /**
     * 是否显示删除按钮
     */
    private var isShowDeleteIcon = true

    /**
     * 设置删除按钮的位置
     */
    fun setDeleteLocation(deleteLocation: Int) {
        this.deleteLocation = deleteLocation
    }

    fun isShowDeleteIcon(isShowDeleteIcon: Boolean) {
        this.isShowDeleteIcon = isShowDeleteIcon
    }

    /**
     * 删除按钮如果显示在中间需要设置背景的透明度
     */
    private var deleteBgAlpha = 0.6f

    fun setDeleteBgAlpha(deleteBgAlpha: Float) {
        this.deleteBgAlpha = deleteBgAlpha
    }

    /**
     * 设置图片圆角
     */
    private var imageRadius = 8.0f

    fun setImageRadius(imageRadius: Float) {
        this.imageRadius = imageRadius
    }

    /**
     * 设置删除按钮
     */
    @SuppressLint("SupportAnnotationUsage")
    @DrawableRes
    private var deleteIconRes: Int? = null

    fun setDeleteIconRes(deleteIconRes: Int?) {
        this.deleteIconRes = deleteIconRes
    }

    /**
     * 设置删除图标的宽
     *
     * 删除图标的宽高,点击区域的宽高是 图标的二倍
     */
    private var deleteWidth = 0f

    /**
     * 设置删除图标的高
     *  删除图标的宽高,点击区域的宽高是 图标的二倍
     */
    private var deleteHeight = 0f

    fun setDeleteWidth(deleteWidth: Float) {
        this.deleteWidth = deleteWidth
    }

    fun setDeleteHeight(deleteHeight: Float) {
        this.deleteHeight = deleteHeight
    }

    /**
     * 设置图片资源
     */
    public fun setPictureList(imageList: MutableList<SelectPictureBean>) {
        if (imageList.isNotEmpty()) {
            this.pictureList.addAll(imageList)
            //刷新条目
            notifyItemRangeChanged(0, this.pictureList.size)
        }
    }

    /**
     * 获取图片资源
     */
    fun getPictureList(): MutableList<SelectPictureBean> {
        return pictureList
    }

    /**
     * 设置最大选择数量
     */
    private var selectImageNum: Int = 9

    /**
     * 设置最大选择数量
     */
    fun setMaxSelectNum(selectImageNum: Int) {
        this.selectImageNum = selectImageNum
    }

    /**
     * 获取最大数量
     */
    fun getMaxSelectNum(): Int {
        return selectImageNum
    }

    /**
     * 设置图片显示引擎
     */
    fun setImageEngine(imageEngine: DisplayPictureEngine) {
        this.imageEngine = imageEngine
    }
    /**
     * 获取引擎
     */

    /**
     * 创建布局
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemSelectImageLayoutBinding: ItemSelectImageLayoutBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_select_image_layout, parent, false)
        return ItemSelectImageHolder(itemSelectImageLayoutBinding)
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItemPosition(position)) {
            ITEM_TYPE_PICTURE
        } else {
            ITEM_TYPE_CAMERA
        }
    }

    private fun getItemPosition(position: Int): Boolean {
        return if (pictureList.size == 0) {
            false
        } else {
            position < pictureList.size
        }
    }

    /**
     * 绑定布局
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemHolder: ItemSelectImageHolder = holder as ItemSelectImageHolder
        val selectImageBinding = itemHolder.selectImageBinding

        selectImageBinding.viewBgZ.visibility = View.GONE
        selectImageBinding.butCenterDelete.visibility = View.GONE
        selectImageBinding.llRightBottomIcon.visibility = View.GONE
        selectImageBinding.llRightTopDelete.visibility = View.GONE
        selectImageBinding.ivSelectImage.cornerRadius = imageRadius

        //设置默认的删除图片
        selectImageBinding.ivRightBottomIcon.setBackgroundResource(R.mipmap.img_delete_icon)
        selectImageBinding.ivRightTopDelete.setBackgroundResource(R.mipmap.img_delete_icon)
        selectImageBinding.butCenterDelete.setBackgroundResource(R.mipmap.img_delete_icon)

        if (deleteIconRes != null) {
            selectImageBinding.ivRightBottomIcon.setBackgroundResource(deleteIconRes!!)
            selectImageBinding.ivRightTopDelete.setBackgroundResource(deleteIconRes!!)
            selectImageBinding.butCenterDelete.setBackgroundResource(deleteIconRes!!)
        }

        //设置删除图片的宽高， 点击区域是图标宽高的二倍
        val rightBottomParams: LinearLayout.LayoutParams = selectImageBinding.ivRightBottomIcon.layoutParams as LinearLayout.LayoutParams
        rightBottomParams.width = deleteWidth.toInt()
        rightBottomParams.height = deleteWidth.toInt()
        selectImageBinding.ivRightBottomIcon.layoutParams = rightBottomParams

        val rightTopParams: LinearLayout.LayoutParams = selectImageBinding.ivRightTopDelete.layoutParams as LinearLayout.LayoutParams
        rightTopParams.width = deleteWidth.toInt()
        rightTopParams.height = deleteWidth.toInt()
        selectImageBinding.ivRightTopDelete.layoutParams = rightTopParams

        val centerParams: ConstraintLayout.LayoutParams = selectImageBinding.butCenterDelete.layoutParams as ConstraintLayout.LayoutParams
        centerParams.width = deleteWidth.toInt()
        centerParams.height = deleteWidth.toInt()
        selectImageBinding.butCenterDelete.layoutParams = centerParams

        //设置 右上和右下 点击区域
        val llRightTopParams: ConstraintLayout.LayoutParams = selectImageBinding.llRightTopDelete.layoutParams as ConstraintLayout.LayoutParams
        llRightTopParams.width = (deleteWidth*1.5).toInt()
        llRightTopParams.height = (deleteWidth*1.5).toInt()
        selectImageBinding.llRightTopDelete.layoutParams = llRightTopParams

        val llRightBottomParams: ConstraintLayout.LayoutParams = selectImageBinding.llRightBottomIcon.layoutParams as ConstraintLayout.LayoutParams
        llRightBottomParams.width = (deleteWidth*1.5).toInt()
        llRightBottomParams.height = (deleteWidth*1.5).toInt()
        selectImageBinding.llRightBottomIcon.layoutParams = llRightBottomParams


        if (getItemViewType(position) == ITEM_TYPE_CAMERA) {
            //相机图片
            if (imageEngine != null) {
                imageEngine?.loadImage(context, R.mipmap.select_picture_video, selectImageBinding.ivSelectImage)
            } else {
                selectImageBinding.ivSelectImage.setBackgroundResource(R.mipmap.select_picture_video)
            }
        } else if (getItemViewType(position) == ITEM_TYPE_PICTURE) {
            if (isShowDeleteIcon) {
                when (deleteLocation) {
                    SelectImageRecycleView.D_RIGHT_TOP -> {
                        selectImageBinding.llRightTopDelete.visibility = View.VISIBLE
                    }
                    SelectImageRecycleView.D_RIGHT_BOT -> {
                        selectImageBinding.llRightBottomIcon.visibility = View.VISIBLE
                    }
                    SelectImageRecycleView.D_CENTER -> {
                        selectImageBinding.viewBgZ.visibility = View.VISIBLE
                        selectImageBinding.butCenterDelete.visibility = View.VISIBLE
                        selectImageBinding.viewBgZ.alpha = deleteBgAlpha
                    }
                }
            }


            //图片布局
            val selectPictureBean = pictureList[position]
            if (imageEngine != null) {
                imageEngine?.loadImage(context, selectPictureBean.imagePath, R.mipmap.select_picture_video, selectImageBinding.ivSelectImage)
            } else {
                selectImageBinding.ivSelectImage.setBackgroundResource(R.mipmap.img_default_icon)
            }
        }

        //条目点击事件
        selectImageBinding.ivSelectImage.setOnClickListener {
            if (getItemViewType(position) == ITEM_TYPE_CAMERA) {
                if (selectImageListener != null) {
                    selectImageListener?.selectImageClick()
                }
            } else {
                if (selectImageListener != null) {
                    selectImageListener?.checkImageClick(pictureList, selectImageBinding.ivSelectImage, position)
                }
            }
        }

        //删除图片点击事件
        selectImageBinding.viewBgZ.setOnClickListener {
            notifyChanged(position)
        }
        selectImageBinding.butCenterDelete.setOnClickListener {
            notifyChanged(position)
        }
        selectImageBinding.llRightBottomIcon.setOnClickListener {
            notifyChanged(position)
        }
        selectImageBinding.llRightTopDelete.setOnClickListener {
            notifyChanged(position)
        }
        selectImageBinding.viewBgZ.setOnClickListener {
            notifyChanged(position)
        }
    }

    private fun notifyChanged(position: Int) {
        pictureList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(0, this.pictureList.size)
    }

    override fun getItemCount(): Int {
        return if (pictureList.size < selectImageNum) {
            pictureList.size + 1
        } else {
            pictureList.size
        }
    }

    private var selectImageListener: SelectImageListener? = null

    fun setSelectImageListener(selectImageListener: SelectImageListener, imageEngine: DisplayPictureEngine?) {
        //传空使用默认的引擎
        if (imageEngine == null) {
            this.imageEngine = GlideDisplayPictureEngine()
        } else {
            this.imageEngine = imageEngine
        }
        this.selectImageListener = selectImageListener
    }
}