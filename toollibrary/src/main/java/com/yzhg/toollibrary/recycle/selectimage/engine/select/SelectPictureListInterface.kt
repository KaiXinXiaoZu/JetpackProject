package com.yzhg.toollibrary.recycle.selectimage.engine.select

import com.yzhg.toollibrary.recycle.selectimage.SelectPictureBean

interface SelectPictureListInterface {

    /**
     * 选择图片成功 转化为String类型
     */
    fun selectPictureList(pictureList:MutableList<SelectPictureBean>)

    /**
     * 选择图片失败
     */
    fun selectPictureFail()
}