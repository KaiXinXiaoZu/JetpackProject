package com.yzhg.toollibrary.recycle.selectimage.engine.select

import android.app.Activity
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.yzhg.toollibrary.recycle.selectimage.SelectPictureBean

/**
 * 选择图片引擎
 */
interface SelectPictureEngineInterface {

    /**
     * 选择图片  activity中使用
     */
    fun selectPicture(
        activity: FragmentActivity,
        selectNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    )

    /**
     * 选择图片 fragment中使用
     */
    fun selectPicture(
        fragment: Fragment,
        selectNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    )

    /**
     * 选择视频 activity中使用
     */
    fun selectVideo(
        activity: FragmentActivity,
        videoNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    )

    /**
     * 选择视频  fragment中使用
     */
    fun selectVideo(
        fragment: Fragment,
        videoNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    )


    /**
     * 选择图片和视频
     * activity中使用
     */
    fun selectPictureVideo(
        activity: FragmentActivity,
        pictureNum: Int,
        videoNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    )

    /**
     * 选择图片和视频
     * fragment中使用
     */
    fun selectPictureVideo(
        fragment: Fragment,
        pictureNum: Int,
        videoNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    )

    /**
     * 查看图片
     */
    fun checkPicture(activity: FragmentActivity, pictureList: MutableList<SelectPictureBean>, view: ImageView, position: Int)


}