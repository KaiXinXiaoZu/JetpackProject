package com.yzhg.toollibrary.recycle.selectimage.engine.permission

/**
 * author : yzhg
 * 包名: com.yzhg.toollibrary.recycle.selectimage.engine.permission
 * 时间: 2023-02-17 18:38
 * 描述：
 */
interface PermissionEngineListener {

    /**
     * 权限通过
     */
    fun permissionPass()
    /**
     * 权限拒绝
     */
    fun permissionNoPass()
}