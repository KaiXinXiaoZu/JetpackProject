package com.yzhg.toollibrary.recycle.selectimage.engine.select

import android.app.Activity
import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.luck.picture.lib.config.SelectMimeType
import com.luck.picture.lib.entity.LocalMedia
import com.luck.picture.lib.interfaces.OnResultCallbackListener
import com.yzhg.toollibrary.image.PictureSelectorTools
import com.yzhg.toollibrary.image.PopLookImageTools
import com.yzhg.toollibrary.recycle.selectimage.SelectPictureBean

class SelectPictureEngine : SelectPictureEngineInterface {

    /**
     * 选择图片
     * activity中使用
     */
    override fun selectPicture(
        activity: FragmentActivity,
        selectNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    ) {
        PictureSelectorTools.goPhotoAlbumActivity(
            activity, SelectMimeType.ofImage(), selectNum, getLocalMediaList(activity, selectPicture),
            onResultCallbackListener(pictureListInterface)
        )
    }

    /**
     * 选择图片 Fragment中使用
     */
    override fun selectPicture(
        fragment: Fragment,
        selectNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    ) {
        PictureSelectorTools.goPhotoAlbumFragment(
            fragment, SelectMimeType.ofImage(), selectNum, getLocalMediaList(fragment.requireActivity(), selectPicture),
            onResultCallbackListener(pictureListInterface)
        )
    }

    /**
     * 选择视频
     * @param videoNum 视频数量
     */
    override fun selectVideo(
        activity: FragmentActivity,
        videoNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    ) {
        PictureSelectorTools.goPhotoAlbumActivity(
            activity,
            SelectMimeType.ofVideo(),
            1,
            videoNum,
            getLocalMediaList(activity, selectPicture),
            onResultCallbackListener(pictureListInterface)
        )
    }

    private fun getLocalMediaList(activity: Context, selectPicture: MutableList<SelectPictureBean>): MutableList<LocalMedia> {
        val selectPictureList: MutableList<LocalMedia> = mutableListOf()
        for (selectPictureBean in selectPicture) {
            // selectPictureList.add(LocalMedia.generateLocalMedia(activity, selectPictureBean.imagePath))
            val localMedia: LocalMedia = LocalMedia()
            localMedia.id = selectPictureBean.id
            localMedia.path = selectPictureBean.path
            localMedia.realPath = selectPictureBean.realPath
            selectPictureList.add(localMedia)
        }
        return selectPictureList
    }

    /**
     * Fragment中选择视频
     * @param videoNum 视频数量
     */
    override fun selectVideo(
        fragment: Fragment,
        videoNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    ) {
        PictureSelectorTools.goPhotoAlbumFragment(
            fragment,
            SelectMimeType.ofVideo(),
            1,
            videoNum,
            getLocalMediaList(fragment.requireActivity(), selectPicture),
            onResultCallbackListener(pictureListInterface)
        )
    }

    /**
     * Activity中选择 图片 和 视频
     * @param pictureNum 图片数量
     * @param videoNum 视频数量
     * 图片和视频不能一起选择
     */
    override fun selectPictureVideo(
        activity: FragmentActivity,
        pictureNum: Int,
        videoNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    ) {
        PictureSelectorTools.goPhotoAlbumActivity(
            activity,
            SelectMimeType.ofAll(),
            pictureNum,
            videoNum,
            getLocalMediaList(activity, selectPicture),
            onResultCallbackListener(pictureListInterface)
        )
    }

    /**
     * fragment中选择 图片 和 视频
     * @param pictureNum 图片数量
     * @param videoNum 视频数量
     * 图片和视频不能一起选择
     */
    override fun selectPictureVideo(
        fragment: Fragment,
        pictureNum: Int,
        videoNum: Int,
        selectPicture: MutableList<SelectPictureBean>,
        pictureListInterface: SelectPictureListInterface,
    ) {
        PictureSelectorTools.goPhotoAlbumFragment(
            fragment,
            SelectMimeType.ofAll(),
            pictureNum,
            videoNum,
            getLocalMediaList(fragment.requireActivity(), selectPicture),
            onResultCallbackListener(pictureListInterface)
        )
    }

    private fun onResultCallbackListener(pictureListInterface: SelectPictureListInterface) =
        object : OnResultCallbackListener<LocalMedia> {
            override fun onResult(result: ArrayList<LocalMedia>?) {
                selectPictureSuccess(result, pictureListInterface)
            }

            override fun onCancel() {
                pictureListInterface.selectPictureFail()
            }
        }

    /**
     * 选择图片成功
     */
    private fun selectPictureSuccess(
        result: ArrayList<LocalMedia>?,
        pictureListInterface: SelectPictureListInterface,
    ) {
        if (result != null) {
            val pictureList: MutableList<SelectPictureBean> = getPictureList(result)
            pictureListInterface.selectPictureList(pictureList)
        } else {
            pictureListInterface.selectPictureFail()
        }
    }

    /***
     * 获取图片地址
     */
    private fun getPictureList(result: ArrayList<LocalMedia>): MutableList<SelectPictureBean> {
        val imagePathList = mutableListOf<SelectPictureBean>()
        for (localMedia in result) {
            imagePathList.add(getSinglePhotoUri(localMedia))
        }
        return imagePathList
    }

    /**
     * 获取文件路径
     */
    private fun getSinglePhotoUri(localMedia: LocalMedia?): SelectPictureBean {
        val selectPictureBean = SelectPictureBean()
        if (localMedia == null) {
            return selectPictureBean
        }
        selectPictureBean.id = localMedia.id
        selectPictureBean.videoThumbnailPath = localMedia.videoThumbnailPath
        selectPictureBean.path = localMedia.path
        selectPictureBean.realPath = localMedia.realPath

        if (localMedia.isCompressed) {
            selectPictureBean.imagePath = localMedia.compressPath
        } else {
            val version = Build.VERSION.SDK_INT
            if (version >= 29) {
                if (TextUtils.isEmpty(localMedia.sandboxPath)) {
                    selectPictureBean.imagePath = localMedia.realPath
                } else {
                    selectPictureBean.imagePath = localMedia.sandboxPath
                }
            } else {
                selectPictureBean.imagePath = localMedia.path
            }
        }
        return selectPictureBean
    }

    /**
     * 查看图片 selectPicture: MutableList<SelectPictureBean>
     */
    override fun checkPicture(activity: FragmentActivity, pictureList: MutableList<SelectPictureBean>, view: ImageView, position: Int) {
        /*  val imageList = mutableListOf<String>()
          for (selectPictureBean in pictureList) {
              imageList.add(selectPictureBean.imagePath)
          }*/
        val localMediaList: MutableList<LocalMedia> = getLocalMediaList(activity, pictureList)
        PictureSelectorTools.openImageList(activity, position, localMediaList as ArrayList<LocalMedia>)
        // PopLookImageTools.lookImageNoNull(activity, view, position, imageList)
    }


}