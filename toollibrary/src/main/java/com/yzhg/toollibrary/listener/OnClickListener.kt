package com.yzhg.toollibrary.listener

import android.view.View

/**
 * @Description: 无返回值 事件操作
 * @Author: yzhg
 * @Date: 2020/7/17 10:33
 */
interface OnClickListener<T> {
    fun onClick(v: View?, clickStr: T)
}