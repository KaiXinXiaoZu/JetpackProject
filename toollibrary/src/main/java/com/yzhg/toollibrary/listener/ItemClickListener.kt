package com.yzhg.toollibrary.listener

import android.view.View


/*
 * 作者：yzhg
 * 时间：2022-03-02 08:38
 * 包名：shuini.manager.yiande.com.ui
 * 描述：
 */
interface ItemClickListener<T> {

    fun itemOnClicked(v: View?, tag: String, position: Int, t: T?)

}