package com.yzhg.toollibrary.listener


/*
 * 作者：yzhg
 * 时间：2021-12-11 09:25
 * 包名：com.yzhg.toollibrary.listener
 * 描述：
 */
interface OpenMapListener {

    /**
     * 没有安装APP
     */
    fun noInstallAppListener()

    /**
     * 没有找到位置
     */
    fun noAddressListener()

}