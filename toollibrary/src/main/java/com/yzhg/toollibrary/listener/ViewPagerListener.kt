package com.yzhg.toollibrary.listener

import androidx.viewpager.widget.ViewPager


/*
 * 作者：yzhg
 * 时间：2021-12-03 11:15
 * 包名：com.yzhg.toollibrary.listener
 * 描述：
 */
open class ViewPagerListener : ViewPager.OnPageChangeListener {
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

}