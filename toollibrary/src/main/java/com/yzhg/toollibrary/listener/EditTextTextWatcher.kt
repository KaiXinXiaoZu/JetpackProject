package com.yzhg.toollibrary.listener

import android.text.Editable
import android.text.TextWatcher


/*
 * 作者：yzhg
 * 时间：2021-11-27 16:26
 * 包名：com.yzhg.toollibrary.listener
 * 描述：
 */
open class EditTextTextWatcher: TextWatcher {

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun afterTextChanged(editable: Editable) {
    }
}