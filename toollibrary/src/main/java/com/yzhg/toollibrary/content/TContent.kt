package com.yzhg.toollibrary.content


/*
 * 作者：yzhg
 * 时间：2021-12-11 09:18
 * 包名：com.yzhg.toollibrary.content
 * 描述：
 */
object TContent {
    /**
     * 是否打印日志  正式上线之前要改为ture   false为测试模式
     */
    const val DEBUG = false

    /**
     * 高德地图包名
     */
    const val GD_MAP_PACKAGER_NAME = "com.autonavi.minimap"


    /**
     * 百度地图包名
     */
    const val BD_MAP_PACKAGER_NAME = "com.baidu.BaiduMap"

    /**
     * 腾讯地图包名
     */
    const val TENCENT_MAP_PACKAGER_NAME = "com.tencent.map"


}