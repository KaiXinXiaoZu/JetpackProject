package com.yzhg.toollibrary.common;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class HtmlTools {


    /**
     * 在Html上追加body标签
     */
    public static String addBody() {
        Document doc = Jsoup.parseBodyFragment("<html><head><title>Example HTML</title></head></html>");

        if (doc.body() == null) {
            // 创建一个新的<body>标签
            Element body = doc.appendElement("body");

            // 添加内容到<body>标签（可选）
            body.append("<p>This is the body content</p>");
        }

        return doc.outerHtml();
        // 输出修改后的HTML
        //  System.out.println(doc.outerHtml());
    }

}
