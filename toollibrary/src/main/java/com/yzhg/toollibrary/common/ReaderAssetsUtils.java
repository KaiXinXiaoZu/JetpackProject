package com.yzhg.toollibrary.common;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 包名：uni.UNIEA80E46.utils.common
 * 创建人：yzhg
 * 时间：2021/04/06 11:24
 * 描述：
 */
public class ReaderAssetsUtils {


    public static String readerAssetsJson(Context context, String finePath) {
        BufferedReader br = null;
        InputStreamReader inputStreamReader = null;
        try {
            AssetManager assetManager = context.getAssets(); //获得assets资源管理器（assets中的文件无法直接访问，可以使用AssetManager访问）
            inputStreamReader = new InputStreamReader(assetManager.open(finePath), "UTF-8"); //使用IO流读取json文件内容
            //使用字符高效流
            br = new BufferedReader(inputStreamReader);
            String line;
            StringBuilder builder = new StringBuilder();
            while ((line = br.readLine()) != null) {
                builder.append(line);
            }
            br.close();
            inputStreamReader.close();
            return builder.toString();
        } catch (Exception e) {
            return "";
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                return "";
            }
            try {
                inputStreamReader.close();
            } catch (IOException e) {
                return "";
            }
        }
    }
}
