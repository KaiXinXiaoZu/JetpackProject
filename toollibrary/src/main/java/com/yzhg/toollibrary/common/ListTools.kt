package com.wsdz.tools.common

import java.util.*

/**
 * 包名：com.wsdz.tools.common
 * 创建人：yzhg
 * 时间：2021/06/16 18:26
 * 描述：List工具
 */
object ListTools {

    /**
     * list 集合分组
     *
     * @param list    待分组集合
     * @param groupBy 分组Key算法
     * @param <K>     分组Key类型
     * @param <V>     行数据类型
     * @return 分组后的Map集合
     */
    fun <K, V> groupBy(list: List<V>, groupBy: GroupBy<K, V>): Map<K, List<V>?>? {
        return groupBy(list as Collection<V>, groupBy)
    }


    /**
     * list 集合分组
     *
     * @param list    待分组集合
     * @param groupBy 分组Key算法
     * @param <K>     分组Key类型
     * @param <V>     行数据类型
     * @return 分组后的Map集合
     */
    fun <K, V> groupBy(list: Collection<V>, groupBy: GroupBy<K, V>): Map<K, MutableList<V>>? {
        val resultMap: MutableMap<K, MutableList<V>> = LinkedHashMap()
        for (e in list) {
            val k = groupBy.groupBy(e)
            if (resultMap.containsKey(k)) {
                resultMap[k]!!.add(e)
            } else {
                val tmp: MutableList<V> = LinkedList()
                tmp.add(e)
                resultMap[k] = tmp
            }
        }
        return resultMap
    }

    /**
     * List分组
     *
     * @param <K> 返回分组Key
     * @param <V> 分组行
     */
    interface GroupBy<K, V> {
        fun groupBy(row: V): K
    }
}