package com.yzhg.toollibrary.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.yzhg.toollibrary.R;
import com.yzhg.toollibrary.Tools;


/**
 * 作 者: yzhg
 * 历 史: (版本) 1.0
 * 描 述: 倒计时控件
 */
public class CountDomeTime {

    private static Animation animation;

    public static CountDomeTime createTime() {
        if (animation == null) {
            animation = AnimationUtils.loadAnimation(Tools.getContext(), R.anim.animation_text);
        }
        return new CountDomeTime();
    }

    @SuppressLint("SetTextI18n")
    public CountDownTimer countDomeTimer(final TextView view1,long millisInFuture, long countDownInterval) {
        return new CountDownTimer(millisInFuture, countDownInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
                view1.setEnabled(false);
                view1.setTextColor(Tools.getColor(R.color.color_2F55E1));
                long l = millisUntilFinished / 1000;
                view1.setText("剩余" + ((l < 10) ? "0" + l : String.valueOf(l)) + "秒");
            }

            @Override
            public void onFinish() {
                view1.setEnabled(true);
                view1.setText("获取验证码");
            }
        };
    }
}
