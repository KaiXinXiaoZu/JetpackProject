package com.yzhg.toollibrary.common;

import android.app.Activity;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/****
 *  Android 随时随地的退出
 * @author yzhg
 */
public class ActivitysManager {

    private static ActivitysManager activitysManager;

    /**
     * 存放Activity的map
     */
    private List<Activity> mActivities = new ArrayList<>();

    /**
     * 将构造方法私有化，所以不能通构造方法来初始化ActivityManager
     */
    private ActivitysManager() {
    }

    /**
     * 采用单例模式初始化ActivityManager，使只初始化一次
     */
    public static ActivitysManager getInstance() {
        if (activitysManager == null) {
            activitysManager = new ActivitysManager();
        }
        return activitysManager;
    }

    /**
     * 获取当前Activity（堆栈中最后一个压入的）
     */
    public Activity currentActivity() {
        return mActivities.get(mActivities.size() - 1);
    }

    /**
     * 添加activity
     */
    public void addActivity(Activity activity) {
        if (!mActivities.contains(activity)) {
            mActivities.add(activity);
        }
    }

    /**
     * 移除activity
     */
    public void removeActivity(Activity activity) {
        if (activity != null) {
            if (mActivities.contains(activity)) {
                mActivities.remove(activity);
            }
            activity.finish();
            activity = null;
        }
    }

    public boolean judgeActivity(Activity activity) {
        return mActivities.contains(activity);
    }

    /**
     * 将activity全部关闭掉
     */
    public void clearAll() {
        for (Activity activity : mActivities) {
            activity.finish();
        }
    }

    /**
     * 关闭掉LoginActivity以外的activity
     */
    public void clearOther() {
        for (Activity activity : mActivities) {
            if ("LoginActivity".equals(activity.getClass().getSimpleName())) {
                continue;
            }
            activity.finish();
        }
    }

    /**
     * 判断某个act 是否存在
     * @param activityName
     * @return
     */
    public boolean judgeHaveActivity(String activityName){

        for (Activity activity : mActivities) {
            if (TextUtils.equals(activity.getClass().getSimpleName(),activityName)) {
               return true;
            }
        }
        return false;
    }
}
