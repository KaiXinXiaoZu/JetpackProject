package com.wsdz.tools.common

import android.util.Log
import com.yzhg.toollibrary.Tools

/**
 * 包名：com.wsdz.tools.common
 * 创建人：yzhg
 * 时间：2021/06/16 18:01
 * 描述：日志工具
 */
object LogTools {


    /**
     * h获取当前类名
     */

    fun getClassName(): String {
        val thisMethodStack = Exception().stackTrace[2]
        var result = thisMethodStack.className
        val lastIndex = result.lastIndexOf(".")
        result = result.substring(lastIndex + 1, result.length)
        return "======日志/类名$result======"
    }


    /**
     * 统一说明:  以下日志打印  都是成对出现的 ，
     * 一个参数的，不需要传入名称，直接获取当前类名
     * 两个参数的需要手动传入名称
     *
     * @param msg
     */
    fun w(msg: String) {
        if (Tools.isDebug) {
            Log.w(getClassName(), msg)
        }
    }

    fun w(name: String, msg: String) {
        if (Tools.isDebug) {
            Log.w(name, msg)
        }
    }

    fun d(message: String) {
        var msg = message
        if (Tools.isDebug) {
            val segmentSize = 3 * 1024
            val length = msg.length.toLong()
            if (length <= segmentSize) { // 长度小于等于限制直接打印
                Log.d(getClassName(), msg)
            } else {
                while (msg.length > segmentSize) { // 循环分段打印日志
                    val logContent = msg.substring(0, segmentSize)
                    msg = msg.replace(logContent, "")
                    Log.d(getClassName(), msg)
                }
                Log.d(getClassName(), msg)
            }
        }
    }

    fun d(name: String, message: String) {
        if (Tools.isDebug) {
            Log.d(name, message)
        }
    }

    fun e(message: String) {
        if (Tools.isDebug) {
            Log.e(getClassName(), message)
        }
    }

    fun e(name: String, message: String) {
        if (Tools.isDebug) {
            Log.e(name, message)
        }
    }


    fun i(message: String) {
        if (Tools.isDebug) {
            Log.i(getClassName(), message)
        }
    }

    fun i(name: String, message: String) {
        var msg = message
        if (Tools.isDebug) {
            if (msg.length <= 1000) {
                Log.i(name, msg)
            } else {
                while (msg.length > 1000) {
                    Log.i(name, msg.substring(0, 1000))
                    msg = msg.substring(1000, msg.length)
                }
                Log.i(name, msg)
            }
        }
    }


    fun v(message: String) {
        if (Tools.isDebug) {
            Log.v(getClassName(), message)
        }
    }

    fun v(name: String, message: String) {
        if (Tools.isDebug) {
            Log.v(name, message)
        }
    }

    ///////////////打印重大BUG//////////////////

    fun wtf(message: String) {
        if (Tools.isDebug) {
            Log.wtf(getClassName(), message)
        }
    }

    fun wtf(name: String, message: String) {
        if (Tools.isDebug) {
            Log.wtf(name, message)
        }
    }


}