package com.yzhg.toollibrary.common

import androidx.annotation.StringRes
import com.hjq.toast.ToastUtils
import com.yzhg.toollibrary.Tools

/**
 * 包名：com.wsdz.tools.common
 * 创建人：yzhg
 * 时间：2021/06/16 17:04
 * 描述：Toast工具类
 */
object ToastTools {

    /**
     * 显示Toast
     * @param resourcesMsg 资源字符串
     *
     */
    fun showToast(@StringRes resourcesMsg: Int?) {
        resourcesMsg?.let {
            ToastUtils.show(Tools.getStirng(it))
        }
    }


    /**
     * @param message 要显示的字符串
     */
    fun showToast(message: String?) {
        if (!message.isNullOrEmpty()) {
            ToastUtils.show(message)
        }
    }
}