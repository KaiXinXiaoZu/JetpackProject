package com.wsdz.tools.common

import android.content.Context
import android.content.SharedPreferences
import android.util.Base64
import java.io.*

/**
 * 包名：com.wsdz.tools.common
 * 创建人：yzhg
 * 时间：2021/06/16 17:09
 * 描述：SP储存工具类
 *
 */
object SpTools {


    private var sharedPreferences: SharedPreferences? = null

    /**
     * SP储存名称
     */
    private val SP_TABLE_NAME: String = "SP_CONFIG"

    /**
     * 获取SP实例
     */
    fun getSharedPreferences(context: Context,tableName: String): SharedPreferences? {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(tableName, Context.MODE_PRIVATE)
        }
        return sharedPreferences
    }


    ///////////////////////////////////字符串类型///////////////////////////////////////////////////

    /**
     * 储存字符串 键值对形式储存
     * @param key
     * @param value
     */
    fun putString(context: Context,key: String, value: String) {
        getSharedPreferences(context,SP_TABLE_NAME)?.edit()?.putString(key, value)?.apply()
    }

    /**
     * 获取储存的字符串 默认字符串为空字符串
     * @param key 存入的键
     */
    fun getString(context: Context,key: String): String {
        return getSharedPreferences(context,SP_TABLE_NAME)?.getString(key, "") ?: ""
    }


    /**
     * 获取储存的字符串
     * @param key 存入的键
     * @param defaultValue 默认值
     */
    fun getString(context: Context,key: String, defaultValue: String?): String? {
        return getSharedPreferences(context,SP_TABLE_NAME)?.getString(key, defaultValue) ?: defaultValue
    }


    ///////////////////////////////////Int类型///////////////////////////////////////////////////

    /**
     * 储存字符串 键值对形式储存
     * @param key
     * @param value
     */
    fun putInt(context: Context,key: String, value: Int) {
        getSharedPreferences(context,SP_TABLE_NAME)?.edit()?.putInt(key, value)?.apply()
    }

    /**
     * 获取储存的字符串
     * @param key 存入的键
     * @param defaultValue 默认值  为null  返回0
     */
    fun getInt(context: Context,key: String, defaultValue: Int?): Int {
        return getSharedPreferences(context,SP_TABLE_NAME)?.getInt(key, defaultValue ?: 0) ?: defaultValue ?: 0
    }

    ///////////////////////////////////Boolean类型///////////////////////////////////////////////////

    fun putBoolean(context: Context,key: String, value: Boolean) {
        getSharedPreferences(context,SP_TABLE_NAME)?.edit()?.putBoolean(key, value)?.apply()
    }

    /**
     * 获取储存的字符串
     * @param key 存入的键
     * @param defaultValue 默认值  为null  返回false
     */
    fun getBoolean(context: Context,key: String, defaultValue: Boolean?): Boolean {
        return getSharedPreferences(context,SP_TABLE_NAME)?.getBoolean(key, defaultValue ?: false) ?: defaultValue ?: false
    }


    ///////////////////////////////////Long类型///////////////////////////////////////////////////

    fun putLong(context: Context,key: String, value: Long) {
        getSharedPreferences(context,SP_TABLE_NAME)?.edit()?.putLong(key, value)?.apply()
    }

    /**
     * 获取储存的字符串
     * @param key 存入的键
     * @param defaultValue 默认值  为null  返回 0L
     */
    fun getLong(context: Context,key: String, defaultValue: Long?): Long {
        return getSharedPreferences(context,SP_TABLE_NAME)?.getLong(key, defaultValue ?: 0L) ?: defaultValue ?: 0L
    }


    /**
     * 将对象保存在SP中
     */
    fun <T : Serializable?> putObject(context: Context,key: String, obj: T) {
        putSerializable(context,key, obj)
    }

    /**
     * 获取序列化对象
     */
    fun <T : Serializable?> getObject(context: Context,key: String): T? {
        return getSerializable(context,key) as T
    }

    /**
     * 储存List集合
     */
    fun putList(context: Context,key: String, list: List<Serializable>) {
        putSerializable(context,key, list)
    }

    /**
     * 获取List集合
     */
    fun <T : List<Serializable>> getList(context: Context,key: String): T? {
        return getSerializable(context,key) as T?
    }


    /**
     * 储存MAP集合
     */
    fun <K : Serializable, V : Serializable> putMap(context: Context,key: String, map: Map<K, V>) {
        putSerializable(context,key, map)
    }

    /**
     * 获取MAP集合
     */
    fun <K : Serializable, V : Serializable> getMap(context: Context,key: String): Map<K, V> {
        return getSerializable(context,key) as Map<K, V>
    }

    /**
     * 储存序列对象
     */
    fun putSerializable(context: Context,key: String, any: Any?) {
        if (any == null) {
            //对象为空
            putString(context,key, "")
        } else {
            val baos: ByteArrayOutputStream = ByteArrayOutputStream()
            val oos = ObjectOutputStream(baos)
            oos.writeObject(any)
            // 将对象放到OutputStream中
            // 将对象转换成byte数组，并将其进行base64编码
            val objectStr = String(Base64.encode(baos.toByteArray(), Base64.DEFAULT))
            baos.close()
            oos.close()
            putString(context,key, objectStr)
        }
    }

    /**
     * 获取序列化对象
     */
    fun getSerializable(context: Context,key: String): Any? {
        val wordBase64: String? = getString(context,key, "")
        if (wordBase64.isNullOrEmpty()) {
            return null
        }
        val objBytes = Base64.decode(wordBase64.toByteArray(), Base64.DEFAULT)
        val bais = ByteArrayInputStream(objBytes)
        val ois = ObjectInputStream(bais)
        // 将byte数组转换成product对象
        val obj = ois.readObject()
        bais.close()
        ois.close()
        return obj
    }
}