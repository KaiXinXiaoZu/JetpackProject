package com.yzhg.toollibrary.event

import com.jeremyliao.liveeventbus.LiveEventBus
import com.jeremyliao.liveeventbus.core.Observable
import com.yzhg.toollibrary.bean.EventBean

/**
 * 作者：yzhg - 20123
 * 时间：2022-04-22 17:24
 * 报名：com.yzhg.toollibrary.event
 * 备注：
 */
object EventBusTools {

    fun getEventBus(): Observable<EventBean<*>> {
        return LiveEventBus.get(EventBean::class.java)
    }

    /**
     * 发送自定义消息
     */
    fun postEventBus(event: EventBean<*>) {
        LiveEventBus.get(EventBean::class.java).post(event)
    }

    /**
     * 发送一个有标识的消息
     */
    fun postEventBus(eventTag: Int) {
        LiveEventBus.get(EventBean::class.java).post(EventBean<Any>(eventTag))
    }

    /**
     * 发送一个有标识和Str内容的消息
     */
    fun postEventBus(eventTag: Int, eventStr: String) {
        LiveEventBus.get(EventBean::class.java).post(EventBean<Any>(eventTag, eventStr))
    }


    fun postEventBus(eventTag: Int, eventInt: Int) {
        LiveEventBus.get(EventBean::class.java).post(EventBean<Int>(eventTag, eventInt))
    }


}