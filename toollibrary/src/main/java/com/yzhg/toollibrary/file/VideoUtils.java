package com.yzhg.toollibrary.file;

import android.media.MediaMetadataRetriever;

/*
 * 作者：yzhg
 * 时间：2021-12-24 18:16
 * 包名：com.yzhg.toollibrary.file
 * 描述：
 */
public class VideoUtils {

    /**
     * 获取视频比特率
     *
     * @param videoPath 文件地址
     * @return 返回比特率
     */
    public static String getVideoPlay(String videoPath) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(videoPath);
        return mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE);
    }

    /**
     * 获取视频的宽高
     *
     * @param videoPath 视频地址
     * @return 返回数组
     */
    public static String[] getVideoResolution(String videoPath) {
        MediaMetadataRetriever retr = new MediaMetadataRetriever();
        retr.setDataSource(videoPath);

        String height = retr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT); // 视频高度
        String width = retr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH); // 视频宽度
        return new String[]{width, height};
    }

    /**
     * 获取视频的时长
     *
     * @param videoPath 视频地址
     * @return 返回数组
     */
    public static Long getVideoDuration(String videoPath) {
        MediaMetadataRetriever retr = new MediaMetadataRetriever();
        retr.setDataSource(videoPath);
        String videoDuration = retr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION); // 视频时长
        return Long.parseLong(videoDuration);
    }
}
