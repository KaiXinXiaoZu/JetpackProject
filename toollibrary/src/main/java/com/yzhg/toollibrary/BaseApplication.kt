package com.yzhg.toollibrary

import android.R
import androidx.multidex.MultiDexApplication
import com.hjq.toast.ToastUtils
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.scwang.smart.refresh.layout.SmartRefreshLayout


/**
 * 包名：com.yzhg.toollibrary
 * 创建人：yzhg
 * 时间：2021/06/23 10:52
 * 描述：
 */
open class BaseApplication : MultiDexApplication() {


    //static 代码段可以防止内存泄露
    companion object {

        init {
            //设置第三方开源刷新框架的全局的Header构建器
            SmartRefreshLayout.setDefaultRefreshHeaderCreator { context, layout ->
                layout.setPrimaryColorsId(android.R.color.white, R.color.black)//全局设置主题颜色
                ClassicsHeader(context)//指定为经典Header，默认是 贝塞尔雷达Header
            }
            //设置全局的Footer构建器
            SmartRefreshLayout.setDefaultRefreshFooterCreator { context, _ ->
                //指定为经典Footer，默认是 BallPulseFooter
                ClassicsFooter(context).setDrawableSize(20f)
            }
        }
    }


    override fun onCreate() {
        super.onCreate()
        //初始化工具类
        Tools.init(this, true)
        ToastUtils.init(this)

    }
}