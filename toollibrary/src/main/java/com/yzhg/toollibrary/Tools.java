package com.yzhg.toollibrary;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.yzhg.toollibrary.common.ToastTools;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 包名：com.wsdz.tools
 * 创建人：yzhg
 * 时间：2021/06/16 15:24
 * 描述：
 */
public class Tools {


    /**
     * 上下文，需要使用全局上下文的时候可以直接使用这个
     */
    @SuppressLint("StaticFieldLeak")
    private static Context context;

    /**
     * 是否开启DeBug
     */
    public static Boolean isDebug = false;


    /**
     * 私有化示例对象  不允许未初始化时使用此对象
     */
    private Tools() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }


    /**
     * 初始化对象 Application
     */
    public static void init(Context context, Boolean isDebug) {
        Tools.context = context.getApplicationContext();
        Tools.isDebug = isDebug;
    }

    /**
     * 获取上下文
     */
    public static Context getContext() {
        if (context != null) {
            return context;
        }
        throw new NullPointerException("请在Application中初始化操作");
    }

    /**
     * 时间戳转时间
     */
    public static String getDateToString(long milSecond) {
        if (milSecond == 0) {
            return "";
        }
        Date date = new Date(milSecond);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return format.format(date);
    }

    public static String getDateToString(long milSecond, String pattern) {
        if (milSecond == 0) {
            return "";
        }
        Date date = new Date(milSecond);
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        format.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return format.format(date);
    }

    /**
     * 转时间戳
     */
    public static long dateToStamp(String time) {
        try {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date = null;
            date = simpleDateFormat.parse(time);
            return date.getTime();
        } catch (ParseException e) {
            return 0;
        }
    }

    public static long dateToStamp2(String time) {
        if (time.isEmpty()) return 0L;
        try {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            date = simpleDateFormat.parse(time);
            return date.getTime();
        } catch (ParseException e) {
            return 0;
        }
    }

    /**
     * 获取字符串
     */
    public static String getStirng(int id) {
        return getContext().getResources().getString(id);
    }


    /**
     * 获取图片资源
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    public static Drawable getDrawable(int id) {
        return getContext().getResources().getDrawable(id);
    }

    /**
     * 获取颜色
     */
    public static int getColor(int id) {
        return ContextCompat.getColor(context, id);
    }

    /**
     * 获取屏幕尺寸
     */
    public static int getScreenWidth(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        return dm.widthPixels;
    }

    /**
     * 获取屏幕的宽度px
     *
     * @param context 上下文
     * @return 屏幕宽px
     */
    public static int getScreenWidth2(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();// 创建了一张白纸
        windowManager.getDefaultDisplay().getMetrics(outMetrics);// 给白纸设置宽高
        return outMetrics.widthPixels;
    }

    /**
     * 获取屏幕的高度px
     *
     * @param context 上下文
     * @return 屏幕高px
     */
    public static int getScreenHeight(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();// 创建了一张白纸
        windowManager.getDefaultDisplay().getMetrics(outMetrics);// 给白纸设置宽高
        return outMetrics.heightPixels;
    }

    /**
     * dp 与 px之间的转换
     */
    public static int dip2px(float dip) {
        //获取屏幕的密度
        float density = getContext().getResources().getDisplayMetrics().density;
        return (int) (dip * density + 0.5f);
    }

    /**
     * px转DP
     *
     * @param px
     * @return
     */
    public static float px2dip(float px) {
        float density = getContext().getResources().getDisplayMetrics().density;
        return px / density;
    }

    /**
     * SP转PX
     *
     * @param sp
     * @return
     */
    public static float sp2px(float sp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, Resources.getSystem().getDisplayMetrics());
    }


    /**
     * 比较SDK
     */
    public static boolean compareSDK(int SDKVersion) {
        return Build.VERSION.SDK_INT >= SDKVersion;
    }

    /**
     * 获取当前版本名称
     *
     * @return 返回当前版本号
     */
    public static String getPackageVersionName() {
        try {
            PackageInfo packageInfo = getContext().getPackageManager().getPackageInfo(context
                    .getPackageName(), 0);
            return packageInfo.versionName;
        } catch (Exception e) {
            // e.printStackTrace();
            return "";
        }
    }

    /**
     * 获取当前版本号
     *
     * @return 当前版本号
     */
    public static int getPackageVersionCode() {
        try {
            PackageInfo packageInfo = getContext().getPackageManager().getPackageInfo(context
                    .getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (Exception e) {
            // e.printStackTrace();
            return -1;
        }
    }


    /**
     * 校验手机号
     */
    public static boolean isPhoneLegal(String str) {
        return '1' == (str.charAt(0)) && str.length() == 11;
    }

    /**
     * 验证输入的身份证号是否合法
     */
    public static boolean isLegalId(String id) {
        if (id.toUpperCase().matches("(^\\d{15}$)|(^\\d{17}([0-9]|X)$)")) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 加载View
     */
    public static View getLayoutID(int layoutId) {
        return LayoutInflater.from(context).inflate(layoutId, null);
    }

    /**
     * 获取手机型号
     */
    public static String getSystemModel() {
        return Build.MODEL;
    }

    /**
     * 获取当前时间戳
     */
    public static long getCurTimeLong() {
        return System.currentTimeMillis();
    }

    /**
     * 获取文件目录
     */
    public static String getSDPath(String type) {
        return Tools.getContext().getExternalFilesDir(type).getAbsolutePath() + "/";
    }

    /**
     * 调用拨号界面
     *
     * @param phone 电话号码
     */
    public static void callPhone(Context context, String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 从字符串中查找数字字符串
     */
    public static List<String> getNumbers(String content) {
        if (content.isEmpty()) {
            return null;
        }
        List<String> digitList = new ArrayList<>();
        Pattern p = Pattern.compile("(\\d+)");
        Matcher m = p.matcher(content);
        while (m.find()) {
            String find = m.group(1).toString();
            int i = content.indexOf(find);
            int lastIndex = content.indexOf(i + find.length());
            if (i > 0 && lastIndex < content.length()) {
                if (content.charAt(i - 1) == '【' && content.charAt(i + find.length()) == '】') {
                    if (find.length() >= 7) {
                        digitList.add(find);
                    }
                }
            }
           /* if (Tools.isPhoneLegal(find)) {
                digitList.add(find);
            }*/
        }
        return digitList;
    }

    /**
     * 设置TextRightDrawable
     */
    public static void setDrawAbleRight(int drawableInt, TextView textView) {
        if (drawableInt == 0) {
            textView.setCompoundDrawables(null, null, null, null);
        } else {
            Drawable drawable = Tools.getDrawable(drawableInt);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            textView.setCompoundDrawables(null, null, drawable, null);
        }
    }

    public static void setDrawAbleLeft(int drawableInt, TextView textView) {
        if (drawableInt == 0) {
            textView.setCompoundDrawables(null, null, null, null);
        } else {
            Drawable drawable = Tools.getDrawable(drawableInt);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            textView.setCompoundDrawables(drawable, null, null, null);
        }
    }

    /**
     * 去除.00
     */
    public static String clearDecimalZero(Double number) {
        if (number == null) {
            return "0";
        }
        String numberStr = String.valueOf(number);
        if (numberStr.contains(".")) {
            return numberStr.substring(0, numberStr.indexOf("."));
        } else {
            return numberStr;
        }
    }


    /**
     * 判断系统是否安装某app
     *
     * @param packageName 包名
     *                    https://blog.csdn.net/u014714188/article/details/113938053
     * @return
     */
    public static boolean appIsInstalled(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        @SuppressLint("QueryPermissionsNeeded")
        List<PackageInfo> packageInfoList = manager.getInstalledPackages(0);
        if (packageInfoList != null) {
            for (int i = 0; i < packageInfoList.size(); i++) {
                String package_name = packageInfoList.get(i).packageName;
                if (package_name.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * 安装apk
     */
    public static void installApkFile(Context context, String apkPath) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        //版本在7.0以上是不能直接通过uri访问的
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            File file = (new File(String.valueOf(apkPath)));
            // 由于没有在Activity环境下启动Activity,设置下面的标签
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //参数1 上下文, 参数2 .FileProvider 和 Manifest配置文件中保持一致   参数3  共享的文件
            Uri apkUri = FileProvider.getUriForFile(context, context.getPackageName() + ".FileProvider", file);
            //添加这一句表示对目标应用临时授权该Uri所代表的文件
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(new File(String.valueOf(apkPath))),
                    "application/vnd.android.package-archive");
        }
        context.startActivity(intent);
        System.exit(0);
    }

    /**
     * 获取进程号对应的进程名
     *
     * @param pid 进程号
     * @return 进程名
     */
    public static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    public static String saveString(String money) {
        if ("".equals(money) || money == null) {
            money = "0";
        }
        DecimalFormat df = new DecimalFormat("###########0.00");
        return df.format(Double.valueOf(money));
    }

    public static String saveString(double money) {
        DecimalFormat df = new DecimalFormat("#########0.00");
        return df.format(money);
    }

    /**
     * 复制
     */
    public static void copyText(Context context, String copyText) {
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(context.CLIPBOARD_SERVICE);
        clipboardManager.setPrimaryClip(ClipData.newPlainText("text", copyText));
        ToastTools.INSTANCE.showToast("复制成功");
    }

    /*操作小眼睛*/
    public static boolean eyeState(EditText editText, ImageButton eyeImage, boolean eye) {
        String userPsw = editText.getText().toString().trim();
        editText.setTransformationMethod(eye ? PasswordTransformationMethod.getInstance() :
                HideReturnsTransformationMethod.getInstance());
        eyeImage.setImageResource(eye ? R.mipmap.icon_eye : R.mipmap.icon_eye_show);
        editText.setSelection(userPsw.length());
        return !eye;
    }


    public static void openSettingPage(Context context) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        context.startActivity(intent);
    }


}
