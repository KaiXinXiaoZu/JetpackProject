package com.yzhg.toollibrary;

import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.style.AbsoluteSizeSpan;

/*
 * 操作人 :  yzhg
 * 时  间 :  2019/9/23 0023
 * 描  述 :
 */
public class ShapeTools {

    /**
     * 产生shape类型的drawable
     * @param solidColor    填充的颜色
     * @param strokeColor   描边的颜色
     * @param strokeWidth   描边的大小
     * @param radius        圆角的度数
     * @return
     */
    public static GradientDrawable getBackgroundDrawable(int solidColor, int strokeColor, int strokeWidth, float radius) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(solidColor);
        drawable.setStroke(strokeWidth, strokeColor);
        drawable.setCornerRadius(radius);
        return drawable;
    }

    /**
     * 产生shape类型的渐变drawable
     * @param solidColor    填充的渐变颜色
     * @param orientation    渐变色方向
     * @param strokeColor   描边的颜色
     * @param strokeWidth   描边的大小
     * @param radius        圆角的度数
     * @return
     */
    public static GradientDrawable getGradientBackgroundDrawable(int[] solidColor,
                                                                 GradientDrawable.Orientation orientation,
                                                                 int gradient,
                                                                 int strokeColor,
                                                                 int strokeWidth,
                                                                 float radius) {
        GradientDrawable drawable = new GradientDrawable(orientation,solidColor);
        drawable.setShape(gradient);
        drawable.setStroke(strokeWidth, strokeColor);
        drawable.setGradientType(GradientDrawable.RECTANGLE);
        drawable.setCornerRadius(radius);
        return drawable;
    }

    /**
     * 1、2两个参数表示左上角，3、4表示右上角，5、6表示右下角，7、8表示左下角
     * @param solidColor
     * @param strokeColor
     * @param strokeWidth
     * @param topLeftRadius
     * @param topRightRadius
     * @param bottomRightRadius
     * @param bottomLeftRadius
     * @return
     */
    public static GradientDrawable getBackgroundDrawable1(int solidColor, int strokeColor, int strokeWidth, float topLeftRadius, float topRightRadius
            , float bottomRightRadius, float bottomLeftRadius) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(solidColor);
        drawable.setStroke(strokeWidth, strokeColor);
        drawable.setCornerRadii(new float[]{
                topLeftRadius,
                topLeftRadius,
                topRightRadius,
                topRightRadius,
                bottomRightRadius,
                bottomRightRadius,
                bottomLeftRadius,
                bottomLeftRadius});
        return drawable;
    }


    /**
     *  颜色点击选择器 selector
     * @param intnormal   默认颜色
     * @param intchecked  点击时的颜色
     * @return
     */
    public static ColorStateList getTextColorSelector(int intnormal, int intchecked){
        int[][] states = new int[2][];
        states[0] =  new int[]{android.R.attr.state_pressed};
        states[1] = new int[]{};
        int[] colors = new int[]{intchecked,intnormal};
        ColorStateList csl = new ColorStateList(states,colors);
        return csl;
    }


    /**
     * 动态改变textTexview Hint显示的大小，但不影响字体大小
     * @param hintMsg
     * @return
     */

    public static CharSequence getTextHint(String hintMsg) {
        SpannableString ss = new SpannableString(hintMsg);//定义hint的值
        AbsoluteSizeSpan ass = new AbsoluteSizeSpan(15, true);//设置字体大小 true表示单位是sp
        ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return new SpannedString(ss);
    }
}
