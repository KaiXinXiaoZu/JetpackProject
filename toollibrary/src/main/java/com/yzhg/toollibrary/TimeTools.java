package com.yzhg.toollibrary;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * 作者：yzhg - 20123
 * 时间：2022-06-07 14:42
 * 报名：com.yzhg.toollibrary
 * 备注：
 */
@SuppressLint("SimpleDateFormat")
public class TimeTools {

    /**
     * 获取年月日
     */
    public static String getYearMonthDay() {
        return getYear() + "-" + getMonth() + "-" + getDay();
    }

    /**
     * 获取本月第一天
     */
    public static String currentMonthFirstDay(String pattern) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date calendarTime = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        format.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return format.format(calendarTime);
    }

    /**
     * 获取上月的第一天
     */
    public static String getLastMonthFirstDay(String pattern) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONDAY, calendar.get(Calendar.MONDAY) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date calendarTime = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        format.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return format.format(calendarTime);
    }

    /**
     * 获取上月的最后一天
     */
    public static String getLastMonthLastDay(String pattern) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONDAY, calendar.get(Calendar.MONDAY) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date calendarTime = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        format.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return format.format(calendarTime);
    }

    /**
     * 获取年
     *
     * @return
     */
    public static int getYear() {
        Calendar cd = Calendar.getInstance();
        return cd.get(Calendar.YEAR);
    }

    /**
     * 获取月
     *
     * @return
     */
    public static int getMonth() {
        Calendar cd = Calendar.getInstance();
        return cd.get(Calendar.MONTH) + 1;
    }

    /**
     * 获取日
     *
     * @return
     */
    public static int getDay() {
        Calendar cd = Calendar.getInstance();
        return cd.get(Calendar.DATE);
    }

    /**
     * 获取时
     *
     * @return
     */
    public static int getHour() {
        Calendar cd = Calendar.getInstance();
        return cd.get(Calendar.HOUR);
    }

    /**
     * 获取分
     *
     * @return
     */
    public static int getMinute() {
        Calendar cd = Calendar.getInstance();
        return cd.get(Calendar.MINUTE);
    }

    /**
     * 获取当前时间的时间戳
     *
     * @return
     */
    public static long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }

    /**
     * 获取当前时间
     */
    public static String getCurrentTime() {
        return getFormatedDateTime(System.currentTimeMillis());
    }

    /**
     * 将long转换为日期（yyyy-MM-dd HH:mm）
     *
     * @param dateTime
     * @return 到分
     */
    @SuppressLint("SimpleDateFormat")
    public static String getFormatedDateTime(long dateTime) {
        String time = "";
        try {
            SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            time = sDateFormat.format(new Date(dateTime + 0));
        } catch (Exception e) {

        }
        return time;
    }

}
