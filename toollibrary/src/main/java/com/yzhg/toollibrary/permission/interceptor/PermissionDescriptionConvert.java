package com.yzhg.toollibrary.permission.interceptor;

import android.content.Context;


import com.yzhg.toollibrary.R;

import java.util.List;

import androidx.annotation.NonNull;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/XXPermissions
 *    time   : 2023/01/02
 *    desc   : 权限描述转换器
 */
public final class PermissionDescriptionConvert {
    /**
     * 将权限名称列表转换成对应权限描述
     */
    @NonNull
    public static String permissionsToDescription(Context context, String permissionName) {
        // 请根据权限名称转换成对应权限说明
        String location1 = context.getString(R.string.common_permission_location);
        String location2 = context.getString(R.string.common_permission_location_background);
        String imageAndVideo = context.getString(R.string.common_permission_image_and_video);
        String camera = context.getString(R.string.common_permission_camera);
        String storage = context.getString(R.string.common_permission_storage);
        if (permissionName.equals(location1) || permissionName.equals(location2)) {
            return "用于 后台定位权限 业务";
        } else if (permissionName.equals(imageAndVideo) || permissionName.equals(storage)) {
            return "用于 图片和视频权限 业务";
        }else if (permissionName.equals(camera)) {
            return "用于 相机权限 业务";
        } else {
            return "用于 其他业务 业务";
        }
    }
    /**
     * 获取权限描述
     */
    public static String getPermissionDescription(Context context, List<String> permissions) {
        StringBuilder stringBuilder = new StringBuilder();
        List<String> permissionNames = PermissionNameConvert.permissionsToNames(context, permissions);
        for (String permissionName : permissionNames) {
            stringBuilder.append(permissionName)
                    .append(context.getString(R.string.common_permission_colon))
                    .append(permissionsToDescription(context, permissionName))
                    .append("\n");
        }
        return stringBuilder.toString().trim();
    }
}