package com.yzhg.toollibrary.permission;

import android.Manifest;
import android.content.Context;

import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.XXPermissions;
import com.yzhg.toollibrary.permission.interceptor.PermissionDescriptionConvert;
import com.yzhg.toollibrary.permission.interceptor.PermissionInterceptor;

import java.util.List;

import androidx.annotation.NonNull;

/**
 * 全局权限配置
 *  框架依赖：api 'com.github.getActivity:XXPermissions:18.63'
 *
 *  配置说明文本：
 *  @see PermissionDescriptionConvert
 *
 *  使用文本参考 在最下面
 */

public class PermissionTool {

    /**
     * 内部类实现单例模式
     * 延迟加载，减少内存开销
     *
     * @author xuzhaohu
     */
    private static class SingletonHolder {
        private static PermissionTool instance = new PermissionTool();
    }


    /**
     * 私有的构造函数
     */
    private PermissionTool() {
    }

    public static PermissionTool getInstance() {
        return SingletonHolder.instance;
    }

    /**
     * 定位权限
     * ACCESS_FINE_LOCATION 精准定位权限
     * ACCESS_COARSE_LOCATION 模糊定位权限
     */
    public static String[] PERMISSION_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    /**
     * CAMERA 相机权限
     */
    public static String[] PERMISSION_CAMERA = {Manifest.permission.CAMERA};

    /**
     * AUDIO 录音权限
     */
    public static String[] PERMISSION_AUDIO = {Manifest.permission.READ_MEDIA_AUDIO};


    /**
     * 同时申请图片视频相机权限
     */
    public static String[] PERMISSION_IMAGE_VIDEO_CAMERA = {Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO, Manifest.permission.CAMERA};

    /**
     * 同时申请图片和视频权限
     * READ_MEDIA_IMAGES 图片权限
     * READ_MEDIA_VIDEO 视频权限
     */
    public static String[] PERMISSION_IMAGE_VIDEO = {Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO};

    /**
     * 同时申请音频和图片权限
     * READ_MEDIA_IMAGES 图片权限
     * READ_MEDIA_AUDIO 音频权限
     */
    public static String[] PERMISSION_IMAGE_VIDEO_AUDIO = {Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO, Manifest.permission.READ_MEDIA_AUDIO};


    ///////////////////////////////////////请求权限方案///////////////////////////////////////////////////////

    /**
     * 请求定位权限
     */
    public void requestLocationPermission(Context context, OnPermissionListener permissionListener) {
        requestPermission(context, permissionListener, PERMISSION_LOCATION);
    }

    /**
     * 请求相机权限
     */
    public void requestCameraPermission(Context context, OnPermissionListener permissionListener) {
        requestPermission(context, permissionListener, PERMISSION_CAMERA);
    }


    /**
     * 请求储存权限 能同时选择相册和视频的时候使用
     */
    public void requestImageAndVideoPermission(Context context, OnPermissionListener permissionListener) {
        requestPermission(context, permissionListener, PERMISSION_IMAGE_VIDEO);
    }



    /**
     * 同时申请相图片、视频、相机权限
     * 不建议使用，选择图片时把拍照功能去掉（除非客户要求）
     */
    public void requestImageVideoCameraPermission(Context context, OnPermissionListener permissionListener) {
        requestPermission(context, permissionListener, PERMISSION_IMAGE_VIDEO_CAMERA);
    }

    /**
     * 请求录音权限
     */
    public void requestAudioPermission(Context context, OnPermissionListener permissionListener) {
        requestPermission(context, permissionListener, PERMISSION_AUDIO);
    }


    /**
     * 同时申请相图片、视频、录音权限
     */
    public void requestImageVideoAudioPermission(Context context, OnPermissionListener permissionListener) {
        requestPermission(context, permissionListener, PERMISSION_IMAGE_VIDEO_AUDIO);
    }


    public void requestPermission(Context context, OnPermissionListener permissionListener, String... permissions) {
        XXPermissions.with(context).permission(permissions).interceptor(new PermissionInterceptor()).request(new OnPermissionCallback() {
            @Override
            public void onGranted(@NonNull List<String> list, boolean b) {
                if (b) {
                    permissionListener.agreePermission();
                } else {
                    permissionListener.refusePermission();
                }
            }

            @Override
            public void onDenied(@NonNull List<String> permissions, boolean doNotAskAgain) {
                permissionListener.refusePermission();
            }
        });
    }

    /**
     * 是否打开权限
     * @param context
     * @return
     */
    public static boolean isGranted(Context context, @NonNull String... permissions) {
        return XXPermissions.isGranted(context, permissions);
    }

    ///////////////////////////////////////回调///////////////////////////////////////////////////////


    public interface OnPermissionListener {

        /**
         * 同意权限时回调
         */

        void agreePermission();

        /**
         * 拒绝权限时回调
         */
        default void refusePermission() {
        }
    }

}


/*

    使用的所有的文本
    <string name="system_download_component_disable">下载服务不可用,请您启用</string>
    <string name="system_download_description">版本更新</string>
    <string name="permission_setting">权限设置</string>
    <string name="common_permission_description_title">权限说明</string>
    <string name="common_permission_granted">授予</string>
    <string name="common_permission_denied">取消</string>
    <string name="common_permission_media_location_hint_fail">获取媒体位置权限失败\n请清除应用数据后重试</string>
    <string name="common_permission_background_location_fail_hint">获取后台定位权限失败，\n请选择%s选项</string>
    <string name="common_permission_background_sensors_fail_hint">获取后台传感器权限失败，\n请选择%s选项</string>
    <string name="common_permission_fail_assign_hint">授权失败，请正确授予%s</string>
    <string name="common_permission_fail_hint">授权失败，请正确授予权限</string>
    <string name="common_permission_unknown">权限</string>
    <string name="common_permission_comma">、</string>
    <string name="common_permission_colon">：</string>
    <string name="common_permission_storage">存储权限</string>
    <string name="common_permission_post_notifications">发送通知权限</string>
    <string name="common_permission_image_and_video">照片和视频权限</string>
    <string name="common_permission_image">视频权限</string>
    <string name="common_permission_video">照片权限</string>
    <string name="common_permission_music_and_audio">音乐和音频权限</string>
    <string name="common_permission_get_installed_apps">读取应用列表权限</string>
    <string name="common_permission_calendar">日历权限</string>
    <string name="common_permission_camera">相机权限</string>
    <string name="common_permission_contacts">通讯录权限</string>
    <string name="common_permission_location">定位权限</string>
    <string name="common_permission_location_background">后台定位权限</string>
    <string name="common_permission_nearby_devices">附近设备权限</string>
    <string name="common_permission_microphone">麦克风权限</string>
    <string name="common_permission_phone">电话权限</string>
    <string name="common_permission_call_logs">通话记录权限</string>
    <string name="common_permission_body_sensors">身体传感器权限</string>
    <string name="common_permission_body_sensors_background">后台身体传感器权限</string>
    <string name="common_permission_activity_recognition_api29">健身运动权限</string>
    <string name="common_permission_activity_recognition_api30">身体活动权限</string>
    <string name="common_permission_access_media_location">读取媒体文件位置权限</string>
    <string name="common_permission_sms">短信权限</string>
    <string name="common_permission_all_file_access">所有文件访问权限</string>
    <string name="common_permission_install_unknown_apps">安装应用权限</string>
    <string name="common_permission_display_over_other_apps">悬浮窗权限</string>
    <string name="common_permission_modify_system_settings">修改系统设置权限</string>
    <string name="common_permission_allow_notifications">通知权限</string>
    <string name="common_permission_allow_notifications_access">通知栏监听权限</string>
    <string name="common_permission_apps_with_usage_access">查看使用情况权限</string>
    <string name="common_permission_alarms_reminders">查看闹钟提醒权限</string>
    <string name="common_permission_do_not_disturb_access">勿扰权限</string>
    <string name="common_permission_ignore_battery_optimize">忽略电池优化权限</string>
    <string name="common_permission_picture_in_picture">画中画权限</string>
    <string name="common_permission_vpn">\tVPN\t权限</string>
    <string name="common_permission_manual_assign_fail_background_location_hint">获取后台定位权限失败，请在定位权限中选择%s</string>
    <string name="common_permission_manual_assign_fail_background_sensors_hint">获取后台传感器权限失败，请在传感器权限中选择%s</string>
    <string name="common_permission_manual_assign_fail_hint">获取权限失败，请手动授予%s</string>
    <string name="common_permission_manual_fail_hint">获取权限失败，请手动授予权限</string>
    <string name="common_permission_alert">授权提醒</string>
    <string name="common_permission_goto_setting_page">前往授权</string>
    <string name="common_permission_background_default_option_label">始终允许</string>
 */