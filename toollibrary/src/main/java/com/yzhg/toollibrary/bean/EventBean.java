package com.yzhg.toollibrary.bean;

import com.jeremyliao.liveeventbus.core.LiveEvent;

/**
 * 包名：com.yzhg.examples.event
 * 创建人：yzhg
 * 时间：2021/06/24 11:21
 * 描述：
 */
public class EventBean<T> implements LiveEvent {

    private int eventTag;

    private T data;

    private int eventInt;

    private String eventStr;

    public EventBean(int eventTag) {
        this.eventTag = eventTag;
    }

    public EventBean(int eventTag, T data) {
        this.eventTag = eventTag;
        this.data = data;
    }

    public EventBean(int eventTag, int eventInt) {
        this.eventTag = eventTag;
        this.eventInt = eventInt;
    }

    public EventBean(int eventTag, String eventStr) {
        this.eventTag = eventTag;
        this.eventStr = eventStr;
    }

    public EventBean(int eventTag, T data, String eventStr) {
        this.eventTag = eventTag;
        this.data = data;
        this.eventStr = eventStr;
    }


    public EventBean(int eventTag, int eventInt, String eventStr) {
        this.eventTag = eventTag;
        this.eventInt = eventInt;
        this.eventStr = eventStr;
    }

    public EventBean(int eventTag, T data, int eventInt) {
        this.eventTag = eventTag;
        this.data = data;
        this.eventInt = eventInt;
    }

    public EventBean(int eventTag, T data, int eventInt, String eventStr) {
        this.eventTag = eventTag;
        this.data = data;
        this.eventInt = eventInt;
        this.eventStr = eventStr;
    }

    public int getEventTag() {
        return eventTag;
    }

    public void setEventTag(int eventTag) {
        this.eventTag = eventTag;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getEventInt() {
        return eventInt;
    }

    public void setEventInt(int eventInt) {
        this.eventInt = eventInt;
    }

    public String getEventStr() {
        return eventStr;
    }

    public void setEventStr(String eventStr) {
        this.eventStr = eventStr;
    }
}
