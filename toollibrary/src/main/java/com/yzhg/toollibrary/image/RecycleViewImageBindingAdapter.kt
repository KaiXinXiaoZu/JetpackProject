package com.yzhg.toollibrary.image

import android.graphics.Color
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.yzhg.toollibrary.R
import com.yzhg.toollibrary.Tools
import com.yzhg.toollibrary.image.GlideTools

/**
 * 包名：com.yzhg.examples.ui.fragment.mine.demo
 * 创建人：yzhg
 * 时间：2021/06/23 11:30
 * 描述：
 */
class RecycleViewImageBindingAdapter {

    companion object {

        @BindingAdapter("itemImage")
        @JvmStatic
        fun setImage(imageView: ImageView, imageUrl: String) {
            if (imageUrl.isNotEmpty()) {
                GlideTools.Builder(Tools.getContext())
                    .setImageView(imageView)
                    .setErrorImage(R.mipmap.default_image)
                    .setImagePath(imageUrl)
                    .onCreate()
            } else {
                GlideTools.Builder(Tools.getContext())
                    .setImageView(imageView)
                    .setImagePath(R.mipmap.default_image)
                    .isCircleCrop(true)
                    .onCreate()
            }
        }

        @BindingAdapter("itemImageR")
        @JvmStatic
        fun setImageRadiu(imageView: ImageView, imageUrl: String) {
            if (imageUrl.isNotEmpty()) {
                GlideTools.Builder(Tools.getContext())
                    .setImageView(imageView)
                    .setImagePath(imageUrl)
                    .setErrorImage(R.mipmap.default_image)
                    .setRadius(Tools.dip2px(10f))
                    .onCreate()
            } else {
                GlideTools.Builder(Tools.getContext())
                    .setImageView(imageView)
                    .setImagePath(R.mipmap.default_image)
                    .isCircleCrop(true)
                    .onCreate()
            }
        }

        @BindingAdapter("itemImageR5")
        @JvmStatic
        fun setImageRadiu5r(imageView: ImageView, imageUrl: String) {
            if (imageUrl.isNotEmpty()) {
                GlideTools.Builder(Tools.getContext())
                    .setImageView(imageView)
                    .setImagePath(imageUrl)
                    .setErrorImage(R.mipmap.default_image)
                    .setRadius(Tools.dip2px(5f))
                    .onCreate()
            } else {
                GlideTools.Builder(Tools.getContext())
                    .setImageView(imageView)
                    .setImagePath(R.mipmap.default_image)
                    .isCircleCrop(true)
                    .onCreate()
            }
        }

        @BindingAdapter("itemImageRound")
        @JvmStatic
        fun setImageRound(imageView: ImageView, imageUrl: String) {
            if (imageUrl.isNotEmpty()) {
                GlideTools.Builder(Tools.getContext())
                    .setImageView(imageView)
                    .setErrorImage(R.mipmap.default_avatar_t)
                    .setImagePath(imageUrl)
                    .isCircleCrop(true)
                    .onCreate()
            } else {
                GlideTools.Builder(Tools.getContext())
                    .setImageView(imageView)
                    .setImagePath(R.mipmap.default_avatar_t)
                    .isCircleCrop(true)
                    .onCreate()
                // imageView.setBackgroundResource(R.mipmap.default_avatar_t)
            }
        }

    }
}