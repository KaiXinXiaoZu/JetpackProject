package com.yzhg.toollibrary.image;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

/**
 * 包名：com.wsdz.tools.image
 * 创建人：yzhg
 * 时间：2021/06/17 10:19
 * 描述：建造者模式构建Glide
 */
public class GlideTools {


    @SuppressLint("CheckResult")
    public GlideTools(Builder builder) {
        //判断是否有上下文
        if (builder.context == null) {
            throw new RuntimeException("未设置上下文");
        }
        if (builder.imageView == null) {
            throw new RuntimeException("未设置要展示的控件");
        }


        RequestOptions options = new RequestOptions();
        //设置加载中的图片
        if (builder.placeholderImage != 0) {
            options.placeholder(builder.placeholderImage);
        }
        //设置加载失败图片
        if (builder.errorImage != 0) {
            options.placeholder(builder.errorImage);
        }

        //设置图片宽度

        if (builder.width != -1 && builder.height != -1) {
            options.override(builder.width, builder.height);
        }

        //是否缓存
        options.skipMemoryCache(builder.isCache);

        //是否是圆形图片
        if (builder.isCircleCrop) {
            options.apply(RequestOptions.bitmapTransform(new CircleCrop()));
        }

        //是否设置圆角
        if (builder.isCircleCrop && builder.radius != -1) {
            throw new RuntimeException("不能为圆形图片设置圆角");
        }

        //设置圆角
        if (builder.radius != -1) {
            options.transform(new MultiTransformation<>(new CenterCrop(), new RoundedCorners(builder.radius)));
        }

        //设置缓存策略
        if (builder.strategy != null) {
            options.diskCacheStrategy(builder.strategy);
        }

        Glide.with(builder.context)
                .load(builder.path)
                .apply(options)
                .into(builder.imageView);
    }


    public  static final class  Builder {

        /**
         * 上下文
         */
        private Context context;

        /**
         * 地址
         */
        private Object path;

        /**
         * 加载中的图片
         */
        private int placeholderImage = 0;

        /**
         * 加载失败的图片
         */
        private int errorImage = 0;

        /**
         * 图片宽度
         */
        private int width = -1;

        /**
         * 图片高度
         */
        private int height = -1;

        /**
         * 是否跳过缓存  true  缓存  false 不缓存
         */
        private boolean isCache = true;

        /**
         * 是否是圆形头像  默认  false
         */
        private boolean isCircleCrop = false;

        /**
         * 设置圆角  圆形头像和圆角不能同时设置
         */
        private int radius = -1;

        /**
         * 设置imageView
         */
        private ImageView imageView;

        /**
         * 设置缓存策略
         */
        private DiskCacheStrategy strategy;


        public Builder(Context context) {
            this.context = context;
        }

        /**
         * 设置地址
         */
        public Builder setImagePath(String path) {
            this.path = path;
            return this;
        }

        public Builder setImagePath(@DrawableRes int drawable){
            this.path = drawable;
            return this;
        }

        /**
         * 设置控件
         */
        public Builder setImageView(ImageView imageView) {
            this.imageView = imageView;
            return this;
        }

        /**
         * 设置加载中的图片
         */
        public Builder setPlaceholderImage(int placeholderImage) {
            this.placeholderImage = placeholderImage;
            return this;
        }

        /**
         * 设置加载错误的图片
         */
        public Builder setErrorImage(int errorImage) {
            this.errorImage = errorImage;
            return this;
        }

        /**
         * 设置图片宽度
         */
        public Builder setImageWidth(int width) {
            this.width = width;
            return this;
        }

        /**
         * 设置图片高度
         */
        public Builder setImageHeight(int height) {
            this.height = height;
            return this;
        }

        /**
         * 是否跳过缓存  true  跳过缓存
         */
        public Builder isCache(boolean isCache) {
            this.isCache = !isCache;
            return this;
        }

        /**
         * 是否是默认头像
         */
        public Builder isCircleCrop(boolean isCircleCrop) {
            this.isCircleCrop = isCircleCrop;
            return this;
        }

        /**
         * 设置图片圆角
         */
        public Builder setRadius(int radius) {
            this.radius = radius;
            return this;
        }


        /**
         * 策略解说：
         * <p>
         * all:缓存源资源和转换后的资源
         * <p>
         * none:不作任何磁盘缓存
         * <p>
         * source:缓存源资源
         * <p>
         * result：缓存转换后的资源
         */
        public Builder setDiskCacheStrategy(DiskCacheStrategy strategy) {
            this.strategy = strategy;
            return this;
        }


        public GlideTools onCreate() {
            return new GlideTools(this);
        }

    }

}
