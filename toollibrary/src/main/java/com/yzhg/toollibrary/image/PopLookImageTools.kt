package com.yzhg.toollibrary.image

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import com.lxj.xpopup.XPopup
import com.lxj.xpopup.interfaces.OnSrcViewUpdateListener
import com.lxj.xpopup.interfaces.XPopupImageLoader
import com.lxj.xpopup.util.SmartGlideImageLoader
import com.yzhg.toollibrary.R
import java.io.File

/**
 * 包名：uni.UNIEA80E46.ui.pop
 * 创建人：yzhg
 * 时间：2021/04/27 11:24
 * 描述：
 */
object PopLookImageTools {


    /**
     * 查看多张图片  可传空
     */
    fun lookImage(context: Context, srcImageView: ImageView?, position: Int, selectImage: MutableList<String?>) {
        val lookSelectImage = mutableListOf<String>()
        for (s in selectImage) {
            if (s != null) {
                lookSelectImage.add(s)
            }
        }
        XPopup.Builder(context).asImageViewer(
            srcImageView,
            position,
            lookSelectImage as List<Any>?,
            false,
            true,
            -1,
            -1,
            -1,
            true,
            Color.rgb(32, 36, 46),
            { popupView, position -> },
            SmartGlideImageLoader(R.mipmap.default_image),
            null
        ).show()
    }

    fun lookImageNoNull(context: Context, srcImageView: ImageView, position: Int, selectImage: MutableList<String>) {
        val lookSelectImage = mutableListOf<String>()
        for (s in selectImage) {
            if (s != null) {
                lookSelectImage.add(s)
            }
        }
        XPopup.Builder(context).asImageViewer(
            srcImageView,
            position,
            lookSelectImage as List<Any>?,
            false,
            true,
            -1,
            -1,
            -1,
            true,
            Color.rgb(32, 36, 46),
            { popupView, position -> },
            SmartGlideImageLoader(R.mipmap.default_image),
            null
        ).show()
    }

    fun lookImage(context: Context, srcImageView: ImageView?, selectImage: String?) {
        val lookSelectImage = mutableListOf<String>()
        if (selectImage != null) {
            lookSelectImage.add(selectImage)
        }
        XPopup.Builder(context).asImageViewer(
            srcImageView,
            0,
            lookSelectImage as List<Any>?,
            false,
            true,
            -1,
            -1,
            -1,
            true,
            Color.rgb(32, 36, 46),
            { popupView, position -> },
            SmartGlideImageLoader(R.mipmap.default_image),
            null
        ).show()
    }

    /**
     * 查看多张图片  可传空
     */
    fun lookImage(cosPath: String, context: Context, srcImageView: ImageView?, position: Int, selectImage: MutableList<String?>) {
        val lookSelectImage = mutableListOf<String>()
        for (s in selectImage) {
            if (s != null) {
                lookSelectImage.add(cosPath + s)
            }
        }
        XPopup.Builder(context).asImageViewer(
            srcImageView,
            position,
            lookSelectImage as List<Any>?,
            false,
            true,
            -1,
            -1,
            -1,
            true,
            Color.rgb(32, 36, 46),
            { popupView, position -> },
            SmartGlideImageLoader(R.mipmap.default_image),
            null
        ).show()
    }



}