package com.yzhg.toollibrary.image;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.luck.lib.camerax.SimpleCameraX;
import com.luck.picture.lib.basic.FragmentInjectManager;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.SelectLimitType;
import com.luck.picture.lib.config.SelectMimeType;
import com.luck.picture.lib.config.SelectModeConfig;
import com.luck.picture.lib.config.SelectorConfig;
import com.luck.picture.lib.engine.CompressFileEngine;
import com.luck.picture.lib.engine.CropFileEngine;
import com.luck.picture.lib.engine.UriToFileTransformEngine;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.interfaces.OnBitmapWatermarkEventListener;
import com.luck.picture.lib.interfaces.OnCameraInterceptListener;
import com.luck.picture.lib.interfaces.OnKeyValueResultCallbackListener;
import com.luck.picture.lib.interfaces.OnMediaEditInterceptListener;
import com.luck.picture.lib.interfaces.OnPreviewInterceptListener;
import com.luck.picture.lib.interfaces.OnResultCallbackListener;
import com.luck.picture.lib.interfaces.OnSelectLimitTipsListener;
import com.luck.picture.lib.interfaces.OnVideoThumbnailEventListener;
import com.luck.picture.lib.language.LanguageConfig;
import com.luck.picture.lib.style.PictureSelectorStyle;
import com.luck.picture.lib.style.SelectMainStyle;
import com.luck.picture.lib.style.TitleBarStyle;
import com.luck.picture.lib.utils.DateUtils;
import com.luck.picture.lib.utils.PictureFileUtils;
import com.luck.picture.lib.utils.SandboxTransformUtils;
import com.luck.picture.lib.utils.StyleUtils;
import com.luck.picture.lib.utils.ToastUtils;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropImageEngine;
import com.yzhg.toollibrary.R;
import com.yzhg.toollibrary.Tools;
import com.yzhg.toollibrary.image.picture.ImageUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import top.zibin.luban.Luban;
import top.zibin.luban.OnNewCompressListener;
import top.zibin.luban.OnRenameListener;

import static com.yzhg.toollibrary.image.PictureSelectorTools.goPhotoAlbumFragment;


/**
 * 包名：uni.UNIEA80E46.utils.image
 * 创建人：yzhg
 * 时间：2021/04/12 09:30
 * 描述：
 */

/*
 * getPath(); 指从MediaStore查询返回的路径；SDK_INT >=29 返回content://类型；其他情况返回绝对路径。
 * getRealPath(); 绝对路径；SDK_INT >=29且处于沙盒环境下直接使用会报FileNotFoundException异常；
 * getOriginalPath(); 原图路径；isOriginalImageControl(true);
 *      且勾选了原图选项时返回；但SDK_INT >=29且未实现.setSandboxFileEngine();
 *      直接使用会报FileNotFoundException异常；
 * getCompressPath(); 压缩路径；实现了setCompressEngine();时返回；
 * getCutPath(); 裁剪或编辑路径；实现了setCropEngine();或setEditMediaInterceptListener();时返回；
 * getSandboxPath(); SDK_INT >=29且实现了.setSandboxFileEngine();返回；
 * getVideoThumbnailPath(); 视频缩略图，需要实现setVideoThumbnailListener接口
 * getWatermarkPath(); 水印地址，需要实现setAddBitmapWatermarkListener接口
 * getAvailablePath(); SDK_INT为任意版本都返回直接可用地址(但SDK_INT >29且未开启压缩、裁剪或未实现setSandboxFileEngine，请参考getPath())，但如果你需要具体业务场景下的地址，请参考如上几种路径；
 */
public class PictureSelectorTools {


    /**
     * 在Activity使用 选择头像的时候使用 仅用于拍照
     * 实现自定义相机 -  裁剪 - 压缩 - 功能
     */
    public static void goTakePhotosActivity(Activity context, OnResultCallbackListener<LocalMedia> listener) {
        PictureSelector
                .create(context)
                // 单独拍照，也可录像或也可音频 看你传入的类型是图片or视频
                .openCamera(SelectMimeType.ofImage())
                //拦截相机事件，实现自定义相机
                .setCameraInterceptListener(new MeOnCameraInterceptListener())
                // 是否裁剪
                .setCropEngine(new ImageFileCropEngine())
                //设置压缩引擎
                .setCompressEngine(new ImageFileCompressEngine())
                //设置语音简体中文
                .setLanguage(LanguageConfig.SYSTEM_LANGUAGE)
                //自定义沙盒文件处理
                .setSandboxFileEngine(new MeSandboxFileEngine())
                //是否开启原图功能 -- 这里不开启原图功能
                .isOriginalControl(false)
                //权限说明-这里不处理，权限在外层调用时统一处理
                //.setPermissionDescriptionListener(getPermissionDescriptionListener())
                .forResult(listener);
    }

    /**
     * 在Activity使用 选择头像的时候使用 相册与拍照
     * 实现自定义相机 -  裁剪 - 压缩 - 功能
     */
    public static void goAlbumActivity(Activity context, OnResultCallbackListener<LocalMedia> listener) {
        PictureSelector
                .create(context)
                //设置选择图片类型
                .openGallery(SelectMimeType.ofImage())
                //设置UI效果  使用默认的UI
                // .setSelectorUIStyle(selectorStyle)
                // 外部传入图片加载引擎，必传项
                .setImageEngine(GlideEngine.createGlideEngine())
                //设置裁剪引擎
                .setCropEngine(new ImageFileCropEngine())
                //设置压缩引擎
                .setCompressEngine(new ImageFileCompressEngine())
                //自定义沙盒文件处理
                .setSandboxFileEngine(new MeSandboxFileEngine())
                //拦截相机事件，实现自定义相机
                .setCameraInterceptListener(new MeOnCameraInterceptListener())
                //设置录音回调
                //.setRecordAudioInterceptListener(new MeOnRecordAudioInterceptListener())
                //拦截自定义提示
                .setSelectLimitTipsListener(new MeOnSelectLimitTipsListener())
                //是否开启图片编辑
                .setEditMediaInterceptListener(new MeOnMediaEditInterceptListener(getSandboxPath(), buildOptions()))
                //设置权限说明  --  这里不设置
                // .setPermissionDescriptionListener(getPermissionDescriptionListener())
                //自定义预览  --  这里使用系统自带的
                //.setPreviewInterceptListener(getPreviewInterceptListener())
                //权限拒绝后回调  -- 权限问题  统一在外层处理
                //.setPermissionDeniedListener(getPermissionDeniedListener())
                //给图片添加水印
                //.setAddBitmapWatermarkListener(new MeBitmapWatermarkEventListener())
                //处理视频压缩图
                // .setVideoThumbnailListener(new MeOnVideoThumbnailEventListener(getSandboxPath()))
                //自定义布局
                //.setInjectLayoutResourceListener(getInjectLayoutResource())
                //设置单选还是多选 --  这里设置单选（SelectModeConfig.SINGLE） 多选 （SelectModeConfig.MULTIPLE）
                .setSelectionMode(SelectModeConfig.SINGLE)
                //设置语言
                .setLanguage(LanguageConfig.SYSTEM_LANGUAGE)
                //设置升序还是降序  不用设置  使用默认
                // .setQuerySortOrder(MediaStore.MediaColumns.DATE_MODIFIED + " ASC")
                //设置目录
                .setOutputCameraDir(getSandboxPath())
                .setOutputAudioDir(getSandboxPath())
                .setQuerySandboxDir(getSandboxPath())
                //显示时间资源轴
                .isDisplayTimeAxis(true)
                //查询指定目录  这里不设置
                //.isOnlyObtainSandboxDir(true)
                //设置分页模式
                .isPageStrategy(true)
                //是否开启原图功能
                .isOriginalControl(false)
                //显示or隐藏拍摄
                .isDisplayCamera(true)
                //是否开启点击声音
                .isOpenClickSound(false)
                //跳过裁剪gif
                .setSkipCropMimeType(getNotSupportCrop())
                //滑动选择
                .isFastSlidingSelect(true)
                //视频和图片同时选择
                .isWithSelectVideoImage(false)
                //全屏预览(点击)
                .isPreviewFullScreenMode(true)
                //预览缩放效果
                .isPreviewZoomEffect(true)
                //是否预览图片
                .isPreviewImage(true)
                //是否预览视频
                .isPreviewVideo(true)
                //是否预览音频
                .isPreviewAudio(true)
                //是否显示蒙层(达到最大可选数量)
                .isMaxSelectEnabledMask(true)
                //单选模式直接返回
                .isDirectReturnSingle(false)
                //最大选择数量
                .setMaxSelectNum(1)
                //最小选择数量
                .setMinSelectNum(1)
                //最大选择视频的数量
                //.setMaxVideoSelectNum(1)
                //是否选择gif
                .isGif(false)
                //设置选择的数据
                .setSelectedData(null)
                .forResult(listener);
    }


    public static void goPhotoAlbumActivity(Activity context, int chooseMode, int maxSelectNum, List<LocalMedia> selectionData, OnResultCallbackListener listener) {
        goPhotoAlbumActivity(context, chooseMode, maxSelectNum, 1, selectionData, listener);
    }


    /**
     * 在Activity中使用相册
     *
     * @param context       上下文
     * @param chooseMode    选择的模式
     * @param maxSelectNum  最大选择图片的数量
     * @param selectionData 已经选择的图片列表
     * @param listener      回调监听
     */
    public static void goPhotoAlbumActivity(Activity context, int chooseMode, int maxSelectNum, int videoSelectNum, List<LocalMedia> selectionData, OnResultCallbackListener listener) {
        List<LocalMedia> selectLocalMedia = new ArrayList<>();

        if (selectionData != null) {
            for (LocalMedia selectionDatum : selectionData) {
                if (selectionDatum != null) {
                    selectLocalMedia.add(selectionDatum);
                }
            }
        }
        PictureSelector
                .create(context)
                //设置选择图片类型
                .openGallery(chooseMode)
                //设置UI效果  使用默认的UI
                // .setSelectorUIStyle(selectorStyle)
                // 外部传入图片加载引擎，必传项
                .setImageEngine(GlideEngine.createGlideEngine())
                //设置裁剪引擎
                //  .setCropEngine(new ImageFileCropEngine())
                //设置压缩引擎
                .setCompressEngine(new ImageFileCompressEngine())
                //自定义沙盒文件处理
                .setSandboxFileEngine(new MeSandboxFileEngine())
                //拦截相机事件，实现自定义相机
                .setCameraInterceptListener(new MeOnCameraInterceptListener())
                //设置录音回调
                //.setRecordAudioInterceptListener(new MeOnRecordAudioInterceptListener())
                //拦截自定义提示
                .setSelectLimitTipsListener(new MeOnSelectLimitTipsListener())
                //是否开启图片编辑
                .setEditMediaInterceptListener(new MeOnMediaEditInterceptListener(getSandboxPath(), buildOptions()))
                //设置权限说明  --  这里不设置
                // .setPermissionDescriptionListener(getPermissionDescriptionListener())
                //自定义预览  --  这里使用系统自带的
                //.setPreviewInterceptListener(getPreviewInterceptListener())
                //权限拒绝后回调  -- 权限问题  统一在外层处理
                //.setPermissionDeniedListener(getPermissionDeniedListener())
                //给图片添加水印
                //.setAddBitmapWatermarkListener(new MeBitmapWatermarkEventListener())
                //处理视频压缩图
                .setVideoThumbnailListener(new MeOnVideoThumbnailEventListener(getSandboxPath()))
                //自定义布局
                //.setInjectLayoutResourceListener(getInjectLayoutResource())
                //设置单选还是多选 --  这里设置单选（SelectModeConfig.SINGLE） 多选 （SelectModeConfig.MULTIPLE）
                .setSelectionMode(SelectModeConfig.MULTIPLE)
                //设置语言
                .setLanguage(LanguageConfig.SYSTEM_LANGUAGE)
                //设置升序还是降序  不用设置  使用默认
                // .setQuerySortOrder(MediaStore.MediaColumns.DATE_MODIFIED + " ASC")
                //设置目录
                .setOutputCameraDir(getSandboxPath())
                .setOutputAudioDir(getSandboxPath())
                .setQuerySandboxDir(getSandboxPath())
                //显示时间资源轴
                .isDisplayTimeAxis(true)
                //查询指定目录  这里不设置
                //.isOnlyObtainSandboxDir(true)
                //设置分页模式
                .isPageStrategy(true)
                //是否开启原图功能
                .isOriginalControl(false)
                //显示or隐藏拍摄
                .isDisplayCamera(true)
                //是否开启点击声音
                .isOpenClickSound(false)
                //跳过裁剪gif
                .setSkipCropMimeType(getNotSupportCrop())
                //滑动选择
                .isFastSlidingSelect(true)
                //视频和图片同时选择
                .isWithSelectVideoImage(false)
                //全屏预览(点击)
                .isPreviewFullScreenMode(true)
                //预览缩放效果
                .isPreviewZoomEffect(true)
                //是否预览图片
                .isPreviewImage(true)
                //是否预览视频
                .isPreviewVideo(true)
                //是否预览音频
                .isPreviewAudio(true)
                //是否显示蒙层(达到最大可选数量)
                .isMaxSelectEnabledMask(true)
                //单选模式直接返回
                .isDirectReturnSingle(false)
                //最大选择数量
                .setMaxSelectNum(maxSelectNum)
                //最小选择数量
                .setMinSelectNum(1)
                //最大选择视频的数量
                .setMaxVideoSelectNum(videoSelectNum)
                //最小选择视频的数量
                .setMinVideoSelectNum(1)
                //是否选择gif
                .isGif(false)
                //设置选择的数据
                .setSelectedData(selectLocalMedia)
                .forResult(listener);
    }

    public static void goPhotoAlbumFragment(Fragment context, int chooseMode, int maxSelectNum, List<LocalMedia> selectionData, OnResultCallbackListener listener) {
        goPhotoAlbumFragment(context, chooseMode, maxSelectNum, 1, selectionData, listener);
    }

    public static void goPhotoAlbumFragment(Fragment context, int chooseMode, int maxSelectNum, int videoSelectNum, List<LocalMedia> selectionData, OnResultCallbackListener listener) {
        List<LocalMedia> selectLocalMedia = new ArrayList<>();
        if (selectionData != null && selectionData.size() > 0) {
            for (LocalMedia selectionDatum : selectionData) {
                if (selectionDatum != null) {
                    selectLocalMedia.add(selectionDatum);
                }
            }
        }
        PictureSelector
                .create(context)
                //设置选择图片类型
                .openGallery(chooseMode)
                //设置UI效果  使用默认的UI
                // .setSelectorUIStyle(selectorStyle)
                // 外部传入图片加载引擎，必传项
                .setImageEngine(GlideEngine.createGlideEngine())
                //设置裁剪引擎
                // .setCropEngine(new ImageFileCropEngine())
                //设置压缩引擎
                .setCompressEngine(new ImageFileCompressEngine())
                //自定义沙盒文件处理
                .setSandboxFileEngine(new MeSandboxFileEngine())
                //拦截相机事件，实现自定义相机
                .setCameraInterceptListener(new MeOnCameraInterceptListener())
                //设置录音回调
                //.setRecordAudioInterceptListener(new MeOnRecordAudioInterceptListener())
                //拦截自定义提示
                .setSelectLimitTipsListener(new MeOnSelectLimitTipsListener())
                //是否开启图片编辑
                .setEditMediaInterceptListener(new MeOnMediaEditInterceptListener(getSandboxPath(), buildOptions()))
                //设置权限说明  --  这里不设置
                // .setPermissionDescriptionListener(getPermissionDescriptionListener())
                //自定义预览  --  这里使用系统自带的
                //.setPreviewInterceptListener(getPreviewInterceptListener())
                //权限拒绝后回调  -- 权限问题  统一在外层处理
                //.setPermissionDeniedListener(getPermissionDeniedListener())
                //给图片添加水印
                //.setAddBitmapWatermarkListener(new MeBitmapWatermarkEventListener())
                //处理视频压缩图
                .setVideoThumbnailListener(new MeOnVideoThumbnailEventListener(getSandboxPath()))
                //自定义布局
                //.setInjectLayoutResourceListener(getInjectLayoutResource())
                //设置单选还是多选 --  这里设置单选（SelectModeConfig.SINGLE） 多选 （SelectModeConfig.MULTIPLE）
                .setSelectionMode(SelectModeConfig.MULTIPLE)
                //设置语言
                .setLanguage(LanguageConfig.SYSTEM_LANGUAGE)
                //设置升序还是降序  不用设置  使用默认
                // .setQuerySortOrder(MediaStore.MediaColumns.DATE_MODIFIED + " ASC")
                //设置目录
                .setOutputCameraDir(getSandboxPath())
                .setOutputAudioDir(getSandboxPath())
                .setQuerySandboxDir(getSandboxPath())
                //显示时间资源轴
                .isDisplayTimeAxis(true)
                //查询指定目录  这里不设置
                //.isOnlyObtainSandboxDir(true)
                //设置分页模式
                .isPageStrategy(true)
                //是否开启原图功能
                .isOriginalControl(false)
                //显示or隐藏拍摄
                .isDisplayCamera(true)
                //是否开启点击声音
                .isOpenClickSound(false)
                //跳过裁剪gif
                .setSkipCropMimeType(getNotSupportCrop())
                //滑动选择
                .isFastSlidingSelect(true)
                //视频和图片同时选择
                .isWithSelectVideoImage(false)
                //全屏预览(点击)
                .isPreviewFullScreenMode(true)
                //预览缩放效果
                .isPreviewZoomEffect(true)
                //是否预览图片
                .isPreviewImage(true)
                //是否预览视频
                .isPreviewVideo(true)
                //是否预览音频
                .isPreviewAudio(true)
                //是否显示蒙层(达到最大可选数量)
                .isMaxSelectEnabledMask(true)
                //单选模式直接返回
                .isDirectReturnSingle(false)
                //最大选择数量
                .setMaxSelectNum(maxSelectNum)
                //最小选择数量
                .setMinSelectNum(1)
                //最大选择视频的数量
                .setMaxVideoSelectNum(videoSelectNum)
                //选择图片最小数量
                .setMinVideoSelectNum(1)
                //是否选择gif
                .isGif(false)
                //设置选择的数据
                .setSelectedData(selectLocalMedia)
                .forResult(listener);
    }

    /**
     * 查看图片
     *
     * @param context
     * @param position
     * @param selectionData
     */
    public static void openImageList(Fragment context, int position, ArrayList<LocalMedia> selectionData) {
        PictureSelector.create(context)
                .openPreview()
                .setImageEngine(GlideEngine.createGlideEngine())
                // .setSelectorUIStyle(selectorStyle)
                .setLanguage(LanguageConfig.SYSTEM_LANGUAGE)
                .isPreviewFullScreenMode(true)
                // .setExternalPreviewEventListener(com.luck.pictureselector.MainActivity.MyExternalPreviewEventListener(mAdapter))
                .startActivityPreview(position, false, selectionData);
    }

    public static void openImageList(Activity context, int position, ArrayList<LocalMedia> selectionData) {
        PictureSelector.create(context)
                .openPreview()
                .setImageEngine(GlideEngine.createGlideEngine())
                // .setSelectorUIStyle(selectorStyle)
                .setLanguage(LanguageConfig.SYSTEM_LANGUAGE)
                .isPreviewFullScreenMode(true)
                // .setExternalPreviewEventListener(com.luck.pictureselector.MainActivity.MyExternalPreviewEventListener(mAdapter))
                .startActivityPreview(position, false, selectionData);


    }


    /*************************************************下面是操作*****************************************************************/
    /*************************************************下面是操作*****************************************************************/
    /*************************************************下面是操作*****************************************************************/
    /*************************************************下面是操作*****************************************************************/
    /*************************************************下面是操作*****************************************************************/
    /*************************************************下面是操作*****************************************************************/
    /*************************************************下面是操作*****************************************************************/
    /*************************************************下面是操作*****************************************************************/
    /*************************************************下面是操作*****************************************************************/
    /*************************************************下面是操作*****************************************************************/


    private static String[] getNotSupportCrop() {
        return new String[]{PictureMimeType.ofGIF(), PictureMimeType.ofWEBP()};
    }

    /**
     * 处理视频缩略图
     */
    private static class MeOnVideoThumbnailEventListener implements OnVideoThumbnailEventListener {
        private final String targetPath;

        public MeOnVideoThumbnailEventListener(String targetPath) {
            this.targetPath = targetPath;
        }

        @Override
        public void onVideoThumbnail(Context context, String videoPath, OnKeyValueResultCallbackListener call) {
            Glide.with(context).asBitmap().sizeMultiplier(0.6F).load(videoPath).into(new CustomTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    resource.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                    FileOutputStream fos = null;
                    String result = null;
                    try {
                        File targetFile = new File(targetPath, "thumbnails_" + System.currentTimeMillis() + ".jpg");
                        fos = new FileOutputStream(targetFile);
                        fos.write(stream.toByteArray());
                        fos.flush();
                        result = targetFile.getAbsolutePath();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        PictureFileUtils.close(fos);
                        PictureFileUtils.close(stream);
                    }
                    if (call != null) {
                        call.onCallback(videoPath, result);
                    }
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {
                    if (call != null) {
                        call.onCallback(videoPath, "");
                    }
                }
            });
        }
    }

    /**
     * 给图片添加水印
     */
    private static class MeBitmapWatermarkEventListener implements OnBitmapWatermarkEventListener {
        private final String targetPath;

        public MeBitmapWatermarkEventListener(String targetPath) {
            this.targetPath = targetPath;
        }

        @Override
        public void onAddBitmapWatermark(Context context, String srcPath, String mimeType, OnKeyValueResultCallbackListener call) {
            if (PictureMimeType.isHasHttp(srcPath) || PictureMimeType.isHasVideo(mimeType)) {
                // 网络图片和视频忽略，有需求的可自行扩展
                call.onCallback(srcPath, "");
            } else {
                // 暂时只以图片为例
                Glide.with(context).asBitmap().sizeMultiplier(0.6F).load(srcPath).into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        Bitmap watermark = BitmapFactory.decodeResource(context.getResources(), R.mipmap.default_image);
                        Bitmap watermarkBitmap = ImageUtil.createWaterMaskRightTop(context, resource, watermark, 15, 15);
                        watermarkBitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                        watermarkBitmap.recycle();
                        FileOutputStream fos = null;
                        String result = null;
                        try {
                            File targetFile = new File(targetPath, DateUtils.getCreateFileName("Mark_") + ".jpg");
                            fos = new FileOutputStream(targetFile);
                            fos.write(stream.toByteArray());
                            fos.flush();
                            result = targetFile.getAbsolutePath();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            PictureFileUtils.close(fos);
                            PictureFileUtils.close(stream);
                        }
                        if (call != null) {
                            call.onCallback(srcPath, result);
                        }
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                        if (call != null) {
                            call.onCallback(srcPath, "");
                        }
                    }
                });
            }
        }
    }

    /**
     * 图片编辑
     */
    private static class MeOnMediaEditInterceptListener implements OnMediaEditInterceptListener {
        private final String outputCropPath;
        private final UCrop.Options options;

        public MeOnMediaEditInterceptListener(String outputCropPath, UCrop.Options options) {
            this.outputCropPath = outputCropPath;
            this.options = options;
        }

        @Override
        public void onStartMediaEdit(Fragment fragment, LocalMedia currentLocalMedia, int requestCode) {
            String currentEditPath = currentLocalMedia.getAvailablePath();
            Uri inputUri = PictureMimeType.isContent(currentEditPath)
                    ? Uri.parse(currentEditPath) : Uri.fromFile(new File(currentEditPath));
            Uri destinationUri = Uri.fromFile(
                    new File(outputCropPath, DateUtils.getCreateFileName("CROP_") + ".jpeg"));
            UCrop uCrop = UCrop.of(inputUri, destinationUri);
            options.setHideBottomControls(false);
            uCrop.withOptions(options);
            uCrop.startEdit(fragment.getActivity(), fragment, requestCode);
        }
    }

    /**
     * 压缩图片引擎
     */
    public static class ImageFileCompressEngine implements CompressFileEngine {

        @Override
        public void onStartCompress(Context context, ArrayList<Uri> source, OnKeyValueResultCallbackListener call) {
            Luban.with(context).load(source).ignoreBy(100).setRenameListener(new OnRenameListener() {
                @Override
                public String rename(String filePath) {
                    int indexOf = filePath.lastIndexOf(".");
                    String postfix = indexOf != -1 ? filePath.substring(indexOf) : ".jpg";
                    return DateUtils.getCreateFileName("CMP_") + postfix;
                }
            }).setCompressListener(new OnNewCompressListener() {
                @Override
                public void onStart() {

                }

                @Override
                public void onSuccess(String source, File compressFile) {
                    if (call != null) {
                        call.onCallback(source, compressFile.getAbsolutePath());
                    }
                }

                @Override
                public void onError(String source, Throwable e) {
                    if (call != null) {
                        call.onCallback(source, null);
                    }
                }
            }).launch();
        }
    }


    /**
     * 自定义沙盒文件处理
     */
    public static class MeSandboxFileEngine implements UriToFileTransformEngine {
        @Override
        public void onUriToFileAsyncTransform(Context context, String srcPath, String mineType, OnKeyValueResultCallbackListener call) {
            if (call != null) {
                call.onCallback(srcPath, SandboxTransformUtils.copyPathToSandbox(context, srcPath, mineType));
            }
        }
    }

    /**
     * 自定相机功能
     */
    private static class MeOnCameraInterceptListener implements OnCameraInterceptListener {

        @Override
        public void openCamera(Fragment fragment, int cameraMode, int requestCode) {
            SimpleCameraX camera = SimpleCameraX.of();
            camera.setCameraMode(cameraMode);
            //视频帧率，越高视频体积越大
            camera.setVideoFrameRate(25);
            //bit率， 越大视频体积越大
            camera.setVideoBitRate(3 * 1024 * 1024);
            //是否显示录制时间  -- 设置为true
            camera.isDisplayRecordChangeTime(true);
            //是否支持手指点击对焦  设置true
            camera.isManualFocusCameraPreview(true);
            //支持手指缩放相机   设置true
            camera.isZoomCameraPreview(true);
            //拍照自定义输出路径
            camera.setOutputPathDir(getSandboxPath());
            //权限说明  这里不处理 交给外层统一处理
            // camera.setPermissionDeniedListener(getSimpleXPermissionDeniedListener());
            //camera.setPermissionDescriptionListener(getSimpleXPermissionDescriptionListener());
            camera.setImageEngine((context, url, imageView) -> Glide.with(context).load(url).into(imageView));
            camera.start(fragment.getActivity(), fragment, requestCode);
        }
    }

    /**
     * 不支持类型提示
     */
    private static class MeOnSelectLimitTipsListener implements OnSelectLimitTipsListener {

        @Override
        public boolean onSelectLimitTips(Context context, @Nullable LocalMedia media, SelectorConfig config, int limitType) {
            if (limitType == SelectLimitType.SELECT_NOT_SUPPORT_SELECT_LIMIT) {
                ToastUtils.showToast(context, "暂不支持的选择类型");
                return true;
            }
            return false;
        }
    }

    /**
     * 设置裁剪引擎
     */
    private static class ImageFileCropEngine implements CropFileEngine {
        @Override
        public void onStartCrop(Fragment fragment, Uri srcUri, Uri destinationUri, ArrayList<String> dataSource, int requestCode) {
            UCrop.Options options = buildOptions();
            UCrop uCrop = UCrop.of(srcUri, destinationUri, dataSource);
            uCrop.withOptions(options);
            uCrop.setImageEngine(new UCropImageEngine() {
                @Override
                public void loadImage(Context context, String url, ImageView imageView) {
                    if (!ImageLoaderUtils.assertValidRequest(context)) {
                        return;
                    }
                    Glide.with(context).load(url).override(180, 180).into(imageView);
                }

                @Override
                public void loadImage(Context context, Uri url, int maxWidth, int maxHeight, OnCallbackListener<Bitmap> call) {
                    if (!ImageLoaderUtils.assertValidRequest(context)) {
                        return;
                    }
                    Glide.with(context).asBitmap().override(maxWidth, maxHeight).load(url).into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            if (call != null) {
                                call.onCall(resource);
                            }
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            if (call != null) {
                                call.onCall(null);
                            }
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                        }
                    });
                }
            });
            uCrop.start(fragment.getActivity(), fragment, requestCode);
        }
    }

    /**
     * 配制UCrop，可根据需求自我扩展
     *
     * @return
     */
    private static UCrop.Options buildOptions() {
        UCrop.Options options = new UCrop.Options();
        //是否显示裁剪菜单栏  ----  剪裁时  可以左右旋转，可以利用控制组件放大缩小图片
        options.setHideBottomControls(true);
        //是否显示裁剪框or图片拖动  设置false
        options.setFreeStyleCropEnabled(false);
        //是否显示裁剪边框  true
        options.setShowCropFrame(true);
        //是否显示裁剪框网格
        options.setShowCropGrid(true);
        //圆形头像剪裁模式  设置为false 一律传正方形，在展示的时候设置为圆形
        options.setCircleDimmedLayer(false);
        //剪裁比例  这里设置1:1
        options.withAspectRatio(1, 1);
        //创建自定义输出目录
        options.setCropOutputPathDir(getSandboxPath());
        //裁剪并自动拖动到中心
        options.isCropDragSmoothToCenter(false);
        //自定义Loader Bitmap
        options.isUseCustomLoaderBitmap(false);
        //是否不剪裁GIF 需要跳过
        // options.setSkipCropMimeType(getNotSupportCrop());
        //不剪裁GIF
        options.isForbidCropGifWebp(true);
        options.isForbidSkipMultipleCrop(false);
        //最大可剪裁数量
        options.setMaxScaleMultiplier(100);
        PictureSelectorStyle selectorStyle = new PictureSelectorStyle();
        if (selectorStyle != null && selectorStyle.getSelectMainStyle().getStatusBarColor() != 0) {
            SelectMainStyle mainStyle = selectorStyle.getSelectMainStyle();
            boolean isDarkStatusBarBlack = mainStyle.isDarkStatusBarBlack();
            int statusBarColor = mainStyle.getStatusBarColor();
            options.isDarkStatusBarBlack(isDarkStatusBarBlack);
            if (StyleUtils.checkStyleValidity(statusBarColor)) {
                options.setStatusBarColor(statusBarColor);
                options.setToolbarColor(statusBarColor);
            } else {
                options.setStatusBarColor(ContextCompat.getColor(Tools.getContext(), R.color.color_393A3E));
                options.setToolbarColor(ContextCompat.getColor(Tools.getContext(), R.color.color_393A3E));
            }
            TitleBarStyle titleBarStyle = selectorStyle.getTitleBarStyle();
            if (StyleUtils.checkStyleValidity(titleBarStyle.getTitleTextColor())) {
                options.setToolbarWidgetColor(titleBarStyle.getTitleTextColor());
            } else {
                options.setToolbarWidgetColor(ContextCompat.getColor(Tools.getContext(), R.color.color_FFFFFF));
            }
        } else {
            options.setStatusBarColor(ContextCompat.getColor(Tools.getContext(), R.color.color_393A3E));
            options.setToolbarColor(ContextCompat.getColor(Tools.getContext(), R.color.color_393A3E));
            options.setToolbarWidgetColor(ContextCompat.getColor(Tools.getContext(), R.color.color_FFFFFF));
        }
        return options;
    }

    /**
     * 获取目录
     */
    public static String getSandboxPath() {
        File externalFilesDir = Tools.getContext().getExternalFilesDir("");
        File customFile = new File(externalFilesDir.getAbsolutePath(), "Sandbox");
        if (!customFile.exists()) {
            customFile.mkdirs();
        }
        return customFile.getAbsolutePath() + File.separator;
    }
}
