package com.yzhg.toollibrary.location

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.lxj.xpopup.XPopup
import com.yzhg.toollibrary.Tools
import com.yzhg.toollibrary.common.ToastTools
import com.yzhg.toollibrary.content.TContent
import com.yzhg.toollibrary.listener.OpenMapListener


/*
 * 作者：yzhg
 * 时间：2021-12-11 09:15
 * 包名：com.yzhg.toollibrary
 * 描述：
 */
object KTools {
    /**
     * 打开导航软件
     */
    public fun openMapApp(context: Context, bdPoint: String,gdPoint: String, address: String, openMapListener: OpenMapListener) {
        val mapList: MutableList<String> = mutableListOf()
        //检测本地是否有高德和百度地图、腾讯地图
        if (Tools.appIsInstalled(context, TContent.GD_MAP_PACKAGER_NAME)) {
            //安装了高德地图
            mapList.add("高德地图")
        }

        if (Tools.appIsInstalled(context, TContent.BD_MAP_PACKAGER_NAME)) {
            //安装了百度地图
            mapList.add("百度地图")
        }

        if (Tools.appIsInstalled(context, TContent.TENCENT_MAP_PACKAGER_NAME)) {
            //安装了腾讯地图
            mapList.add("腾讯地图")
        }

        if (mapList.isNullOrEmpty()) {
            /*AlertCommonDialog.newInstance("您还未地图类APP，请安装高德地图、百度地图、腾讯地图任意一个后，重新导航", "", "确定", true)
                .setOnDialogRightClickListener { }
                .showDialog(supportFragmentManager)*/
            openMapListener.noInstallAppListener()
            return
        }
        XPopup.Builder(context)
            .isDarkTheme(false)
            .hasShadowBg(true)
            .asBottomList(
                "选择地图", mapList.toTypedArray()
            ) { _, text ->
                when (text) {
                    "高德地图" -> {
                        openGaoDeMap(context, gdPoint, address, openMapListener)
                    }
                    "百度地图" -> {
                        openBaiduMap(context, bdPoint, address, openMapListener)
                    }
                    "腾讯地图" -> {
                        openTencentMap(context, gdPoint, address, openMapListener)
                    }
                }
            }.show();
    }



    /**
     * 打开高德地图（公交出行，起点位置使用地图当前位置）
     *
     * t = 0（驾车）= 1（公交）= 2（步行）= 3（骑行）= 4（火车）= 5（长途客车）
     *
     * @param dlat  终点纬度
     * @param dlon  终点经度
     * @param dname 终点名称
     */
    private fun openGaoDeMap(context: Context, point: String, address: String, openMapListener: OpenMapListener) {
        val xyz = getPointXYZ(point)
        if (xyz == null) {
            openMapListener.noAddressListener()
            ToastTools.showToast("未获取到商家位置")
            return
        }
        //获取商家位置
        val mUri = Uri.parse("amapuri://route/plan/?dlat=" + xyz[1] + "&dlon=" + xyz[0] + "&dname=" + address + "&dev=0&t=0")
        val intent = Intent("android.intent.action.VIEW", mUri)
        context.startActivity(intent)
    }

    /**
     * 打开百度地图（公交出行，起点位置使用地图当前位置）
     *
     * mode = transit（公交）、driving（驾车）、walking（步行）和riding（骑行）. 默认:driving
     * 当 mode=transit 时 ： sy = 0：推荐路线 、 2：少换乘 、 3：少步行 、 4：不坐地铁 、 5：时间短 、 6：地铁优先
     *
     * @param dlat  终点纬度
     * @param dlon  终点经度
     * @param dname 终点名称
     */
    private fun openBaiduMap(context: Context, point: String, address: String, openMapListener: OpenMapListener) {
        val xyz = getPointXYZ(point)
        if (xyz == null) {
            openMapListener.noAddressListener()
            ToastTools.showToast("未获取到商家位置")
            return
        }
        //获取商家位置
        val mUri = Uri.parse("baidumap://map/direction?destination=latlng:" + xyz[1] + "," + xyz[0] + "|name:" + address + "&mode=driving")
        val intent = Intent("android.intent.action.VIEW", mUri)
        context.startActivity(intent)
    }

    /**
     * 打开腾讯地图（公交出行，起点位置使用地图当前位置）
     *
     * 公交：type=bus，policy有以下取值
     * 0：较快捷 、 1：少换乘 、 2：少步行 、 3：不坐地铁
     * 驾车：type=drive，policy有以下取值
     * 0：较快捷 、 1：无高速 、 2：距离短
     * policy的取值缺省为0
     *
     * @param dlat  终点纬度
     * @param dlon  终点经度
     * @param dname 终点名称
     */

    private fun openTencentMap(context: Context, point: String, address: String, openMapListener: OpenMapListener) {
        val xyz = getPointXYZ(point)
        if (xyz == null) {
            openMapListener.noAddressListener()
            ToastTools.showToast("未获取到商家位置")
            return
        }
        //获取商家位置
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data =
            Uri.parse("qqmap://map/routeplan?type=drive&from=我的位置&fromcoord=0,0&to=" + address + "&tocoord=" + xyz[1] + "," + xyz[0] + "&policy=1&referer=myapp")
        context.startActivity(intent)
    }

    /**
     * 解析经纬度
     */
    private fun getPointXYZ(point: String): List<String>? {
        if (point.contains(",")) {
            val split = point.split(",")
            return if (split.size == 2) {
                split
            } else {
                null
            }
        }
        return null
    }


}